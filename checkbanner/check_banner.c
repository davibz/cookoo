#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stddef.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>




//#include "lib.h"



int spai_find_cookie_consensus_from_file(char* html_file, char* agreeString);
int main();

void usage(char prog[]) {
    fprintf(stderr, "Usage: %s HTMLFILENAME\n", prog);
}

int main(int argc, char** argv)
{
    if(argc != 2) {
        usage(argv[0]);
        return 2;
    }
    if(access(argv[1], F_OK) == -1) {
        fprintf(stderr, "Invalid filename: %s\n", argv[1]);
        usage(argv[0]);
        return 2;
    }

    int stringSize;
    char agreeString[1024];
    stringSize = spai_find_cookie_consensus_from_file(argv[1], agreeString);
    if(stringSize == 0) {
        fprintf(stderr, "No consensus request found\n");
        return 1;
    }
    printf("%s\n", agreeString);
    return 0;
}

