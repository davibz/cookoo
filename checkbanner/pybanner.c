#include <stdio.h>
#include <Python.h>

#include "agree.c"

int spai_find_cookie_consensus_from_file(char* html_file, char* agreeString);

static PyObject *check_banner(PyObject* self, PyObject *args)
{
    char* fname;
    if(!PyArg_ParseTuple(args, "s", &fname))
        return NULL;
    char agreeString[1024];
    int agreeLen;
    agreeLen = spai_find_cookie_consensus_from_file(fname, agreeString);
    if(agreeLen == 0) {
        return Py_False;
    }
    return Py_BuildValue("s", agreeString);
}

static PyMethodDef BannerMethods[] = {
    {"check",  check_banner, METH_VARARGS,
        "Check the presence of a banner inside an HTML file"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef bannermodule = {
    PyModuleDef_HEAD_INIT,
    "banner",
    NULL,
    -1,
    BannerMethods
};

PyMODINIT_FUNC PyInit_banner(void) {
    return PyModule_Create(&bannermodule);
}
