#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stddef.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>


#define C1 2147483647
#define C2 268435456
#define C3 9223372036854775807
#define C4 4611686018427387904
#define C5 262144
#define C6 32
#define C7 1048576
#define C8 2097152
#define C9 262144
#define C10 -2
#define C11 -1
#define C12 65536
#define C13 4194304
#define C14 65536
#define C15 65536
#define C16 1024
#define C17 16777216
#define C18 8388608
#define C19 8388608
#define C20 65536
#define C21 131072
#define C22 524288
#define nil 16777214
#define C24 66407
#define C25 32
#define C26 23
#define C27 8
#define C28 3758096384
#define C29 1024
#define C30 64
#define C31 1024
#define C32 4096


typedef unsigned char  tcpBuf[1024];


int V1[C5];
int V2;
int V3 = 1;
int V4 = 0;
int V5[C12];
int V6[C12];
int V7[C12];
int V8[C12];
int V9[C12];
int V10[C12];
int V11[C12];
int V12[C12];
int V13[C12];
int V14[C12];
int64_t V15[C12];
int64_t V16[C12];
int64_t V17[C12];
int64_t V18[C12];
int64_t V19[C12];
int V20[C12];
int V21[C12];
int V22[C12];
int V23[C12];
int V24[C12];
int V25[C12];
int V26[C12];
int V27[C12];
int V28[C12];
int V29[C12];
int V30[C12];
int V31[C12];
int V32[C12];
int V33[C13];
int V34 = 0;
int64_t V35[C16];
int V36 = 0;
int64_t V37;
int V38[262144];
int V39;
int _SH[C17];
int V41;
int V42;
int V43;
int V44;
int V45;
int V46;
int V47;
int V48;
int V49;
int V50;
int V51;
char V52[4096];
char V53[4096];
char V54[4096];
char V55[4096];
FILE* V56[C29];
int V57[C30];
tcpBuf V58[C30];
int V59[C30];
int V60[C30];
tcpBuf V61[C30];
int V62[C30];


extern int F1(int object, int stream, int delta, int levels, int len1);
extern int F2(int vect, int index);
extern int F3(int str, int stream, int len1);
extern int F4(int csym);
extern int F5(int F443);
extern int F6(int iter_spec);
extern int F7(int list);
extern int F8(int str);
extern int F9(int rec_name, int rec, int field_name, int value);
extern int F10(int rec_name);
extern int F11(int stream, int n, int inTOout_fn);
extern int SHARKexit(int message);
extern int F13(int str);
extern int F14();
extern int F15(int pattern, int aux_var);
extern int F16(int object_index);
extern int F17(int stream, int unmatched_tags_as_singleP, int print_errorsP);
extern double F18(double x);
extern int F19(int num);
extern int F20(int aux_stream);
extern int F21(char* html_file);
extern int F22(int stream_index);
extern int F23(int stream, int condition);
extern int F24(int iter_spec);
extern int F25(int spec_value, int rec_value, int test_fn);
extern int F26();
extern int F27(int lines);
extern int F28(int stack);
extern int F29(int x);
extern int F30(int str);
extern int F31(int stream);
extern int F32(int clauses);
extern int F33(int stream);
extern int F34(int counter, int it_counter, int en_counter, int string);
extern int F35(int tag_name);
extern int F36(int sock_index);
extern int F37(int num, int base, int add_signP);
extern int F38(int lp_file);
extern int F39(int size);
extern int F40(int lp, int os);
extern int64_t F41(int buf_size, int buf_index);
extern int F42(int list, int fn, int other_args);
extern int SHARKread(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F44(int x);
extern int F45(int arc, int node);
extern void F46(int* flag_vect, int index);
extern int F47(int csym);
extern int F48(int str);
extern int F49();
extern int F50(int a1, int a2, int a3, int a4);
extern int F51(int stream, int skip_attlistP);
extern int F52(int stream, int separator, int read_conversion_fn);
extern int F53(int list, int test_fn);
extern int F54(int stream);
extern int F55(int nums);
extern int F56(int str);
extern int F57(int in_stream, int condition, int condition_type, int inTOout_fn);
extern int F58();
extern int F59(int term, int lang);
extern int F60(int pred, int list, int args);
extern int F61(int sock_index);
extern int F62(int field_specs);
extern int F63(int str1, int str2);
extern int F64(int act_spec);
extern int F65(int csym, int base);
extern int F66(int pred, int list, int args);
extern int F67(int str);
extern int F68(int pred, int list, int args);
extern int F69(int list, int last_cons);
extern int F70(int iter_spec);
extern int F71(int row, int stream, int separator, int terminator);
extern int F72(int stream, int F757, int last_pos, int initial_pos);
extern int F73(int str, int stream);
extern int F74(int element);
extern int F75();
extern int F76(int fn);
extern int F77(int exprs);
extern int F78(int plist, int field);
extern int F79(int stream);
extern int F80(int var_name);
extern int F81(int tag_case);
extern int F82(int lp, int full_nameP);
extern int F83(int csym, int stream);
extern int F84(int a1, int a2, int a3);
extern int F85(int F339);
extern int _c(int object1, int object2);
extern int F87(int str);
extern int F88(int object, int check_allP);
extern int F89(int lists, int delimiter, int grammar, int include_empty_itemsP);
extern int F90(int object, int pred, int list, int args);
extern int F91(int vect, int index, int value_vset);
extern int F92(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26);
extern int F93(int var, int exp_by, int exp_while);
extern int F94(int regex, int start_node, int end_node);
extern int F95(int num);
extern int F96(int subdirs);
extern int F97(int stream, int buf_start, int nbytes);
extern int F98(int str);
extern int F99(int label, int start_node, int end_node);
extern int F100(int node);
extern int F101(int list, int sl);
extern int F102(int stream, int64_t F60);
extern int F103(int stack);
extern int F104(int x, int y);
extern int F105(int str);
extern int F106(int stream, int terminator);
extern int F107(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24);
extern int F108(int str1, int str2);
extern int F109(int str);
extern int F110(int object, int stream, int delta, int levels, int F510);
extern int F111(int str);
extern int F112(int norm_regex);
extern int F113(int stream, int trim_spaceP);
extern int F114(int sock_index);
extern int F115(int stream);
extern int F116(int expr);
extern int F117(int iter_spec);
extern int F118(int lp);
extern int F119(int pattern);
extern int F120();
extern int F121(int lp);
extern int F122(int stream, int F443, int row_n);
extern int F123(int object_index);
extern int F124(int list_len_var, int index_vars);
extern int F125(int list, int F60);
extern int F126(int iter_spec);
extern int F127(int csym, int stream_index);
extern int F128(int csym, int stream);
extern int F129(int stream, int condition, int include_empty_itemsP);
extern int F130(int stream, int sc_comment_flag, int hm_comment_flag);
extern int spai_find_cookie_consensus_from_file(char* html_file, char* agreeString);
extern int F132(int csym);
extern void sharkInitSyms();
extern int F134(int stream, int n);
extern int F135(int str);
extern int F136(int fields, int aux_rec);
extern int F137(int list, int i);
extern int F138(int old_lp, int new_lp);
extern int F139(int stream_index);
extern int F140(int node, int previous_pnode);
extern int F141(int mode);
extern int F142(int sym, int stream);
extern int F143(int stream);
extern int F144(int el, int delimiter, int stream, int conversion_fn);
extern int F145(int stream, int buf_start, int nbytes);
extern int F146(int list, int base);
extern int F147(int a1, int a2, int a3, int a4, int a5, int a6);
extern int F148(int stream, int condition);
extern int F149(int x);
extern int F150(int in_stream, int out_stream);
extern int F151(int stream, int not_condition);
extern void F152();
extern int F153(int stream);
extern int F154();
extern int F155(int stream, int F60);
extern int F156(int rec_type_name, int size, int field_specs);
extern int F157(int stack);
extern int F158(int rec, int field, int value);
extern int F159(int condition);
extern void F160();
extern int F161(int list);
extern int F162(int list, int fn);
extern int F163(int condition_name);
extern int F164();
extern int F165(int str);
extern int F166(int csym);
extern int F167(int object, int list);
extern int F168(int stream, int act_spec);
extern int F169(int stream, int str, int test1_fn);
extern int F170(int nums);
extern int F171(int stream);
extern int F172(int hashtable, int str);
extern int F173(int object, int list, int size, int mode);
extern int F174(int stream);
extern int F175(int stream, int read_attlistP);
extern int F176(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10);
extern int F177(int strs, int stream);
extern int stdoutTyo(int csym, int stream);
extern int F179(int num_str, int base);
extern int F180(int field_type_name);
extern int F181(int stream, int str_size, int64_t delta);
extern int F182(int rec_name);
extern int F183();
extern int F184(int html_file);
extern int F185(int stream);
extern int F186(int list);
extern int F187(int els);
extern int F188(int args);
extern int F189(double num, int decimals, int expP);
extern int F190(int lp);
extern int F191(int lines);
extern int F192(int exa_num_sym);
extern int F193(int str);
extern int F194(int value, int stream);
extern int F195(int stream, int delta_x, int fill_str);
extern int F196(int nums, int exp1_fn);
extern int F197(int stream_index, int64_t position);
extern int F198(int stream);
extern int F199(int object, int index);
extern int F200(int stream, int condition);
extern int F201(int stream, int condition, int out_stream, int inTOout_fn);
extern void F202();
extern int F203(int regex, int start_node, int end_node);
extern int F204(char* cstr);
extern int F205(int fn, int args);
extern int F206(int table, int rtype);
extern int F207(int csym, int stream);
extern int F208(int csym);
extern char* F209(int string);
extern void F210();
extern int F211(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8);
extern int F212(int stream);
extern int F213(int iter_specs);
extern int F214(int* flag_vect, int vect_size);
extern int F215(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30, int a31, int a32);
extern int F216(int regex_node, int stream);
extern int F217(int stream);
extern int F218(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29);
extern int F219(int atom_name);
extern int F220(int dir1, int dest_dir, int traceP);
extern int F221(int csym);
extern int F222(int lp);
extern int F223(int str);
extern int F224(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25);
extern int F225(int gmtP);
extern int F226(int index);
extern int F227(int iter_spec);
extern int F228(int stream, int separator);
extern int F229(int sym_index);
extern int F230(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F231(int specs, int body);
extern int F232(int str);
extern int F233(int vect, int index);
extern int F234(int sock_index);
extern int F235(int stream, int str);
extern int F236(int str);
extern int64_t F237(int stream);
extern int F238(int stream);
extern int F239(int sym_index, int value_index);
extern int F240(int stream, int buf_start, int nbytes);
extern int F241(int lp, int make_missing_dirsP);
extern int F242(int object1, int object2);
extern int F243(int stream, int64_t delta);
extern int F244(int value, int stack);
extern int F245(int x);
extern int F246(int stream, int condition);
extern int F247(int stream, int split_condition, int transf_fn, int prune_fnP);
extern int F248(int rec, int field, int allP);
extern int F249(int stream, int split_condition, int transf_fn, int prune_fnP);
extern int F250(int f_38vs, int field_specs);
extern int F251(int in_stream, int out_stream);
extern int F252(int attlist);
extern int F253(int sock_index);
extern int F254(int csym);
extern int F255(int stream);
extern int F256(int x, int y);
extern int F257(int list, int object);
extern void F258(int n);
extern int F259(int csym);
extern int F260(int f_38v_list, int field_specs);
extern int F261(int stream);
extern int F262(int a1);
extern int F263(int stream_index);
extern int F264();
extern int F265(int hashtable, int str, int value);
extern int F266(int iter_spec);
extern int F267(int stream);
extern int F268(int stream, int separator, int terminator, int F443);
extern int F269(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F270(int object, int stream);
extern int F271(int tag);
extern int F272(int pnode);
extern int F273(int rec_name, int size);
extern int F274(int regex, int start_node, int end_node);
extern int F275(int object, int list, int size, int mode);
extern int F276(int object, int stream);
extern int F277(int stream, int str);
extern int F278(int sock_index);
extern int F279(int test_fn, int key, int clauses);
extern int F280(int trie, int str);
extern int F281(int str);
extern int F282(int stream, int act_spec);
extern int F283(int list);
extern int F284(int str);
extern int F285();
extern int F286(int list, int base);
extern int F287(int align);
extern int F288(int recs);
extern int F289(int list);
extern int F290(int stream, int condition);
extern int F291(int object, int stream, int delta, int levels, int len1);
extern int F292(int sym_index);
extern int F293(int list, int object);
extern int F294(int lp);
extern int F295(int csym1, int csym2);
extern int F296(int host);
extern int F297(int class_value);
extern int F298(int new, int old, int list);
extern int F299(int stream);
extern int F300(int str1, int str2);
extern int F301(int lp, int type);
extern int F302(int command, int read_answerP);
extern int F303(int fn, int a1, int a2, int a3, int a4);
extern int F304(int stream);
extern int F305(int field_specs);
extern int F306(int line);
extern int F307(int str1, int str2);
extern int F308(int stream, int act_spec);
extern int F309(int lp_rec, int lp_name);
extern int F310(int it_counter, int en_counter);
extern int F311(int list, int object);
extern int F312();
extern int F313(int hashtable);
extern int F314(int stream);
extern int F315(int table, int file, int F443, int row_n);
extern int F316(int table, int stream, int F443, int row_n);
extern int F317(int stream);
extern int F318(int stream, int F443);
extern int F319(int rec, int F443);
extern int F320(int csym);
extern int F321(int expr);
extern int F322(int csym1, int csym2);
extern int F323(int stream, int buf_index);
extern int F324(int exp_instr);
extern int F325(int fn, int a1, int a2);
extern int F326(int trie, int str);
extern int F327(int rec_type_name);
extern int F328(int str);
extern int F329(int stream, int condition, int max_delta);
extern int F330(int lp_name, int path);
extern void F331();
extern int F332(int stream);
extern int F333(int stream_index);
extern int F334(int delimiter, int stream);
extern int F335(int num);
extern int F336(int str);
extern int F337(int lp_file);
extern int F338(int list, int fn, int include_empty_itemsP);
extern int F339(int lp);
extern int F340(int aux_stream, int64_t F237);
extern int F341(int par_name);
extern int F342(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27);
extern int F343(int list1, int list2, int pred1);
extern int F344(int stream_index);
extern int F345(int recs, int out_file, int F443, int row_n);
extern int F346(int stream);
extern int F347(int stream, int buf_index, int buf_start, int nbytes);
extern int F348(int list, int delimiter, int pred1, int include_empty_itemsP);
extern int F349(int stream, int buf_start, int nbytes);
extern int F350(int F339);
extern int F351(int F339);
extern int F352(int lp, int new_lp, int traceP);
extern int F353(int stream, int condition, int delta);
extern int F354(int unmatched_tags_as_singleP, int print_errorsP);
extern int F355(int stream);
extern int F356(int object);
extern int F357(int rec, int rec_name, int fields, int value);
extern int F358(int stream1, int buf_start, int nbytes);
extern int F359(int file, int F443, int row_n);
extern int F360(int csym, int stream);
extern int F361(int stream, int condition);
extern int F362(int stream, int or_condition);
extern int F363(int list, int n);
extern int F364(int object);
extern int F365(int stream);
extern int F366(int stream, int condition);
extern int F367(int str);
extern int64_t F368(int stream, int condition);
extern int F369(int iter_spec);
extern int F370(int lp_name);
extern int F371(int trie, int str);
extern int F372(int pnode, int var_name);
extern int F373(int stream);
extern int F374(int rec_name, int size, int field_specs);
extern int F375(int stream);
extern int F376(int lp);
extern int F377(int args, int result_var);
extern int F378(int regex);
extern int F379(int regex, int start_node, int end_node);
extern int F380();
extern int F381(int str);
extern int F382(int node);
extern int F383(int patterns, int expr);
extern int F384(int rec_name, int rec, int field_38values);
extern int F385(int rec_name);
extern int F386(int row, int stream, int separator, int terminator, int F443);
extern int F387(int list, int fn, int other_args);
extern int F388(int str);
extern int F389(int stream);
extern int F390(int iter_spec);
extern int F391(int csym);
extern int F392(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9);
extern int F393(int var_name);
extern int F394(int str, int makeP);
extern int F395(int n, int unmatched_tags_as_singleP, int print_errorsP);
extern int F396(int stream, int64_t delta);
extern int F397(int size);
extern int F398(int in_stream, int out_stream, int64_t start, int64_t end);
extern int F399(int stream);
extern int F400(int object, int list, int size);
extern int F401();
extern int F402(int var);
extern int F403(int stream_index);
extern int F404(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F405(int stream);
extern int F406(int rec_name);
extern int F407(int tag_name);
extern int F408(int str);
extern int F409(int mode);
extern int F410(int value, int stream);
extern int F411(int csym, int class_spec);
extern int F412(int iter_spec);
extern int F413(int list, int n);
extern int F414(int stream, int split_condition, int transf_fn, int prune_fnP);
extern int F415(int iter_spec);
extern int F416(int stream);
extern int F417(int cons1);
extern int64_t F418(int stream);
extern int F419(int lp);
extern int F420(int list, int sl);
extern int F421(int element);
extern int F422(int object, int list, int F60);
extern int F423(int str);
extern int F424(int object, int list);
extern int F425(int stream);
extern int F426(int tag, int stream);
extern int F427(int csym, int stream);
extern int F428(int lp);
extern int F429(int args, int result_var);
extern int F430(int stack);
extern int F431(int csym);
extern int F432(int rec_name);
extern int F433(int stream, int condition, int inTOout_fn);
extern int F434(int str);
extern int F435(int iter_spec);
extern int F436(int stream);
extern int F437(int trie, int str, int value);
extern int F438(int rec_type_name, int field_name);
extern int F439(int expr);
extern int F440(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F441(int object, int index);
extern int F442(int sub, int tnode);
extern int F443(int name);
extern int F444(int str);
extern int F445(int tag_name);
extern int F446(int stream, int buf_start, int nbytes);
extern int F447(int sock_index);
extern int F448(int stream);
extern int F449(int list);
extern int F450(char* cstr);
extern int F451(int stream);
extern int F452(int object, int list);
extern void F453(double num, int expP, int size);
extern int F454(int stream);
extern int F455(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12);
extern int F456(int str, int size);
extern int F457(int list, int F60, int object);
extern int F458(int var_38value_list);
extern int F459(int stream, int condition);
extern int F460(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20);
extern int F461(int csym);
extern int F462(int fn, int a1, int a2, int a3);
extern int F463(int object, int index);
extern int F464(struct tm tm);
extern int F465(int F339, int full_pathP);
extern int F466(int stream);
extern int F467(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28);
extern int F468(int act_spec);
extern int F469(int file, int F443, int row_n);
extern int F470(int expr, int stream, int delta, int levels, int F510);
extern int stdoutPs1(char* str);
extern int F472(int condition);
extern int F473(int args);
extern int F474(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7);
extern int F475(int list, int spec);
extern int F476(int stream, int act_spec);
extern int F477(int F339);
extern int F478(int stream);
extern int F479(int object, int pred, int list, int args);
extern int F480(int stream, int condition, int inTOout_fn);
extern int F481(int stream);
extern int F482(int var_name);
extern int F483(int fn, int a1);
extern int F484(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18);
extern int F485(int recs, int stream, int F443, int row_n);
extern int F486(int lists);
extern int F487();
extern int F488(int guard, int size, int test_fn);
extern int F489(int stream, int buf_start);
extern int F490(int iter_spec);
extern int F491(int in_stream, int out_stream);
extern int F492(int a1, int a2, int a3, int a4, int a5);
extern int F493(int str);
extern int F494(int exprs);
extern int F495(int str);
extern int F496(int list, int F60, int sl);
extern int F497(int stream, int object);
extern int F498(int list, int sl);
extern int F499(int aux_stream, int64_t F237);
extern int64_t F500(int stream);
extern int F501(int stream, int condition, int delta, int plusP);
extern int64_t F502(int stream);
extern int F503(int list);
extern int F504(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13);
extern int F505(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16);
extern int F506();
extern int F507(int start_node, int end_node);
extern double F508(int num_str);
extern int F509(int stream, int condition, int max_delta);
extern int F510(int list);
extern int F511(int lp_file);
extern int F512(int str);
extern int F513(int stream, int seq_condition);
extern int F514(int size, int object);
extern int F515(int new, int old, int list);
extern int F516(int stream, int end_csym);
extern int F517(int lp_file);
extern int F518(int stream);
extern int F519(int object, int list);
extern int F520(int stream);
extern int F521(int stream, int strs, int test1_fn);
extern int F522();
extern int F523(int str);
extern int F524(int stream, int condition);
extern double F525();
extern int F526(int el, int list);
extern int F527(int prefix, int print_env);
extern int F528(int str);
extern int F529(int type);
extern int F530(int old_lp, int new_lp);
extern int F531(int norm_regex);
extern int F532(int list, int F424, int pos2);
extern int F533(int list, int str, int pred1, int include_empty_itemsP);
extern int F534(int list, int csym, int pred1, int include_empty_itemsP);
extern int F535(int iter_spec);
extern int F536(int expr, int stream, int delta, int levels, int F510);
extern int F537(int stream);
extern int F538(int stream);
extern int F539(int condition_type);
extern int F540(int cur_tag_singleP);
extern int F541();
extern int F542(int lists);
extern int F543(int rec_name);
extern int F544(int stream, int condition);
extern int F545(int a1, int a2, int a3, int a4, int a5, int a6, int a7);
extern int F546(int stream);
extern int F547(int stream);
extern int F548(int stream, int condition, int condition_type);
extern int F549(int csym);
extern int F550(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10);
extern int F551(int x);
extern int F552(int vect);
extern int F553(int stream, int condition, int out_stream, int inTOout_fn);
extern int F554(int stream, int n);
extern char* F555(int lp, int buf2P);
extern int F556(int object_index);
extern int F557(int str, int datum);
extern int F558(int stream, int F443, int row_n);
extern char* F559(int string);
extern int F560(int pred, int list, int mode, int args);
extern int F561(int act_spec);
extern int F562(int fn, int a1, int a2, int a3, int a4, int a5, int a6);
extern int F563(int fn, int argn);
extern int F564(int str, int size, int pad_csym, int align);
extern int F565(int sym_index);
extern int F566(int stream);
extern int F567(int64_t F60, int buf_size);
extern int F568(int stream);
extern int F569(int str);
extern int F570(int opr, int exprs);
extern int F571(int regex);
extern int F572(int lp_file);
extern int F573(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30);
extern int F574(int list);
extern int F575(int str);
extern int F576(int rec_name);
extern int F577();
extern int F578(int pred, int list, int args);
extern FILE* F579(int stream_index);
extern int F580(int stack);
extern int F581(int stream, int condition, int inTOout_fn);
extern int F582(int lp_name);
extern int F583(int str);
extern int64_t F584(int lp_file);
extern int F585(int rec, int rec_name, int fields);
extern int F586(int csym);
extern int F587(int stream);
extern int F588(int pred, int list, int args);
extern int F589(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15);
extern int F590(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23);
extern int F591(int stream, int act_spec);
extern int F592(int a1, int a2);
extern int F593(int rec_name, int field_var_specs, int rec, int body);
extern int F594(int field, int value, int rec_list, int test_fn);
extern int F595(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17);
extern int F596(int lp, int remove_subdirsP);
extern int F597(int get_form, int opr, int par);
extern int F598(const char* cstr);
extern int F599(int row, int F443);
extern int F600(int rec_name, int field_name);
extern int F601(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21);
extern int F602(int stream, int condition, int condition_type, int moveP, int max_delta);
extern int F603(int type_name);
extern int F604(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30, int a31);
extern int F605(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19);
extern int F606(int iter_spec);
extern int F607(int list, int test_fn, int include_empty_itemsP);
extern int F608(int stream);
extern int F609(int elements);
extern int F610(int stream, int message);
extern int F611(int stream, int condition, int out_stream, int inTOout_fn);
extern int F612(int list);
extern int F613(int str, int expFn);
extern int F614(int stream, int objects);
extern int F615(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11);
extern int F616(int tag, int stream);
extern int64_t F617(int lp_file);
extern int F618(int csym);
extern int F619(int lp);
extern int F620(int sock_index);
extern int F621(int csym);
extern void F622();
extern int F623(int64_t F60, int buf_size, int buf_index, int buf_start);
extern int F624(int stream, int condition, int inTOout_fn);
extern int F625(int stream, int buf_index);
extern int F626(int spec, int spec_rec);
extern int F627();
extern int F628(int stream, int instr);
extern int F629();
extern int F630(int csym, int tnode);
extern int F631(int stream, int condition);
extern int F632(int tnode);
extern int F633(int regex, int start_node, int end_node);
extern int F634(int stream, int condition, int out_stream, int inTOout_fn);
extern int F635(int stream, int unmatched_tags_as_singleP, int tag_case, int print_errorsP, int trim_spaceP, int read_attlistP, int stream_skip_commentP);
extern int F636(int iter_spec);
extern int F637(int F339, int full_pathP, int levels);
extern int F638(int lp);
extern int F639(int stream, int re_condition);
extern int F640(int stream, int condition);
extern int F641(int rec, int stream, int F443);
extern int F642(int stream);
extern int F643(int lines, int lang);
extern int F644(int pnode, int var_name);
extern int F645(int size);
extern int F646(int linked_node, int node);
extern int F647(int lines);
extern int64_t F648(int stream);
extern int F649(int iter_spec);
extern int F650(int fields, int aux_rec);
extern int F651(int F339, int full_pathP);
extern int F652(int csym);
extern int F653(int csym);
extern int F654(int pred, int list, int args);
extern int F655(int rec_type);
extern int F656(int stream, int buf_index);
extern int F657(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14);
extern int F658(int csym);
extern int F659(int csym, int stream);
extern int F660(int stream);
extern int F661(int stream);
extern int F662(int os);
extern int F663(int stream, int64_t delta);
extern int F664(int pred, int list, int args);
extern int F665(int list, int prefix, int pred1);
extern int F666(int stream, int act_spec);
extern int F667(int stream);
extern int F668(int lp);
extern int F669(int stream, int condition);
extern int F670(int stream, int separator, int terminator, int F443);
extern int F671(int term_recs, int traceP, int string);
extern int F672(int csym);
extern int F673(int value, int stream);
extern int F674(int stream, int delta);
extern int F675(int list, int n);
extern int F676(int list);
extern int F677(int F443);
extern int F678(int x, int y);
extern int F679(int str);
extern int F680(int in_stream, int out_stream, int condition, int condition_type, int inTOout_fn);
extern int F681(int str);
extern int F682(int x);
extern int F683(int expr, int stream, int delta, int levels, int F510);
extern int F684(int stream);
extern int F685(int stream, int buf_start, int nbytes);
extern int F686(int stream);
extern int F687(int stream_index);
extern int F688(int spec_name);
extern int F689(int expr, int stream, int delta, int levels, int F510);
extern int F690(int stream, int condition, int delta);
extern int F691(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22);
extern int F692(int var, int value);
extern int F693(int stream);
extern int F694(int list, int n);
extern int F695(int lp_old, int lp_new);
extern int F696(int stream);
extern int F697(int int1);
extern int64_t F698(int stream_index);
extern int F699(int iter_spec);
extern int F700(int rec_name);
extern int F701(int csym);
extern int F702(int host, int port);
extern int F703(int filename, int open_flag);
extern int F704();
extern int F705(int stream);
extern int F706(int F339, int levels, int full_pathP);
extern int F707(int stream);
extern int F708(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9);
extern int F709(int lp);
extern int F710(int rec_type_name, int field_name);
extern char* F711(int string, int temp_buf2);
extern int F712(int command);
extern int F713(int stream, int condition);
extern int F714(int number, int min_size, int pad_csym, int align, int base);
extern int F715(int term_list);
extern int F716(int stream, int buf_start, int nbytes);
extern int F717(int fn, int args);
extern int F718(int list, int sublist, int pred1);
extern int F719(int str);
extern int F720(int els);
extern int F721(int pred, int list, int mode, int args);
extern int F722(int lp_parts);
extern int F723(int buf_size);
extern int F724(int str1, int str2);
extern int F725(int strs);
extern int F726(int fn, int a1, int a2, int a3, int a4, int a5);
extern int F727(int list, int fn);
extern int F728(int object, int list, int F60);
extern int F729(int var_38value_list);
extern int F730(int iter_spec);
extern int F731(int rec_name, int size, int f_38vs);
extern int F732(int iter_spec);
extern int F733(int stream, int act_specs);
extern int F734();
extern int F735(int lists);
extern int F736(int mode);
extern int F737(int csym);
extern int F738(int stream, int condition);
extern int F739(int stream, int buf_index, int nbytes);
extern int F740(int int1);
extern int F741(int object);
extern int F742(int str);
extern int F743(int stream);
extern int F744(int lists, int delimiter, int grammar, int include_empty_itemsP);
extern int F745(int stream);
extern int F746(int stream);
extern int F747(int pred, int list, int args);
extern int F748(int rec_type_name);
extern int F749(int object);
extern int F750(int rec, int field);
extern int F751(int rec_name, int rec, int field_name);
extern int F752(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8);
extern int F753(int instrs);
extern int F754(int str1, int str2);
extern int F755(int lists);
extern int F756(int stream, int sc_comment_flag, int hm_comment_flag);
extern int F757(int list, int suffix, int pred1);
extern int F758(int par_name, int par_value);
extern int F759(int list, int spec);
extern int F760(int stream);
extern int F761(int act_spec);
extern int F762(int csym);
extern int F763(int list1, int list2);
extern int F764(int stream, int condition);
extern int F765(int stream, int n, int out_stream, int inTOout_fn);
extern int F766();
extern int F767(int object);
extern int F768(int str1, int str2);
extern int F769(int lp);
extern int F770(int in_stream, int out_stream);
extern int F771(int buf_index);
extern int F772(int stream, int condition_type);
extern int F773(int csym_index);
extern int F774(int object, int type);
extern int F775(int stream, int out_stream, int terminator);
extern int F776(int csym, int sock_index);
extern int F777(int fields);
extern int F778(int stream);
extern int F779(int list, int F60);
extern int F780(int var_38value_list);


int F1(int object, int stream, int delta, int levels, int len1)
{{int rv_aux = nil; {int temp_str = nil; int max_pos = nil; int last_pos = -1; if (delta != nil)
{max_pos= (_SH[66602] - delta - -2);}
else {max_pos= _SH[66602];}
 {int withG_value = nil; int MULTprint_prettyPMULTCOPY = _SH[66596]; _SH[66596]= nil;
 {int temp_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{temp_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= 66612;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = 66583; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(temp_str != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= temp_str;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 temp_stream= STREAM__BUF_935;
}
}
}

 unwind_value= temp_stream;
}

 if (temp_stream == -1)
;
else {{ {int64_t start = 1; int64_t end = max_pos; if (start > end)
{stream_rv= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65632];
 stream_rv= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[temp_stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[temp_stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[temp_stream])
{start= V15[temp_stream];}
 V15[temp_stream]= start;
 F102(temp_stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[temp_stream])
{end= V16[temp_stream];}
 V16[temp_stream]= end;
}
}
 { { F291(object, temp_stream, delta, levels, len1);
 last_pos= (int)F237(temp_stream);
 if (last_pos < max_pos)
{stream_slice_rv= F398(temp_stream, stream, 1, C1);}
else {{ last_pos= -1;
 stream_slice_rv= last_pos;
}
}
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[temp_stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[temp_stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(temp_stream, (end + 1));}
 stream_rv= unwind_value;
}

}
}}
}

 if (!(temp_stream == -1))
{{ F143(temp_stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(66583, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(66583, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ temp_str= _SH[(V8[temp_stream] + 1)];
 F417(V8[temp_stream]);
}
}
}

 F405(temp_stream);
}
}
}
}
}

 withG_value= stream_rv;
}

 _SH[66596]= MULTprint_prettyPMULTCOPY;
}

 rv_aux= last_pos;
}

 return rv_aux;
}
}

int F2(int vect, int index)
{{ if ((vect >= C22) && (vect <= (C17 - 2)) && ((unsigned int)_SH[vect] == C28))
{if ((index >= 1) && (index <= _SH[(vect + 1)]))
{return _SH[(vect + index + 1)];}
else {F233(vect, index);}}
else {{ printf("\n***** EXIT: ---NOT a vect---\n");
 fflush(stdout);
 exit(0);
}
}
 return 0;
}
}

int F3(int str, int stream, int len1)
{{int rv_aux = nil; { {int stream__1 = stream; int str__2; if (_SH[66597] != nil)
{str__2= _SH[65621];}
else {str__2= nil;}
 F235(stream__1, str__2);
}

 {int stream__4 = stream; int str__5; {int csym = nil; int csym_on = nil; int index = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 index= 1;
 { {int test_aux_var; {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (index <= (len1 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (index == (len1 + 1))
{F235(stream, _SH[65622]);}
else {{int test_aux_var; {int or_result = nil; { if (F510(F292(csym)) == 3)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(csym, 32);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (!(_SH[66597] != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}

}

 if (test_aux_var)
{F673(csym, stream);}
else {F83(csym, stream);}
}
}
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

 index= (index + 1);
 {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (index <= (len1 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 str__5= nil;
}

}

 F235(stream__4, str__5);
}

 {int stream__38 = stream; int str__39; if (_SH[66597] != nil)
{str__39= _SH[65623];}
else {str__39= nil;}
 rv_aux= F235(stream__38, str__39);
}

}

 return rv_aux;
}
}

int F4(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int or_result = nil; { if (csym_code < 32)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {{ if (csym_code == 127)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

}

 return rv_aux;
}
}

int F5(int F443)
{{int rv_aux = nil; {int field_names = nil; int headers = nil; int read_conversion_fns = nil; int write_conversion_fns = nil; int read_fn_flagP = nil; int write_fn_flagP = nil; if (!(F750(F443, 66797) != nil))
{{ F158(F443, 66797, 66407);
 if (!(F750(F443, 66798) != nil))
{F158(F443, 66798, 9);}
 if (!(F750(F443, 66799) != nil))
{F158(F443, 66799, 10);}
 {int field_specs = nil; int read_conversion_fn = nil; int write_conversion_fn = nil; int aux_rec = nil; aux_rec= F443;
 field_specs= F750(aux_rec, 66572);
 read_conversion_fn= F750(aux_rec, 66800);
 write_conversion_fn= F750(aux_rec, 66801);
 {int field_spec = nil; int field_spec_on = nil; { field_spec_on= field_specs;
 field_spec= _SH[field_spec_on];
}

 while (field_spec_on != nil){ {int field_name = nil; int header = nil; int field_read_conversion_fn = nil; int field_write_conversion = nil; int aux = nil; aux= field_spec;
 field_name= _SH[aux];
 aux= _SH[(aux + 1)];
 header= _SH[aux];
 aux= _SH[(aux + 1)];
 field_read_conversion_fn= _SH[aux];
 aux= _SH[(aux + 1)];
 field_write_conversion= _SH[aux];
 field_names= _c(field_name, field_names);
 headers= _c(header, headers);
 if (field_read_conversion_fn != nil)
{{ read_fn_flagP= 66407;
 read_conversion_fns= _c(field_read_conversion_fn, read_conversion_fns);
}
}
else {read_conversion_fns= _c(read_conversion_fn, read_conversion_fns);}
 if (field_write_conversion != nil)
{{ write_fn_flagP= 66407;
 write_conversion_fns= _c(field_write_conversion, write_conversion_fns);
}
}
else {write_conversion_fns= _c(write_conversion_fn, write_conversion_fns);}
}

 { field_spec_on= _SH[(field_spec_on + 1)];
 field_spec= _SH[field_spec_on];
}

}

}

 F158(F443, 66802, F574(field_names));
 F158(F443, 66803, F574(headers));
 {int test_aux_var; {int or_result = nil; { or_result= read_conversion_fn;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= read_fn_flagP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{F158(F443, 66804, F574(read_conversion_fns));}
}

 {int test_aux_var; {int or_result = nil; { or_result= write_conversion_fn;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= write_fn_flagP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{F158(F443, 66805, F574(write_conversion_fns));}
}

}

}
}
 rv_aux= F443;
}

 return rv_aux;
}
}

int F6(int iter_spec)
{{int rv_aux = nil; {int var = nil; int value = nil; int aux = nil; aux= iter_spec;
 var= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 rv_aux= _c(66481, _c(_c(66476, _c(_c(var, nil), nil)), _c(_c(66477, _c(_c(66409, _c(var, _c(value, nil))), nil)), _c(_c(66461, _c(_c(66485, _c(var, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66409, _c(var, _c(_c(66441, _c(var, nil)), nil))), nil)), nil)))));
}

 return rv_aux;
}
}

int F7(int list)
{return _SH[(_SH[(list + 1)] + 1)];}

int F8(int str)
{return F664(66734, _SH[66754], _c(str, nil));}

int F9(int rec_name, int rec, int field_name, int value)
{return _c(66575, _c(F600(rec_name, field_name), _c(rec, _c(value, nil))));}

int F10(int rec_name)
{return F394(F755(_c(F292(rec_name), _c(_SH[65565], nil))), 66407);}

int F11(int stream, int n, int inTOout_fn)
{{int rv_aux = nil; {int str = nil; {int out_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{out_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= 66612;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = 66583; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(str != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= str;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 out_stream= STREAM__BUF_935;
}
}
}

 unwind_value= out_stream;
}

 if (out_stream == -1)
;
else {{ stream_rv= F765(stream, n, out_stream, inTOout_fn);
 if (!(out_stream == -1))
{{ F143(out_stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(66583, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(66583, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ str= _SH[(V8[out_stream] + 1)];
 F417(V8[out_stream]);
}
}
}

 F405(out_stream);
}
}
}
}
}

}

 rv_aux= str;
}

 return rv_aux;
}
}

int SHARKexit(int message)
{{int rv_aux = nil; { F209(message);
 printf("\n***** EXIT: %s\n", V53);
 fflush(stdout);
 exit(0);
 return nil;
}

 return rv_aux;
}
}

int F13(int str)
{return F280(_SH[66764], str);}

int F14()
{return F401();}

int F15(int pattern, int aux_var)
{{int rv_aux = nil; {int var = nil; int set_list = nil; int _38optP = nil; int value = nil; while (pattern != nil){ { var= _SH[pattern];
 pattern= _SH[(pattern + 1)];
}

 if (var == 66433)
{{ { var= _SH[pattern];
 pattern= _SH[(pattern + 1)];
}

 if (F556(var) != nil)
{{ value= F283(var);
 var= _SH[var];
}
}
else {value= nil;}
 set_list= _c(_c(66420, _c(_c(aux_var, _c(_c(66409, _c(var, _c(_c(66438, _c(aux_var, _c(1, _c(((- F510(pattern)) - 1), nil)))), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(value, nil))), nil)), nil))), set_list);
 if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66439, _c(aux_var, _c(_c(66440, _c(var, nil)), nil))), nil))), set_list);}
}
}
else {if (var == nil)
{if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66441, _c(aux_var, nil)), nil))), set_list);}}
else {if (F88(var, 66407) != nil)
{if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66441, _c(aux_var, nil)), nil))), set_list);}}
else {if (var == 66434)
{_38optP= 66407;}
else {if (var != nil)
{{ if (F556(var) != nil)
{{ value= F283(var);
 var= _SH[var];
}
}
else {value= nil;}
 if (!(_38optP != nil))
{{ set_list= _c(_c(66409, _c(var, _c(_c(66442, _c(aux_var, nil)), nil))), set_list);
 if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66441, _c(aux_var, nil)), nil))), set_list);}
}
}
else {{ set_list= _c(_c(66420, _c(_c(aux_var, _c(_c(66409, _c(var, _c(_c(66442, _c(aux_var, nil)), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(value, nil))), nil)), nil))), set_list);
 if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66441, _c(aux_var, nil)), nil))), set_list);}
}
}
}
}
else {if (pattern != nil)
{set_list= _c(_c(66409, _c(aux_var, _c(_c(66441, _c(aux_var, nil)), nil))), set_list);}}}}}}
}

 rv_aux= F574(set_list);
}

 return rv_aux;
}
}

int F16(int object_index)
{if ((object_index >= 0) && (object_index <= (C20 - 1)))
{return C24;}
else {return nil;}}

int F17(int stream, int unmatched_tags_as_singleP, int print_errorsP)
{{int rv_aux = nil; {int tag_name_str = nil; int tag_name = nil; int tag_pos = nil; tag_name_str= F51(stream, nil);
 tag_name= F583(tag_name_str);
 tag_pos= F35(tag_name);
 if (tag_pos == nil)
{if (print_errorsP != nil)
{{ F87(_SH[66435]);
 F87(_SH[65757]);
 F87(tag_name_str);
 F373(nil);
}
}}
else {{ F395((_SH[66836] - tag_pos), unmatched_tags_as_singleP, print_errorsP);
 F540(nil);
}
}
 rv_aux= F366(stream, 62);
}

 return rv_aux;
}
}

double F18(double x)
{return (log(x) / log(2.000000000));}

int F19(int num)
{return num;}

int F20(int aux_stream)
{{int rv_aux = nil; if (_SH[(aux_stream + 1)] != nil)
{rv_aux= 66407;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F21(char* html_file)
{{int rv_aux = nil; { F87(_SH[66435]);
 F87(_SH[65763]);
 F87(F184(F450(html_file)));
 rv_aux= F373(nil);
}

 return rv_aux;
}
}

int F22(int stream_index)
{return F139(stream_index);}

int F23(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66713);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F24(int iter_spec)
{{int rv_aux = nil; {int var_index = nil; {int var = nil; int vect = nil; int start = nil; int end = nil; int aux = nil; aux= iter_spec;
 var= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 vect= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 start= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 end= _SH[aux];
 var_index= F393(var);
 if (!(start != nil))
{start= 1;}
 {int object1__13 = 66481; int object2__14; {int object1__15 = _c(66476, _c(_c(var, _c(var_index, _c(66515, nil))), nil)); int object2__16; {int object1__27; int object2__28; {int object1__29 = 66477; int object2__30; {int object1__31; int object2__32; {int object1__33 = 66405; int object2__34; {int object1__35 = _c(66409, _c(var_index, _c(start, nil))); int object2__36; {int object1__43 = _c(66409, _c(var, _c(_c(66516, _c(vect, _c(var_index, nil))), nil))); int object2__44; {int object1__57; int object2__58; if (end != nil)
{object1__57= _c(66409, _c(66515, _c(end, nil)));}
else {object1__57= _c(66409, _c(66515, _c(_c(66517, _c(vect, nil)), nil)));}
 object2__58= nil;
 object2__44= _c(object1__57, object2__58);
}

 object2__36= _c(object1__43, object2__44);
}

 object2__34= _c(object1__35, object2__36);
}

 object1__31= _c(object1__33, object2__34);
}

 object2__32= nil;
 object2__30= _c(object1__31, object2__32);
}

 object1__27= _c(object1__29, object2__30);
}

 object2__28= _c(_c(66461, _c(_c(66428, _c(var_index, _c(66515, nil))), nil)), _c(_c(66479, _c(_c(66405, _c(_c(66409, _c(var_index, _c(_c(66432, _c(var_index, _c(1, nil))), nil))), _c(_c(66430, _c(_c(66428, _c(var_index, _c(66515, nil))), _c(_c(66409, _c(var, _c(_c(66516, _c(vect, _c(var_index, nil))), nil))), nil))), nil))), nil)), nil));
 object2__16= _c(object1__27, object2__28);
}

 object2__14= _c(object1__15, object2__16);
}

 rv_aux= _c(object1__13, object2__14);
}

}

}

 return rv_aux;
}
}

int F25(int spec_value, int rec_value, int test_fn)
{{int rv_aux = nil; if (test_fn == nil)
{rv_aux= F256(spec_value, rec_value);}
else {rv_aux= F325(test_fn, spec_value, rec_value);}
 return rv_aux;
}
}

int F26()
{{int rv_aux = nil; {int index; index= 0;
 { while (index < C29){ V56[index]= NULL;
 index= (index + 1);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F27(int lines)
{{int rv_aux = nil; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = nil; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F486(lines) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= F486(lines);}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ {int term_recs = nil; int aux = nil; aux= F247(stream, 66847, 66848, nil);
 term_recs= _SH[aux];
 stream_rv= F671(term_recs, nil, nil);
}

 if (!(stream == -1))
{{ F143(stream);
 F417(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}

 return rv_aux;
}
}

int F28(int stack)
{{int rv_aux = nil; if (!(F103(stack) != nil))
{{int top_pos = F2(stack, 1); top_pos= (top_pos - 1);
 F91(stack, 1, top_pos);
 rv_aux= F2(stack, top_pos);
}
}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F29(int x)
{return (nil != nil);}

int F30(int str)
{{int rv_aux = nil; {int str_copy = nil; int word_startP = nil; str_copy= str;
 word_startP= 66407;
 while (str != nil){ if (!(F586(_SH[str]) != nil))
{word_startP= 66407;}
else {if (word_startP != nil)
{{ _SH[str]= F320(_SH[str]);
 word_startP= nil;
}
}
else {_SH[str]= F672(_SH[str]);}}
 { _SH[str];
 str= _SH[(str + 1)];
}

}

 rv_aux= str_copy;
}

 return rv_aux;
}
}

int F31(int stream)
{{int rv_aux = nil; {int end_of_attlistP = nil; int attname = nil; int attvalue = nil; int attlist = nil; while (!(end_of_attlistP != nil)){ { F361(stream, 66834);
 attname= F624(stream, _SH[66841], nil);
 F361(stream, 66834);
}

 if (!(attname != nil))
{end_of_attlistP= 66407;}
else {if (F690(stream, 61, nil) != nil)
{{ F361(stream, 66834);
 if (F690(stream, 34, nil) != nil)
{{ attvalue= F624(stream, 34, nil);
 F554(stream, 1);
}
}
else {if (F690(stream, 39, nil) != nil)
{{ attvalue= F624(stream, 39, nil);
 F554(stream, 1);
}
}
else {if (F690(stream, 96, nil) != nil)
{{ attvalue= F624(stream, 96, nil);
 F554(stream, 1);
}
}
else {attvalue= F624(stream, _SH[66833], nil);}}}
 attlist= _c(_c(F583(attname), _c(attvalue, nil)), attlist);
}
}
else {attlist= _c(_c(F583(attname), _c(F328(attname), nil)), attlist);}}
}

 rv_aux= F574(attlist);
}

 return rv_aux;
}
}

int F32(int clauses)
{{int rv_aux = nil; {int clause1 = nil; int rest_clauses = nil; clause1= _SH[clauses];
 rest_clauses= _SH[(clauses + 1)];
 if (clause1 == nil)
{rv_aux= nil;}
else {if (_SH[clause1] == 66407)
{rv_aux= F753(_SH[(clause1 + 1)]);}
else {if (rest_clauses != nil)
{rv_aux= _c(66408, _c(_SH[clause1], _c(F753(_SH[(clause1 + 1)]), _c(F32(rest_clauses), nil))));}
else {rv_aux= _c(66408, _c(_SH[clause1], _c(F753(_SH[(clause1 + 1)]), nil)));}}}
}

 return rv_aux;
}
}

int F33(int stream)
{{int rv_aux = nil; {int or_result = nil; { if (F237(stream) == F502(stream))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F658(F396(stream, -1));
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F34(int counter, int it_counter, int en_counter, int string)
{{int rv_aux = nil; {float ratio; {int test_aux_var; {int or_result = nil; { {int and_result = nil; { if (it_counter == 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (en_counter == 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (it_counter == en_counter)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ F235(_SH[66416], _SH[65689]);
 F235(_SH[66416], _SH[66435]);
 rv_aux= F235(_SH[66416], string);
}
}
else {{ { F235(_SH[66416], _SH[65690]);
 F235(_SH[66416], F564(F189(((100.000000000 * it_counter) / counter), 2, nil), 5, 32, 66444));
 F235(_SH[66416], _SH[65691]);
 F235(_SH[66416], F564(F189(((100.000000000 * en_counter) / counter), 2, nil), 5, 32, 66444));
}

 {int test_aux_var; {int or_result = nil; { if (it_counter == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (en_counter == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{rv_aux= nil;}
else {{ if (it_counter > en_counter)
{ratio= (it_counter / en_counter);}
else {ratio= (en_counter / it_counter);}
 { F235(_SH[66416], _SH[65692]);
 F235(_SH[66416], F564(F189(ratio, 2, nil), 5, 32, 66444));
}

 if (ratio <= 3)
{{ F235(_SH[66416], _SH[65693]);
 F235(_SH[66416], F714(it_counter, nil, 48, 66444, 10));
 F235(_SH[66416], _SH[65694]);
 F235(_SH[66416], F714(en_counter, nil, 48, 66444, 10));
 F235(_SH[66416], _SH[65695]);
 rv_aux= F235(_SH[66416], string);
}
}
else {rv_aux= nil;}
}
}
}

}
}
}

}

 return rv_aux;
}
}

int F35(int tag_name)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int index = nil; index= _SH[66836];
 {int test_aux_var; {int and_result = nil; { if (index >= 1)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (tag_name == F2(_SH[66837], index))
{for_thereis_result= index;}
else {for_thereis_result= nil;}
 index= (index - 1);
 {int and_result = nil; { if (index >= 1)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F36(int sock_index)
{{int rv_aux = nil; if (V60[sock_index] == -1)
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F37(int num, int base, int add_signP)
{{int rv_aux = nil; if (base <= 16)
{{int remainder = nil; int str = nil; int negativeP = nil; if (num < 0)
{{ negativeP= 66407;
 num= (- num);
}
}
 while (num >= base){ remainder= (num % base);
 str= _c(F740(remainder), str);
 num= (num / base);
}

 str= _c(F740(num), str);
 {int test_aux_var; {int and_result = nil; { and_result= nil;
 if (and_result != nil)
{{ if (F510(str) < nil)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{str= F755(_c(F514((nil - F510(str)), 48), _c(str, nil)));}
}

 if (negativeP != nil)
{rv_aux= _c(45, str);}
else {if (add_signP != nil)
{rv_aux= _c(43, str);}
else {rv_aux= str;}}
}
}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F38(int lp_file)
{{int rv_aux; {struct stat file_status; int size; if (stat(F555(lp_file, nil), &file_status) >= 0)
{{ size= (int)file_status.st_size;
 rv_aux= size;
}
}
else {rv_aux= -1;}
}

 return rv_aux;
}
}

int F39(int size)
{{int rv_aux = nil; {int hashtable = nil; hashtable= F397(size);
 {int index = nil; index= 1;
 while (index <= size){ F91(hashtable, index, nil);
 index= (index + 1);
}

}

 rv_aux= hashtable;
}

 return rv_aux;
}
}

int F40(int lp, int os)
{{int rv_aux = nil; {int exp_path = nil; exp_path= F668(lp);
 if (F665(F750(_SH[66427], 66576), _SH[65578], 66443) != nil)
{rv_aux= F222(exp_path);}
else {{int test_aux_var; {int or_result = nil; { or_result= F343(os, _SH[65579], 66443);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F343(os, _SH[65580], 66443);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F343(os, _SH[65581], 66443);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}

}

 if (test_aux_var)
{rv_aux= F294(exp_path);}
else {if (F343(os, _SH[65582], 66443) != nil)
{rv_aux= F709(exp_path);}
else {{ { F87(_SH[66435]);
 F87(_SH[65583]);
 F87(os);
 F373(nil);
}

 rv_aux= nil;
}
}}
}
}
}

 return rv_aux;
}
}

int64_t F41(int buf_size, int buf_index)
{{int64_t buf_start = buf_size; return ((buf_start * buf_index) + 1);
}
}

int F42(int list, int fn, int other_args)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F717(fn, _c(el, other_args)) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 el= _SH[(el + 1)];
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int SHARKread(int stream, int sc_comment_flag, int hm_comment_flag)
{return F440(stream, sc_comment_flag, hm_comment_flag);}

int F44(int x)
{return (nil != nil);}

int F45(int arc, int node)
{return F158(node, 66631, _c(arc, F750(node, 66631)));}

void F46(int* flag_vect, int index)
{flag_vect[index]= 0;}

int F47(int csym)
{return F167(csym, _SH[65667]);}

int F48(int str)
{{int rv_aux = nil; {int new_sym = nil; int prop_rec = nil; { new_sym= V42;
 V42= (V42 + 1);
 _SH[new_sym]= nil;
 if (str)
{{ _SH[(new_sym + C21)]= str;
 prop_rec= _c(66741, _c(_c(66742, _c(C24, nil)), nil));
 _SH[(new_sym + C21 + C21)]= prop_rec;
 F437(V45, str, new_sym);
}
}
 return new_sym;
}

}

 return rv_aux;
}
}

int F49()
{{int rv_aux = nil; { V3= (V3 - (2 * C9));
 rv_aux= nil;
}

 return rv_aux;
}
}

int F50(int a1, int a2, int a3, int a4)
{return _c(a1, _c(a2, _c(a3, _c(a4, nil))));}

int F51(int stream, int skip_attlistP)
{{int rv_aux = nil; {int tag_name = nil; tag_name= F624(stream, _SH[66833], nil);
 if (skip_attlistP != nil)
{F366(stream, _SH[65762]);}
 rv_aux= tag_name;
}

 return rv_aux;
}
}

int F52(int stream, int separator, int read_conversion_fn)
{{int rv_aux = nil; {int el = nil; { el= F624(stream, separator, nil);
 F366(stream, separator);
}

 if (read_conversion_fn != nil)
{el= F483(read_conversion_fn, el);}
 rv_aux= el;
}

 return rv_aux;
}
}

int F53(int list, int test_fn)
{{int rv_aux = nil; {int size = nil; int length = nil; int curr = nil; int guard = nil; size= 1;
 length= F510(list);
 guard= _c(nil, list);
 while (size < length){ curr= guard;
 while (_SH[(curr + 1)] != nil){ curr= F488(curr, size, test_fn);
}

 size= (size * 2);
}

 list= _SH[(guard + 1)];
 F417(guard);
 rv_aux= list;
}

 return rv_aux;
}
}

int F54(int stream)
{return getc(stdin);}

int F55(int nums)
{{int rv_aux = nil; if (F510(nums) == 1)
{rv_aux= _SH[nums];}
else {if (F510(nums) == 2)
{rv_aux= _c(66420, _c(_c(_c(66428, _c(_SH[nums], _c(F283(nums), nil))), _c(_SH[nums], nil)), _c(_c(66407, _c(F283(nums), nil)), nil)));}
else {rv_aux= _c(66420, _c(_c(_c(66428, _c(_SH[nums], _c(F283(nums), nil))), _c(F55(_c(_SH[nums], F7(nums))), nil)), _c(_c(66407, _c(F55(_SH[(nums + 1)]), nil)), nil)));}}
 return rv_aux;
}
}

int F56(int str)
{return F664(66734, _SH[66758], _c(str, nil));}

int F57(int in_stream, int condition, int condition_type, int inTOout_fn)
{{int rv_aux = nil; {int str = nil; {int out_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{out_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= 66612;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = 66583; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(str != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= str;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 out_stream= STREAM__BUF_935;
}
}
}

 unwind_value= out_stream;
}

 if (out_stream == -1)
;
else {{ stream_rv= F680(in_stream, out_stream, condition, condition_type, inTOout_fn);
 if (!(out_stream == -1))
{{ F143(out_stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(66583, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(66583, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ str= _SH[(V8[out_stream] + 1)];
 F417(V8[out_stream]);
}
}
}

 F405(out_stream);
}
}
}
}
}

}

 rv_aux= str;
}

 return rv_aux;
}
}

int F58()
{{int rv_aux = nil; { _SH[66764]= F629();
 {int term = nil; int term_on = nil; { term_on= _SH[66754];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66765);
 F437(_SH[66764], F336(term), 66765);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66755];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66766);
 F437(_SH[66764], F336(term), 66766);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66757];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66767);
 F437(_SH[66764], F336(term), 66767);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66762];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66768);
 F437(_SH[66764], F336(term), 66768);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66756];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66769);
 F437(_SH[66764], F336(term), 66769);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66758];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66770);
 F437(_SH[66764], F336(term), 66770);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66759];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66771);
 F437(_SH[66764], F336(term), 66771);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66760];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66772);
 F437(_SH[66764], F336(term), 66772);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66761];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66764], term, 66773);
 F437(_SH[66764], F336(term), 66773);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66763];
 term= _SH[term_on];
}

 { while (term_on != nil){ F437(_SH[66764], term, 66774);
 F437(_SH[66764], F336(term), 66774);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F59(int term, int lang)
{{int rv_aux = nil; if (lang == 66792)
{rv_aux= F232(term);}
else {if (lang == 66793)
{rv_aux= F13(term);}
else {{int or_result = nil; { or_result= F232(term);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F13(term);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}
}}
 return rv_aux;
}
}

int F60(int pred, int list, int args)
{{int rv_aux = nil; {int el = nil; int res = nil; int F424 = nil; args= _c(nil, args);
 res= 1;
 F424= -1;
 while (list != nil){ { el= _SH[list];
 list= _SH[(list + 1)];
}

 _SH[args]= el;
 if (F717(pred, args) != nil)
{{ F424= res;
 list= nil;
}
}
 res= (res + 1);
}

 F417(args);
 rv_aux= F424;
}

 return rv_aux;
}
}

int F61(int sock_index)
{{int rv_aux = nil; if (V59[sock_index] < V60[sock_index])
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F62(int field_specs)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int field_spec = nil; int field_spec_on = nil; { field_spec_on= field_specs;
 field_spec= _SH[field_spec_on];
}

 while (field_spec_on != nil){ {int object1__4; int object2__5 = for_list_result; {int type = nil; int name = nil; int aux = nil; aux= field_spec;
 type= _SH[aux];
 aux= _SH[(aux + 1)];
 name= _SH[aux];
 object1__4= _c(66573, _c(_c(66548, _c(name, nil)), _c(_c(66567, _c(type, nil)), _c(_c(66549, _c(F655(type), nil)), nil))));
}

 for_list_result= _c(object1__4, object2__5);
}

 { field_spec_on= _SH[(field_spec_on + 1)];
 field_spec= _SH[field_spec_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F63(int str1, int str2)
{{int rv_aux = nil; {int csym1 = nil; int csym2 = nil; int eq_flagP = nil; int flag = nil; eq_flagP= 66407;
 while (eq_flagP != nil){ { csym1= _SH[str1];
 str1= _SH[(str1 + 1)];
}

 { csym2= _SH[str2];
 str2= _SH[(str2 + 1)];
}

 if (csym1 != nil)
{csym1= F672(csym1);}
 if (csym2 != nil)
{csym2= F672(csym2);}
 if (csym1 == nil)
{{ eq_flagP= nil;
 if (csym2 == nil)
{flag= 66452;}
else {flag= 66454;}
}
}
else {if (csym2 == nil)
{{ eq_flagP= nil;
 flag= 66453;
}
}
else {if (csym1 == csym2)
;
else {if (F773(csym1) > F773(csym2))
{{ eq_flagP= nil;
 flag= 66453;
}
}
else {{ eq_flagP= nil;
 flag= 66454;
}
}}}}
}

 rv_aux= flag;
}

 return rv_aux;
}
}

int F64(int act_spec)
{{int rv_aux = nil; {int and_result = nil; { and_result= F556(act_spec);
 if (and_result != nil)
{{ and_result= F664(66443, _SH[65637], _c(_SH[act_spec], nil));
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F65(int csym, int base)
{{int rv_aux = nil; {int int1 = nil; if (F16(csym) != nil)
{{ int1= F391(csym);
 {int test_aux_var; {int and_result = nil; { and_result= int1;
 if (and_result != nil)
{{ if (int1 < base)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= 66407;}
else {rv_aux= nil;}
}

}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F66(int pred, int list, int args)
{{int rv_aux = nil; {int res_list = nil; res_list= list;
 args= _c(_SH[list], args);
 if (F717(pred, args) != nil)
{{ _SH[res_list];
 res_list= _SH[(res_list + 1)];
}
}
 while (_SH[(list + 1)] != nil){ _SH[args]= F283(list);
 if (F717(pred, args) != nil)
{_SH[(list + 1)]= F7(list);}
 { _SH[list];
 list= _SH[(list + 1)];
}

}

 F417(args);
 rv_aux= res_list;
}

 return rv_aux;
}
}

int F67(int str)
{{int rv_aux = nil; {int str_buf = nil; str_buf= str;
 while (str_buf != nil){ _SH[str_buf]= F672(_SH[str_buf]);
 { _SH[str_buf];
 str_buf= _SH[(str_buf + 1)];
}

}

 rv_aux= str;
}

 return rv_aux;
}
}

int F68(int pred, int list, int args)
{{int rv_aux = nil; {int buf = list; int last = nil; int e2 = nil; int trimmed_list = nil; if (list != nil)
{{ args= _c(nil, args);
 while (list != nil){ _SH[args]= _SH[list];
 if (!(F717(pred, args) != nil))
{last= list;}
 { _SH[list];
 list= _SH[(list + 1)];
}

}

 while (!(buf == last)){ { e2= _SH[buf];
 buf= _SH[(buf + 1)];
}

 trimmed_list= _c(e2, trimmed_list);
}

 { e2= _SH[buf];
 buf= _SH[(buf + 1)];
}

 trimmed_list= _c(e2, trimmed_list);
 F417(args);
 rv_aux= F574(trimmed_list);
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F69(int list, int last_cons)
{{int rv_aux = nil; {int test_aux_var; {int _ARG1__1; {int or_result = nil; { if (V46 == 1)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {{ or_result= F256(list, nil);
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {_ARG1__1= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__1);
}

 if (test_aux_var)
{{ { if (!(last_cons != nil))
{last_cons= F676(list);}
 _SH[(last_cons + 1)]= V41;
 V41= list;
}

 return nil;
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F70(int iter_spec)
{{int rv_aux = nil; {int condition = nil; int aux = nil; aux= iter_spec;
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 rv_aux= _c(66481, _c(_c(66430, _c(_c(66482, _c(condition, nil)), nil)), nil));
}

 return rv_aux;
}
}

int F71(int row, int stream, int separator, int terminator)
{{int rv_aux = nil; {int el = nil; int el_on = nil; { el_on= row;
 el= _SH[el_on];
}

 { while (el_on != nil){ if (_SH[(el_on + 1)] != nil)
{F144(el, separator, stream, nil);}
else {F144(el, terminator, stream, nil);}
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F72(int stream, int F757, int last_pos, int initial_pos)
{{int rv_aux = nil; { {int test_aux_var; {int and_result = nil; { and_result= F256(F757, 66423);
 if (and_result != nil)
{{ if (last_pos == initial_pos)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{ F757= nil;
 F102(stream, initial_pos);
}
}
else {{ F102(stream, (last_pos - 1));
 F757= 66407;
}
}
}

 rv_aux= F757;
}

 return rv_aux;
}
}

int F73(int str, int stream)
{{int rv_aux = nil; {int csym = nil; { while (str != nil){ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 F673(csym, stream);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F74(int element)
{return F774(element, 66828);}

int F75()
{return F2(_SH[66838], _SH[66836]);}

int F76(int fn)
{{int rv_aux = nil; switch (fn) {
   case 66744: rv_aux= F225(nil); break;
   case 65793: rv_aux= F541(); break;
   case 66344: rv_aux= F755(nil); break;
   case 66343: rv_aux= F735(nil); break;
   case 66497: rv_aux= F49(); break;
   case 65792: rv_aux= F734(); break;
   case 65791: rv_aux= F629(); break;
   case 65958: rv_aux= F662(F750(_SH[66427], 66740)); break;
   case 65790: rv_aux= F766(); break;
   case 65789: rv_aux= F154(); break;
   case 65788: rv_aux= F14(); break;
   case 65787: rv_aux= F164(); break;
   case 65786: rv_aux= F380(); break;
   case 65785: rv_aux= F487(); break;
   case 65784: rv_aux= F522(); break;
   case 65783: rv_aux= F401(); break;
   case 65782: rv_aux= F26(); break;
   case 65781: rv_aux= F183(); break;
   case 65780: rv_aux= F285(); break;
   case 65779: rv_aux= F58(); break;
   case 65778: rv_aux= F577(); break;
   case 65777: rv_aux= F264(); break;
   case 65776: rv_aux= F120(); break;
   case 65775: rv_aux= F506(); break;
   case 65774: rv_aux= F75(); break;
   case 65806: rv_aux= F540(nil); break;
   case 65773: rv_aux= F312(); break;
   case 65772: rv_aux= F627(); break;
   default: return F563(fn, 0); break;
}
 return rv_aux;
}
}

int F77(int exprs)
{{int rv_aux = nil; {int flag = 66407; int expr = nil; while (exprs != nil){ { expr= _SH[exprs];
 exprs= _SH[(exprs + 1)];
}

 {int test_aux_var; {int _ARG1__3; {int or_result = nil; { or_result= F551(expr);
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ or_result= F123(expr);
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

 if (test_aux_var)
{{ exprs= nil;
 flag= nil;
}
}
}

}

 rv_aux= flag;
}

 return rv_aux;
}
}

int F78(int plist, int field)
{{int rv_aux = nil; {int field1 = nil; int value = nil; while (plist != nil){ { field1= _SH[plist];
 plist= _SH[(plist + 1)];
}

 if (field1 == field)
{{ { value= _SH[plist];
 plist= _SH[(plist + 1)];
}

 plist= nil;
}
}
else {{ _SH[plist];
 plist= _SH[(plist + 1)];
}
}
}

 rv_aux= value;
}

 return rv_aux;
}
}

int F79(int stream)
{{int rv_aux = nil; { {int buf_index = V20[stream]; if (F771(buf_index) != nil)
{V27[stream]= (V27[stream] + 1);}
else {V31[stream]= (V31[stream] + 1);}
}

 rv_aux= nil;
}

 return rv_aux;
}
}

int F80(int var_name)
{return F394(F755(_c(F292(var_name), _c(_SH[65558], nil))), 66407);}

int F81(int tag_case)
{{int rv_aux = nil; if (!(_SH[66830] != nil))
{{ _SH[66830]= 66407;
 _SH[66839]= tag_case;
 _SH[66836]= 0;
 rv_aux= F445(66840);
}
}
else {{ { { F87(_SH[66435]);
 F87(_SH[65761]);
 F87(_SH[66435]);
 F373(nil);
}

}

 rv_aux= nil;
}
}
 return rv_aux;
}
}

int F82(int lp, int full_nameP)
{{int rv_aux = nil; {int slash_pos = nil; int point_pos = nil; int name = nil; if (F137(lp, -1) == 47)
{lp= F532(lp, 1, -2);}
 slash_pos= F526(47, lp);
 if (slash_pos == nil)
{name= lp;}
else {if (slash_pos == 1)
{name= _SH[(lp + 1)];}
else {name= F779(lp, slash_pos);}}
 if (full_nameP != nil)
{rv_aux= F289(name);}
else {{ point_pos= F424(46, name);
 if (point_pos == -1)
{rv_aux= F289(name);}
else {rv_aux= F532(name, 1, (point_pos - 1));}
}
}
}

 return rv_aux;
}
}

int F83(int csym, int stream)
{{int rv_aux = nil; if (_SH[66597] != nil)
{rv_aux= F235(stream, F292(csym));}
else {rv_aux= F673(csym, stream);}
 return rv_aux;
}
}

int F84(int a1, int a2, int a3)
{return _c(a1, _c(a2, _c(a3, nil)));}

int F85(int F339)
{{int rv_aux = nil; {int answer = nil; answer= F302(F755(_c(_SH[65661], _c(F40(F339, F750(_SH[66427], 66740)), nil))), 66407);
 {int for_list_result = nil; for_list_result= nil;
 {int line = nil; int line_on = nil; { line_on= answer;
 line= _SH[line_on];
}

 while (line_on != nil){ if (!(F757(line, _SH[65662], 66443) != nil))
{for_list_result= _c(F560(66753, line, 66444, nil), for_list_result);}
 { line_on= _SH[(line_on + 1)];
 line= _SH[line_on];
}

}

}

 rv_aux= F574(for_list_result);
}

}

 return rv_aux;
}
}

int _c(int object1, int object2)
{{int tmp; if (V41 == nil)
{return F242(object1, object2);}
else {{ tmp= V41;
 V41= _SH[(V41 + 1)];
 _SH[tmp]= object1;
 _SH[(tmp + 1)]= object2;
 return tmp;
}
}
}
}

int F87(int str)
{{int rv_aux = nil; {int csym = nil; { while (str != nil){ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 stdoutTyo(csym, nil);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F88(int object, int check_allP)
{{int rv_aux = nil; if (F556(object) != nil)
{if (check_allP != nil)
{rv_aux= F767(object);}
else {rv_aux= F16(_SH[object]);}}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F89(int lists, int delimiter, int grammar, int include_empty_itemsP)
{{int rv_aux = nil; {int firstP = nil; int cur = nil; int join_list = nil; firstP= 66407;
 {int test_aux_var; {int and_result = nil; { and_result= lists;
 if (and_result != nil)
{{ and_result= F256(grammar, 66449);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{int cur_list = nil; int push_rl_value = nil; cur_list= delimiter;
 while (cur_list != nil){ { push_rl_value= _SH[cur_list];
 cur_list= _SH[(cur_list + 1)];
}

 join_list= _c(push_rl_value, join_list);
}

}
}
}

 while (lists != nil){ cur= _SH[lists];
 lists= _SH[(lists + 1)];
 {int test_aux_var; {int or_result = nil; { or_result= cur;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ if (firstP != nil)
{firstP= nil;}
else {{int cur_list = nil; int push_rl_value = nil; cur_list= delimiter;
 while (cur_list != nil){ { push_rl_value= _SH[cur_list];
 cur_list= _SH[(cur_list + 1)];
}

 join_list= _c(push_rl_value, join_list);
}

}
}
 {int cur_list = nil; int push_rl_value = nil; cur_list= cur;
 while (cur_list != nil){ { push_rl_value= _SH[cur_list];
 cur_list= _SH[(cur_list + 1)];
}

 join_list= _c(push_rl_value, join_list);
}

}

}
}
}

}

 {int test_aux_var; {int and_result = nil; { and_result= join_list;
 if (and_result != nil)
{{ and_result= F256(grammar, 66450);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{int cur_list = nil; int push_rl_value = nil; cur_list= delimiter;
 while (cur_list != nil){ { push_rl_value= _SH[cur_list];
 cur_list= _SH[(cur_list + 1)];
}

 join_list= _c(push_rl_value, join_list);
}

}
}
}

 rv_aux= F574(join_list);
}

 return rv_aux;
}
}

int F90(int object, int pred, int list, int args)
{{int rv_aux = nil; {int el = nil; int new_list = nil; args= _c(nil, args);
 while (list != nil){ { el= _SH[list];
 list= _SH[(list + 1)];
}

 _SH[args]= el;
 if (F717(pred, args) != nil)
{new_list= _c(object, new_list);}
else {new_list= _c(el, new_list);}
}

 F417(args);
 rv_aux= F574(new_list);
}

 return rv_aux;
}
}

int F91(int vect, int index, int value_vset)
{{ if ((vect >= C22) && (vect <= (C17 - 2)) && ((unsigned int)_SH[vect] == C28))
{if ((index >= 1) && (index <= _SH[(vect + 1)]))
{{ _SH[(vect + index + 1)]= value_vset;
 return value_vset;
}
}
else {F233(vect, index);}}
else {{ printf("\n***** EXIT: ---NOT a vect---\n");
 fflush(stdout);
 exit(0);
}
}
 return 0;
}
}

int F92(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, nil))))))))))))))))))))))))));}

int F93(int var, int exp_by, int exp_while)
{{int rv_aux = nil; {int test_aux_var; {int and_result = nil; { and_result= exp_by;
 if (and_result != nil)
{{ and_result= exp_while;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= _c(66420, _c(_c(exp_while, _c(_c(66414, _c(_c(66409, _c(var, _c(exp_by, nil))), nil)), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(nil, nil))), nil)), nil)));}
else {if (exp_while != nil)
{rv_aux= _c(66460, _c(exp_while, _c(_c(66409, _c(var, _c(nil, nil))), nil)));}
else {if (exp_by != nil)
{rv_aux= _c(66409, _c(var, _c(exp_by, nil)));}
else {rv_aux= nil;}}}
}

 return rv_aux;
}
}

int F94(int regex, int start_node, int end_node)
{{int rv_aux = nil; {int cur_start = nil; int cur_end = nil; cur_start= start_node;
 {int el = nil; int el_on = nil; { el_on= _SH[(regex + 1)];
 el= _SH[el_on];
}

 { while (el_on != nil){ if (_SH[(el_on + 1)] != nil)
{cur_end= _c(66623, nil);}
else {cur_end= end_node;}
 F203(el, cur_start, cur_end);
 cur_start= cur_end;
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F95(int num)
{return F755(_c(_SH[65551], _c(F564(F37(num, 16, nil), 4, 48, 66444), nil)));}

int F96(int subdirs)
{{int rv_aux = nil; {int new_subdirs = nil; {int subdir = nil; int subdir_on = nil; { subdir_on= subdirs;
 subdir= _SH[subdir_on];
}

 while (subdir_on != nil){ if (F343(subdir, _SH[65590], 66443) != nil)
;
else {if (F343(subdir, _SH[65591], 66443) != nil)
{{ _SH[new_subdirs];
 new_subdirs= _SH[(new_subdirs + 1)];
}
}
else {new_subdirs= _c(subdir, new_subdirs);}}
 { subdir_on= _SH[(subdir_on + 1)];
 subdir= _SH[subdir_on];
}

}

}

 rv_aux= F574(new_subdirs);
}

 return rv_aux;
}
}

int F97(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 aux_stream= F499(aux_stream, V37);
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__5; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {{ if (!(F20(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {_ARG1__5= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__5);
}

 while (test_aux_var){ {int value = F283(aux_stream); aux_stream= _SH[(aux_stream + 1)];
 csym= value;
}

 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__5; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {{ if (!(F20(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {_ARG1__5= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__5);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F98(int str)
{return F664(66734, _SH[66777], _c(str, nil));}

int F99(int label, int start_node, int end_node)
{return F45(_c(66636, _c(_c(66632, _c(label, nil)), _c(_c(66623, _c(end_node, nil)), nil))), start_node);}

int F100(int node)
{return F256(F750(node, 66567), 66625);}

int F101(int list, int sl)
{{int rv_aux = nil; { _SH[(_SH[(_SH[(list + 1)] + 1)] + 1)]= sl;
 rv_aux= sl;
}

 return rv_aux;
}
}

int F102(int stream, int64_t F60)
{{int rv_aux = nil; {int64_t min_limit; int64_t max_limit; int flagP = nil; {int64_t min = V15[stream]; int64_t max = V16[stream]; int64_t end = V17[stream]; if (0 >= (min - 1))
{min_limit= 0;}
else {min_limit= (min - 1);}
 if ((end + 1) <= (max + 1))
{max_limit= (end + 1);}
else {max_limit= (max + 1);}
 if (F60 < min_limit)
{F60= min_limit;}
else {if (F60 > max_limit)
{F60= max_limit;}
else {flagP= 66407;}}
 V18[stream]= F60;
 { V21[stream]= C1;
 V23[stream]= (- C1);
}

 V24[stream]= (- C1);
}

 rv_aux= flagP;
}

 return rv_aux;
}
}

int F103(int stack)
{{int rv_aux = nil; if (F2(stack, 1) == 2)
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F104(int x, int y)
{{int rv_aux = nil; if (x == y)
{rv_aux= _SH[66407];}
else {{int test_aux_var; {int and_result = nil; { and_result= F256(x, nil);
 if (and_result != nil)
{{ and_result= F256(y, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= 66407;}
else {{int test_aux_var; {int and_result = nil; { and_result= F256(x, nil);
 if (and_result != nil)
{{ and_result= F256(y, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= 66407;}
else {if (F556(x) != nil)
{if (F556(y) != nil)
{{int flag = nil; int x1 = nil; int y1 = nil; while (!(flag != nil)){ {int test_aux_var; {int and_result = nil; { and_result= x;
 if (and_result != nil)
{{ and_result= y;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{ { x1= _SH[x];
 x= _SH[(x + 1)];
}

 { y1= _SH[y];
 y= _SH[(y + 1)];
}

 if (!(F104(x1, y1) != nil))
{flag= 66423;}
}
}
else {{int test_aux_var; {int and_result = nil; { if (!(x != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(y != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{flag= 66407;}
else {flag= 66423;}
}
}
}

}

 if (flag == 66407)
{rv_aux= 66407;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}}
else {rv_aux= nil;}}
}
}
}
}
 return rv_aux;
}
}

int F105(int str)
{return F664(66734, _SH[66780], _c(str, nil));}

int F106(int stream, int terminator)
{return F366(stream, terminator);}

int F107(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, nil))))))))))))))))))))))));}

int F108(int str1, int str2)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(F63(str1, str2), 66454);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(F63(str1, str2), 66452);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F109(int str)
{return F664(66734, _SH[66784], _c(str, nil));}

int F110(int object, int stream, int delta, int levels, int F510)
{{int rv_aux = nil; { if (levels == nil)
{levels= C2;}
 if (F510 == nil)
{F510= C2;}
 rv_aux= F291(object, stream, delta, levels, F510);
}

 return rv_aux;
}
}

int F111(int str)
{return F179(F7(str), 16);}

int F112(int norm_regex)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int var = nil; int var_on = nil; { var_on= F378(norm_regex);
 var= _SH[var_on];
}

 while (var_on != nil){ for_list_result= _c(_c(66409, _c(var, _c(_c(66614, _c(66613, _c(_c(66413, _c(var, nil)), nil))), nil))), for_list_result);
 { var_on= _SH[(var_on + 1)];
 var= _SH[var_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F113(int stream, int trim_spaceP)
{{int rv_aux = nil; {int element = nil; if (trim_spaceP != nil)
{{int64_t slice_start; int64_t slice_end; int slice_result = nil; slice_start= F237(stream);
 { F459(stream, 60);
 F23(stream, 66834);
}

 slice_end= F237(stream);
 if (slice_end > F418(stream))
{slice_end= F418(stream);}
 {int64_t start = slice_start; int64_t end = slice_end; if (start > end)
;
else {if (V36 >= C16)
{_SH[66422]= _SH[65758];}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { { element= F433(stream, _SH[66407], nil);
 slice_result= element;
}

 stream_slice_rv= slice_result;
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
}

}
}}
}

 F102(stream, (slice_end + 1));
}
}
else {{int64_t slice_start; int64_t slice_end; int slice_result = nil; slice_start= F237(stream);
 F459(stream, 60);
 slice_end= F237(stream);
 if (slice_end > F418(stream))
{slice_end= F418(stream);}
 {int64_t start = slice_start; int64_t end = slice_end; if (start > end)
;
else {if (V36 >= C16)
{_SH[66422]= _SH[65759];}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { { element= F433(stream, _SH[66407], nil);
 slice_result= element;
}

 stream_slice_rv= slice_result;
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
}

}
}}
}

 F102(stream, (slice_end + 1));
}
}
 rv_aux= F421(element);
}

 return rv_aux;
}
}

int F114(int sock_index)
{{int rv_aux = nil; {int end = nil; int counter = nil; int size = nil; V59[sock_index]= 0;
 counter= 0;
 { while (!(end != nil)){ size= read(V57[sock_index], V58[sock_index], C31);
 if (size > 0)
{{ end= 66407;
 V60[sock_index]= size;
}
}
else {if (size == 0)
{{ end= 66407;
 V60[sock_index]= 0;
}
}
else {if (counter < 1000)
{{ counter= (counter + 1);
 usleep(10000);
}
}
else {{ end= 66407;
 V60[sock_index]= -1;
}
}}}
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F115(int stream)
{return F353(stream, _SH[65743], nil);}

int F116(int expr)
{{int rv_aux = nil; {int and_result = nil; { and_result= F556(expr);
 if (and_result != nil)
{{ {int or_result = nil; { or_result= F256(_SH[expr], 66412);
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= F256(_SH[expr], 66413);
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}

}

 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F117(int iter_spec)
{{int rv_aux = nil; {int loc_vars = nil; int set_instrs = nil; {int f_38v = nil; int rec = nil; int aux = nil; aux= iter_spec;
 f_38v= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 rec= _SH[aux];
 if (F556(f_38v) != nil)
{{int field = nil; int value = nil; int aux = nil; aux= f_38v;
 field= _SH[aux];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 if (!(F245(field) != nil))
{{ loc_vars= _c(field, loc_vars);
 set_instrs= _c(_c(66409, _c(field, _c(_c(66442, _c(_c(66442, _c(66524, nil)), nil)), nil))), set_instrs);
}
}
 if (!(F245(value) != nil))
{{ loc_vars= _c(value, loc_vars);
 set_instrs= _c(_c(66409, _c(value, _c(_c(66525, _c(_c(66442, _c(66524, nil)), nil)), nil))), set_instrs);
}
}
 loc_vars= _c(66524, loc_vars);
}
}
else {{ loc_vars= _c(f_38v, loc_vars);
 loc_vars= _c(66524, loc_vars);
 set_instrs= _c(_c(66409, _c(f_38v, _c(_c(66442, _c(66524, nil)), nil))), set_instrs);
}
}
 rv_aux= _c(66481, _c(_c(66476, _c(loc_vars, nil)), _c(_c(66477, _c(_c(66405, _c(_c(66409, _c(66524, _c(_c(66441, _c(rec, nil)), nil))), _c(_c(66430, _c(66524, F498(set_instrs, nil))), nil))), nil)), _c(_c(66461, _c(_c(66485, _c(66524, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66405, _c(_c(66409, _c(66524, _c(_c(66441, _c(66524, nil)), nil))), _c(_c(66430, _c(66524, F498(F289(set_instrs), nil))), nil))), nil)), nil)))));
}

}

 return rv_aux;
}
}

int F118(int lp)
{if (access(F555(lp, nil), F_OK) == 0)
{return C24;}
else {return nil;}}

int F119(int pattern)
{{int rv_aux = nil; {int var = nil; int var_list = nil; int rest_counter = 0; int pattern_buf = nil; pattern_buf= pattern;
 while (pattern != nil){ { var= _SH[pattern];
 pattern= _SH[(pattern + 1)];
}

 if (var == 66433)
{{ rest_counter= (rest_counter + 1);
 { var= _SH[pattern];
 pattern= _SH[(pattern + 1)];
}

 var_list= _c(var, var_list);
}
}
else {if (var == 66434)
;
else {if (var == nil)
;
else {if (F88(var, 66407) != nil)
;
else {if (var != nil)
{{ if (F556(var) != nil)
{var= _SH[var];}
 var_list= _c(var, var_list);
}
}}}}}
}

 if (!(rest_counter <= 1))
{{ { F87(_SH[66435]);
 F373(nil);
}

 F110(pattern_buf, _SH[66416], 0, _SH[66436], _SH[66437]);
 { { F87(_SH[66435]);
 F87(_SH[66435]);
 F87(_SH[65545]);
 F87(_SH[66435]);
 F373(nil);
}

}

}
}
 rv_aux= F574(var_list);
}

 return rv_aux;
}
}

int F120()
{{int rv_aux = nil; { _SH[66823]= _SH[65706];
 rv_aux= F677(_SH[65707]);
}

 return rv_aux;
}
}

int F121(int lp)
{return remove(F555(lp, nil));}

int F122(int stream, int F443, int row_n)
{{int rv_aux = nil; {int separator = nil; int terminator = nil; int row_i = 0; int row = nil; int table = nil; if (F443 != nil)
{{ separator= F750(F443, 66798);
 terminator= F750(F443, 66799);
}
}
else {{ separator= 9;
 terminator= 10;
}
}
 {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (row_i < row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ row_i= (row_i + 1);
 if (!(F443 != nil))
{row= F268(stream, 9, 10, nil);}
else {{int test_aux_var; {int and_result = nil; { and_result= F750(F443, 66808);
 if (and_result != nil)
{{ if (row_i == 1)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{row= F268(stream, 9, 10, nil);}
else {row= F268(stream, separator, terminator, F443);}
}
}
 table= _c(row, table);
 {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (row_i < row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= F574(table);
}

 return rv_aux;
}
}

int F123(int object_index)
{if ((object_index >= 0) && (object_index <= (C21 - 1)))
{return C24;}
else {return nil;}}

int F124(int list_len_var, int index_vars)
{{int rv_aux = nil; {int index_var = nil; int set_list = nil; while (index_vars != nil){ { index_var= _SH[index_vars];
 index_vars= _SH[(index_vars + 1)];
}

 set_list= _c(_c(66405, _c(_c(66430, _c(_c(66431, _c(index_var, _c(0, nil))), _c(_c(66409, _c(index_var, _c(_c(66432, _c(list_len_var, _c(index_var, _c(1, nil)))), nil))), nil))), nil)), set_list);
}

 rv_aux= F574(set_list);
}

 return rv_aux;
}
}

int F125(int list, int F60)
{{int rv_aux = nil; {int F125 = nil; {int list_len_var = nil; list_len_var= F510(list);
 if (F60 < 0)
{F60= (list_len_var + F60 + 1);}
}

 if (F60 != nil)
{{ while (list != nil){ F60= (F60 - 1);
 if (F60 == 0)
{{ F125= list;
 list= nil;
}
}
else {list= _SH[(list + 1)];}
}

 rv_aux= F125;
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F126(int iter_spec)
{{int rv_aux = nil; {int on_var = nil; {int var = nil; int value = nil; int aux = nil; aux= iter_spec;
 var= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 on_var= F402(var);
 rv_aux= _c(66481, _c(_c(66476, _c(_c(var, _c(on_var, nil)), nil)), _c(_c(66477, _c(_c(66405, _c(_c(66409, _c(on_var, _c(value, nil))), _c(_c(66409, _c(var, _c(_c(66442, _c(on_var, nil)), nil))), nil))), nil)), _c(_c(66461, _c(_c(66485, _c(on_var, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66405, _c(_c(66409, _c(on_var, _c(_c(66441, _c(on_var, nil)), nil))), _c(_c(66409, _c(var, _c(_c(66442, _c(on_var, nil)), nil))), nil))), nil)), nil)))));
}

}

 return rv_aux;
}
}

int F127(int csym, int stream_index)
{return fputc(csym, F579(stream_index));}

int F128(int csym, int stream)
{return fputc(csym, stderr);}

int F129(int stream, int condition, int include_empty_itemsP)
{{int rv_aux = nil; {int el = nil; int el_list = nil; while (F243(stream, nil)){ el= F624(stream, condition, nil);
 {int test_aux_var; {int or_result = nil; { or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= el;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{el_list= _c(el, el_list);}
}

 F366(stream, condition);
}

 rv_aux= F574(el_list);
}

 return rv_aux;
}
}

int F130(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; { F663(stream, 1);
 rv_aux= _c(66413, _c(F440(stream, sc_comment_flag, hm_comment_flag), nil));
}

 return rv_aux;
}
}

int spai_find_cookie_consensus_from_file(char* html_file, char* agreeString)
{{int rv_aux = nil; {int html_file1 = nil; int agree_line = nil; int utf8_agree_line = nil; int i = 0; int max_agree_size = 1024; int size = 0; int buf_sr = nil; F202();
 html_file1= F450(html_file);
 if (F511(html_file1) >= 4194304)
{return -1;}
else {{ agree_line= F184(html_file1);
 {int out_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{out_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= 66612;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = 66583; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(utf8_agree_line != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= utf8_agree_line;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 out_stream= STREAM__BUF_935;
}
}
}

 unwind_value= out_stream;
}

 if (out_stream == -1)
;
else {{ {int stream_rv__51; {int buf_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{buf_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= 66612;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = 66583; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(buf_sr != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= buf_sr;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 buf_stream= STREAM__BUF_935;
}
}
}

 unwind_value= buf_stream;
}

 if (buf_stream == -1)
;
else {{ {int csym = nil; int csym_on = nil; { csym_on= agree_line;
 csym= _SH[csym_on];
}

 { {int test_aux_var; {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (size < (max_agree_size - 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ F102(buf_stream, 1);
 F360(csym, buf_stream);
 size= (size + F237(buf_stream));
 if (size <= (max_agree_size - 1))
{F360(csym, out_stream);}
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

 {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (size < (max_agree_size - 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 stream_rv= nil;
}

}

 if (!(buf_stream == -1))
{{ F143(buf_stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(66583, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(66583, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ buf_sr= _SH[(V8[buf_stream] + 1)];
 F417(V8[buf_stream]);
}
}
}

 F405(buf_stream);
}
}
}
}
}

 stream_rv__51= stream_rv;
}

 stream_rv= stream_rv__51;
}

 if (!(out_stream == -1))
{{ F143(out_stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(66583, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(66583, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ utf8_agree_line= _SH[(V8[out_stream] + 1)];
 F417(V8[out_stream]);
}
}
}

 F405(out_stream);
}
}
}
}
}

}

 i= 0;
 {int csym = nil; int csym_on = nil; { csym_on= utf8_agree_line;
 csym= _SH[csym_on];
}

 while (csym_on != nil){ agreeString[i]= csym;
 i= (i + 1);
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

}

 agreeString[i]= 0;
 return i;
}
}
}

 return rv_aux;
}
}

int F132(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F221(csym);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F664(66443, _SH[65549], _c(csym, nil));
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

void sharkInitSyms()
{{ V45= _c(C10, nil);
 F135(F492(124, 110, 117, 108, 124));
 F135(F492(124, 115, 111, 104, 124));
 F135(F492(124, 115, 116, 120, 124));
 F135(F492(124, 101, 116, 120, 124));
 F135(F492(124, 101, 111, 116, 124));
 F135(F492(124, 101, 110, 113, 124));
 F135(F492(124, 97, 99, 107, 124));
 F135(F492(124, 98, 101, 108, 124));
 F135(F50(124, 98, 115, 124));
 F135(F492(124, 116, 97, 98, 124));
 F135(F50(124, 108, 102, 124));
 F135(F50(124, 118, 116, 124));
 F135(F50(124, 110, 112, 124));
 F135(F50(124, 99, 114, 124));
 F135(F50(124, 115, 111, 124));
 F135(F50(124, 115, 105, 124));
 F135(F492(124, 100, 108, 101, 124));
 F135(F492(124, 100, 99, 49, 124));
 F135(F492(124, 100, 99, 50, 124));
 F135(F492(124, 100, 99, 51, 124));
 F135(F492(124, 100, 99, 52, 124));
 F135(F492(124, 110, 97, 107, 124));
 F135(F492(124, 115, 121, 110, 124));
 F135(F492(124, 101, 116, 98, 124));
 F135(F492(124, 99, 97, 110, 124));
 F135(F50(124, 101, 109, 124));
 F135(F492(124, 115, 117, 98, 124));
 F135(F492(124, 101, 115, 99, 124));
 F135(F50(124, 102, 115, 124));
 F135(F50(124, 103, 115, 124));
 F135(F50(124, 114, 115, 124));
 F135(F50(124, 117, 115, 124));
 F135(F50(124, 115, 112, 124));
 F135(F84(124, 33, 124));
 F135(F50(124, 100, 113, 124));
 F135(F50(124, 104, 109, 124));
 F135(F84(124, 36, 124));
 F135(F84(124, 37, 124));
 F135(F84(124, 38, 124));
 F135(F84(124, 39, 124));
 F135(F84(124, 40, 124));
 F135(F84(124, 41, 124));
 F135(F84(124, 42, 124));
 F135(F84(124, 43, 124));
 F135(F84(124, 44, 124));
 F135(F84(124, 45, 124));
 F135(F84(124, 46, 124));
 F135(F84(124, 47, 124));
 F135(F84(124, 48, 124));
 F135(F84(124, 49, 124));
 F135(F84(124, 50, 124));
 F135(F84(124, 51, 124));
 F135(F84(124, 52, 124));
 F135(F84(124, 53, 124));
 F135(F84(124, 54, 124));
 F135(F84(124, 55, 124));
 F135(F84(124, 56, 124));
 F135(F84(124, 57, 124));
 F135(F84(124, 58, 124));
 F135(F84(124, 59, 124));
 F135(F84(124, 60, 124));
 F135(F84(124, 61, 124));
 F135(F84(124, 62, 124));
 F135(F84(124, 63, 124));
 F135(F84(124, 64, 124));
 F135(F84(124, 65, 124));
 F135(F84(124, 66, 124));
 F135(F84(124, 67, 124));
 F135(F84(124, 68, 124));
 F135(F84(124, 69, 124));
 F135(F84(124, 70, 124));
 F135(F84(124, 71, 124));
 F135(F84(124, 72, 124));
 F135(F84(124, 73, 124));
 F135(F84(124, 74, 124));
 F135(F84(124, 75, 124));
 F135(F84(124, 76, 124));
 F135(F84(124, 77, 124));
 F135(F84(124, 78, 124));
 F135(F84(124, 79, 124));
 F135(F84(124, 80, 124));
 F135(F84(124, 81, 124));
 F135(F84(124, 82, 124));
 F135(F84(124, 83, 124));
 F135(F84(124, 84, 124));
 F135(F84(124, 85, 124));
 F135(F84(124, 86, 124));
 F135(F84(124, 87, 124));
 F135(F84(124, 88, 124));
 F135(F84(124, 89, 124));
 F135(F84(124, 90, 124));
 F135(F84(124, 91, 124));
 F135(F492(124, 98, 115, 104, 124));
 F135(F84(124, 93, 124));
 F135(F84(124, 94, 124));
 F135(F84(124, 95, 124));
 F135(F84(124, 96, 124));
 F135(F84(124, 97, 124));
 F135(F84(124, 98, 124));
 F135(F84(124, 99, 124));
 F135(F84(124, 100, 124));
 F135(F84(124, 101, 124));
 F135(F84(124, 102, 124));
 F135(F84(124, 103, 124));
 F135(F84(124, 104, 124));
 F135(F84(124, 105, 124));
 F135(F84(124, 106, 124));
 F135(F84(124, 107, 124));
 F135(F84(124, 108, 124));
 F135(F84(124, 109, 124));
 F135(F84(124, 110, 124));
 F135(F84(124, 111, 124));
 F135(F84(124, 112, 124));
 F135(F84(124, 113, 124));
 F135(F84(124, 114, 124));
 F135(F84(124, 115, 124));
 F135(F84(124, 116, 124));
 F135(F84(124, 117, 124));
 F135(F84(124, 118, 124));
 F135(F84(124, 119, 124));
 F135(F84(124, 120, 124));
 F135(F84(124, 121, 124));
 F135(F84(124, 122, 124));
 F135(F84(124, 123, 124));
 F135(F50(124, 118, 98, 124));
 F135(F84(124, 125, 124));
 F135(F84(124, 126, 124));
 F135(F492(124, 100, 101, 108, 124));
 F258(65408);
 F258(236);
 F135(F595(104, 116, 109, 108, 45, 109, 97, 107, 101, 45, 116, 111, 112, 45, 116, 97, 103));
 F135(F590(104, 116, 109, 108, 45, 109, 117, 108, 116, 105, 112, 108, 101, 45, 116, 111, 112, 45, 116, 97, 103, 115, 63));
 F135(F595(104, 116, 109, 108, 45, 99, 117, 114, 45, 116, 97, 103, 45, 98, 111, 100, 121));
 F135(F595(104, 116, 109, 108, 45, 99, 117, 114, 45, 116, 97, 103, 45, 110, 97, 109, 101));
 F135(F657(104, 116, 109, 108, 45, 117, 116, 105, 108, 45, 105, 110, 105, 116));
 F135(F460(105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 116, 114, 105, 101));
 F135(F460(105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 105, 110, 105, 116));
 F135(F460(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 116, 114, 105, 101));
 F135(F460(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 105, 110, 105, 116));
 F135(F545(116, 99, 112, 73, 110, 105, 116));
 F135(F657(102, 105, 108, 101, 83, 116, 114, 101, 97, 109, 73, 110, 105, 116));
 F135(F484(115, 104, 97, 114, 107, 83, 101, 116, 83, 116, 97, 116, 105, 99, 72, 101, 97, 112));
 F135(F505(115, 104, 97, 114, 107, 83, 101, 116, 84, 101, 109, 112, 72, 101, 97, 112));
 F135(F595(115, 104, 97, 114, 107, 73, 110, 105, 116, 84, 101, 109, 112, 72, 101, 97, 112));
 F135(F107(115, 104, 97, 114, 107, 68, 101, 108, 108, 111, 99, 97, 116, 101, 83, 116, 97, 116, 105, 99, 72, 101, 97, 112));
 F135(F590(115, 104, 97, 114, 107, 65, 108, 108, 111, 99, 97, 116, 101, 83, 116, 97, 116, 105, 99, 72, 101, 97, 112));
 F135(F590(115, 104, 97, 114, 107, 68, 101, 65, 108, 108, 111, 99, 97, 116, 101, 84, 101, 109, 112, 72, 101, 97, 112));
 F135(F601(115, 104, 97, 114, 107, 65, 108, 108, 111, 99, 97, 116, 101, 84, 101, 109, 112, 72, 101, 97, 112));
 F135(F545(105, 111, 45, 105, 110, 105, 116));
 F135(F708(116, 114, 105, 101, 45, 105, 110, 105, 116));
 F135(F505(115, 111, 114, 116, 38, 115, 101, 97, 114, 99, 104, 45, 105, 110, 105, 116));
 F135(F708(108, 105, 115, 116, 45, 105, 110, 105, 116));
 F135(F589(115, 112, 97, 105, 45, 108, 105, 110, 101, 115, 45, 108, 97, 110, 103));
 F135(F224(115, 112, 97, 105, 45, 101, 120, 116, 114, 97, 99, 116, 45, 99, 111, 111, 107, 105, 101, 45, 108, 105, 110, 101, 115));
 F135(F92(115, 112, 97, 105, 45, 102, 105, 110, 100, 45, 99, 111, 111, 107, 105, 101, 45, 99, 111, 110, 115, 101, 110, 115, 117, 115));
 F135(_c(115, _c(112, _c(97, _c(105, _c(45, _c(102, F215(105, 110, 100, 45, 99, 111, 111, 107, 105, 101, 45, 99, 111, 110, 115, 101, 110, 115, 117, 115, 45, 102, 114, 111, 109, 45, 102, 105, 108, 101, 45, 49))))))));
 F135(F601(117, 116, 102, 56, 45, 115, 116, 114, 101, 97, 109, 45, 102, 108, 117, 115, 104, 45, 98, 117, 102));
 F135(F590(117, 116, 102, 56, 45, 99, 111, 110, 116, 105, 110, 117, 97, 116, 105, 111, 110, 45, 99, 115, 121, 109, 63));
 F135(F455(101, 120, 97, 45, 115, 121, 109, 45, 62, 105, 110, 116));
 F135(F752(117, 116, 102, 56, 45, 116, 121, 105));
 F135(F504(104, 116, 109, 108, 45, 115, 116, 114, 45, 62, 116, 97, 103));
 F135(F601(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 116, 97, 103, 45, 97, 116, 116, 108, 105, 115, 116));
 F135(F657(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 105, 110, 105, 116));
 F135(F595(104, 116, 109, 108, 45, 102, 105, 110, 100, 45, 116, 97, 103, 45, 112, 111, 115));
 F135(F455(104, 116, 109, 108, 45, 112, 111, 112, 45, 116, 97, 103));
 F135(F460(104, 116, 109, 108, 45, 112, 117, 115, 104, 45, 108, 45, 116, 97, 103, 45, 98, 111, 100, 121));
 F135(F484(104, 116, 109, 108, 45, 112, 117, 115, 104, 45, 116, 97, 103, 45, 98, 111, 100, 121));
 F135(F504(104, 116, 109, 108, 45, 112, 117, 115, 104, 45, 116, 97, 103));
 F135(F589(104, 116, 109, 108, 45, 99, 108, 111, 115, 101, 45, 116, 97, 103, 63));
 F135(F657(104, 116, 109, 108, 45, 111, 112, 101, 110, 45, 116, 97, 103, 63));
 F135(F460(104, 116, 109, 108, 45, 115, 107, 105, 112, 45, 115, 116, 121, 108, 101, 45, 98, 111, 100, 121));
 F135(F590(104, 116, 109, 108, 45, 115, 107, 105, 112, 45, 110, 111, 115, 99, 114, 105, 112, 116, 45, 98, 111, 100, 121));
 F135(F601(104, 116, 109, 108, 45, 115, 107, 105, 112, 45, 115, 99, 114, 105, 112, 116, 45, 98, 111, 100, 121));
 F135(F455(104, 116, 109, 108, 45, 115, 107, 105, 112, 45, 112, 105));
 F135(F455(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 112, 105));
 F135(F752(104, 116, 109, 108, 45, 112, 105, 63));
 F135(F107(104, 116, 109, 108, 45, 115, 116, 114, 101, 97, 109, 45, 115, 107, 105, 112, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F595(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F504(104, 116, 109, 108, 45, 99, 111, 109, 109, 101, 110, 116, 63));
 F135(F589(104, 116, 109, 108, 45, 115, 107, 105, 112, 45, 99, 100, 97, 116, 97));
 F135(F589(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 99, 100, 97, 116, 97));
 F135(F615(104, 116, 109, 108, 45, 99, 100, 97, 116, 97, 63));
 F135(F595(104, 116, 109, 108, 45, 101, 120, 116, 114, 97, 99, 116, 45, 116, 101, 120, 116));
 F135(F460(104, 116, 109, 108, 45, 110, 111, 116, 45, 101, 109, 112, 108, 121, 45, 108, 105, 110, 101, 63));
 F135(F505(104, 116, 109, 108, 45, 105, 110, 108, 105, 110, 101, 45, 116, 97, 103, 63));
 F135(F460(120, 109, 108, 45, 115, 116, 114, 101, 97, 109, 45, 102, 108, 117, 115, 104, 45, 98, 117, 102));
 F135(F545(120, 109, 108, 45, 116, 121, 105));
 F135(F595(104, 116, 109, 108, 45, 109, 97, 107, 101, 45, 97, 116, 116, 108, 105, 115, 116));
 F135(F504(104, 116, 109, 108, 45, 97, 116, 116, 108, 105, 115, 116, 63));
 F135(F505(104, 116, 109, 108, 45, 113, 117, 111, 116, 101, 45, 99, 115, 121, 109, 63));
 F135(F708(104, 116, 109, 108, 45, 115, 116, 114, 63));
 F135(F505(104, 116, 109, 108, 45, 115, 105, 110, 103, 108, 101, 45, 116, 97, 103, 63));
 F135(F708(116, 114, 101, 99, 45, 106, 111, 105, 110));
 F135(F550(116, 114, 101, 99, 45, 115, 112, 108, 105, 116));
 F135(F615(114, 101, 99, 115, 45, 62, 116, 97, 98, 108, 101));
 F135(F505(116, 114, 115, 112, 101, 99, 45, 99, 111, 109, 112, 108, 101, 116, 101, 33));
 F135(F550(116, 114, 115, 112, 101, 99, 45, 100, 101, 102));
 F135(F484(116, 117, 45, 102, 105, 110, 100, 45, 116, 101, 114, 109, 115, 45, 108, 97, 110, 103));
 F135(F224(105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 117, 120, 45, 116, 101, 114, 109, 63));
 F135(F215(105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 117, 120, 105, 108, 105, 97, 114, 121, 45, 118, 101, 114, 98, 115, 63));
 F135(_c(105, _c(116, _c(97, _c(108, _c(105, _c(97, _c(110, _c(45, F215(103, 114, 97, 109, 109, 97, 114, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 63))))))))));
 F135(_c(105, _c(116, _c(97, F215(108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 105, 110, 100, 101, 102, 105, 110, 105, 116, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63)))));
 F135(_c(105, _c(116, _c(97, _c(108, _c(105, _c(97, F215(110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63))))))));
 F135(_c(105, _c(116, F215(97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63))));
 F135(_c(105, F215(116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 114, 101, 108, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63)));
 F135(_c(105, F215(116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 112, 101, 114, 115, 111, 110, 97, 108, 45, 112, 114, 111, 110, 111, 117, 110, 63)));
 F135(F467(105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 99, 111, 110, 106, 117, 110, 99, 116, 105, 111, 110, 63));
 F135(_c(105, F215(116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 114, 116, 105, 99, 108, 101, 100, 45, 97, 114, 116, 105, 99, 108, 101, 63)));
 F135(_c(105, _c(116, _c(97, _c(108, _c(105, F215(97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 114, 116, 105, 99, 108, 101, 100, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 63)))))));
 F135(_c(105, _c(116, _c(97, F215(108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 115, 105, 109, 112, 108, 101, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 63)))));
 F135(F224(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 117, 120, 45, 116, 101, 114, 109, 63));
 F135(F92(101, 110, 103, 108, 105, 115, 104, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63));
 F135(F342(101, 110, 103, 108, 105, 115, 104, 45, 105, 110, 100, 101, 102, 105, 110, 105, 116, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63));
 F135(F224(101, 110, 103, 108, 105, 115, 104, 45, 114, 101, 108, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 63));
 F135(_c(101, F215(110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 112, 101, 114, 115, 111, 110, 97, 108, 45, 112, 114, 111, 110, 111, 117, 110, 63)));
 F135(F467(101, 110, 103, 108, 105, 115, 104, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 63));
 F135(F215(101, 110, 103, 108, 105, 115, 104, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 63));
 F135(F604(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 117, 120, 105, 108, 105, 97, 114, 121, 45, 118, 101, 114, 98, 63));
 F135(F467(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 99, 111, 110, 106, 117, 110, 99, 116, 105, 111, 110, 63));
 F135(F107(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 97, 114, 116, 105, 99, 108, 101, 63));
 F135(F467(101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 63));
 F135(F657(116, 99, 112, 83, 116, 114, 101, 97, 109, 70, 108, 117, 115, 104));
 F135(F455(116, 99, 112, 83, 116, 114, 101, 97, 109, 84, 121, 105));
 F135(F657(116, 99, 112, 83, 116, 114, 101, 97, 109, 80, 101, 101, 107, 80));
 F135(F589(116, 99, 112, 45, 98, 117, 102, 45, 102, 105, 108, 108, 101, 100, 63));
 F135(F455(116, 99, 112, 45, 116, 105, 109, 101, 111, 117, 116, 63));
 F135(F589(116, 99, 112, 45, 102, 105, 108, 108, 45, 98, 117, 102, 102, 101, 114));
 F135(F589(116, 99, 112, 45, 104, 111, 115, 116, 45, 99, 104, 101, 99, 107, 63));
 F135(F752(108, 112, 68, 101, 108, 65, 117, 120));
 F135(F589(108, 112, 68, 105, 114, 83, 117, 98, 100, 105, 114, 115, 65, 117, 120));
 F135(F504(108, 112, 68, 105, 114, 70, 105, 108, 101, 115, 65, 117, 120));
 F135(F504(108, 112, 77, 97, 107, 101, 70, 105, 108, 101, 65, 117, 120));
 F135(F455(108, 112, 77, 97, 107, 101, 68, 105, 114, 65, 117, 120));
 F135(F708(108, 112, 68, 97, 116, 101, 65, 117, 120));
 F135(F550(108, 112, 67, 104, 101, 99, 107, 65, 117, 120));
 F135(F550(115, 121, 109, 70, 105, 110, 100, 65, 117, 120));
 F135(F708(115, 121, 109, 77, 97, 107, 101, 70, 110));
 F135(F752(115, 121, 109, 77, 97, 107, 101, 49));
 F135(F550(115, 121, 109, 77, 97, 107, 101, 65, 117, 120));
 F135(F492(118, 101, 99, 116, 63));
 F135(F455(112, 97, 114, 115, 101, 45, 115, 107, 105, 112, 98, 63));
 F135(F92(112, 97, 114, 115, 101, 45, 99, 115, 121, 109, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 99, 97, 115, 101, 115));
 F135(F589(112, 97, 114, 115, 101, 45, 108, 105, 110, 101, 45, 101, 110, 100, 63));
 F135(F595(112, 97, 114, 115, 101, 45, 108, 105, 110, 101, 45, 115, 116, 97, 114, 116, 63));
 F135(F550(112, 97, 114, 115, 101, 45, 101, 110, 100, 63));
 F135(F455(112, 97, 114, 115, 101, 45, 115, 116, 97, 114, 116, 63));
 F135(F550(112, 97, 114, 115, 101, 45, 109, 97, 120, 63));
 F135(F550(112, 97, 114, 115, 101, 45, 109, 105, 110, 63));
 F135(F224(112, 97, 114, 115, 101, 45, 112, 111, 115, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 99, 97, 115, 101, 115));
 F135(F224(112, 97, 114, 115, 101, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 102, 111, 114, 109, 45, 110, 97, 109, 101));
 F135(F595(112, 97, 114, 115, 101, 45, 116, 101, 115, 116, 45, 105, 110, 115, 116, 114, 63));
 F135(F595(112, 97, 114, 115, 101, 45, 99, 111, 110, 99, 45, 105, 110, 115, 116, 114, 63));
 F135(F595(112, 97, 114, 115, 101, 45, 99, 111, 112, 121, 45, 105, 110, 115, 116, 114, 63));
 F135(F595(112, 97, 114, 115, 101, 45, 109, 111, 118, 101, 45, 105, 110, 115, 116, 114, 63));
 F135(F224(112, 97, 114, 115, 101, 45, 101, 120, 116, 114, 97, 99, 116, 45, 114, 101, 45, 115, 101, 116, 45, 118, 97, 114, 115));
 F135(F605(112, 97, 114, 115, 101, 45, 114, 101, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 63));
 F135(F504(112, 97, 114, 115, 101, 45, 114, 101, 45, 118, 97, 114, 115));
 F135(F460(114, 101, 45, 101, 120, 116, 114, 97, 99, 116, 45, 118, 97, 114, 45, 110, 97, 109, 101, 115));
 F135(F455(114, 101, 45, 115, 101, 116, 45, 110, 111, 100, 101, 63));
 F135(F455(114, 101, 45, 101, 110, 100, 45, 110, 111, 100, 101, 63));
 F135(F504(114, 101, 45, 112, 117, 115, 104, 45, 112, 110, 111, 100, 101));
 F135(F545(114, 101, 45, 109, 97, 107, 101));
 F135(F589(114, 101, 45, 101, 120, 112, 97, 110, 100, 45, 99, 108, 97, 115, 115));
 F135(F455(114, 101, 45, 110, 111, 114, 109, 97, 108, 105, 122, 101));
 F135(F691(112, 97, 114, 115, 101, 45, 109, 97, 107, 101, 45, 115, 101, 116, 45, 114, 101, 45, 118, 97, 114, 115));
 F135(F550(110, 117, 109, 45, 97, 115, 45, 115, 116, 114));
 F135(F708(114, 101, 97, 100, 45, 97, 116, 111, 109));
 F135(F615(114, 101, 97, 100, 45, 115, 116, 114, 105, 110, 103));
 F135(F484(114, 101, 97, 100, 45, 99, 115, 121, 109, 45, 110, 111, 116, 97, 116, 105, 111, 110));
 F135(F589(114, 101, 97, 100, 45, 104, 109, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F589(115, 107, 105, 112, 45, 104, 109, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F589(114, 101, 97, 100, 45, 115, 99, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F589(115, 107, 105, 112, 45, 115, 99, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F505(115, 116, 114, 101, 97, 109, 45, 116, 99, 112, 45, 102, 108, 117, 115, 104));
 F135(F505(115, 116, 114, 101, 97, 109, 45, 115, 116, 114, 45, 112, 101, 101, 107, 63));
 F135(F595(115, 116, 114, 101, 97, 109, 45, 105, 110, 99, 45, 110, 98, 121, 116, 101, 115));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 115, 101, 116, 45, 109, 111, 100));
 F135(F605(115, 116, 114, 101, 97, 109, 45, 115, 101, 116, 45, 111, 117, 116, 45, 112, 97, 114, 115));
 F135(F589(115, 116, 114, 101, 97, 109, 45, 112, 111, 115, 62, 101, 110, 100, 63));
 F135(F484(115, 116, 114, 101, 97, 109, 45, 111, 117, 116, 45, 111, 102, 45, 98, 117, 102, 63));
 F135(F484(115, 116, 114, 101, 97, 109, 45, 99, 117, 114, 61, 111, 117, 116, 45, 109, 97, 120));
 F135(F484(115, 116, 114, 101, 97, 109, 45, 99, 117, 114, 60, 111, 117, 116, 45, 109, 97, 120));
 F135(F484(115, 116, 114, 101, 97, 109, 45, 105, 110, 115, 105, 100, 101, 45, 112, 111, 115, 63));
 F135(F455(115, 116, 114, 101, 97, 109, 45, 98, 117, 102, 48, 63));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 115, 101, 114, 105, 97, 108, 63));
 F135(F484(115, 116, 114, 101, 97, 109, 45, 119, 114, 105, 116, 101, 45, 109, 111, 100, 101, 63));
 F135(F505(115, 116, 114, 101, 97, 109, 45, 110, 101, 119, 45, 109, 111, 100, 101, 63));
 F135(F595(115, 116, 114, 101, 97, 109, 45, 114, 101, 97, 100, 45, 109, 111, 100, 101, 63));
 F135(F504(115, 116, 114, 101, 97, 109, 45, 116, 121, 111, 45, 102, 110));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 116, 121, 111, 45, 102, 110, 63));
 F135(F504(115, 116, 114, 101, 97, 109, 45, 116, 121, 105, 45, 102, 110));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 116, 121, 105, 45, 102, 110, 63));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 112, 101, 101, 107, 45, 102, 110));
 F135(F589(115, 116, 114, 101, 97, 109, 45, 112, 101, 101, 107, 45, 102, 110, 63));
 F135(F615(115, 116, 114, 101, 97, 109, 45, 111, 117, 116, 63));
 F135(F550(115, 116, 114, 101, 97, 109, 45, 105, 110, 63));
 F135(F595(115, 116, 114, 101, 97, 109, 45, 100, 101, 97, 108, 108, 111, 99, 97, 116, 101));
 F135(F589(115, 116, 114, 101, 97, 109, 45, 97, 108, 108, 111, 99, 97, 116, 101));
 F135(F708(108, 112, 45, 109, 97, 107, 101, 45, 114));
 F135(F615(108, 112, 45, 118, 101, 114, 115, 105, 111, 110, 49));
 F135(F107(108, 112, 45, 112, 97, 114, 115, 101, 45, 112, 111, 105, 110, 116, 45, 110, 111, 116, 97, 116, 105, 111, 110, 49));
 F135(F545(108, 112, 45, 101, 120, 112, 49));
 F135(F545(108, 112, 45, 106, 111, 105, 110));
 F135(F752(108, 112, 45, 115, 112, 108, 105, 116));
 F135(F545(108, 112, 45, 102, 105, 110, 100));
 F135(F545(108, 112, 45, 100, 97, 116, 101));
 F135(F147(108, 112, 45, 101, 120, 112));
 F135(F550(108, 112, 45, 118, 101, 114, 115, 105, 111, 110));
 F135(F545(108, 112, 45, 116, 121, 112, 101));
 F135(F147(108, 112, 45, 100, 105, 114));
 F135(F455(108, 112, 45, 102, 105, 108, 101, 110, 97, 109, 101, 63));
 F135(F545(108, 112, 45, 100, 105, 114, 63));
 F135(F708(108, 112, 45, 99, 104, 101, 99, 107, 63));
 F135(F657(108, 112, 45, 62, 109, 97, 99, 111, 115, 45, 112, 97, 116, 104));
 F135(F505(108, 112, 45, 62, 119, 105, 110, 100, 111, 119, 115, 45, 112, 97, 116, 104));
 F135(F657(108, 112, 45, 62, 112, 111, 115, 105, 120, 45, 112, 97, 116, 104));
 F135(F455(103, 101, 116, 45, 98, 114, 45, 118, 97, 108, 117, 101));
 F135(F550(118, 114, 101, 99, 45, 116, 121, 112, 101, 63));
 F135(F589(118, 114, 101, 99, 45, 116, 121, 112, 101, 45, 110, 97, 109, 101, 63));
 F135(F708(118, 114, 101, 99, 45, 115, 105, 122, 101));
 F135(F505(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 45, 115, 112, 101, 99, 115));
 F135(F589(118, 114, 101, 99, 45, 116, 121, 112, 101, 45, 62, 110, 97, 109, 101));
 F135(F224(118, 114, 101, 99, 45, 109, 97, 107, 101, 45, 102, 105, 101, 108, 100, 45, 115, 112, 101, 99, 45, 114, 101, 99, 115));
 F135(F589(118, 114, 101, 99, 45, 114, 109, 97, 107, 101, 45, 110, 97, 109, 101));
 F135(F484(118, 114, 101, 99, 45, 119, 105, 116, 104, 45, 114, 101, 99, 45, 110, 97, 109, 101));
 F135(F657(118, 114, 101, 99, 45, 114, 115, 101, 116, 45, 110, 97, 109, 101));
 F135(F657(118, 114, 101, 99, 45, 114, 103, 101, 116, 45, 110, 97, 109, 101));
 F135(F460(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 45, 118, 101, 99, 116, 45, 116, 121, 112, 101));
 F135(F605(118, 114, 101, 99, 45, 102, 108, 97, 103, 45, 118, 101, 99, 116, 45, 110, 97, 109, 101));
 F135(F605(118, 114, 101, 99, 45, 119, 105, 116, 104, 45, 114, 101, 99, 45, 109, 97, 99, 114, 111));
 F135(F589(118, 114, 101, 99, 45, 114, 115, 101, 116, 45, 109, 97, 99, 114, 111));
 F135(F589(118, 114, 101, 99, 45, 114, 103, 101, 116, 45, 109, 97, 99, 114, 111));
 F135(F708(116, 114, 105, 101, 45, 108, 101, 97, 102));
 F135(F550(104, 97, 115, 104, 45, 99, 108, 101, 97, 114));
 F135(F708(104, 97, 115, 104, 45, 105, 110, 105, 116));
 F135(F589(102, 111, 114, 45, 102, 110, 45, 105, 115, 112, 101, 99, 45, 102, 110));
 F135(F455(102, 111, 114, 45, 102, 110, 45, 105, 115, 112, 101, 99));
 F135(F595(102, 111, 114, 45, 105, 110, 45, 104, 97, 115, 104, 45, 105, 115, 112, 101, 99));
 F135(F595(102, 111, 114, 45, 105, 110, 45, 116, 114, 105, 101, 45, 105, 115, 112, 101, 99));
 F135(F460(102, 111, 114, 45, 101, 120, 116, 114, 97, 99, 116, 45, 116, 114, 105, 101, 45, 107, 101, 121));
 F135(F505(102, 111, 114, 45, 105, 110, 45, 114, 101, 99, 45, 105, 115, 112, 101, 99));
 F135(F605(102, 111, 114, 45, 105, 110, 45, 115, 116, 114, 101, 97, 109, 45, 105, 115, 112, 101, 99));
 F135(F504(102, 111, 114, 45, 105, 110, 100, 101, 120, 45, 118, 97, 114));
 F135(F595(102, 111, 114, 45, 105, 110, 45, 118, 101, 99, 116, 45, 105, 115, 112, 101, 99));
 F135(F484(102, 111, 114, 45, 115, 101, 116, 38, 116, 104, 101, 110, 45, 105, 115, 112, 101, 99));
 F135(F657(102, 111, 114, 45, 102, 114, 111, 109, 45, 105, 115, 112, 101, 99));
 F135(F605(102, 111, 114, 45, 105, 110, 45, 119, 105, 100, 116, 104, 65, 45, 105, 115, 112, 101, 99));
 F135(F504(102, 111, 114, 45, 108, 101, 118, 101, 108, 45, 118, 97, 114));
 F135(F484(102, 111, 114, 45, 105, 110, 45, 119, 105, 100, 116, 104, 45, 105, 115, 112, 101, 99));
 F135(F605(102, 111, 114, 45, 105, 110, 45, 100, 101, 112, 116, 104, 65, 45, 105, 115, 112, 101, 99));
 F135(F484(102, 111, 114, 45, 105, 110, 45, 100, 101, 112, 116, 104, 45, 105, 115, 112, 101, 99));
 F135(F595(102, 111, 114, 45, 97, 116, 111, 109, 105, 99, 45, 118, 97, 108, 117, 101, 63));
 F135(F455(102, 111, 114, 45, 111, 110, 45, 105, 115, 112, 101, 99));
 F135(F550(102, 111, 114, 45, 111, 110, 45, 118, 97, 114));
 F135(F455(102, 111, 114, 45, 105, 110, 45, 105, 115, 112, 101, 99));
 F135(F505(102, 111, 114, 45, 114, 101, 112, 101, 97, 116, 45, 105, 115, 112, 101, 99));
 F135(F589(102, 111, 114, 45, 117, 110, 116, 105, 108, 45, 105, 115, 112, 101, 99));
 F135(F589(102, 111, 114, 45, 119, 104, 105, 108, 101, 45, 105, 115, 112, 101, 99));
 F135(F505(102, 111, 114, 45, 117, 110, 108, 101, 115, 115, 45, 105, 115, 112, 101, 99));
 F135(F657(102, 111, 114, 45, 119, 104, 101, 110, 45, 105, 115, 112, 101, 99));
 F135(F484(102, 111, 114, 45, 109, 117, 108, 116, 105, 112, 108, 101, 45, 115, 112, 101, 99, 63));
 F135(F595(102, 111, 114, 45, 109, 97, 107, 101, 45, 115, 112, 101, 99, 45, 114, 101, 99));
 F135(F589(115, 116, 97, 99, 107, 45, 99, 117, 114, 45, 105, 110, 100, 101, 120));
 F135(F708(115, 116, 97, 99, 107, 45, 116, 111, 112));
 F135(F708(115, 116, 97, 99, 107, 45, 112, 111, 112));
 F135(F615(115, 116, 97, 99, 107, 45, 102, 117, 108, 108, 63));
 F135(F455(115, 116, 97, 99, 107, 45, 101, 109, 112, 116, 121, 63));
 F135(F550(115, 116, 97, 99, 107, 45, 109, 97, 107, 101));
 F135(F601(114, 101, 99, 45, 101, 120, 116, 114, 97, 99, 116, 45, 118, 97, 114, 45, 110, 97, 109, 101, 115));
 F135(F455(114, 109, 97, 107, 101, 45, 102, 105, 101, 108, 100, 115));
 F135(F545(115, 121, 109, 45, 114, 101, 99));
 F135(F708(115, 121, 109, 45, 118, 97, 108, 117, 101));
 F135(F752(115, 121, 109, 45, 110, 97, 109, 101));
 F135(F50(115, 121, 109, 63));
 F135(F505(117, 110, 105, 99, 111, 100, 101, 45, 105, 110, 116, 45, 62, 115, 116, 114));
 F135(F505(117, 110, 105, 99, 111, 100, 101, 45, 115, 116, 114, 45, 62, 105, 110, 116));
 F135(F504(117, 110, 105, 99, 111, 100, 101, 45, 110, 97, 109, 101, 63));
 F135(F455(105, 110, 118, 101, 114, 116, 45, 97, 108, 105, 103, 110));
 F135(F752(115, 116, 114, 45, 99, 97, 112, 33));
 F135(F545(115, 116, 114, 45, 99, 97, 112));
 F135(F545(115, 116, 114, 45, 117, 112, 33));
 F135(F147(115, 116, 114, 45, 117, 112));
 F135(F752(115, 116, 114, 45, 108, 111, 119, 33));
 F135(F752(115, 116, 114, 45, 110, 117, 109, 63));
 F135(F615(99, 115, 121, 109, 45, 112, 117, 110, 99, 116, 63));
 F135(F615(99, 115, 121, 109, 45, 103, 114, 97, 112, 104, 63));
 F135(F615(99, 115, 121, 109, 45, 99, 110, 116, 114, 108, 63));
 F135(F615(99, 115, 121, 109, 45, 112, 114, 105, 110, 116, 63));
 F135(F615(99, 115, 121, 109, 45, 98, 114, 101, 97, 107, 63));
 F135(F615(99, 115, 121, 109, 45, 98, 108, 97, 110, 107, 63));
 F135(F615(99, 115, 121, 109, 45, 117, 112, 112, 101, 114, 63));
 F135(F615(99, 115, 121, 109, 45, 108, 111, 119, 101, 114, 63));
 F135(F615(99, 115, 121, 109, 45, 97, 108, 110, 117, 109, 63));
 F135(F455(99, 115, 121, 109, 45, 120, 100, 105, 103, 105, 116, 63));
 F135(F615(99, 115, 121, 109, 45, 100, 105, 103, 105, 116, 63));
 F135(F615(99, 115, 121, 109, 45, 97, 108, 112, 104, 97, 63));
 F135(F595(99, 115, 121, 109, 45, 97, 108, 112, 104, 97, 63, 45, 112, 111, 115, 105, 120));
 F135(F545(99, 115, 121, 109, 45, 117, 112));
 F135(F752(99, 115, 121, 109, 45, 108, 111, 119));
 F135(F615(105, 110, 116, 45, 62, 120, 100, 105, 103, 105, 116));
 F135(F615(120, 100, 105, 103, 105, 116, 45, 62, 105, 110, 116));
 F135(F550(105, 110, 116, 45, 62, 100, 105, 103, 105, 116));
 F135(F550(100, 105, 103, 105, 116, 45, 62, 105, 110, 116));
 F135(F550(99, 115, 121, 109, 45, 110, 97, 109, 101, 63));
 F135(F492(99, 115, 121, 109, 63));
 F135(F492(110, 117, 108, 108, 63));
 F135(F492(108, 105, 115, 116, 63));
 F135(F84(114, 101, 118));
 F135(F492(100, 99, 111, 110, 115));
 F135(F545(99, 111, 110, 99, 45, 108, 33));
 F135(F147(99, 111, 110, 99, 45, 108));
 F135(F50(99, 111, 112, 121));
 F135(F50(103, 114, 45, 49));
 F135(F84(103, 114, 51));
 F135(F84(103, 114, 50));
 F135(F84(103, 45, 49));
 F135(F592(103, 51));
 F135(F504(108, 105, 115, 116, 45, 100, 115, 116, 45, 118, 97, 114, 115));
 F135(F492(109, 97, 120, 45, 114));
 F135(F492(109, 105, 110, 45, 114));
 F135(F484(116, 101, 109, 112, 45, 118, 97, 114, 115, 38, 105, 110, 105, 116, 115, 45, 102, 110));
 F135(F455(110, 117, 109, 45, 111, 114, 45, 115, 121, 109, 115, 63));
 F135(F147(102, 108, 111, 97, 116, 63));
 F135(F50(105, 110, 116, 63));
 F135(F50(110, 117, 109, 63));
 F135(F545(112, 103, 101, 116, 45, 102, 110));
 F135(F592(105, 100));
 F135(F147(111, 114, 45, 101, 120, 112));
 F135(F545(97, 110, 100, 45, 101, 120, 112));
 F135(F50(101, 120, 105, 116));
 F135(F107(119, 105, 116, 104, 71, 45, 114, 101, 115, 116, 111, 114, 101, 45, 111, 108, 100, 45, 118, 97, 108, 117, 101, 115));
 F135(F605(119, 105, 116, 104, 71, 45, 109, 97, 107, 101, 45, 110, 101, 119, 45, 105, 110, 105, 116));
 F135(F691(119, 105, 116, 104, 71, 45, 115, 116, 111, 114, 101, 45, 111, 108, 100, 45, 118, 97, 108, 117, 101, 115));
 F135(F657(119, 105, 116, 104, 71, 45, 118, 97, 114, 45, 99, 111, 112, 121));
 F135(F50(114, 101, 118, 33));
 F135(F492(115, 116, 114, 103, 63));
 F135(F492(97, 116, 111, 109, 63));
 F135(F752(110, 111, 116, 45, 110, 105, 108, 63));
 F135(F50(110, 105, 108, 63));
 F135(F455(98, 113, 117, 111, 116, 101, 45, 101, 120, 112, 114, 63));
 F135(F615(113, 117, 111, 116, 101, 100, 45, 115, 116, 114, 63));
 F135(F615(113, 117, 111, 116, 101, 45, 101, 120, 112, 114, 63));
 F135(F455(97, 117, 120, 45, 99, 111, 110, 100, 45, 62, 105, 102));
 F135(F589(97, 117, 120, 45, 108, 105, 115, 116, 42, 45, 62, 99, 111, 110, 115));
 F135(F657(97, 117, 120, 45, 108, 105, 115, 116, 45, 62, 99, 111, 110, 115));
 F135(F615(97, 117, 120, 45, 97, 100, 100, 45, 115, 101, 113));
 F135(F50(37, 95, 76, 49));
 F135(F589(115, 112, 97, 105, 45, 97, 103, 114, 101, 101, 45, 108, 105, 110, 101));
 F135(F455(117, 116, 102, 56, 45, 116, 121, 111, 45, 97, 117, 120));
 F135(F615(117, 116, 102, 56, 45, 100, 101, 99, 111, 100, 101));
 F135(F615(117, 116, 102, 56, 45, 101, 110, 99, 111, 100, 101));
 F135(F752(117, 116, 102, 56, 45, 116, 121, 111));
 F135(F484(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 116, 97, 103, 45, 110, 97, 109, 101));
 F135(F601(104, 116, 109, 108, 45, 101, 120, 116, 114, 97, 99, 116, 45, 109, 97, 105, 110, 45, 116, 97, 103));
 F135(F595(104, 116, 109, 108, 45, 114, 101, 97, 100, 45, 101, 108, 101, 109, 101, 110, 116));
 F135(F504(104, 116, 109, 108, 45, 111, 112, 101, 110, 45, 116, 97, 103));
 F135(F589(104, 116, 109, 108, 45, 115, 117, 45, 116, 97, 103, 45, 101, 110, 100));
 F135(F595(104, 116, 109, 108, 45, 115, 117, 45, 116, 97, 103, 45, 115, 116, 97, 114, 116));
 F135(F615(120, 109, 108, 45, 116, 121, 111, 45, 97, 117, 120));
 F135(F545(120, 109, 108, 45, 112, 115, 49));
 F135(F545(120, 109, 108, 45, 112, 114, 49));
 F135(F550(120, 109, 108, 45, 100, 101, 99, 111, 100, 101));
 F135(F550(120, 109, 108, 45, 101, 110, 99, 111, 100, 101));
 F135(F545(120, 109, 108, 45, 116, 121, 111));
 F135(F752(114, 111, 119, 45, 62, 114, 101, 99));
 F135(F615(116, 97, 98, 108, 101, 45, 62, 114, 101, 99, 115));
 F135(F752(114, 101, 99, 45, 62, 114, 111, 119));
 F135(F460(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 100, 101, 108, 105, 109, 105, 116, 101, 114));
 F135(F589(116, 114, 101, 99, 45, 114, 101, 97, 100, 45, 114, 111, 119, 45, 49));
 F135(F484(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 104, 101, 97, 100, 101, 114, 115));
 F135(F595(116, 114, 101, 99, 45, 115, 107, 105, 112, 45, 104, 101, 97, 100, 101, 114, 115));
 F135(F657(116, 117, 45, 115, 101, 108, 101, 99, 116, 45, 108, 97, 110, 103));
 F135(F505(116, 117, 45, 103, 114, 97, 109, 109, 97, 114, 45, 116, 101, 114, 109, 63));
 F135(F455(116, 99, 112, 83, 116, 114, 101, 97, 109, 84, 121, 111));
 F135(F708(108, 112, 77, 111, 118, 101, 65, 117, 120));
 F135(F708(108, 112, 67, 111, 112, 121, 65, 117, 120));
 F135(F708(115, 121, 109, 77, 97, 107, 101, 77, 99));
 F135(F460(112, 114, 105, 110, 116, 45, 98, 97, 100, 45, 118, 101, 99, 116, 45, 105, 110, 100, 101, 120));
 F135(F545(110, 101, 119, 67, 111, 110, 115));
 F135(F615(102, 99, 97, 108, 108, 45, 101, 114, 114, 111, 114));
 F135(F484(112, 97, 114, 115, 101, 45, 105, 110, 105, 116, 45, 99, 117, 114, 45, 112, 111, 115));
 F135(F589(112, 97, 114, 115, 101, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110));
 F135(F708(112, 97, 114, 115, 101, 45, 110, 111, 116));
 F135(F752(112, 97, 114, 115, 101, 45, 111, 114));
 F135(F708(112, 97, 114, 115, 101, 45, 115, 101, 113));
 F135(F342(112, 97, 114, 115, 101, 45, 111, 116, 104, 101, 114, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 99, 97, 115, 101, 115));
 F135(F460(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 116, 101, 115, 116, 45, 105, 110, 115, 116, 114));
 F135(F504(112, 97, 114, 115, 101, 45, 109, 97, 121, 45, 115, 101, 116));
 F135(F460(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 99, 111, 110, 99, 45, 105, 110, 115, 116, 114));
 F135(F460(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 99, 111, 112, 121, 45, 105, 110, 115, 116, 114));
 F135(F460(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 109, 111, 118, 101, 45, 105, 110, 115, 116, 114));
 F135(F691(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 97, 116, 111, 109, 105, 99, 45, 105, 110, 115, 116, 114));
 F135(F605(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 115, 101, 113, 45, 105, 110, 115, 116, 114));
 F135(F505(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 105, 110, 115, 116, 114, 115));
 F135(F589(112, 97, 114, 115, 101, 45, 101, 120, 112, 45, 105, 110, 115, 116, 114));
 F135(F550(114, 101, 45, 97, 100, 100, 45, 97, 114, 99));
 F135(F484(114, 101, 45, 97, 100, 100, 45, 108, 105, 110, 107, 101, 100, 45, 110, 111, 100, 101));
 F135(F589(114, 101, 45, 101, 120, 116, 114, 97, 99, 116, 45, 118, 97, 114, 49));
 F135(F460(114, 101, 45, 112, 117, 115, 104, 45, 108, 105, 110, 107, 101, 100, 45, 110, 111, 100, 101, 115));
 F135(F708(114, 101, 45, 99, 108, 97, 115, 115, 63));
 F135(F752(114, 101, 45, 109, 97, 116, 99, 104));
 F135(F504(114, 101, 45, 109, 97, 107, 101, 45, 101, 109, 112, 116, 121));
 F135(F752(112, 97, 114, 115, 101, 45, 114, 101));
 F135(F589(112, 114, 105, 110, 116, 45, 115, 101, 112, 97, 114, 97, 116, 111, 114));
 F135(F708(112, 114, 105, 110, 116, 45, 115, 121, 109));
 F135(F550(112, 114, 105, 110, 116, 45, 99, 115, 121, 109));
 F135(F595(112, 114, 105, 110, 116, 45, 97, 116, 111, 109, 105, 99, 45, 101, 120, 112, 114));
 F135(F147(112, 114, 45, 101, 120, 112));
 F135(F550(114, 101, 97, 100, 45, 101, 114, 114, 111, 114));
 F135(F84(114, 115, 110));
 F135(F50(114, 115, 45, 49));
 F135(F455(105, 111, 45, 105, 110, 105, 116, 45, 118, 97, 114, 115));
 F135(F657(115, 116, 114, 101, 97, 109, 45, 116, 99, 112, 45, 116, 121, 111));
 F135(F92(115, 116, 114, 101, 97, 109, 45, 101, 120, 112, 97, 110, 100, 45, 102, 105, 108, 101, 45, 115, 116, 114, 101, 97, 109, 115));
 F135(F505(115, 116, 114, 101, 97, 109, 45, 98, 117, 102, 45, 115, 116, 97, 114, 116));
 F135(F550(108, 112, 45, 102, 105, 108, 101, 115, 45, 49));
 F135(F615(108, 112, 45, 112, 111, 115, 45, 108, 97, 115, 116));
 F135(F147(108, 112, 45, 100, 101, 108));
 F135(F545(108, 112, 45, 109, 111, 118, 101));
 F135(F545(108, 112, 45, 109, 97, 107, 101));
 F135(F550(108, 112, 45, 115, 117, 98, 100, 105, 114, 115));
 F135(F615(108, 112, 45, 102, 105, 108, 101, 110, 97, 109, 101));
 F135(F752(108, 112, 45, 116, 121, 112, 101, 63));
 F135(F147(108, 112, 45, 100, 101, 102));
 F135(F147(108, 112, 45, 62, 112, 112));
 F135(F589(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 45, 116, 121, 112, 101));
 F135(F460(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 45, 116, 121, 112, 101, 45, 110, 97, 109, 101));
 F135(F460(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 45, 118, 101, 99, 116, 45, 110, 97, 109, 101));
 F135(F691(118, 114, 101, 99, 45, 102, 105, 101, 108, 100, 38, 118, 97, 108, 117, 101, 45, 115, 112, 101, 99, 63));
 F135(F589(118, 114, 101, 99, 45, 112, 97, 114, 115, 101, 45, 102, 38, 118, 115));
 F135(F505(118, 114, 101, 99, 45, 114, 109, 97, 107, 101, 45, 109, 97, 99, 114, 111));
 F135(F504(116, 114, 105, 101, 45, 109, 97, 107, 101, 45, 115, 117, 98));
 F135(F504(116, 114, 105, 101, 45, 97, 100, 100, 45, 115, 117, 98, 33));
 F135(F504(116, 114, 105, 101, 45, 102, 105, 110, 100, 45, 115, 117, 98));
 F135(F107(116, 114, 105, 101, 45, 102, 105, 110, 100, 45, 111, 114, 45, 105, 110, 115, 101, 114, 116, 45, 110, 111, 100, 101));
 F135(F657(116, 114, 105, 101, 45, 102, 105, 110, 100, 45, 110, 111, 100, 101));
 F135(F752(116, 114, 105, 101, 45, 103, 101, 116));
 F135(F752(104, 97, 115, 104, 45, 107, 101, 121));
 F135(F752(104, 97, 115, 104, 45, 103, 101, 116));
 F135(F550(109, 101, 114, 103, 101, 45, 115, 111, 114, 116));
 F135(F147(103, 101, 116, 45, 102, 110));
 F135(F545(103, 101, 116, 45, 101, 120, 112));
 F135(F460(109, 117, 108, 116, 105, 112, 108, 101, 45, 62, 98, 105, 110, 97, 114, 121, 45, 111, 112, 114));
 F135(F504(102, 111, 114, 45, 97, 100, 100, 45, 115, 112, 101, 99, 33));
 F135(F615(112, 108, 105, 115, 116, 45, 105, 110, 105, 116, 115));
 F135(F550(115, 116, 97, 99, 107, 45, 112, 117, 115, 104));
 F135(F460(119, 105, 116, 104, 45, 114, 101, 99, 45, 117, 112, 100, 97, 116, 101, 45, 108, 105, 115, 116));
 F135(F595(119, 105, 116, 104, 45, 114, 101, 99, 45, 115, 101, 116, 45, 108, 105, 115, 116));
 F135(F545(114, 103, 101, 116, 45, 102, 110));
 F135(F752(115, 121, 109, 45, 102, 105, 110, 100));
 F135(F545(115, 121, 109, 45, 115, 101, 116));
 F135(F504(99, 115, 121, 109, 45, 100, 105, 103, 105, 116, 45, 103, 63));
 F135(F752(115, 116, 114, 45, 116, 101, 115, 116));
 F135(F50(115, 45, 62, 105));
 F135(F492(115, 116, 114, 60, 61));
 F135(F50(115, 116, 114, 60));
 F135(F492(115, 116, 114, 62, 61));
 F135(F50(115, 116, 114, 62));
 F135(F492(115, 116, 114, 60, 62));
 F135(F550(115, 116, 114, 45, 102, 108, 111, 97, 116, 63));
 F135(F752(115, 116, 114, 45, 105, 110, 116, 63));
 F135(F492(99, 115, 121, 109, 60));
 F135(F492(100, 108, 105, 115, 116));
 F135(F147(99, 111, 110, 99, 50, 33));
 F135(F50(115, 114, 45, 49));
 F135(F84(115, 114, 51));
 F135(F84(115, 114, 50));
 F135(F84(115, 114, 49));
 F135(F84(115, 45, 49));
 F135(F592(115, 51));
 F135(F592(115, 50));
 F135(F592(115, 49));
 F135(F492(100, 101, 108, 49, 33));
 F135(F50(100, 101, 108, 49));
 F135(F50(109, 97, 107, 101));
 F135(F50(112, 111, 115, 49));
 F135(F492(102, 105, 110, 100, 49));
 F135(F84(98, 108, 33));
 F135(F592(98, 108));
 F135(F492(114, 105, 103, 104, 116));
 F135(F50(108, 101, 102, 116));
 F135(F492(103, 105, 45, 111, 110));
 F135(F504(108, 105, 115, 116, 45, 100, 115, 116, 45, 115, 101, 116, 115));
 F135(F505(108, 105, 115, 116, 45, 115, 101, 116, 45, 105, 110, 100, 101, 120, 101, 115));
 F135(F504(101, 120, 112, 45, 116, 101, 115, 116, 45, 101, 120, 112, 114));
 F135(F592(60, 62));
 F135(F545(112, 115, 101, 116, 45, 102, 110));
 F135(F752(111, 114, 45, 101, 120, 112, 45, 114));
 F135(F708(97, 110, 100, 45, 101, 120, 112, 45, 114));
 F135(F50(101, 113, 71, 63));
 F135(F752(115, 121, 115, 45, 99, 97, 108, 108));
 F135(F492(97, 112, 112, 108, 121));
 F135(F492(112, 115, 49, 45, 50));
 F135(F50(112, 115, 49, 109));
 F135(F455(112, 115, 45, 62, 112, 115, 49, 45, 108, 105, 115, 116));
 F135(F50(115, 116, 114, 63));
 F135(F147(114, 116, 121, 112, 101, 63));
 F135(F50(37, 95, 76, 50));
 F135(F590(104, 116, 109, 108, 45, 112, 111, 112, 45, 117, 110, 109, 97, 116, 99, 104, 101, 100, 45, 116, 97, 103, 115));
 F135(F657(104, 116, 109, 108, 45, 99, 108, 111, 115, 101, 45, 116, 97, 103));
 F135(F605(120, 109, 108, 45, 115, 116, 114, 101, 97, 109, 45, 102, 105, 108, 108, 45, 98, 117, 102));
 F135(F455(116, 114, 101, 99, 45, 114, 101, 97, 100, 45, 101, 108));
 F135(F657(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 114, 101, 99));
 F135(F657(116, 114, 101, 99, 45, 114, 101, 97, 100, 45, 114, 101, 99, 115));
 F135(F657(116, 114, 101, 99, 45, 108, 111, 97, 100, 45, 114, 101, 99, 115));
 F135(F708(116, 114, 101, 99, 45, 114, 101, 97, 100));
 F135(F708(116, 114, 101, 99, 45, 108, 111, 97, 100));
 F135(F595(116, 114, 101, 99, 45, 99, 111, 112, 121, 45, 104, 101, 97, 100, 101, 114, 115));
 F135(F691(116, 117, 45, 102, 105, 110, 100, 45, 116, 101, 114, 109, 45, 114, 101, 99, 115, 45, 108, 97, 110, 103));
 F135(F595(112, 97, 114, 115, 101, 45, 115, 107, 105, 112, 45, 115, 99, 104, 101, 109, 97));
 F135(F691(112, 97, 114, 115, 101, 45, 115, 116, 114, 115, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 49));
 F135(F601(112, 97, 114, 115, 101, 45, 115, 116, 114, 45, 99, 111, 110, 100, 105, 116, 105, 111, 110, 45, 49));
 F135(F615(114, 101, 45, 109, 97, 107, 101, 45, 97, 114, 99));
 F135(F752(114, 101, 45, 109, 97, 107, 101, 42));
 F135(F550(114, 101, 45, 109, 97, 107, 101, 45, 111, 114));
 F135(F615(114, 101, 45, 109, 97, 107, 101, 45, 115, 101, 113));
 F135(F615(114, 101, 45, 109, 97, 107, 101, 45, 115, 101, 116));
 F135(F708(114, 101, 45, 109, 97, 107, 101, 45, 114));
 F135(F708(112, 114, 105, 110, 116, 45, 115, 116, 114));
 F135(F484(114, 101, 97, 100, 45, 108, 105, 115, 116, 45, 101, 108, 101, 109, 101, 110, 116, 115));
 F135(F605(114, 101, 97, 100, 45, 99, 111, 109, 109, 97, 45, 110, 111, 116, 97, 116, 105, 111, 110));
 F135(F615(114, 101, 97, 100, 45, 98, 113, 117, 111, 116, 101));
 F135(F550(114, 101, 97, 100, 45, 113, 117, 111, 116, 101));
 F135(F107(115, 107, 105, 112, 45, 115, 101, 112, 97, 114, 97, 116, 111, 114, 115, 38, 99, 111, 109, 109, 101, 110, 116, 115));
 F135(F147(114, 101, 97, 100, 45, 114));
 F135(F50(114, 101, 97, 100));
 F135(F605(115, 116, 114, 101, 97, 109, 45, 116, 99, 112, 45, 102, 105, 108, 108, 45, 98, 117, 102));
 F135(F615(112, 97, 114, 115, 101, 45, 115, 112, 108, 105, 116));
 F135(F615(108, 112, 45, 99, 111, 112, 121, 45, 100, 105, 114));
 F135(F550(108, 112, 45, 102, 105, 108, 101, 115, 45, 114));
 F135(F545(108, 112, 45, 99, 111, 112, 121));
 F135(F752(108, 112, 45, 102, 105, 108, 101, 115));
 F135(F589(118, 114, 101, 99, 45, 115, 116, 111, 114, 101, 45, 115, 112, 101, 99));
 F135(F708(118, 114, 103, 101, 116, 45, 101, 120, 112));
 F135(F657(118, 114, 101, 99, 45, 118, 101, 99, 116, 45, 100, 101, 102, 115));
 F135(F752(116, 114, 105, 101, 45, 115, 101, 116));
 F135(F752(104, 97, 115, 104, 45, 115, 101, 116));
 F135(F589(109, 101, 114, 103, 101, 45, 115, 111, 114, 116, 45, 102, 110, 45, 49));
 F135(F484(102, 111, 114, 45, 101, 120, 112, 97, 110, 115, 105, 111, 110, 45, 102, 111, 114, 109));
 F135(F505(114, 101, 99, 45, 109, 97, 116, 99, 104, 45, 102, 105, 101, 108, 100, 63));
 F135(F545(114, 115, 101, 116, 45, 102, 110));
 F135(F752(112, 97, 100, 45, 108, 101, 102, 116));
 F135(F752(99, 111, 110, 116, 97, 105, 110, 63));
 F135(F147(98, 101, 103, 105, 110, 63));
 F135(F50(101, 113, 108, 63));
 F135(F615(115, 112, 108, 105, 116, 45, 111, 110, 45, 102, 110));
 F135(F752(115, 112, 108, 105, 116, 45, 102, 110));
 F135(F84(115, 114, 105));
 F135(F615(116, 114, 105, 109, 45, 114, 105, 103, 104, 116, 33));
 F135(F550(116, 114, 105, 109, 45, 114, 105, 103, 104, 116));
 F135(F708(116, 114, 105, 109, 45, 108, 101, 102, 116));
 F135(F545(115, 117, 98, 115, 116, 49, 33));
 F135(F147(115, 117, 98, 115, 116, 49));
 F135(F84(97, 100, 100));
 F135(F50(97, 100, 100, 33));
 F135(F657(97, 117, 120, 45, 99, 97, 115, 101, 45, 62, 99, 111, 110, 100));
 F135(F605(97, 117, 120, 45, 101, 120, 112, 45, 117, 112, 100, 97, 116, 101, 45, 102, 111, 114, 109));
 F135(F50(37, 95, 76, 51));
 F135(F504(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 101, 108));
 F135(F505(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 114, 111, 119, 45, 49));
 F135(F504(116, 114, 101, 99, 45, 114, 101, 97, 100, 45, 114, 111, 119));
 F135(F589(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 114, 101, 99, 115));
 F135(F504(116, 114, 101, 99, 45, 114, 101, 97, 100, 45, 114, 101, 99));
 F135(F657(116, 114, 101, 99, 45, 115, 97, 118, 101, 45, 114, 101, 99, 115));
 F135(F550(116, 114, 101, 99, 45, 119, 114, 105, 116, 101));
 F135(F708(116, 114, 101, 99, 45, 115, 97, 118, 101));
 F135(F460(116, 117, 45, 101, 120, 116, 114, 97, 99, 116, 45, 116, 101, 114, 109, 45, 116, 114, 105, 101));
 F135(F505(116, 117, 45, 101, 120, 116, 114, 97, 99, 116, 45, 116, 101, 114, 109, 115));
 F135(F460(116, 117, 45, 101, 120, 116, 114, 97, 99, 116, 45, 116, 101, 114, 109, 45, 114, 101, 99, 115));
 F135(F590(116, 117, 45, 116, 114, 97, 99, 101, 45, 108, 97, 110, 103, 45, 115, 101, 108, 101, 99, 116, 105, 111, 110));
 F135(F595(112, 97, 114, 115, 101, 45, 99, 111, 110, 99, 45, 115, 99, 104, 101, 109, 97));
 F135(F595(112, 97, 114, 115, 101, 45, 116, 101, 115, 116, 45, 115, 99, 104, 101, 109, 97));
 F135(F605(112, 97, 114, 115, 101, 45, 102, 105, 110, 105, 115, 104, 45, 119, 104, 105, 108, 101, 98));
 F135(F657(118, 114, 101, 99, 45, 114, 115, 101, 116, 45, 102, 111, 114, 109));
 F135(F708(118, 114, 115, 101, 116, 45, 101, 120, 112));
 F135(F492(114, 102, 105, 110, 100));
 F135(F592(102, 115));
 F135(F708(115, 112, 108, 105, 116, 45, 115, 116, 114));
 F135(F550(115, 112, 108, 105, 116, 45, 99, 115, 121, 109));
 F135(F492(115, 112, 108, 105, 116));
 F135(F50(106, 111, 105, 110));
 F135(F492(106, 111, 105, 110, 33));
 F135(F50(112, 97, 100, 33));
 F135(F84(112, 97, 100));
 F135(F50(37, 95, 76, 52));
 F135(F657(116, 114, 101, 99, 45, 119, 114, 105, 116, 101, 45, 114, 111, 119));
 F135(F595(112, 97, 114, 115, 101, 45, 99, 111, 112, 121, 45, 115, 99, 104, 101, 109, 97));
 F135(F595(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 45, 115, 99, 104, 101, 109, 97));
 F135(F484(112, 114, 105, 110, 116, 101, 100, 45, 105, 110, 45, 97, 45, 108, 105, 110, 101, 63));
 F135(F455(112, 114, 105, 110, 116, 45, 108, 105, 115, 116, 45, 49));
 F135(F550(112, 114, 105, 110, 116, 45, 108, 105, 115, 116));
 F135(F455(112, 114, 105, 110, 116, 45, 118, 101, 99, 116, 45, 49));
 F135(F550(112, 114, 105, 110, 116, 45, 118, 101, 99, 116));
 F135(F545(112, 114, 105, 110, 116, 45, 114));
 F135(F592(102, 105));
 F135(F50(37, 95, 76, 53));
 F135(F50(37, 95, 76, 54));
 F135(F708(104, 116, 109, 108, 45, 114, 101, 97, 100));
 F135(F50(37, 95, 76, 55));
 F135(F50(37, 95, 76, 56));
 F135(F50(37, 95, 76, 57));
 F135(F492(99, 111, 110, 99, 33));
 F135(F50(99, 111, 110, 99));
 F135(F492(116, 114, 105, 109, 33));
 F135(F50(116, 114, 105, 109));
 F135(F147(115, 117, 98, 115, 116, 33));
 F135(F492(115, 117, 98, 115, 116));
 F135(F50(100, 101, 108, 33));
 F135(F84(100, 101, 108));
 F135(F492(102, 105, 110, 100, 63));
 F135(F262(47));
 F135(F492(37, 95, 76, 49, 48));
 F135(F708(98, 117, 102, 49, 45, 109, 111, 100, 63));
 F135(F615(98, 117, 102, 49, 45, 110, 98, 121, 116, 101, 115));
 F135(F550(98, 117, 102, 49, 45, 105, 110, 100, 101, 120));
 F135(F550(98, 117, 102, 49, 45, 115, 116, 97, 114, 116));
 F135(F708(98, 117, 102, 48, 45, 109, 111, 100, 63));
 F135(F615(98, 117, 102, 48, 45, 110, 98, 121, 116, 101, 115));
 F135(F550(98, 117, 102, 48, 45, 105, 110, 100, 101, 120));
 F135(F550(98, 117, 102, 48, 45, 115, 116, 97, 114, 116));
 F135(F615(111, 117, 116, 45, 98, 117, 102, 45, 109, 97, 120));
 F135(F550(105, 110, 45, 98, 117, 102, 45, 109, 97, 120));
 F135(F545(98, 117, 102, 45, 99, 117, 114));
 F135(F550(105, 110, 45, 98, 117, 102, 45, 109, 105, 110));
 F135(F708(98, 117, 102, 45, 105, 110, 100, 101, 120));
 F135(F550(115, 101, 114, 105, 97, 108, 45, 112, 111, 115));
 F135(F84(112, 111, 115));
 F135(F84(109, 97, 120));
 F135(F545(105, 110, 116, 54, 52, 95, 116));
 F135(F752(37, 105, 110, 116, 54, 52, 95, 116));
 F135(F147(116, 121, 111, 45, 102, 110));
 F135(F147(116, 121, 105, 45, 102, 110));
 F135(F545(112, 101, 101, 107, 45, 102, 110));
 F135(F752(102, 108, 117, 115, 104, 45, 102, 110));
 F135(F492(115, 104, 97, 114, 107));
 F135(F545(102, 105, 108, 108, 45, 102, 110));
 F135(F50(97, 117, 120, 50));
 F135(F84(97, 117, 120));
 F135(F545(115, 101, 114, 105, 97, 108, 63));
 F135(F147(115, 104, 45, 105, 110, 116));
 F135(F752(98, 117, 102, 45, 115, 105, 122, 101));
 F135(F147(115, 116, 114, 101, 97, 109));
 F135(F708(97, 114, 103, 45, 105, 110, 100, 101, 120));
 F135(F752(109, 97, 120, 45, 104, 105, 110, 116));
 F135(F545(109, 97, 120, 45, 105, 110, 116));
 F135(F504(101, 120, 112, 111, 110, 101, 110, 116, 45, 115, 105, 122, 101));
 F135(F504(102, 114, 97, 99, 116, 105, 111, 110, 45, 115, 105, 122, 101));
 F135(F589(115, 104, 97, 114, 107, 45, 112, 114, 101, 99, 105, 115, 105, 111, 110));
 F135(F147(105, 45, 115, 105, 122, 101));
 F135(F545(117, 110, 98, 111, 120, 101, 100));
 F135(F147(105, 45, 116, 121, 112, 101));
 F135(F147(98, 111, 120, 101, 100, 63));
 F135(_c(115, _c(112, _c(97, _c(105, F215(45, 102, 105, 110, 100, 45, 99, 111, 111, 107, 105, 101, 45, 99, 111, 110, 115, 101, 110, 115, 117, 115, 45, 102, 114, 111, 109, 45, 102, 105, 108, 101))))));
 F135(F455(101, 120, 116, 101, 114, 110, 97, 108, 45, 102, 110, 115));
 F135(F595(110, 117, 109, 101, 114, 105, 99, 45, 102, 110, 45, 110, 97, 109, 101, 115, 63));
 F135(F708(115, 104, 97, 114, 107, 45, 100, 101, 102));
 F135(F50(108, 97, 110, 103));
 F135(F708(104, 111, 115, 116, 45, 110, 97, 109, 101));
 F135(F550(117, 110, 98, 111, 120, 101, 100, 45, 51, 50));
 F135(F147(105, 45, 115, 112, 101, 99));
 F135(F84(108, 105, 98));
 F135(F147(97, 45, 116, 121, 112, 101));
 F135(F708(115, 104, 97, 114, 107, 45, 112, 97, 114));
 F135(F84(115, 101, 113));
 F135(F50(99, 111, 110, 115));
 F135(F262(116));
 F135(F592(105, 102));
 F135(F84(115, 101, 116));
 F135(F592(103, 105));
 F135(F592(115, 105));
 F135(F492(81, 85, 79, 84, 69));
 F135(F492(113, 117, 111, 116, 101));
 F135(F147(98, 113, 117, 111, 116, 101));
 F135(F84(116, 97, 98));
 F135(F84(111, 117, 116));
 F135(F592(100, 113));
 F135(F545(112, 115, 49, 45, 111, 117, 116));
 F135(F84(112, 115, 49));
 F135(F50(99, 111, 110, 100));
 F135(F505(115, 116, 114, 101, 97, 109, 45, 112, 105, 112, 101, 45, 102, 105, 108, 108));
 F135(F657(42, 115, 116, 114, 101, 97, 109, 45, 101, 114, 114, 111, 114, 42));
 F135(F50(102, 97, 105, 108));
 F135(F50(119, 105, 116, 104));
 F135(F550(97, 110, 100, 45, 114, 101, 115, 117, 108, 116));
 F135(F708(111, 114, 45, 114, 101, 115, 117, 108, 116));
 F135(F615(42, 115, 104, 97, 114, 107, 45, 112, 97, 114, 42));
 F135(F592(60, 61));
 F135(F592(62, 61));
 F135(F50(119, 104, 101, 110));
 F135(F262(60));
 F135(F262(43));
 F135(F492(38, 114, 101, 115, 116));
 F135(F50(38, 111, 112, 116));
 F135(F50(42, 98, 114, 42));
 F135(F657(42, 112, 114, 105, 110, 116, 45, 108, 101, 118, 101, 108, 115, 42));
 F135(F615(42, 112, 114, 105, 110, 116, 45, 108, 101, 110, 42));
 F135(F50(115, 117, 98, 108));
 F135(F84(103, 114, 105));
 F135(F84(108, 101, 110));
 F135(F84(103, 114, 49));
 F135(F592(103, 49));
 F135(F84(101, 113, 63));
 F135(F262(114));
 F135(F262(108));
 F135(F262(99));
 F135(F84(97, 108, 108));
 F135(F262(98));
 F135(F147(112, 114, 101, 102, 105, 120));
 F135(F545(112, 111, 115, 116, 102, 105, 120));
 F135(F592(111, 110));
 F135(F492(101, 113, 117, 97, 108));
 F135(F545(103, 114, 101, 97, 116, 101, 114));
 F135(F50(108, 101, 115, 115));
 F135(F50(108, 105, 115, 116));
 F135(F50(114, 103, 101, 116));
 F135(F50(114, 115, 101, 116));
 F135(F708(112, 108, 105, 115, 116, 45, 103, 101, 116));
 F135(F708(102, 111, 114, 45, 105, 115, 112, 101, 99));
 F135(F147(117, 110, 108, 101, 115, 115));
 F135(F492(119, 104, 105, 108, 101));
 F135(F492(117, 110, 116, 105, 108));
 F135(F147(114, 101, 112, 101, 97, 116));
 F135(F592(105, 110));
 F135(F752(105, 110, 45, 100, 101, 112, 116, 104));
 F135(F708(105, 110, 45, 100, 101, 112, 116, 104, 65));
 F135(F752(105, 110, 45, 119, 105, 100, 116, 104));
 F135(F708(105, 110, 45, 119, 105, 100, 116, 104, 65));
 F135(F50(102, 114, 111, 109));
 F135(F262(61));
 F135(F545(105, 110, 45, 118, 101, 99, 116));
 F135(F708(105, 110, 45, 115, 116, 114, 101, 97, 109));
 F135(F147(105, 110, 45, 114, 101, 99));
 F135(F545(105, 110, 45, 104, 97, 115, 104));
 F135(F545(105, 110, 45, 116, 114, 105, 101));
 F135(F84(108, 111, 99));
 F135(F50(105, 110, 105, 116));
 F135(F84(97, 110, 100));
 F135(F147(117, 112, 100, 97, 116, 101));
 F135(F147(102, 105, 110, 105, 115, 104));
 F135(F50(115, 112, 101, 99));
 F135(F84(110, 111, 116));
 F135(F550(114, 101, 112, 101, 97, 116, 45, 118, 97, 114));
 F135(F84(105, 110, 99));
 F135(F50(110, 101, 113, 63));
 F135(F657(42, 102, 111, 114, 45, 97, 117, 120, 45, 118, 101, 99, 116, 42));
 F135(F147(101, 120, 112, 45, 98, 121));
 F135(F708(101, 120, 112, 45, 119, 104, 105, 108, 101));
 F135(F50(37, 105, 110, 116));
 F135(F589(102, 111, 114, 45, 115, 116, 97, 114, 116, 45, 105, 110, 100, 101, 120));
 F135(F504(102, 111, 114, 45, 99, 117, 114, 45, 105, 110, 100, 101, 120));
 F135(F505(102, 111, 114, 45, 97, 108, 108, 111, 99, 97, 116, 101, 45, 98, 117, 102));
 F135(F262(45));
 F135(F592(111, 114));
 F135(F342(102, 111, 114, 45, 115, 116, 111, 114, 101, 45, 114, 101, 118, 45, 108, 105, 115, 116, 45, 101, 108, 101, 109, 101, 110, 116, 115));
 F135(F615(102, 111, 114, 45, 103, 101, 116, 38, 100, 101, 99));
 F135(F484(102, 111, 114, 45, 100, 101, 97, 108, 108, 111, 99, 97, 116, 101, 45, 98, 117, 102));
 F135(F218(102, 111, 114, 45, 105, 110, 45, 100, 101, 112, 116, 104, 65, 45, 115, 116, 111, 114, 101, 45, 101, 120, 112, 97, 110, 115, 105, 111, 110));
 F135(F504(102, 111, 114, 45, 101, 110, 100, 45, 105, 110, 100, 101, 120));
 F135(F460(102, 111, 114, 45, 110, 101, 120, 116, 45, 115, 116, 97, 114, 116, 45, 105, 110, 100, 101, 120));
 F135(F484(102, 111, 114, 45, 110, 101, 120, 116, 45, 101, 110, 100, 45, 105, 110, 100, 101, 120));
 F135(F545(102, 111, 114, 45, 98, 117, 102));
 F135(F505(43, 102, 111, 114, 45, 118, 101, 99, 116, 45, 100, 101, 108, 116, 97, 43));
 F135(F590(102, 111, 114, 45, 115, 116, 111, 114, 101, 45, 108, 105, 115, 116, 45, 101, 108, 101, 109, 101, 110, 116, 115));
 F135(F262(62));
 F135(F615(102, 111, 114, 45, 103, 101, 116, 38, 105, 110, 99));
 F135(F218(102, 111, 114, 45, 105, 110, 45, 119, 105, 100, 116, 104, 65, 45, 115, 116, 111, 114, 101, 45, 101, 120, 112, 97, 110, 115, 105, 111, 110));
 F135(F545(102, 111, 114, 45, 103, 101, 116));
 F135(F592(116, 111));
 F135(F147(100, 111, 119, 110, 116, 111));
 F135(F492(98, 101, 108, 111, 119));
 F135(F492(97, 98, 111, 118, 101));
 F135(F592(98, 121));
 F135(F84(100, 101, 99));
 F135(F455(102, 111, 114, 45, 118, 101, 99, 116, 45, 101, 110, 100));
 F135(F50(118, 103, 101, 116));
 F135(F492(118, 115, 105, 122, 101));
 F135(F657(102, 111, 114, 45, 115, 116, 114, 101, 97, 109, 45, 101, 110, 100));
 F135(F592(103, 111));
 F135(F708(43, 109, 97, 120, 45, 105, 110, 116, 43));
 F135(F492(112, 101, 101, 107, 63));
 F135(F550(115, 116, 114, 101, 97, 109, 45, 112, 111, 115));
 F135(F84(116, 121, 105));
 F135(F545(115, 117, 98, 45, 114, 101, 99));
 F135(F592(103, 50));
 F135(F504(116, 114, 105, 101, 45, 101, 110, 100, 45, 102, 108, 97, 103));
 F135(F484(102, 111, 114, 45, 116, 114, 105, 101, 45, 99, 117, 114, 45, 105, 110, 100, 101, 120));
 F135(F504(102, 111, 114, 45, 105, 110, 99, 38, 115, 116, 111, 114, 101));
 F135(F657(116, 114, 105, 101, 45, 110, 111, 100, 101, 45, 99, 111, 100, 101));
 F135(F504(116, 114, 105, 101, 45, 110, 111, 100, 101, 45, 115, 117, 98));
 F135(F595(102, 111, 114, 45, 103, 101, 116, 45, 116, 114, 105, 101, 45, 108, 101, 97, 102));
 F135(F550(104, 97, 115, 104, 45, 105, 110, 100, 101, 120));
 F135(F550(101, 110, 116, 114, 121, 45, 108, 105, 115, 116));
 F135(F752(101, 110, 100, 45, 102, 108, 97, 103));
 F135(F484(102, 111, 114, 45, 103, 101, 116, 45, 104, 97, 115, 104, 45, 101, 110, 116, 114, 121));
 F135(F708(99, 115, 121, 109, 45, 62, 105, 110, 116));
 F135(F752(114, 103, 101, 116, 45, 102, 38, 118));
 F135(F492(102, 99, 97, 108, 108));
 F135(F708(103, 101, 116, 45, 97, 112, 112, 108, 121));
 F135(F708(103, 101, 116, 45, 102, 99, 97, 108, 108));
 F135(F752(102, 99, 97, 108, 108, 45, 111, 110));
 F135(F455(103, 101, 116, 45, 97, 112, 112, 108, 121, 45, 111, 110));
 F135(F455(103, 101, 116, 45, 102, 99, 97, 108, 108, 45, 111, 110));
 F135(F505(42, 102, 111, 114, 45, 103, 114, 111, 117, 112, 45, 104, 97, 115, 104, 42));
 F135(F50(100, 101, 102, 118));
 F135(F492(37, 105, 110, 116, 42));
 F135(F147(37, 118, 109, 97, 107, 101));
 F135(F50(110, 97, 109, 101));
 F135(F708(116, 121, 112, 101, 45, 110, 97, 109, 101));
 F135(F50(100, 101, 102, 109));
 F135(F545(37, 115, 104, 45, 105, 110, 116));
 F135(F84(114, 101, 99));
 F135(F492(102, 105, 101, 108, 100));
 F135(F657(118, 114, 101, 99, 45, 114, 103, 101, 116, 45, 102, 111, 114, 109));
 F135(F492(38, 98, 111, 100, 121));
 F135(F455(102, 105, 101, 108, 100, 38, 118, 97, 108, 117, 101, 115));
 F135(F504(118, 114, 101, 99, 45, 101, 120, 112, 45, 114, 115, 101, 116));
 F135(F50(102, 38, 118, 115));
 F135(F657(118, 114, 101, 99, 45, 101, 120, 112, 45, 114, 109, 97, 107, 101));
 F135(F545(110, 101, 119, 45, 114, 101, 99));
 F135(F550(118, 114, 101, 99, 45, 97, 108, 108, 111, 99));
 F135(F147(37, 115, 104, 97, 114, 107));
 F135(F147(118, 97, 108, 117, 101, 50));
 F135(F589(102, 105, 101, 108, 100, 45, 118, 97, 114, 45, 115, 112, 101, 99, 115));
 F135(F50(98, 111, 100, 121));
 F135(F595(118, 114, 101, 99, 45, 101, 120, 112, 45, 119, 105, 116, 104, 45, 114, 101, 99));
 F135(F50(116, 121, 112, 101));
 F135(F545(37, 115, 104, 97, 114, 107, 42));
 F135(F455(42, 118, 114, 101, 99, 45, 115, 112, 101, 99, 115, 42));
 F135(F545(118, 114, 45, 115, 112, 101, 99));
 F135(F50(115, 105, 122, 101));
 F135(F615(102, 105, 101, 108, 100, 45, 115, 112, 101, 99, 115));
 F135(F550(102, 105, 101, 108, 100, 45, 115, 112, 101, 99));
 F135(F492(37, 118, 103, 101, 116));
 F135(F492(37, 118, 115, 101, 116));
 F135(F455(108, 105, 115, 112, 45, 118, 101, 114, 115, 105, 111, 110));
 F135(F50(104, 111, 115, 116));
 F135(F592(108, 112));
 F135(F50(112, 97, 116, 104));
 F135(F545(108, 112, 45, 108, 105, 115, 116));
 F135(F752(108, 112, 45, 110, 97, 109, 101, 61));
 F135(F492(105, 110, 102, 105, 120));
 F135(F84(110, 101, 119));
 F135(F592(114, 119));
 F135(F84(114, 119, 43));
 F135(F505(119, 105, 116, 104, 45, 102, 105, 108, 101, 45, 115, 116, 114, 101, 97, 109));
 F135(F708(115, 116, 100, 105, 110, 80, 101, 101, 107));
 F135(F752(115, 116, 100, 105, 110, 84, 121, 105));
 F135(F615(115, 116, 100, 111, 117, 116, 70, 108, 117, 115, 104));
 F135(F708(115, 116, 100, 111, 117, 116, 84, 121, 111));
 F135(F84(101, 114, 114));
 F135(F615(115, 116, 100, 101, 114, 114, 70, 108, 117, 115, 104));
 F135(F708(115, 116, 100, 101, 114, 114, 84, 121, 111));
 F135(F657(42, 114, 101, 97, 100, 45, 97, 116, 111, 109, 45, 102, 110, 42));
 F135(F504(42, 114, 101, 97, 100, 45, 115, 116, 114, 45, 102, 110, 42));
 F135(F589(42, 112, 114, 105, 110, 116, 45, 112, 114, 101, 116, 116, 121, 63, 42));
 F135(F589(42, 112, 114, 105, 110, 116, 45, 101, 115, 99, 97, 112, 101, 63, 42));
 F135(F455(42, 112, 114, 105, 110, 116, 45, 98, 97, 115, 101, 42));
 F135(F460(42, 112, 114, 105, 110, 116, 45, 102, 108, 111, 97, 116, 45, 100, 105, 103, 105, 116, 115, 42));
 F135(F657(42, 112, 114, 105, 110, 116, 45, 105, 110, 100, 101, 110, 116, 42));
 F135(F589(42, 112, 114, 105, 110, 116, 45, 116, 97, 98, 45, 115, 116, 114, 42));
 F135(F484(42, 112, 114, 105, 110, 116, 45, 108, 105, 110, 101, 45, 119, 105, 100, 116, 104, 42));
 F135(F615(42, 99, 111, 109, 109, 97, 45, 115, 121, 109, 42));
 F135(F455(42, 99, 111, 109, 109, 97, 64, 45, 115, 121, 109, 42));
 F135(F615(38, 115, 99, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F615(38, 104, 109, 45, 99, 111, 109, 109, 101, 110, 116));
 F135(F657(119, 105, 116, 104, 45, 112, 114, 105, 110, 116, 45, 101, 110, 118));
 F135(F545(101, 115, 99, 97, 112, 101, 63));
 F135(F592(112, 115));
 F135(F492(112, 114, 105, 110, 116));
 F135(F605(115, 116, 114, 101, 97, 109, 45, 115, 116, 114, 45, 102, 105, 108, 108, 45, 98, 117, 102));
 F135(F460(115, 116, 114, 101, 97, 109, 45, 115, 116, 114, 45, 102, 108, 117, 115, 104, 45, 98, 117, 102));
 F135(F504(42, 114, 101, 77, 97, 116, 99, 104, 78, 111, 100, 101, 42));
 F135(F657(114, 101, 45, 101, 120, 116, 114, 97, 99, 116, 45, 118, 97, 114));
 F135(F262(63));
 F135(F262(42));
 F135(F592(42, 61));
 F135(F592(42, 62));
 F135(F592(42, 60));
 F135(F84(42, 62, 60));
 F135(F492(99, 108, 97, 115, 115));
 F135(F708(99, 108, 97, 115, 115, 45, 110, 111, 116));
 F135(F50(110, 111, 100, 101));
 F135(F492(115, 116, 97, 114, 116));
 F135(F84(101, 110, 100));
 F135(F455(108, 105, 110, 107, 101, 100, 45, 110, 111, 100, 101, 115));
 F135(F50(115, 101, 116, 49));
 F135(F84(118, 97, 114));
 F135(F50(115, 101, 116, 50));
 F135(F492(112, 110, 111, 100, 101));
 F135(F50(97, 114, 99, 115));
 F135(F492(108, 97, 98, 101, 108));
 F135(F84(97, 110, 121));
 F135(F50(99, 115, 121, 109));
 F135(F50(112, 114, 101, 99));
 F135(F84(97, 114, 99));
 F135(F708(112, 97, 114, 115, 101, 45, 97, 110, 100));
 F135(F550(112, 97, 114, 115, 101, 45, 119, 104, 101, 110));
 F135(F455(112, 97, 114, 115, 101, 45, 117, 110, 108, 101, 115, 115));
 F135(F550(112, 97, 114, 115, 101, 45, 99, 111, 110, 100));
 F135(F492(112, 99, 111, 110, 100));
 F135(F615(112, 97, 114, 115, 101, 45, 112, 99, 111, 110, 100));
 F135(F615(112, 97, 114, 115, 101, 45, 119, 104, 105, 108, 101));
 F135(F615(112, 97, 114, 115, 101, 45, 117, 110, 116, 105, 108));
 F135(F455(112, 97, 114, 115, 101, 45, 114, 101, 112, 101, 97, 116));
 F135(F84(110, 111, 112));
 F135(F550(116, 101, 115, 116, 45, 118, 97, 108, 117, 101));
 F135(F592(114, 101));
 F135(F592(115, 119));
 F135(F84(115, 119, 98));
 F135(F592(115, 117));
 F135(F84(115, 117, 98));
 F135(F84(115, 117, 43));
 F135(F84(115, 117, 45));
 F135(F50(115, 117, 98, 43));
 F135(F50(115, 117, 98, 45));
 F135(F592(115, 110));
 F135(F752(112, 97, 114, 115, 101, 45, 115, 119));
 F135(F708(112, 97, 114, 115, 101, 45, 115, 119, 98));
 F135(F752(112, 97, 114, 115, 101, 45, 115, 117));
 F135(F708(112, 97, 114, 115, 101, 45, 115, 117, 98));
 F135(F708(112, 97, 114, 115, 101, 45, 115, 117, 43));
 F135(F708(112, 97, 114, 115, 101, 45, 115, 117, 45));
 F135(F550(112, 97, 114, 115, 101, 45, 115, 117, 98, 43));
 F135(F550(112, 97, 114, 115, 101, 45, 115, 117, 98, 45));
 F135(F752(112, 97, 114, 115, 101, 45, 115, 110));
 F135(F752(112, 97, 114, 115, 101, 45, 103, 111));
 F135(F84(99, 112, 119));
 F135(F84(99, 112, 117));
 F135(F50(99, 112, 117, 43));
 F135(F50(99, 112, 117, 45));
 F135(F84(99, 112, 110));
 F135(F708(112, 97, 114, 115, 101, 45, 99, 112, 119));
 F135(F708(112, 97, 114, 115, 101, 45, 99, 112, 117));
 F135(F550(112, 97, 114, 115, 101, 45, 99, 112, 117, 43));
 F135(F550(112, 97, 114, 115, 101, 45, 99, 112, 117, 45));
 F135(F708(112, 97, 114, 115, 101, 45, 99, 112, 110));
 F135(F592(99, 119));
 F135(F592(99, 117));
 F135(F84(99, 117, 43));
 F135(F84(99, 117, 45));
 F135(F592(99, 110));
 F135(F752(112, 97, 114, 115, 101, 45, 99, 119));
 F135(F752(112, 97, 114, 115, 101, 45, 99, 117));
 F135(F708(112, 97, 114, 115, 101, 45, 99, 117, 43));
 F135(F708(112, 97, 114, 115, 101, 45, 99, 117, 45));
 F135(F752(112, 97, 114, 115, 101, 45, 99, 110));
 F135(F50(102, 105, 110, 100));
 F135(F492(102, 105, 110, 100, 98));
 F135(F492(102, 105, 110, 100, 43));
 F135(F492(102, 105, 110, 100, 45));
 F135(F147(102, 105, 110, 100, 98, 43));
 F135(F147(102, 105, 110, 100, 98, 45));
 F135(F545(116, 104, 101, 114, 101, 105, 115));
 F135(F752(116, 104, 101, 114, 101, 105, 115, 98));
 F135(F50(116, 101, 115, 116));
 F135(F492(116, 101, 115, 116, 43));
 F135(F550(112, 97, 114, 115, 101, 45, 102, 105, 110, 100));
 F135(F615(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 98));
 F135(F615(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 43));
 F135(F615(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 45));
 F135(F455(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 98, 43));
 F135(F455(112, 97, 114, 115, 101, 45, 102, 105, 110, 100, 98, 45));
 F135(F504(112, 97, 114, 115, 101, 45, 116, 104, 101, 114, 101, 105, 115));
 F135(F657(112, 97, 114, 115, 101, 45, 116, 104, 101, 114, 101, 105, 115, 98));
 F135(F550(112, 97, 114, 115, 101, 45, 116, 101, 115, 116));
 F135(F615(112, 97, 114, 115, 101, 45, 116, 101, 115, 116, 43));
 F135(F147(117, 110, 116, 105, 108, 98));
 F135(F147(117, 110, 116, 105, 108, 43));
 F135(F147(117, 110, 116, 105, 108, 45));
 F135(F545(117, 110, 116, 105, 108, 98, 43));
 F135(F545(117, 110, 116, 105, 108, 98, 45));
 F135(F147(119, 104, 105, 108, 101, 98));
 F135(F50(109, 97, 120, 63));
 F135(F50(109, 105, 110, 63));
 F135(F147(115, 116, 97, 114, 116, 63));
 F135(F50(101, 110, 100, 63));
 F135(F615(108, 105, 110, 101, 45, 115, 116, 97, 114, 116, 63));
 F135(F708(108, 105, 110, 101, 45, 101, 110, 100, 63));
 F135(F147(97, 108, 112, 104, 97, 63));
 F135(F147(100, 105, 103, 105, 116, 63));
 F135(F545(120, 100, 105, 103, 105, 116, 63));
 F135(F147(97, 108, 110, 117, 109, 63));
 F135(F147(108, 111, 119, 101, 114, 63));
 F135(F147(117, 112, 112, 101, 114, 63));
 F135(F147(115, 112, 97, 99, 101, 63));
 F135(F147(98, 108, 97, 110, 107, 63));
 F135(F147(98, 114, 101, 97, 107, 63));
 F135(F147(112, 114, 105, 110, 116, 63));
 F135(F147(99, 110, 116, 114, 108, 63));
 F135(F147(103, 114, 97, 112, 104, 63));
 F135(F147(112, 117, 110, 99, 116, 63));
 F135(F492(99, 115, 121, 109, 61));
 F135(F50(115, 116, 114, 61));
 F135(F492(99, 115, 121, 109, 115));
 F135(F147(99, 115, 121, 109, 115, 61));
 F135(F50(115, 116, 114, 115));
 F135(F492(115, 116, 114, 115, 61));
 F135(F550(115, 121, 109, 45, 98, 114, 101, 97, 107, 63));
 F135(F592(111, 115));
 F135(F50(112, 114, 111, 112));
 F135(F50(99, 102, 110, 63));
 F135(F492(101, 120, 112, 70, 110));
 F135(F50(100, 97, 116, 101));
 F135(F50(121, 101, 97, 114));
 F135(F492(109, 111, 110, 116, 104));
 F135(F84(100, 97, 121));
 F135(F50(119, 100, 97, 121));
 F135(F50(104, 111, 117, 114));
 F135(F84(109, 105, 110));
 F135(F84(115, 101, 99));
 F135(F492(105, 115, 100, 115, 116));
 F135(F589(108, 112, 70, 105, 108, 101, 84, 121, 112, 101, 67, 115, 121, 109, 80));
 F135(F691(42, 101, 110, 103, 108, 105, 115, 104, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 115, 42));
 F135(F484(42, 101, 110, 103, 108, 105, 115, 104, 45, 97, 114, 116, 105, 99, 108, 101, 115, 42));
 F135(F224(42, 101, 110, 103, 108, 105, 115, 104, 45, 97, 117, 120, 105, 108, 105, 97, 114, 121, 45, 118, 101, 114, 98, 115, 42));
 F135(F691(42, 101, 110, 103, 108, 105, 115, 104, 45, 99, 111, 110, 106, 117, 110, 99, 116, 105, 111, 110, 115, 42));
 F135(_c(42, _c(101, F215(110, 103, 108, 105, 115, 104, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 115, 42))));
 F135(F573(42, 101, 110, 103, 108, 105, 115, 104, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 115, 42));
 F135(F342(42, 101, 110, 103, 108, 105, 115, 104, 45, 114, 101, 108, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F218(42, 101, 110, 103, 108, 105, 115, 104, 45, 105, 110, 100, 101, 102, 105, 110, 105, 116, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F342(42, 101, 110, 103, 108, 105, 115, 104, 45, 112, 101, 114, 115, 111, 110, 97, 108, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F467(42, 101, 110, 103, 108, 105, 115, 104, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F691(42, 101, 110, 103, 108, 105, 115, 104, 45, 103, 114, 97, 109, 109, 97, 114, 45, 116, 114, 105, 101, 42));
 F135(F615(112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110));
 F135(F545(97, 114, 116, 105, 99, 108, 101));
 F135(F615(99, 111, 110, 106, 117, 110, 99, 116, 105, 111, 110));
 F135(F505(112, 101, 114, 115, 111, 110, 97, 108, 45, 112, 114, 111, 110, 111, 117, 110));
 F135(F657(97, 117, 120, 105, 108, 105, 97, 114, 121, 45, 118, 101, 114, 98));
 F135(F590(100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101));
 F135(F605(112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101));
 F135(F505(114, 101, 108, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110));
 F135(F484(105, 110, 100, 101, 102, 105, 110, 105, 116, 101, 45, 112, 114, 111, 110, 111, 117, 110));
 F135(F595(112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110));
 F135(F218(42, 105, 116, 97, 108, 105, 97, 110, 45, 115, 105, 109, 112, 108, 101, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 115, 42));
 F135(F604(42, 105, 116, 97, 108, 105, 97, 110, 45, 97, 114, 116, 105, 99, 108, 101, 100, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110, 115, 42));
 F135(F484(42, 105, 116, 97, 108, 105, 97, 110, 45, 97, 114, 116, 105, 99, 108, 101, 115, 42));
 F135(F691(42, 105, 116, 97, 108, 105, 97, 110, 45, 99, 111, 110, 106, 117, 110, 99, 116, 105, 111, 110, 115, 42));
 F135(F342(42, 105, 116, 97, 108, 105, 97, 110, 45, 112, 101, 114, 115, 111, 110, 97, 108, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F342(42, 105, 116, 97, 108, 105, 97, 110, 45, 114, 101, 108, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F467(42, 105, 116, 97, 108, 105, 97, 110, 45, 112, 111, 115, 115, 101, 115, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F215(42, 105, 116, 97, 108, 105, 97, 110, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(F218(42, 105, 116, 97, 108, 105, 97, 110, 45, 105, 110, 100, 101, 102, 105, 110, 105, 116, 101, 45, 112, 114, 111, 110, 111, 117, 110, 115, 42));
 F135(_c(42, _c(105, F215(116, 97, 108, 105, 97, 110, 45, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 97, 100, 106, 101, 99, 116, 105, 118, 101, 115, 42))));
 F135(F224(42, 105, 116, 97, 108, 105, 97, 110, 45, 97, 117, 120, 105, 108, 105, 97, 114, 121, 45, 118, 101, 114, 98, 115, 42));
 F135(F691(42, 105, 116, 97, 108, 105, 97, 110, 45, 103, 114, 97, 109, 109, 97, 114, 45, 116, 114, 105, 101, 42));
 F135(F484(115, 105, 109, 112, 108, 101, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110));
 F135(F460(97, 114, 116, 105, 99, 108, 101, 100, 45, 112, 114, 101, 112, 111, 115, 105, 116, 105, 111, 110));
 F135(F601(100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 105, 118, 101, 45, 112, 114, 111, 110, 111, 117, 110));
 F135(F50(116, 101, 120, 116));
 F135(F545(99, 111, 117, 110, 116, 101, 114));
 F135(F592(105, 116));
 F135(F592(101, 110));
 F135(F50(116, 101, 114, 109));
 F135(F545(116, 114, 115, 112, 101, 99, 115));
 F135(F504(42, 116, 114, 101, 99, 45, 116, 114, 97, 99, 101, 63, 42));
 F135(F708(99, 111, 109, 112, 108, 101, 116, 101, 63));
 F135(F708(115, 101, 112, 97, 114, 97, 116, 111, 114));
 F135(F550(116, 101, 114, 109, 105, 110, 97, 116, 111, 114));
 F135(F484(114, 101, 97, 100, 45, 99, 111, 110, 118, 101, 114, 115, 105, 111, 110, 45, 102, 110));
 F135(F605(119, 114, 105, 116, 101, 45, 99, 111, 110, 118, 101, 114, 115, 105, 111, 110, 45, 102, 110));
 F135(F615(102, 105, 101, 108, 100, 45, 110, 97, 109, 101, 115));
 F135(F545(104, 101, 97, 100, 101, 114, 115));
 F135(F224(102, 105, 101, 108, 100, 45, 114, 101, 97, 100, 45, 99, 111, 110, 118, 101, 114, 115, 105, 111, 110, 45, 102, 110, 115));
 F135(F92(102, 105, 101, 108, 100, 45, 119, 114, 105, 116, 101, 45, 99, 111, 110, 118, 101, 114, 115, 105, 111, 110, 45, 102, 110, 115));
 F135(F460(115, 116, 114, 101, 97, 109, 45, 102, 105, 108, 101, 45, 102, 105, 108, 108, 45, 98, 117, 102));
 F135(F601(115, 116, 114, 101, 97, 109, 45, 102, 105, 108, 101, 45, 102, 108, 117, 115, 104, 45, 98, 117, 102));
 F135(F545(104, 101, 97, 100, 101, 114, 63));
 F135(F592(98, 114));
 F135(F492(105, 110, 112, 117, 116));
 F135(F84(105, 109, 103));
 F135(F84(99, 111, 108));
 F135(F50(108, 105, 110, 107));
 F135(F262(112));
 F135(F50(98, 97, 115, 101));
 F135(F50(109, 101, 116, 97));
 F135(F147(115, 112, 97, 99, 101, 114));
 F135(F492(112, 97, 114, 97, 109));
 F135(F592(104, 114));
 F135(F50(97, 114, 101, 97));
 F135(F84(119, 98, 114));
 F135(F492(102, 114, 97, 109, 101));
 F135(F484(42, 104, 116, 109, 108, 45, 115, 105, 110, 103, 108, 101, 45, 116, 97, 103, 115, 42));
 F135(F147(116, 114, 115, 112, 101, 99));
 F135(F752(117, 114, 108, 38, 102, 105, 108, 101));
 F135(F84(117, 114, 108));
 F135(F50(102, 105, 108, 101));
 F135(F262(64));
 F135(F484(42, 104, 116, 109, 108, 45, 105, 110, 108, 105, 110, 101, 45, 116, 97, 103, 115, 42));
 F135(F455(42, 104, 116, 109, 108, 45, 114, 101, 97, 100, 63, 42));
 F135(F545(42, 67, 68, 65, 84, 65, 42));
 F135(F708(42, 67, 79, 77, 77, 69, 78, 84, 42));
 F135(F605(42, 104, 116, 109, 108, 45, 116, 97, 103, 45, 110, 97, 109, 101, 45, 101, 110, 100, 42));
 F135(F615(99, 115, 121, 109, 45, 115, 112, 97, 99, 101, 63));
 F135(F50(42, 80, 73, 42));
 F135(F605(42, 104, 116, 109, 108, 45, 115, 116, 97, 99, 107, 45, 111, 102, 102, 115, 101, 116, 42));
 F135(F601(42, 104, 116, 109, 108, 45, 116, 97, 103, 45, 110, 97, 109, 101, 45, 115, 116, 97, 99, 107, 42));
 F135(F601(42, 104, 116, 109, 108, 45, 116, 97, 103, 45, 98, 111, 100, 121, 45, 115, 116, 97, 99, 107, 42));
 F135(F615(42, 104, 116, 109, 108, 45, 99, 97, 115, 101, 42));
 F135(F492(42, 84, 79, 80, 42));
 F135(F484(42, 104, 116, 109, 108, 45, 97, 116, 116, 110, 97, 109, 101, 45, 101, 110, 100, 42));
 F135(F592(117, 112));
 F135(F50(100, 111, 119, 110));
 F135(F84(99, 97, 112));
 F135(F460(117, 116, 102, 56, 45, 115, 116, 114, 101, 97, 109, 45, 102, 105, 108, 108, 45, 98, 117, 102));
 F135(F50(104, 116, 109, 108));
 F135(F455(116, 117, 45, 116, 101, 114, 109, 45, 115, 101, 112, 63));
 F135(F545(115, 116, 114, 45, 108, 111, 119));
 F135(F601(42, 115, 112, 97, 105, 45, 105, 116, 45, 97, 103, 114, 101, 101, 45, 116, 101, 114, 109, 115, 42));
 F135(F601(42, 115, 112, 97, 105, 45, 101, 110, 45, 97, 103, 114, 101, 101, 45, 116, 101, 114, 109, 115, 42));
 F135(F492(108, 86, 101, 99, 116));
 F135(F752(108, 118, 101, 99, 116, 67, 117, 114));
 F135(F601(42, 102, 111, 114, 45, 97, 117, 120, 45, 118, 101, 99, 116, 45, 111, 102, 102, 115, 101, 116, 42));
 F135(F455(115, 116, 114, 101, 97, 109, 79, 102, 102, 115, 101, 116));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 70, 76, 65, 71, 82, 86, 69, 67, 84));
 F135(F601(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 45, 115, 105, 122, 101, 95, 82, 86, 69, 67, 84));
 F135(F460(115, 116, 114, 101, 97, 109, 95, 115, 101, 114, 105, 97, 108, 63, 95, 82, 86, 69, 67, 84));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 97, 117, 120, 95, 82, 86, 69, 67, 84));
 F135(F595(115, 116, 114, 101, 97, 109, 95, 97, 117, 120, 50, 95, 82, 86, 69, 67, 84));
 F135(F460(115, 116, 114, 101, 97, 109, 95, 102, 105, 108, 108, 45, 102, 110, 95, 82, 86, 69, 67, 84));
 F135(F601(115, 116, 114, 101, 97, 109, 95, 102, 108, 117, 115, 104, 45, 102, 110, 95, 82, 86, 69, 67, 84));
 F135(F460(115, 116, 114, 101, 97, 109, 95, 112, 101, 101, 107, 45, 102, 110, 95, 82, 86, 69, 67, 84));
 F135(F605(115, 116, 114, 101, 97, 109, 95, 116, 121, 105, 45, 102, 110, 95, 82, 86, 69, 67, 84));
 F135(F605(115, 116, 114, 101, 97, 109, 95, 116, 121, 111, 45, 102, 110, 95, 82, 86, 69, 67, 84));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 109, 105, 110, 95, 82, 86, 69, 67, 84));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 109, 97, 120, 95, 82, 86, 69, 67, 84));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 101, 110, 100, 95, 82, 86, 69, 67, 84));
 F135(F505(115, 116, 114, 101, 97, 109, 95, 112, 111, 115, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 115, 101, 114, 105, 97, 108, 45, 112, 111, 115, 95, 82, 86, 69, 67, 84));
 F135(F691(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 45, 105, 110, 100, 101, 120, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 105, 110, 45, 98, 117, 102, 45, 109, 105, 110, 95, 82, 86, 69, 67, 84));
 F135(F460(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 45, 99, 117, 114, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 105, 110, 45, 98, 117, 102, 45, 109, 97, 120, 95, 82, 86, 69, 67, 84));
 F135(F107(115, 116, 114, 101, 97, 109, 95, 111, 117, 116, 45, 98, 117, 102, 45, 109, 97, 120, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 48, 45, 115, 116, 97, 114, 116, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 48, 45, 105, 110, 100, 101, 120, 95, 82, 86, 69, 67, 84));
 F135(F107(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 48, 45, 110, 98, 121, 116, 101, 115, 95, 82, 86, 69, 67, 84));
 F135(F691(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 48, 45, 109, 111, 100, 63, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 49, 45, 115, 116, 97, 114, 116, 95, 82, 86, 69, 67, 84));
 F135(F590(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 49, 45, 105, 110, 100, 101, 120, 95, 82, 86, 69, 67, 84));
 F135(F107(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 49, 45, 110, 98, 121, 116, 101, 115, 95, 82, 86, 69, 67, 84));
 F135(F691(115, 116, 114, 101, 97, 109, 95, 98, 117, 102, 49, 45, 109, 111, 100, 63, 95, 82, 86, 69, 67, 84));
 F135(F550(115, 116, 114, 101, 97, 109, 86, 101, 99, 116));
 F135(F505(115, 116, 114, 101, 97, 109, 86, 101, 99, 116, 111, 102, 102, 115, 101, 116));
 F135(F589(115, 116, 114, 101, 97, 109, 83, 108, 105, 99, 101, 86, 101, 99, 116));
 F135(F595(115, 116, 114, 101, 97, 109, 83, 108, 105, 99, 101, 79, 102, 102, 115, 101, 116));
 F135(F589(37, 115, 116, 114, 101, 97, 109, 83, 116, 97, 114, 116, 80, 111, 115));
 F135(F147(114, 101, 86, 101, 99, 116));
 F135(F455(114, 101, 86, 101, 99, 116, 79, 102, 102, 115, 101, 116));
 F135(F708(115, 104, 97, 114, 107, 72, 69, 65, 80));
 F135(F657(115, 104, 97, 114, 107, 65, 118, 97, 105, 108, 76, 105, 115, 116));
 F135(F504(115, 104, 97, 114, 107, 65, 118, 97, 105, 108, 83, 121, 109));
 F135(F657(115, 104, 97, 114, 107, 65, 118, 97, 105, 108, 67, 111, 110, 115));
 F135(F455(115, 104, 97, 114, 107, 72, 69, 65, 80, 77, 97, 120));
 F135(F455(115, 104, 97, 114, 107, 83, 121, 109, 84, 114, 105, 101));
 F135(F589(115, 104, 97, 114, 107, 72, 69, 65, 80, 83, 116, 97, 116, 117, 115));
 F135(F460(115, 104, 97, 114, 107, 84, 101, 109, 112, 72, 69, 65, 80, 67, 111, 117, 110, 116, 101, 114));
 F135(F460(115, 104, 97, 114, 107, 83, 116, 97, 116, 105, 99, 65, 118, 97, 105, 108, 76, 105, 115, 116));
 F135(F460(115, 104, 97, 114, 107, 83, 116, 97, 116, 105, 99, 65, 118, 97, 105, 108, 67, 111, 110, 115));
 F135(F484(115, 104, 97, 114, 107, 84, 101, 109, 112, 65, 118, 97, 105, 108, 76, 105, 115, 116));
 F135(F484(115, 104, 97, 114, 107, 84, 101, 109, 112, 65, 118, 97, 105, 108, 67, 111, 110, 115));
 F135(F708(115, 116, 114, 105, 110, 103, 66, 117, 102));
 F135(F550(115, 116, 114, 105, 110, 103, 66, 117, 102, 50));
 F135(F708(115, 104, 97, 114, 107, 67, 66, 117, 102));
 F135(F550(115, 104, 97, 114, 107, 67, 66, 117, 102, 50));
 F135(F504(115, 104, 97, 114, 107, 70, 105, 108, 101, 86, 101, 99, 116));
 F135(F615(116, 99, 112, 83, 111, 99, 107, 86, 101, 99, 116));
 F135(F455(116, 99, 112, 73, 110, 66, 117, 102, 86, 101, 99, 116));
 F135(F589(116, 99, 112, 73, 110, 79, 102, 102, 115, 101, 116, 86, 101, 99, 116));
 F135(F589(116, 99, 112, 73, 110, 78, 98, 121, 116, 101, 115, 86, 101, 99, 116));
 F135(F504(116, 99, 112, 79, 117, 116, 66, 117, 102, 86, 101, 99, 116));
 F135(F505(116, 99, 112, 79, 117, 116, 79, 102, 102, 115, 101, 116, 86, 101, 99, 116));
 _SH[65536]= _c(_c(66573, _c(_c(66548, _c(66382, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66380, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66379, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66378, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66377, nil)), _c(_c(66567, _c(66562, nil)), _c(_c(66549, _c(66376, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66375, nil)), _c(_c(66567, _c(66562, nil)), _c(_c(66549, _c(66376, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66374, nil)), _c(_c(66567, _c(66562, nil)), _c(_c(66549, _c(66376, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66373, nil)), _c(_c(66567, _c(66562, nil)), _c(_c(66549, _c(66376, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66372, nil)), _c(_c(66567, _c(66562, nil)), _c(_c(66549, _c(66376, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66750, nil)), _c(_c(66567, _c(66371, nil)), _c(_c(66549, _c(66370, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66369, nil)), _c(_c(66567, _c(66371, nil)), _c(_c(66549, _c(66370, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66625, nil)), _c(_c(66567, _c(66371, nil)), _c(_c(66549, _c(66370, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66368, nil)), _c(_c(66567, _c(66371, nil)), _c(_c(66549, _c(66370, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66367, nil)), _c(_c(66567, _c(66371, nil)), _c(_c(66549, _c(66370, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66366, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66365, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66364, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66363, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66362, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66361, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66360, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66359, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66358, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66357, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66356, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66355, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), _c(_c(66573, _c(_c(66548, _c(66354, nil)), _c(_c(66567, _c(66551, nil)), _c(_c(66549, _c(66381, nil)), nil)))), nil)))))))))))))))))))))))))));
 _SH[65537]= F492(115, 104, 97, 114, 107);
 _SH[65538]= F84(115, 101, 116);
 _SH[65539]= F262(34);
 _SH[65540]= F262(34);
 _SH[65541]= F50(67, 79, 80, 89);
 _SH[65542]= _c(66413, _c(66407, nil));
 _SH[65543]= F92(80, 105, 112, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65544]= F550(97, 117, 120, 84, 101, 115, 116, 86, 97, 114);
 _SH[65545]= F589(66, 97, 100, 32, 100, 115, 116, 32, 112, 97, 116, 116, 101, 114, 110);
 _SH[65546]= F504(66, 97, 100, 32, 100, 101, 108, 105, 109, 105, 116, 101, 114);
 _SH[65547]= F550(48, 49, 50, 51, 52, 53, 54, 55, 56, 57);
 _SH[65548]= F505(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70);
 _SH[65549]= F657(35, 124, 92, 59, 39, 96, 44, 34, 40, 41, 91, 93, 123, 125);
 _SH[65550]= F592(85, 43);
 _SH[65551]= F592(85, 43);
 _SH[65552]= F84(45, 111, 110);
 _SH[65553]= F595(102, 111, 114, 32, 97, 117, 120, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65554]= F595(102, 111, 114, 32, 97, 117, 120, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65555]= F595(102, 111, 114, 32, 97, 117, 120, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65556]= F595(102, 111, 114, 32, 97, 117, 120, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65557]= F595(102, 111, 114, 32, 97, 117, 120, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65558]= F147(45, 108, 101, 118, 101, 108);
 _SH[65559]= F691(42, 42, 42, 42, 42, 42, 42, 32, 101, 114, 114, 111, 114, 101, 32, 42, 42, 42, 42, 42, 42, 42);
 _SH[65560]= F147(45, 105, 110, 100, 101, 120);
 _SH[65561]= F50(102, 111, 114, 45);
 _SH[65562]= F147(45, 105, 115, 112, 101, 99);
 _SH[65563]= F504(66, 97, 100, 32, 118, 114, 101, 99, 32, 115, 112, 101, 99);
 _SH[65564]= F657(118, 114, 101, 99, 45, 118, 101, 99, 116, 32, 70, 85, 76, 76);
 _SH[65565]= F550(95, 70, 76, 65, 71, 82, 86, 69, 67, 84);
 _SH[65566]= F262(95);
 _SH[65567]= F147(95, 82, 86, 69, 67, 84);
 _SH[65568]= F262(37);
 _SH[65569]= F262(42);
 _SH[65570]= F492(45, 114, 103, 101, 116);
 _SH[65571]= F492(45, 114, 115, 101, 116);
 _SH[65572]= F492(119, 105, 116, 104, 45);
 _SH[65573]= F50(45, 114, 101, 99);
 _SH[65574]= F147(45, 114, 109, 97, 107, 101);
 _SH[65575]= F545(119, 105, 110, 100, 111, 119, 115);
 _SH[65576]= F592(13, 10);
 _SH[65577]= _c(42, F215(42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 32, 85, 110, 107, 110, 111, 119, 110, 32, 111, 115, 32, 116, 121, 112, 101, 58, 32));
 _SH[65578]= F84(77, 67, 76);
 _SH[65579]= F50(117, 110, 105, 120);
 _SH[65580]= F492(108, 105, 110, 117, 120);
 _SH[65581]= F84(111, 115, 120);
 _SH[65582]= F545(119, 105, 110, 100, 111, 119, 115);
 _SH[65583]= F595(85, 110, 107, 110, 111, 119, 110, 32, 111, 115, 32, 116, 121, 112, 101, 58, 32);
 _SH[65584]= F262(47);
 _SH[65585]= F262(47);
 _SH[65586]= F262(47);
 _SH[65587]= F504(32, 105, 115, 32, 110, 111, 116, 32, 97, 32, 100, 105, 114);
 _SH[65588]= F262(47);
 _SH[65589]= F262(47);
 _SH[65590]= F262(46);
 _SH[65591]= F592(46, 46);
 _SH[65592]= F262(47);
 _SH[65593]= F262(47);
 _SH[65594]= F262(47);
 _SH[65595]= F545(109, 97, 107, 105, 110, 103, 32);
 _SH[65596]= F545(77, 97, 107, 105, 110, 103, 32);
 _SH[65597]= F589(115, 116, 114, 101, 97, 109, 32, 98, 117, 102, 32, 102, 117, 108, 108);
 _SH[65598]= F505(115, 116, 114, 101, 97, 109, 32, 118, 101, 99, 116, 32, 102, 117, 108, 108);
 _SH[65599]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65600]= F262(32);
 _SH[65601]= F262(44);
 _SH[65602]= F592(44, 64);
 _SH[65603]= F262(42);
 _SH[65604]= F262(45);
 _SH[65605]= F262(42);
 _SH[65606]= F590(117, 110, 109, 97, 116, 99, 104, 101, 100, 45, 99, 108, 111, 115, 101, 100, 45, 112, 97, 114, 101, 110, 115);
 _SH[65607]= F601(98, 97, 100, 45, 104, 97, 115, 104, 45, 109, 97, 114, 107, 45, 99, 111, 109, 109, 101, 110, 116);
 _SH[65608]= F92(117, 110, 99, 108, 111, 115, 101, 100, 45, 104, 97, 115, 104, 45, 109, 97, 114, 107, 45, 99, 111, 109, 109, 101, 110, 116);
 _SH[65609]= F92(117, 110, 99, 108, 111, 115, 101, 100, 45, 104, 97, 115, 104, 45, 109, 97, 114, 107, 45, 99, 111, 109, 109, 101, 110, 116);
 _SH[65610]= F504(66, 97, 100, 32, 99, 115, 121, 109, 45, 110, 97, 109, 101);
 _SH[65611]= F504(66, 97, 100, 32, 99, 115, 121, 109, 45, 110, 97, 109, 101);
 _SH[65612]= F708(98, 97, 100, 45, 99, 111, 109, 109, 97);
 _SH[65613]= F224(117, 110, 109, 97, 116, 99, 104, 101, 100, 32, 108, 105, 115, 116, 32, 101, 120, 112, 114, 101, 115, 115, 105, 111, 110);
 _SH[65614]= F505(117, 110, 109, 97, 116, 99, 104, 101, 100, 45, 115, 116, 114, 105, 110, 103);
 _SH[65615]= F84(110, 105, 108);
 _SH[65616]= F84(78, 73, 76);
 _SH[65617]= F550(42, 42, 42, 42, 42, 42, 42, 42, 42, 32);
 _SH[65618]= F592(46, 46);
 _SH[65619]= F84(78, 73, 76);
 _SH[65620]= F262(39);
 _SH[65621]= F262(34);
 _SH[65622]= F50(46, 46, 46, 46);
 _SH[65623]= F262(34);
 _SH[65624]= F492(91, 46, 46, 46, 93);
 _SH[65625]= F262(91);
 _SH[65626]= F84(46, 46, 46);
 _SH[65627]= F262(93);
 _SH[65628]= F492(40, 46, 46, 46, 41);
 _SH[65629]= F262(40);
 _SH[65630]= F84(46, 46, 46);
 _SH[65631]= F262(41);
 _SH[65632]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65633]= F550(98, 97, 100, 32, 114, 101, 103, 101, 120, 32);
 _SH[65634]= _c(66649, _c(66650, _c(66651, _c(66652, _c(66653, _c(66654, _c(66655, _c(66656, _c(66657, _c(66519, nil))))))))));
 _SH[65635]= _c(66668, _c(66669, _c(66670, _c(66671, _c(66672, nil)))));
 _SH[65636]= _c(66678, _c(66679, _c(66680, _c(66681, _c(66682, nil)))));
 _SH[65637]= _c(66688, _c(66689, _c(66690, _c(66691, _c(66692, _c(66693, _c(66694, _c(66695, _c(66696, _c(66697, nil))))))))));
 _SH[65638]= F147(112, 97, 114, 115, 101, 45);
 _SH[65639]= _c(66715, _c(66714, _c(66716, _c(66717, _c(66718, _c(66719, nil))))));
 _SH[65640]= _c(66720, _c(66721, _c(66722, _c(66723, _c(66724, _c(66725, _c(66726, _c(66727, _c(66728, _c(66729, _c(66730, _c(66731, _c(66732, nil)))))))))))));
 _SH[65641]= _c(66733, _c(66734, _c(66735, _c(66736, _c(66737, _c(66738, _c(66482, _c(66494, _c(66405, _c(66648, nil))))))))));
 _SH[65642]= F504(66, 97, 100, 32, 99, 111, 110, 100, 105, 116, 105, 111, 110);
 _SH[65643]= _c(66713, _c(66708, _c(66711, _c(66712, nil))));
 _SH[65644]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65645]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65646]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65647]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65648]= _c(42, _c(42, _c(42, _c(42, _c(42, _c(42, _c(42, F215(32, 70, 67, 65, 76, 76, 32, 101, 114, 114, 111, 114, 58, 32, 117, 110, 107, 110, 111, 119, 110, 32, 102, 117, 110, 99, 116, 105, 111, 110, 32, 40))))))));
 _SH[65649]= F262(41);
 _SH[65650]= F460(32, 111, 114, 32, 98, 97, 100, 32, 97, 114, 103, 32, 110, 117, 109, 98, 101, 114, 32, 40);
 _SH[65651]= F262(41);
 _SH[65652]= F752(32, 42, 42, 42, 42, 42, 42, 42);
 _SH[65653]= F147(105, 110, 100, 101, 120, 32);
 _SH[65654]= F455(32, 109, 117, 115, 116, 32, 98, 101, 32, 60, 32, 32);
 _SH[65655]= F147(108, 115, 32, 45, 108, 32);
 _SH[65656]= F262(32);
 _SH[65657]= F262(32);
 _SH[65658]= F262(32);
 _SH[65659]= F147(109, 107, 100, 105, 114, 32);
 _SH[65660]= F147(116, 111, 117, 99, 104, 32);
 _SH[65661]= F545(108, 115, 32, 45, 49, 70, 32);
 _SH[65662]= F262(47);
 _SH[65663]= F545(108, 115, 32, 45, 49, 70, 32);
 _SH[65664]= F262(47);
 _SH[65665]= F84(99, 112, 32);
 _SH[65666]= F262(32);
 _SH[65667]= F147(47, 42, 64, 37, 61, 124);
 _SH[65668]= _c(F147(97, 98, 111, 97, 114, 100), _c(F492(97, 98, 111, 117, 116), _c(F492(97, 98, 111, 118, 101), _c(F147(97, 98, 115, 101, 110, 116), _c(F147(97, 99, 114, 111, 115, 115), _c(F492(97, 102, 116, 101, 114), _c(F545(97, 103, 97, 105, 110, 115, 116), _c(F492(97, 108, 111, 110, 103), _c(F708(97, 108, 111, 110, 103, 115, 105, 100, 101), _c(F50(97, 109, 105, 100), _c(F147(97, 109, 105, 100, 115, 116), _c(F492(97, 109, 111, 110, 103), _c(F545(97, 109, 111, 110, 103, 115, 116), _c(F147(97, 114, 111, 117, 110, 100), _c(F592(97, 115), _c(F545(97, 115, 116, 114, 105, 100, 101), _c(F592(97, 116), _c(F50(97, 116, 111, 112), _c(F147(98, 101, 102, 111, 114, 101), _c(F147(98, 101, 104, 105, 110, 100), _c(F492(98, 101, 108, 111, 119), _c(F545(98, 101, 110, 101, 97, 116, 104), _c(F147(98, 101, 115, 105, 100, 101), _c(F545(98, 101, 115, 105, 100, 101, 115), _c(F545(98, 101, 116, 119, 101, 101, 110), _c(F147(98, 101, 121, 111, 110, 100), _c(F592(98, 121), _c(F545(100, 101, 115, 112, 105, 116, 101), _c(F50(100, 111, 119, 110), _c(F147(100, 117, 114, 105, 110, 103), _c(F147(101, 120, 99, 101, 112, 116), _c(F708(102, 111, 108, 108, 111, 119, 105, 110, 103), _c(F84(102, 111, 114), _c(F50(102, 114, 111, 109), _c(F592(105, 110), _c(F147(105, 110, 115, 105, 100, 101), _c(F50(105, 110, 116, 111), _c(F50(108, 105, 107, 101), _c(F84(109, 105, 100), _c(F492(109, 105, 110, 117, 115), _c(F50(110, 101, 97, 114), _c(F545(110, 101, 97, 114, 101, 115, 116), _c(F589(110, 111, 116, 119, 105, 116, 104, 115, 116, 97, 110, 100, 105, 110, 103), _c(F592(111, 102), _c(F84(111, 102, 102), _c(F592(111, 110), _c(F50(111, 110, 116, 111), _c(F752(111, 112, 112, 111, 115, 105, 116, 101), _c(F84(111, 117, 116), _c(F545(111, 117, 116, 115, 105, 100, 101), _c(F50(111, 118, 101, 114), _c(F50(112, 97, 115, 116), _c(F592(114, 101), _c(F492(114, 111, 117, 110, 100), _c(F492(115, 105, 110, 99, 101), _c(F545(116, 104, 114, 111, 117, 103, 104), _c(F550(116, 104, 114, 111, 117, 103, 104, 111, 117, 116), _c(F50(116, 105, 108, 108), _c(F592(116, 111), _c(F147(116, 111, 119, 97, 114, 100), _c(F545(116, 111, 119, 97, 114, 100, 115), _c(F492(117, 110, 100, 101, 114), _c(F550(117, 110, 100, 101, 114, 110, 101, 97, 116, 104), _c(F147(117, 110, 108, 105, 107, 101), _c(F492(117, 110, 116, 105, 108), _c(F592(117, 112), _c(F50(117, 112, 111, 110), _c(F84(118, 105, 97), _c(F50(119, 105, 116, 104), _c(F147(119, 105, 116, 104, 105, 110), _c(F545(119, 105, 116, 104, 111, 117, 116), nil)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65669]= _c(F84(116, 104, 101), _c(F262(97), _c(F592(97, 110), nil)));
 _SH[65670]= _c(F592(98, 101), _c(F592(97, 109), _c(F84(97, 114, 101), _c(F592(105, 115), _c(F84(119, 97, 115), _c(F50(119, 101, 114, 101), _c(F50(98, 101, 101, 110), _c(F492(98, 101, 105, 110, 103), _c(F50(104, 97, 118, 101), _c(F84(104, 97, 115), _c(F84(104, 97, 100), _c(F147(104, 97, 118, 105, 110, 103), _c(F84(109, 97, 121), _c(F492(109, 105, 103, 104, 116), _c(F84(99, 97, 110), _c(F492(99, 111, 117, 108, 100), _c(F492(111, 117, 103, 104, 116), _c(F50(109, 117, 115, 116), _c(F50(119, 105, 108, 108), _c(F492(115, 104, 97, 108, 108), _c(F147(115, 104, 111, 117, 108, 100), nil)))))))))))))))))))));
 _SH[65671]= _c(F592(97, 99), _c(F492(97, 102, 102, 111, 114), _c(F492(97, 102, 116, 101, 114), _c(F545(97, 103, 97, 105, 110, 115, 116), _c(F592(97, 108), _c(F50(97, 108, 98, 101), _c(F147(97, 108, 98, 101, 105, 116), _c(F752(97, 108, 98, 101, 116, 104, 101, 121), _c(F708(97, 108, 108, 116, 104, 111, 117, 103, 104), _c(F492(97, 108, 116, 104, 111), _c(F147(97, 108, 116, 104, 111, 39), _c(F752(97, 108, 116, 104, 111, 117, 103, 104), _c(F592(97, 110), _c(F84(97, 110, 100), _c(F147(97, 110, 100, 47, 111, 114), _c(F50(97, 110, 100, 116), _c(F592(97, 115), _c(F545(98, 101, 99, 97, 117, 115, 101), _c(F147(98, 101, 102, 111, 114, 101), _c(F492(98, 101, 105, 110, 103), _c(F545(98, 105, 99, 97, 117, 115, 101), _c(F50(98, 111, 116, 104), _c(F492(98, 111, 116, 104, 101), _c(F84(98, 117, 116), _c(F545(98, 121, 99, 97, 117, 115, 101), _c(F147(99, 104, 111, 111, 115, 101), _c(F84(99, 111, 115), _c(F84(99, 111, 122), _c(F84(99, 117, 109), _c(F84(99, 117, 115), _c(F84(99, 117, 122), _c(F84(100, 97, 116), _c(F752(100, 105, 114, 101, 99, 116, 108, 121), _c(F147(101, 105, 116, 104, 101, 114), _c(F50(101, 108, 115, 101), _c(F752(101, 110, 97, 117, 110, 116, 101, 114), _c(F50(101, 114, 103, 111), _c(F592(101, 116), _c(F147(101, 120, 99, 101, 112, 116), _c(F84(102, 111, 114), _c(F752(102, 111, 114, 45, 116, 104, 97, 110), _c(F752(102, 111, 114, 99, 97, 117, 115, 101), _c(F147(102, 111, 114, 116, 104, 121), _c(F147(102, 111, 114, 119, 104, 121), _c(F84(104, 111, 119), _c(F545(104, 111, 119, 98, 101, 105, 116), _c(F545(104, 111, 119, 101, 118, 101, 114), _c(F550(104, 111, 119, 115, 111, 109, 101, 118, 101, 114), _c(F147(104, 121, 112, 104, 101, 110), _c(F592(105, 102), _c(F50(105, 102, 101, 110), _c(F492(105, 102, 102, 101, 110), _c(F615(105, 109, 109, 101, 100, 105, 97, 116, 101, 108, 121), _c(F752(105, 110, 97, 115, 109, 117, 99, 104), _c(F147(107, 105, 110, 100, 111, 102), _c(F147(108, 101, 115, 115, 39, 110), _c(F50(108, 101, 115, 116), _c(F50(108, 105, 107, 101), _c(F492(109, 105, 110, 117, 115), _c(F592(110, 39), _c(F147(110, 97, 116, 104, 101, 114), _c(F84(110, 97, 121), _c(F592(110, 100), _c(F592(110, 101), _c(F545(110, 101, 105, 116, 104, 101, 114), _c(F84(110, 111, 114), _c(F84(110, 111, 116), _c(F589(110, 111, 116, 119, 105, 116, 104, 115, 116, 97, 110, 100, 105, 110, 103), _c(F605(110, 111, 117, 103, 104, 116, 45, 119, 105, 116, 104, 115, 116, 97, 110, 100, 105, 110, 103), _c(F484(110, 111, 117, 103, 104, 116, 119, 105, 116, 104, 115, 116, 97, 110, 100, 105, 110, 103), _c(F84(110, 111, 119), _c(F50(111, 110, 99, 101), _c(F50(111, 110, 108, 121), _c(F592(111, 114), _c(F592(79, 82), _c(F492(111, 116, 104, 101, 114), _c(F545(111, 117, 116, 99, 101, 112, 116), _c(F147(111, 117, 116, 104, 101, 114), _c(F545(111, 117, 116, 115, 116, 101, 112), _c(F50(112, 108, 117, 115), _c(F752(112, 114, 111, 118, 105, 100, 101, 100), _c(F50(115, 97, 118, 101), _c(F147(115, 101, 101, 105, 110, 103), _c(F492(115, 105, 110, 99, 101), _c(F752(115, 105, 116, 104, 101, 110, 99, 101), _c(F492(115, 108, 97, 115, 104), _c(F592(115, 111), _c(F50(116, 104, 97, 110), _c(F147(116, 104, 97, 110, 110, 101), _c(F50(116, 104, 97, 116), _c(F492(116, 104, 97, 116, 116), _c(F545(116, 104, 101, 114, 101, 97, 115), _c(F752(116, 104, 101, 114, 102, 111, 114, 101), _c(F84(116, 104, 111), _c(F50(116, 104, 111, 39), _c(F147(116, 104, 111, 98, 117, 116), _c(F50(116, 104, 111, 102), _c(F147(116, 104, 111, 117, 103, 104), _c(F84(116, 104, 121), _c(F84(116, 105, 108), _c(F50(116, 105, 108, 108), _c(F708(116, 111, 45, 119, 104, 105, 108, 101, 115), _c(F147(116, 111, 102, 111, 114, 101), _c(F492(116, 119, 101, 108, 108), _c(F147(117, 110, 108, 101, 115, 115), _c(F545(117, 110, 108, 101, 115, 115, 101), _c(F492(117, 110, 116, 105, 108), _c(F50(117, 110, 116, 111), _c(F147(118, 101, 114, 115, 117, 115), _c(F84(118, 105, 100), _c(F147(119, 104, 97, 116, 39, 115), _c(F50(119, 104, 101, 110), _c(F147(119, 104, 101, 110, 97, 115), _c(F147(119, 104, 101, 110, 99, 101), _c(F492(119, 104, 101, 114, 101), _c(F550(119, 104, 101, 114, 101, 97, 108, 111, 110, 103), _c(F545(119, 104, 101, 114, 101, 97, 115), _c(F545(119, 104, 101, 114, 101, 97, 116), _c(F708(119, 104, 101, 114, 101, 102, 111, 114, 101), _c(F545(119, 104, 101, 114, 101, 105, 110), _c(F504(119, 104, 101, 114, 101, 105, 110, 98, 101, 102, 111, 114, 101), _c(F545(119, 104, 101, 114, 101, 111, 102), _c(F752(119, 104, 101, 114, 101, 111, 117, 116), _c(F708(119, 104, 101, 114, 101, 111, 118, 101, 114), _c(F615(119, 104, 101, 114, 101, 115, 111, 101, 39, 101, 114), _c(F550(119, 104, 101, 114, 101, 115, 111, 101, 101, 114), _c(F615(119, 104, 101, 114, 101, 115, 111, 101, 118, 101, 114), _c(F708(119, 104, 101, 114, 101, 116, 104, 97, 110), _c(F615(119, 104, 101, 114, 101, 116, 111, 119, 97, 114, 100), _c(F708(119, 104, 101, 114, 101, 117, 110, 116, 111), _c(F708(119, 104, 101, 114, 101, 117, 112, 111, 110), _c(F752(119, 104, 101, 114, 101, 118, 101, 114), _c(F545(119, 104, 101, 116, 104, 101, 114), _c(F504(119, 104, 101, 116, 104, 101, 114, 115, 111, 101, 118, 101, 114), _c(F492(119, 104, 105, 108, 101), _c(F147(119, 104, 105, 108, 101, 115), _c(F545(119, 104, 105, 108, 101, 115, 116), _c(F147(119, 104, 105, 108, 111, 109), _c(F545(119, 104, 105, 108, 111, 109, 101), _c(F147(119, 104, 105, 108, 115, 116), _c(F545(119, 104, 105, 116, 104, 101, 114), _c(F84(119, 104, 121), _c(F492(119, 104, 121, 108, 101), _c(F545(119, 104, 121, 108, 101, 115, 116), _c(F147(119, 104, 121, 108, 115, 116), _c(F545(119, 105, 116, 104, 111, 117, 116), _c(F545(119, 121, 116, 104, 111, 119, 116), _c(F752(119, 121, 116, 104, 111, 119, 116, 101), _c(F84(120, 111, 114), _c(F84(121, 101, 116), _c(F84(121, 105, 102), nil)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65672]= _c(F50(116, 104, 105, 115), _c(F50(116, 104, 97, 116), _c(F492(116, 104, 101, 115, 101), _c(F492(116, 104, 111, 115, 101), nil))));
 _SH[65673]= _c(F592(109, 121), _c(F50(121, 111, 117, 114), _c(F84(104, 105, 115), _c(F84(104, 101, 114), _c(F84(105, 116, 115), _c(F84(111, 117, 114), _c(F50(121, 111, 117, 114), _c(F492(116, 104, 101, 105, 114), nil))))))));
 _SH[65674]= _c(F84(119, 104, 111), _c(F50(119, 104, 111, 109), _c(F492(119, 104, 111, 115, 101), _c(F492(119, 104, 105, 99, 104), _c(F492(119, 104, 105, 99, 104), _c(F492(119, 104, 111, 115, 101), _c(F50(116, 104, 97, 116), nil)))))));
 _SH[65675]= _c(F545(97, 110, 121, 98, 111, 100, 121), _c(F147(97, 110, 121, 111, 110, 101), _c(F752(97, 110, 121, 116, 104, 105, 110, 103), _c(F708(101, 118, 101, 114, 121, 98, 111, 100, 121), _c(F752(101, 118, 101, 114, 121, 111, 110, 101), _c(F550(101, 118, 101, 114, 121, 116, 104, 105, 110, 103), _c(F147(110, 111, 98, 111, 100, 121), _c(F50(110, 111, 110, 101), _c(F592(110, 111), _c(F84(111, 110, 101), _c(F545(110, 111, 116, 104, 105, 110, 103), _c(F752(115, 111, 109, 101, 98, 111, 100, 121), _c(F545(115, 111, 109, 101, 111, 110, 101), _c(F708(115, 111, 109, 101, 116, 104, 105, 110, 103), _c(F84(97, 108, 108), _c(F545(97, 110, 111, 116, 104, 101, 114), _c(F84(97, 110, 121), _c(F50(98, 111, 116, 104), _c(F50(101, 97, 99, 104), _c(F147(101, 105, 116, 104, 101, 114), _c(F84(102, 101, 119), _c(F50(109, 97, 110, 121), _c(F545(110, 101, 105, 116, 104, 101, 114), _c(F84(111, 110, 101), _c(F50(115, 111, 109, 101), _c(F545(115, 101, 118, 101, 114, 97, 108), nil))))))))))))))))))))))))));
 _SH[65676]= _c(F262(105), _c(F592(104, 101), _c(F84(104, 101, 114), _c(F84(104, 105, 109), _c(F592(105, 116), _c(F592(109, 101), _c(F84(115, 104, 101), _c(F50(116, 104, 101, 101), _c(F50(116, 104, 101, 109), _c(F50(116, 104, 101, 121), _c(F50(116, 104, 111, 117), _c(F592(117, 115), _c(F592(119, 101), _c(F592(121, 101), _c(F84(121, 111, 117), nil)))))))))))))));
 _SH[65677]= _c(F50(109, 105, 110, 101), _c(F492(121, 111, 117, 114, 115), _c(F84(104, 105, 115), _c(F50(104, 101, 114, 115), _c(F84(105, 116, 115), _c(F50(111, 117, 114, 115), _c(F492(121, 111, 117, 114, 115), _c(F147(116, 104, 101, 105, 114, 115), nil))))))));
 _SH[65678]= _c(F592(100, 105), _c(F262(97), _c(F592(100, 97), _c(F592(105, 110), _c(F84(99, 111, 110), _c(F592(115, 117), _c(F84(112, 101, 114), _c(F84(102, 114, 97), _c(F84(116, 114, 97), _c(F262(101), _c(F592(97, 100), nil)))))))))));
 _SH[65679]= _c(F592(105, 108), _c(F262(108), _c(F592(108, 111), _c(F592(108, 97), _c(F262(105), _c(F84(103, 108, 105), _c(F592(108, 101), _c(F592(100, 105), _c(F262(100), _c(F592(100, 101), _c(F84(100, 101, 108), _c(F50(100, 101, 108, 108), _c(F492(100, 101, 108, 108, 111), _c(F492(100, 101, 108, 108, 97), _c(F84(100, 101, 105), _c(F492(100, 101, 103, 108, 105), _c(F492(100, 101, 108, 108, 101), _c(F262(97), _c(F592(97, 108), _c(F84(97, 108, 108), _c(F50(97, 108, 108, 111), _c(F50(97, 108, 108, 97), _c(F592(97, 105), _c(F50(97, 103, 108, 105), _c(F50(97, 108, 108, 101), _c(F592(100, 97), _c(F84(100, 97, 108), _c(F50(100, 97, 108, 108), _c(F492(100, 97, 108, 108, 111), _c(F492(100, 97, 108, 108, 97), _c(F84(100, 97, 105), _c(F492(100, 97, 103, 108, 105), _c(F492(100, 97, 108, 108, 101), _c(F592(105, 110), _c(F84(110, 101, 108), _c(F50(110, 101, 108, 108), _c(F492(110, 101, 108, 108, 111), _c(F492(110, 101, 108, 108, 97), _c(F84(110, 101, 105), _c(F492(110, 101, 103, 108, 105), _c(F492(110, 101, 108, 108, 101), _c(F592(115, 117), _c(F84(115, 117, 108), _c(F50(115, 117, 108, 108), _c(F492(115, 117, 108, 108, 111), _c(F492(115, 117, 108, 108, 97), _c(F84(115, 117, 105), _c(F492(115, 117, 103, 108, 105), _c(F492(115, 117, 108, 108, 101), _c(F84(99, 111, 110), _c(F84(99, 111, 108), _c(F50(99, 111, 108, 108), _c(F492(99, 111, 108, 108, 111), _c(F492(99, 111, 108, 108, 97), _c(F84(99, 111, 105), _c(F492(99, 111, 103, 108, 105), _c(F492(99, 111, 108, 108, 101), nil)))))))))))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65680]= _c(F592(108, 111), _c(F592(105, 108), _c(F592(108, 97), _c(F84(103, 108, 105), _c(F262(105), _c(F592(108, 101), _c(F84(117, 110, 111), _c(F592(117, 110), _c(F84(117, 110, 97), nil)))))))));
 _SH[65681]= _c(F752(97, 108, 108, 111, 114, 99, 104, 101), _c(F545(103, 105, 97, 99, 99, 104, 101), _c(F147(101, 112, 112, 117, 114, 101), _c(F545(100, 111, 112, 111, 99, 104, 101), _c(F592(110, 101), _c(F545(105, 110, 111, 108, 116, 114, 101), _c(F84(115, 105, 97), _c(F84(99, 104, 101), _c(F262(101), _c(F492(97, 110, 99, 104, 101), _c(F545(110, 101, 97, 110, 99, 104, 101), _c(F545(110, 101, 112, 112, 117, 114, 101), _c(F262(111), _c(F147(111, 118, 118, 101, 114, 111), _c(F147(111, 112, 112, 117, 114, 101), _c(F592(109, 97), _c(F50(112, 101, 114, 111), _c(F752(116, 117, 116, 116, 97, 118, 105, 97), _c(F50(97, 110, 122, 105), _c(F545(105, 110, 102, 97, 116, 116, 105), _c(F50(99, 105, 111, 232), _c(F492(111, 115, 115, 105, 97), _c(F147(100, 117, 110, 113, 117, 101), _c(F147(113, 117, 105, 110, 100, 105), _c(F147(112, 101, 114, 99, 105, 111), _c(F147(113, 117, 97, 110, 100, 111), _c(F147(109, 101, 110, 116, 114, 101), _c(F147(102, 105, 110, 99, 104, 101), _c(F752(97, 102, 102, 105, 110, 99, 104, 101), _c(F147(112, 111, 105, 99, 104, 101), _c(F545(115, 105, 99, 99, 111, 109, 101), _c(F545(115, 101, 98, 98, 101, 110, 101), _c(F550(113, 117, 97, 110, 116, 117, 110, 113, 117, 101), _c(F147(112, 117, 114, 99, 104, 101), _c(F545(113, 117, 97, 108, 111, 114, 97), _c(F147(112, 101, 114, 99, 104, 101), _c(F50(99, 111, 109, 101), _c(F592(115, 101), _c(F545(102, 117, 111, 114, 99, 104, 101), nil)))))))))))))))))))))))))))))))))))))));
 _SH[65682]= _c(F592(105, 111), _c(F592(109, 101), _c(F592(109, 105), _c(F592(116, 117), _c(F592(116, 101), _c(F592(116, 105), _c(F50(101, 103, 108, 105), _c(F50(101, 115, 115, 111), _c(F84(108, 117, 105), _c(F592(108, 111), _c(F84(103, 108, 105), _c(F50(101, 108, 108, 97), _c(F50(101, 115, 115, 97), _c(F84(108, 101, 105), _c(F592(108, 97), _c(F84(110, 111, 105), _c(F592(99, 105), _c(F84(118, 111, 105), _c(F592(118, 105), _c(F50(101, 115, 115, 105), _c(F592(108, 105), _c(F50(101, 115, 115, 101), _c(F50(108, 111, 114, 111), _c(F592(115, 101), _c(F592(108, 101), _c(F592(110, 101), _c(F592(115, 105), nil)))))))))))))))))))))))))));
 _SH[65683]= _c(F84(99, 104, 101), _c(F84(99, 117, 105), _c(F492(113, 117, 97, 108, 101), _c(F492(113, 117, 97, 108, 105), _c(F84(99, 104, 105), nil)))));
 _SH[65684]= _c(F84(109, 105, 111), _c(F84(109, 105, 97), _c(F50(109, 105, 101, 105), _c(F84(109, 105, 101), _c(F84(116, 117, 111), _c(F84(116, 117, 97), _c(F50(116, 117, 111, 105), _c(F84(116, 117, 101), _c(F84(115, 117, 111), _c(F84(115, 117, 97), _c(F50(115, 117, 111, 105), _c(F84(115, 117, 101), _c(F147(110, 111, 115, 116, 114, 111), _c(F147(110, 111, 115, 116, 114, 97), _c(F147(110, 111, 115, 116, 114, 105), _c(F147(110, 111, 115, 116, 114, 101), _c(F147(118, 111, 115, 116, 114, 111), _c(F147(118, 111, 115, 116, 114, 97), _c(F147(118, 111, 115, 116, 114, 105), _c(F147(118, 111, 115, 116, 114, 101), _c(F50(108, 111, 114, 111), nil)))))))))))))))))))));
 _SH[65685]= _c(F147(113, 117, 101, 115, 116, 111), _c(F545(99, 111, 100, 101, 115, 116, 111), _c(F147(113, 117, 101, 108, 108, 111), _c(F147(115, 116, 101, 115, 115, 111), _c(F752(109, 101, 100, 101, 115, 105, 109, 111), _c(F147(113, 117, 101, 115, 116, 105), _c(F147(113, 117, 101, 103, 108, 105), _c(F147(99, 111, 115, 116, 117, 105), _c(F147(99, 111, 115, 116, 101, 105), _c(F545(99, 111, 115, 116, 111, 114, 111), _c(F492(99, 111, 108, 117, 105), _c(F492(99, 111, 108, 101, 105), _c(F147(99, 111, 108, 111, 114, 111), _c(F84(99, 105, 111), _c(F592(110, 101), _c(F592(108, 111), _c(F592(99, 105), _c(F84(117, 110, 111), _c(F752(99, 105, 97, 115, 99, 117, 110, 111), _c(F147(111, 103, 110, 117, 110, 111), _c(F545(110, 101, 115, 115, 117, 110, 111), _c(F147(97, 108, 99, 117, 110, 111), _c(F147(116, 97, 108, 117, 110, 111), _c(F752(113, 117, 97, 108, 99, 117, 110, 111), _c(F147(118, 101, 114, 117, 110, 111), _c(F492(97, 108, 116, 114, 111), _c(F492(97, 108, 116, 114, 105), _c(F492(99, 101, 114, 116, 105), _c(F545(99, 101, 114, 116, 117, 110, 105), _c(F545(100, 105, 118, 101, 114, 115, 105), _c(F50(116, 97, 108, 101), _c(F752(99, 104, 105, 117, 110, 113, 117, 101), _c(F455(99, 104, 105, 99, 99, 104, 101, 115, 115, 105, 97, 59), _c(F492(109, 111, 108, 116, 111), _c(F50(112, 111, 99, 111), _c(F147(116, 114, 111, 112, 112, 111), _c(F492(116, 97, 110, 116, 111), _c(F752(97, 108, 113, 117, 97, 110, 116, 111), _c(F615(97, 108, 116, 114, 101, 116, 116, 97, 110, 116, 111), _c(F708(112, 97, 114, 101, 99, 99, 104, 105, 111), _c(F492(116, 117, 116, 116, 111), _c(F492(110, 117, 108, 108, 97), _c(F147(110, 105, 101, 110, 116, 101), _c(F492(112, 117, 110, 116, 111), _c(F752(113, 117, 97, 108, 99, 111, 115, 97), _c(F545(99, 104, 101, 99, 99, 104, 101), _c(F752(97, 108, 99, 117, 110, 99, 104, 101), _c(F592(115, 105), nil))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65686]= _c(F147(97, 108, 99, 117, 110, 111), _c(F147(97, 108, 99, 117, 110, 97), _c(F147(97, 108, 99, 117, 110, 105), _c(F147(97, 108, 99, 117, 110, 101), _c(F492(97, 108, 116, 114, 111), _c(F492(97, 108, 116, 114, 97), _c(F492(97, 108, 116, 114, 105), _c(F492(97, 108, 116, 114, 101), _c(F492(99, 101, 114, 116, 111), _c(F492(99, 101, 114, 116, 97), _c(F492(99, 101, 114, 116, 105), _c(F492(99, 101, 114, 116, 101), _c(F752(99, 105, 97, 115, 99, 117, 110, 111), _c(F752(99, 105, 97, 115, 99, 117, 110, 97), _c(F545(100, 105, 118, 101, 114, 115, 111), _c(F545(100, 105, 118, 101, 114, 115, 97), _c(F545(100, 105, 118, 101, 114, 115, 105), _c(F545(100, 105, 118, 101, 114, 115, 101), _c(F492(109, 111, 108, 116, 111), _c(F492(109, 111, 108, 116, 97), _c(F492(109, 111, 108, 116, 105), _c(F492(109, 111, 108, 116, 101), _c(F545(110, 101, 115, 115, 117, 110, 111), _c(F545(110, 101, 115, 115, 117, 110, 97), _c(F708(112, 97, 114, 101, 99, 99, 104, 105, 111), _c(F708(112, 97, 114, 101, 99, 99, 104, 105, 97), _c(F752(112, 97, 114, 101, 99, 99, 104, 105), _c(F708(112, 97, 114, 101, 99, 99, 104, 105, 101), _c(F50(112, 111, 99, 111), _c(F50(112, 111, 99, 97), _c(F492(112, 111, 99, 104, 105), _c(F492(112, 111, 99, 104, 101), _c(F50(116, 97, 108, 101), _c(F50(116, 97, 108, 105), _c(F492(116, 97, 110, 116, 111), _c(F492(116, 97, 110, 116, 97), _c(F492(116, 97, 110, 116, 105), _c(F492(116, 97, 110, 116, 101), _c(F147(116, 114, 111, 112, 112, 111), _c(F147(116, 114, 111, 112, 112, 97), _c(F147(116, 114, 111, 112, 112, 105), _c(F147(116, 114, 111, 112, 112, 101), _c(F492(116, 117, 116, 116, 111), _c(F492(116, 117, 116, 116, 97), _c(F492(116, 117, 116, 116, 105), _c(F492(116, 117, 116, 116, 101), _c(F492(118, 97, 114, 105, 111), _c(F492(118, 97, 114, 105, 97), _c(F50(118, 97, 114, 105), _c(F492(118, 97, 114, 105, 101), _c(F492(110, 117, 108, 108, 97), _c(F147(110, 105, 101, 110, 116, 101), _c(F752(113, 117, 97, 108, 99, 117, 110, 111), _c(F752(113, 117, 97, 108, 99, 117, 110, 97), nil))))))))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65687]= _c(F50(113, 117, 101, 105), _c(F492(113, 117, 101, 108, 108), _c(F147(113, 117, 101, 103, 108, 105), _c(F147(113, 117, 101, 115, 116, 97), _c(F147(113, 117, 101, 115, 116, 105), _c(F147(113, 117, 101, 108, 108, 97), _c(F147(113, 117, 101, 108, 108, 105), _c(F147(113, 117, 101, 115, 116, 111), _c(F50(113, 117, 101, 108), _c(F147(113, 117, 101, 108, 108, 111), _c(F147(113, 117, 101, 115, 116, 101), _c(F147(113, 117, 101, 108, 108, 101), nil))))))))))));
 _SH[65688]= _c(F84(115, 101, 105), _c(F262(101), _c(F492(115, 105, 101, 116, 101), _c(F50(115, 111, 110, 111), _c(F84(101, 114, 111), _c(F84(101, 114, 105), _c(F84(101, 114, 97), _c(F545(101, 114, 97, 118, 97, 109, 111), _c(F545(101, 114, 97, 118, 97, 116, 101), _c(F492(101, 114, 97, 110, 111), _c(F84(102, 117, 105), _c(F492(102, 111, 115, 116, 105), _c(F592(102, 117), _c(F492(102, 117, 109, 109, 111), _c(F147(102, 117, 114, 111, 110, 111), _c(F50(115, 97, 114, 111), _c(F492(115, 97, 114, 97, 105), _c(F50(115, 97, 114, 97), _c(F147(115, 97, 114, 101, 109, 111), _c(F147(115, 97, 114, 101, 116, 101), _c(F545(115, 97, 114, 97, 110, 110, 111), _c(F492(102, 111, 115, 115, 105), _c(F492(102, 111, 115, 115, 101), _c(F545(102, 111, 115, 115, 105, 109, 111), _c(F492(102, 111, 115, 116, 101), _c(F545(102, 111, 115, 115, 101, 114, 111), _c(F492(115, 97, 114, 101, 105), _c(F545(115, 97, 114, 101, 115, 116, 105), _c(F545(115, 97, 114, 101, 98, 98, 101), _c(F545(115, 97, 114, 101, 109, 109, 111), _c(F545(115, 97, 114, 101, 115, 116, 101), _c(F708(115, 97, 114, 101, 98, 98, 101, 114, 111), _c(F492(115, 116, 97, 116, 105), _c(F84(115, 105, 105), _c(F84(115, 105, 97), _c(F492(115, 105, 97, 109, 111), _c(F492(115, 105, 97, 116, 101), _c(F492(115, 105, 97, 110, 111), _c(F147(101, 115, 115, 101, 114, 101), _c(F50(101, 110, 116, 101), _c(F545(101, 115, 115, 101, 110, 100, 111), _c(F492(115, 116, 97, 116, 111), _c(F592(104, 111), _c(F84(104, 97, 105), _c(F592(104, 97), _c(F492(97, 118, 101, 116, 101), _c(F492(104, 97, 110, 110, 111), _c(F492(97, 118, 101, 118, 111), _c(F492(97, 118, 101, 118, 105), _c(F492(97, 118, 101, 118, 97), _c(F545(97, 118, 101, 118, 97, 109, 111), _c(F545(97, 118, 101, 118, 97, 116, 101), _c(F545(97, 118, 101, 118, 97, 110, 111), _c(F50(101, 98, 98, 105), _c(F147(97, 118, 101, 115, 116, 105), _c(F50(101, 98, 98, 101), _c(F147(97, 118, 101, 109, 109, 111), _c(F147(101, 98, 98, 101, 114, 111), _c(F50(97, 118, 114, 242), _c(F492(97, 118, 114, 97, 105), _c(F50(97, 118, 114, 224), _c(F147(97, 118, 114, 101, 109, 111), _c(F147(97, 118, 114, 101, 116, 101), _c(F545(97, 118, 114, 97, 110, 110, 111), _c(F147(97, 118, 101, 115, 115, 105), _c(F147(97, 118, 101, 115, 115, 101), _c(F752(97, 118, 101, 115, 115, 105, 109, 111), _c(F147(97, 118, 101, 115, 116, 101), _c(F752(97, 118, 101, 115, 115, 101, 114, 111), _c(F492(97, 118, 114, 101, 105), _c(F545(97, 118, 114, 101, 115, 116, 105), _c(F545(97, 118, 114, 101, 98, 98, 101), _c(F545(97, 118, 114, 101, 109, 109, 111), _c(F545(97, 118, 114, 101, 115, 116, 101), _c(F708(97, 118, 114, 101, 98, 98, 101, 114, 111), _c(F50(97, 98, 98, 105), _c(F492(97, 98, 98, 105, 97), _c(F545(97, 98, 98, 105, 97, 109, 111), _c(F545(97, 98, 98, 105, 97, 116, 101), _c(F545(97, 98, 98, 105, 97, 110, 111), _c(F492(97, 118, 101, 114, 101), _c(F147(97, 118, 101, 110, 116, 101), _c(F147(97, 118, 101, 110, 100, 111), _c(F492(97, 118, 117, 116, 111), nil))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
 _SH[65689]= F92(32, 32, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 32, 66, 65, 68, 32, 115, 116, 114, 105, 110, 103, 32);
 _SH[65690]= F592(32, 32);
 _SH[65691]= F50(32, 47, 32, 32);
 _SH[65692]= F752(32, 32, 32, 32, 32, 32, 32, 32);
 _SH[65693]= F708(32, 42, 42, 42, 42, 42, 42, 42, 32);
 _SH[65694]= F262(47);
 _SH[65695]= F84(32, 32, 32);
 _SH[65696]= F708(42, 42, 42, 42, 42, 42, 42, 42, 32);
 _SH[65697]= F595(32, 116, 114, 115, 112, 101, 99, 32, 114, 101, 100, 101, 102, 105, 110, 101, 100);
 _SH[65698]= F92(70, 105, 108, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65699]= F92(70, 105, 108, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65700]= F92(70, 105, 108, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65701]= F92(70, 105, 108, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65702]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65703]= F262(45);
 _SH[65704]= F262(45);
 _SH[65705]= F262(32);
 _SH[65706]= _c(66809, _c(66810, _c(66811, _c(66812, _c(66813, _c(66814, _c(66815, _c(66816, _c(66817, _c(66818, _c(66819, _c(66820, _c(66821, _c(66822, nil))))))))))))));
 _SH[65707]= _c(66824, _c(_c(66548, _c(66825, nil)), _c(_c(66572, _c(_c(_c(66548, _c(F50(110, 97, 109, 101), nil)), _c(_c(66826, _c(F84(117, 114, 108), nil)), _c(_c(66827, _c(F50(102, 105, 108, 101), nil)), nil))), nil)), _c(_c(66798, F262(9)), _c(_c(66799, _c(F262(10), nil)), _c(_c(66808, _c(66407, nil)), nil))))));
 _SH[65708]= F147(38, 113, 117, 111, 116, 59);
 _SH[65709]= F492(38, 97, 109, 112, 59);
 _SH[65710]= F147(38, 97, 112, 111, 115, 59);
 _SH[65711]= F50(38, 108, 116, 59);
 _SH[65712]= F50(38, 103, 116, 59);
 _SH[65713]= F262(59);
 _SH[65714]= F262(59);
 _SH[65715]= F147(38, 113, 117, 111, 116, 59);
 _SH[65716]= F492(38, 35, 51, 52, 59);
 _SH[65717]= F147(38, 35, 48, 51, 52, 59);
 _SH[65718]= F147(38, 35, 120, 50, 50, 59);
 _SH[65719]= F492(38, 97, 109, 112, 59);
 _SH[65720]= F492(38, 35, 51, 56, 59);
 _SH[65721]= F147(38, 35, 48, 51, 56, 59);
 _SH[65722]= F147(38, 35, 120, 50, 54, 59);
 _SH[65723]= F147(38, 97, 112, 111, 115, 59);
 _SH[65724]= F492(38, 35, 51, 57, 59);
 _SH[65725]= F147(38, 35, 48, 51, 57, 59);
 _SH[65726]= F147(38, 35, 120, 50, 55, 59);
 _SH[65727]= F50(38, 108, 116, 59);
 _SH[65728]= F492(38, 35, 54, 48, 59);
 _SH[65729]= F147(38, 35, 48, 54, 48, 59);
 _SH[65730]= F147(38, 35, 120, 51, 67, 59);
 _SH[65731]= F50(38, 103, 116, 59);
 _SH[65732]= F492(38, 35, 54, 50, 59);
 _SH[65733]= F147(38, 35, 48, 54, 50, 59);
 _SH[65734]= F147(38, 35, 120, 51, 69, 59);
 _SH[65735]= F147(115, 99, 114, 105, 112, 116);
 _SH[65736]= F752(110, 111, 115, 99, 114, 105, 112, 116);
 _SH[65737]= F492(115, 116, 121, 108, 101);
 _SH[65738]= F708(60, 33, 91, 67, 68, 65, 84, 65, 91);
 _SH[65739]= F708(60, 33, 91, 67, 68, 65, 84, 65, 91);
 _SH[65740]= F84(93, 93, 62);
 _SH[65741]= F84(93, 93, 62);
 _SH[65742]= F84(93, 93, 62);
 _SH[65743]= F592(60, 33);
 _SH[65744]= F84(60, 33, 45);
 _SH[65745]= F84(45, 45, 62);
 _SH[65746]= F262(62);
 _SH[65747]= F84(60, 33, 45);
 _SH[65748]= F84(45, 45, 62);
 _SH[65749]= F262(62);
 _SH[65750]= F592(60, 63);
 _SH[65751]= _c(66734, _c(F708(60, 47, 115, 99, 114, 105, 112, 116, 62), nil));
 _SH[65752]= _c(66734, _c(F615(60, 47, 110, 111, 115, 99, 114, 105, 112, 116, 62), nil));
 _SH[65753]= _c(66734, _c(F752(60, 47, 115, 116, 121, 108, 101, 62), nil));
 _SH[65754]= F262(60);
 _SH[65755]= _c(66494, F592(47, 62));
 _SH[65756]= F592(60, 47);
 _SH[65757]= F455(66, 97, 100, 32, 101, 110, 100, 32, 116, 97, 103, 32);
 _SH[65758]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65759]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65760]= F505(32, 119, 105, 116, 104, 111, 117, 116, 32, 101, 110, 100, 32, 116, 97, 103);
 _SH[65761]= _c(42, _c(42, _c(42, _c(42, _c(42, _c(42, _c(32, _c(82, _c(101, F215(99, 117, 114, 115, 105, 118, 101, 32, 99, 97, 108, 108, 32, 116, 111, 32, 104, 116, 109, 108, 45, 114, 101, 97, 100, 32, 42, 42, 42, 42, 42, 42))))))))));
 _SH[65762]= F262(62);
 _SH[65763]= F504(32, 65, 71, 82, 69, 69, 45, 76, 73, 78, 69, 32, 32);
 _SH[65764]= F92(70, 105, 108, 101, 32, 105, 110, 105, 116, 105, 97, 108, 105, 122, 97, 116, 105, 111, 110, 32, 102, 97, 105, 108, 101, 100);
 _SH[65765]= F601(45, 45, 45, 83, 108, 105, 99, 101, 32, 86, 101, 99, 116, 32, 70, 117, 108, 108, 45, 45, 45);
 _SH[65766]= F147(99, 111, 111, 107, 105, 101);
 _SH[65767]= _c(66494, _c(62, _c(F592(47, 62), _c(66834, nil))));
 _SH[65768]= _c(66494, _c(62, _c(61, _c(F592(47, 62), _c(66834, nil)))));
 _SH[65769]= _c(F262(97), _c(F50(97, 98, 98, 114), _c(F545(97, 99, 114, 111, 110, 121, 109), _c(F262(98), _c(F84(98, 100, 111), _c(F84(98, 105, 103), _c(F592(98, 114), _c(F147(98, 117, 116, 116, 111, 110), _c(F50(99, 105, 116, 101), _c(F50(99, 111, 100, 101), _c(F84(100, 102, 110), _c(F592(101, 109), _c(F262(105), _c(F84(105, 109, 103), _c(F492(105, 110, 112, 117, 116), _c(F84(107, 98, 100), _c(F492(108, 97, 98, 101, 108), _c(F84(109, 97, 112), _c(F147(111, 98, 106, 101, 99, 116), _c(F262(113), _c(F50(115, 97, 109, 112), _c(F147(115, 99, 114, 105, 112, 116), _c(F147(115, 101, 108, 101, 99, 116), _c(F492(115, 109, 97, 108, 108), _c(F50(115, 112, 97, 110), _c(F147(115, 116, 114, 111, 110, 103), _c(F84(115, 117, 98), _c(F84(115, 117, 112), _c(F752(116, 101, 120, 116, 97, 114, 101, 97), _c(F592(116, 116), _c(F84(118, 97, 114), _c(F262(117), nil))))))))))))))))))))))))))))))));
 _SH[65770]= _c(F84(117, 115, 111), _c(F504(117, 116, 105, 108, 105, 122, 122, 97, 122, 105, 111, 110, 101), _c(F752(117, 116, 105, 108, 105, 122, 122, 111), _c(F545(105, 109, 112, 105, 101, 103, 111), _c(F615(117, 116, 105, 108, 105, 122, 122, 97, 110, 100, 111), _c(F84(117, 115, 105), _c(F752(117, 116, 105, 108, 105, 122, 122, 97), _c(F84(117, 115, 97), _c(F615(117, 116, 105, 108, 105, 122, 122, 105, 97, 109, 111), _c(F615(105, 109, 112, 105, 101, 103, 104, 105, 97, 109, 111), _c(F545(105, 109, 112, 105, 101, 103, 97), _c(F545(97, 99, 99, 101, 116, 116, 111), _c(F545(97, 99, 99, 101, 116, 116, 105), _c(F752(99, 111, 110, 115, 101, 110, 115, 111), _c(F550(97, 99, 99, 111, 110, 115, 101, 110, 116, 105), nil)))))))))))))));
 _SH[65771]= _c(F50(117, 115, 101, 100), _c(F50(117, 115, 101, 115), _c(F492(117, 115, 105, 110, 103), _c(F84(117, 115, 101), _c(F545(117, 116, 105, 108, 105, 122, 101), _c(F752(117, 116, 105, 108, 105, 122, 101, 115), _c(F752(117, 116, 105, 108, 105, 122, 101, 100), _c(F708(97, 103, 114, 101, 101, 109, 101, 110, 116), _c(F550(97, 99, 99, 101, 112, 116, 97, 110, 99, 101), _c(F492(97, 103, 114, 101, 101), nil))))))))));
 _SH[66407]= 66407;
 _SH[66422]= nil;
 _SH[66486]= F397(C8);
 _SH[66544]= nil;
 _SH[66569]= F39(4096);
 _SH[66594]= nil;
 _SH[66595]= nil;
 _SH[66596]= 66407;
 _SH[66598]= 10;
 _SH[66599]= 4;
 _SH[66796]= nil;
 _SH[66829]= _SH[65769];
 _SH[66830]= nil;
 _SH[66833]= _SH[65767];
 _SH[66837]= F397(C32);
 _SH[66838]= F397(C32);
 _SH[66839]= 66843;
 _SH[66841]= _SH[65768];
 _SH[66849]= _SH[65770];
 _SH[66850]= _SH[65771];
}
}

int F134(int stream, int n)
{{int rv_aux = nil; {int csym = nil; int str = nil; while (n > 0){ n= (n - 1);
 csym= F299(stream);
 if (csym == -1)
{n= 0;}
else {str= _c(csym, str);}
}

 rv_aux= F574(str);
}

 return rv_aux;
}
}

int F135(int str)
{{int rv_aux = nil; {int new_sym = nil; int prop_rec = nil; { new_sym= V42;
 V42= (V42 + 1);
 _SH[new_sym]= nil;
 if (str)
{{ _SH[(new_sym + C21)]= str;
 prop_rec= _c(66741, nil);
 _SH[(new_sym + C21 + C21)]= prop_rec;
 F437(V45, str, new_sym);
}
}
 return new_sym;
}

}

 return rv_aux;
}
}

int F136(int fields, int aux_rec)
{{int rv_aux = nil; {int field = nil; int on_var = nil; int list_var = nil; on_var= fields;
 while (on_var != nil){ field= _SH[on_var];
 {int object1__2; int object2__3 = list_var; if (F556(field) != nil)
{object1__2= _c(66409, _c(F283(field), _c(_c(66456, _c(aux_rec, _c(_SH[field], nil))), nil)));}
else {object1__2= _c(66409, _c(field, _c(_c(66456, _c(aux_rec, _c(field, nil))), nil)));}
 list_var= _c(object1__2, object2__3);
}

 on_var= _SH[(on_var + 1)];
}

 rv_aux= F574(list_var);
}

 return rv_aux;
}
}

int F137(int list, int i)
{{int rv_aux = nil; { {int list_len_var = nil; list_len_var= F510(list);
 if (i < 0)
{i= (list_len_var + i + 1);}
}

 rv_aux= _SH[F125(list, i)];
}

 return rv_aux;
}
}

int F138(int old_lp, int new_lp)
{return F302(F755(_c(_SH[65665], _c(F40(old_lp, F750(_SH[66427], 66740)), _c(_SH[65666], _c(F40(new_lp, F750(_SH[66427], 66740)), nil))))), nil);}

int F139(int stream_index)
{return fgetc(F579(stream_index));}

int F140(int node, int previous_pnode)
{{int rv_aux = nil; {int pnode = nil; if (F382(node) != nil)
{{ previous_pnode= _c(66630, _c(_c(66623, _c(node, nil)), _c(_c(66628, _c(F750(node, 66628), nil)), _c(_c(66567, _c(F750(node, 66567), nil)), _c(_c(66635, _c(previous_pnode, nil)), nil)))));
 F272(previous_pnode);
}
}
 {int for_thereis_result = nil; for_thereis_result= nil;
 {int node2 = nil; int node2_on = nil; { node2_on= F750(node, 66626);
 node2= _SH[node2_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(node2_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F100(node2) != nil)
{{ pnode= _c(66630, _c(_c(66623, _c(node2, nil)), _c(_c(66635, _c(previous_pnode, nil)), nil)));
 F272(pnode);
 for_thereis_result= pnode;
}
}
else {{ if (F750(node2, 66631) != nil)
{F272(_c(66630, _c(_c(66623, _c(node2, nil)), _c(_c(66635, _c(previous_pnode, nil)), nil))));}
 for_thereis_result= F140(node2, previous_pnode);
}
}
 { node2_on= _SH[(node2_on + 1)];
 node2= _SH[node2_on];
}

 {int and_result = nil; { and_result= F678(node2_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

}

 return rv_aux;
}
}

int F141(int mode)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(mode, 66584);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(mode, 66585);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(mode, 66583);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F142(int sym, int stream)
{{int rv_aux = nil; {int csym = nil; int csym_on = nil; { csym_on= F292(sym);
 csym= _SH[csym_on];
}

 { while (csym_on != nil){ if (F132(csym) != nil)
{F83(csym, stream);}
else {F673(csym, stream);}
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F143(int stream)
{{int rv_aux = nil; {int tyo_fn = V14[stream]; int flush_fn = V11[stream]; if (tyo_fn != nil)
{if (flush_fn != nil)
{rv_aux= F483(flush_fn, stream);}
else {rv_aux= nil;}}
else {{int buf0_start = V25[stream]; int buf1_start = V29[stream]; int buf0_index = V26[stream]; int buf1_index = V30[stream]; if (buf1_index > buf0_index)
{{ F489(stream, buf0_start);
 rv_aux= F489(stream, buf1_start);
}
}
else {{ F489(stream, buf1_start);
 rv_aux= F489(stream, buf0_start);
}
}
}
}
}

 return rv_aux;
}
}

int F144(int el, int delimiter, int stream, int conversion_fn)
{{int rv_aux = nil; { if (conversion_fn != nil)
{el= F483(conversion_fn, el);}
 F235(stream, el);
 rv_aux= F334(delimiter, stream);
}

 return rv_aux;
}
}

int F145(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int csym = nil; aux_stream= V8[stream];
 aux_stream= F340(aux_stream, V37);
 {int repeat_var = nil; int buf_pos = nil; repeat_var= 1;
 buf_pos= buf_start;
 while (repeat_var <= nbytes){ csym= V33[buf_pos];
 F127(csym, aux_stream);
 repeat_var= (repeat_var + 1);
 buf_pos= (buf_pos + 1);
}

}

 rv_aux= F333(aux_stream);
}

 return rv_aux;
}
}

int F146(int list, int base)
{{int rv_aux = nil; {int flag = nil; int csym = nil; int point_flag = nil; if (F556(list) != nil)
{{ flag= _SH[66407];
 {int test_aux_var; {int or_result = nil; { or_result= F256(_SH[list], 43);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(_SH[list], 45);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ _SH[list];
 list= _SH[(list + 1)];
}
}
}

 if (list == nil)
{flag= nil;}
 while (list != nil){ { csym= _SH[list];
 list= _SH[(list + 1)];
}

 if (csym == 46)
{if (point_flag == nil)
{point_flag= 66407;}
else {{ flag= nil;
 list= nil;
}
}}
else {if (!(F65(csym, base) != nil))
{{ flag= nil;
 list= nil;
}
}}
}

 if (point_flag != nil)
{rv_aux= flag;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F147(int a1, int a2, int a3, int a4, int a5, int a6)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))));}

int F148(int stream, int condition)
{return F602(stream, condition, 66709, 66407, nil);}

int F149(int x)
{return x;}

int F150(int in_stream, int out_stream)
{return F659(F299(in_stream), out_stream);}

int F151(int stream, int not_condition)
{{int rv_aux = nil; {int delta1 = nil; {int condition = nil; int delta = nil; int aux = nil; aux= not_condition;
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 aux= _SH[(aux + 1)];
 delta= _SH[aux];
 delta1= F524(stream, condition);
 if (delta1 >= 0)
{rv_aux= -1;}
else {rv_aux= delta;}
}

}

 return rv_aux;
}
}

void F152()
{{ F541();
 F734();
 F622();
 F766();
 F26();
 F183();
 F285();
 F577();
 F120();
}
}

int F153(int stream)
{return F366(stream, _SH[65752]);}

int F154()
{{int rv_aux = nil; if (V47 == 1)
{rv_aux= F487();}
else {rv_aux= F522();}
 return rv_aux;
}
}

int F155(int stream, int F60)
{{int rv_aux = nil; { F102(stream, F60);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F156(int rec_type_name, int size, int field_specs)
{return F265(_SH[66569], F292(rec_type_name), _c(66570, _c(_c(66548, _c(rec_type_name, nil)), _c(_c(66571, _c(size, nil)), _c(_c(66572, _c(field_specs, nil)), nil)))));}

int F157(int stack)
{{int rv_aux = nil; if (!(F103(stack) != nil))
{rv_aux= F2(stack, (F2(stack, 1) - 1));}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F158(int rec, int field, int value)
{{int rv_aux = nil; {int f_38v = nil; f_38v= F248(rec, field, nil);
 if (f_38v != nil)
{if (_SH[(f_38v + 1)] != nil)
{F293(f_38v, value);}
else {_SH[(f_38v + 1)]= _c(value, nil);}}
else {F498(rec, _c(_c(field, _c(value, nil)), nil));}
 rv_aux= value;
}

 return rv_aux;
}
}

int F159(int condition)
{{int rv_aux = nil; {int and_result = nil; { and_result= F116(condition);
 if (and_result != nil)
{{ and_result= F774(F283(condition), 66648);
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

void F160()
{{ V44= C19;
 V42= 0;
 V43= C22;
 V41= nil;
 _SH[nil]= nil;
 _SH[(nil + 1)]= nil;
 V46= 0;
 V47= 0;
 V48= nil;
 V49= nil;
 V50= nil;
 V51= nil;
}
}

int F161(int list)
{return _SH[F676(list)];}

int F162(int list, int fn)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F483(fn, _SH[el]) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 el= _SH[(el + 1)];
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F163(int condition_name)
{return F394(F755(_c(_SH[65638], _c(F292(condition_name), nil))), 66407);}

int F164()
{return F401();}

int F165(int str)
{return F664(66734, _SH[66782], _c(str, nil));}

int F166(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int or_result = nil; { {int and_result = nil; { if (csym_code >= 65)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 90)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= 192)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 222)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code != 215)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

}

 return rv_aux;
}
}

int F167(int object, int list)
{{int rv_aux = nil; {int el = nil; int found = nil; while (list != nil){ el= _SH[list];
 list= _SH[(list + 1)];
 if (object == el)
{{ list= nil;
 found= 66407;
}
}
}

 rv_aux= found;
}

 return rv_aux;
}
}

int F168(int stream, int act_spec)
{{int rv_aux = nil; if (F774(act_spec, 66646) != nil)
{rv_aux= nil;}
else {if (F468(act_spec) != nil)
{rv_aux= F324(F308(stream, act_spec));}
else {if (F761(act_spec) != nil)
{rv_aux= F324(F282(stream, act_spec));}
else {if (F561(act_spec) != nil)
{rv_aux= F324(F666(stream, act_spec));}
else {if (F64(act_spec) != nil)
{rv_aux= F324(F591(stream, act_spec));}
else {rv_aux= act_spec;}}}}}
 return rv_aux;
}
}

int F169(int stream, int str, int test1_fn)
{{int rv_aux = nil; {int csym = nil; {int64_t cur_pos = F237(stream); int result = nil; { { csym= _SH[str];
 str= _SH[(str + 1)];
}

 {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ and_result= F325(test1_fn, F299(stream), csym);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{if (!(str != nil))
{result= 1;}
else {{ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ and_result= F325(test1_fn, F299(stream), csym);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{if (!(str != nil))
{result= 2;}
else {{int test_aux_var; {int for_always_result = nil; for_always_result= 66407;
 {int csym = nil; int csym_on = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ and_result= for_always_result;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ and_result= F325(test1_fn, F299(stream), csym);
 if (and_result != nil)
{for_always_result= and_result;}
else {for_always_result= nil;}
}
}
else {for_always_result= nil;}
}

}

 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

 {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ and_result= for_always_result;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 test_aux_var= (for_always_result != nil);
}

 if (test_aux_var)
{result= (F510(str) + 2);}
else {result= -1;}
}
}}
else {result= -1;}
}

}
}}
else {result= -1;}
}

}

 F102(stream, cur_pos);
 rv_aux= result;
}

}

 return rv_aux;
}
}

int F170(int nums)
{{int rv_aux = nil; if (F510(nums) == 1)
{rv_aux= _SH[nums];}
else {if (F510(nums) == 2)
{rv_aux= _c(66420, _c(_c(_c(66429, _c(_SH[nums], _c(F283(nums), nil))), _c(_SH[nums], nil)), _c(_c(66407, _c(F283(nums), nil)), nil)));}
else {rv_aux= _c(66420, _c(_c(_c(66429, _c(_SH[nums], _c(F283(nums), nil))), _c(F170(_c(_SH[nums], F7(nums))), nil)), _c(_c(66407, _c(F170(_SH[(nums + 1)]), nil)), nil)));}}
 return rv_aux;
}
}

int F171(int stream)
{{int rv_aux = nil; if (F237(stream) == 1)
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F172(int hashtable, int str)
{{int rv_aux = nil; {int index = nil; int entry_list = nil; int entry = nil; int size = F552(hashtable); index= F456(str, size);
 entry_list= F2(hashtable, index);
 {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= entry_list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F343(_SH[el], str, 66443) != nil)
{for_thereis_result= _SH[(el + 1)];}
else {for_thereis_result= nil;}
 el= F7(el);
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 entry= for_thereis_result;
}

 rv_aux= _SH[entry];
}

 return rv_aux;
}
}

int F173(int object, int list, int size, int mode)
{{int rv_aux = nil; if (mode == 66444)
{rv_aux= F735(_c(list, _c(F514(size, object), nil)));}
else {if (mode == 66445)
{rv_aux= F735(_c(F514(size, object), _c(list, nil)));}
else {if (mode == 66446)
{rv_aux= F735(_c(F514((size / 2), object), _c(list, _c(F514((size / 2), object), nil))));}
else {if (mode == 66447)
{rv_aux= F735(_c(F514(size, object), _c(list, _c(F514(size, object), nil))));}
else {rv_aux= nil;}}}}
 return rv_aux;
}
}

int F174(int stream)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int pos_test = nil; int pos_test_on = nil; { pos_test_on= _SH[65639];
 pos_test= _SH[pos_test_on];
}

 while (pos_test_on != nil){ for_list_result= _c(_c(pos_test, _c(_c(66408, _c(_c(F163(pos_test), _c(stream, nil)), _c(0, _c(-1, nil)))), nil)), for_list_result);
 { pos_test_on= _SH[(pos_test_on + 1)];
 pos_test= _SH[pos_test_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F175(int stream, int read_attlistP)
{{int rv_aux = nil; {int tag_name = nil; int attlist = nil; tag_name= F583(F51(stream, nil));
 {int test_aux_var; {int and_result = nil; { and_result= F256(F506(), 66814);
 if (and_result != nil)
{{ and_result= F256(tag_name, 66814);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{F540(nil);}
}

 F445(tag_name);
 if (read_attlistP != nil)
{attlist= F31(stream);}
 if (attlist != nil)
{F421(F252(attlist));}
 F200(stream, _SH[65755]);
 {int test_aux_var; {int or_result = nil; { or_result= F353(stream, 47, nil);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F271(tag_name);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{F540(66407);}
}

 rv_aux= F366(stream, 62);
}

 return rv_aux;
}
}

int F176(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10)
{{int rv_aux = nil; switch (fn) {
   case 66353: rv_aux= F550(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10); break;
   case 66538: rv_aux= F392(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5 - a6 - a7 - a8 - a9 - a10); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5 * a6 * a7 * a8 * a9 * a10); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5 / a6 / a7 / a8 / a9 / a10); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil)))))))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil)))))))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil)))))))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil)))))))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))))); break;
   default: return F563(fn, 10); break;
}
 return rv_aux;
}
}

int F177(int strs, int stream)
{{int rv_aux = nil; {int exp_list = nil; int str = nil; while (strs != nil){ { str= _SH[strs];
 strs= _SH[(strs + 1)];
}

 if (F774(str, 66415) != nil)
{if (F245(stream) != nil)
{exp_list= _c(_c(66415, _c(66416, F498(_SH[(str + 1)], nil))), exp_list);}
else {exp_list= _c(_c(66415, _c(stream, F498(_SH[(str + 1)], nil))), exp_list);}}
else {if (F774(str, 66417) != nil)
{{ exp_list= _c(F497(stream, _SH[65539]), exp_list);
 exp_list= _c(F497(stream, F283(str)), exp_list);
 exp_list= _c(F497(stream, _SH[65540]), exp_list);
}
}
else {exp_list= _c(F497(stream, str), exp_list);}}
}

 rv_aux= F574(exp_list);
}

 return rv_aux;
}
}

int stdoutTyo(int csym, int stream)
{return fputc(csym, stdout);}

int F179(int num_str, int base)
{{int rv_aux = nil; {int num_buf; if (base <= 16)
{{int negativeP = nil; int digit_csym = nil; int val = nil; int failP = nil; if (_SH[num_str] == 43)
{num_str= _SH[(num_str + 1)];}
else {if (_SH[num_str] == 45)
{{ num_str= _SH[(num_str + 1)];
 negativeP= 66407;
}
}}
 num_buf= 0;
 while (num_str != nil){ { digit_csym= _SH[num_str];
 num_str= _SH[(num_str + 1)];
}

 val= F391(digit_csym);
 if (val >= base)
{{ num_str= nil;
 failP= 66407;
}
}
else {num_buf= ((num_buf * base) + val);}
}

 if (!(failP != nil))
{{ if (negativeP != nil)
{num_buf= (0 - num_buf);}
 rv_aux= num_buf;
}
}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F180(int field_type_name)
{{int rv_aux = nil; if (F603(field_type_name) != nil)
{rv_aux= 66568;}
else {rv_aux= F394(F755(_c(_SH[65568], _c(F292(field_type_name), _c(_SH[65569], nil)))), 66407);}
 return rv_aux;
}
}

int F181(int stream, int str_size, int64_t delta)
{{int rv_aux = nil; {int str = nil; {int64_t cur_pos = F237(stream); int result = nil; { F663(stream, delta);
 { {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (str_size > 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ str_size= (str_size - 1);
 str= _c(F299(stream), str);
 {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (str_size > 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 result= nil;
}

}

 F102(stream, cur_pos);
}

 rv_aux= F574(str);
}

 return rv_aux;
}
}

int F182(int rec_name)
{return F394(F755(_c(_SH[65572], _c(F292(rec_name), _c(_SH[65573], nil)))), 66407);}

int F183()
{{int rv_aux = nil; {int index; index= 0;
 { while (index < C30){ V57[index]= -1;
 index= (index + 1);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F184(int html_file)
{{int rv_aux = nil; {int start = nil; int end = nil; int seg_list = nil; int agree_line = nil; {int raw_stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{raw_stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66806;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; int filename = html_file; int mode = nil; int64_t end; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F419(filename) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 aux_stream= F703(filename, mode);
 if (!(aux_stream != nil))
{error= _SH[65764];}
else {{ end= F617(filename);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 raw_stream= STREAM__BUF_935;
}
}
}

 unwind_value= raw_stream;
}

 if (raw_stream == -1)
;
else {{ { {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= 66407;
 V10[STREAM__BUF_935]= 66845;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream = nil; int64_t end; aux_stream= raw_stream;
 end= (C3 - 1);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ { while (F243(stream, nil)){ F426(66846, stream);
 start= F237(stream);
 F663(stream, 1);
 F616(66846, stream);
 end= F237(stream);
 seg_list= _c(_c(start, _c(end, nil)), seg_list);
}

 stream_rv= nil;
}

 if (!(stream == -1))
{{ F143(stream);
 F405(stream);
}
}
}
}
}

}

 seg_list= F574(seg_list);
 F102(raw_stream, 1);
 {int stream_rv__127; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= 66407;
 V10[STREAM__BUF_935]= 66845;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream = nil; int64_t end; aux_stream= raw_stream;
 end= (C3 - 1);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ {int seg = nil; int seg_on = nil; { seg_on= seg_list;
 seg= _SH[seg_on];
}

 { {int test_aux_var; {int and_result = nil; { and_result= F678(seg_on, nil);
 if (and_result != nil)
{{ if (!(agree_line != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ {int start = nil; int end = nil; int aux = nil; aux= seg;
 start= _SH[aux];
 aux= _SH[(aux + 1)];
 end= _SH[aux];
 {int64_t start__167 = start; int64_t start = start__167; int64_t end__168 = end; int64_t end = end__168; if (start > end)
;
else {if (V36 >= C16)
{_SH[66422]= _SH[65765];}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { agree_line= F191(F745(stream));
 stream_slice_rv= agree_line;
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
}

}
}}
}

}

 { seg_on= _SH[(seg_on + 1)];
 seg= _SH[seg_on];
}

 {int and_result = nil; { and_result= F678(seg_on, nil);
 if (and_result != nil)
{{ if (!(agree_line != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 stream_rv= nil;
}

}

 if (!(stream == -1))
{{ F143(stream);
 F405(stream);
}
}
}
}
}

 stream_rv__127= stream_rv;
}

 stream_rv= stream_rv__127;
}

}

 if (!(raw_stream == -1))
{{ F143(raw_stream);
 F263(V8[raw_stream]);
 F405(raw_stream);
}
}
}
}
}

}

 rv_aux= agree_line;
}

 return rv_aux;
}
}

int F185(int stream)
{{int rv_aux = nil; {int csym = nil; int end_of_lineP = nil; { while (!(end_of_lineP != nil)){ csym= F299(stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(csym, -1);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F658(csym);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{end_of_lineP= 66407;}
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F186(int list)
{{int rv_aux = nil; {int rev_list = nil; while (list != nil){ rev_list= _c(_SH[list], rev_list);
 list= _SH[(list + 1)];
}

 rv_aux= rev_list;
}

 return rv_aux;
}
}

int F187(int els)
{{int rv_aux = nil; if (_SH[(els + 1)] != nil)
{rv_aux= _c(66406, _c(_SH[els], _c(F187(_SH[(els + 1)]), nil)));}
else {rv_aux= _SH[els];}
 return rv_aux;
}
}

int F188(int args)
{{int rv_aux = nil; if (!(args != nil))
{rv_aux= 66407;}
else {rv_aux= _c(66424, _c(_c(66426, nil), _c(F429(args, 66426), nil)));}
 return rv_aux;
}
}

int F189(double num, int decimals, int expP)
{{int rv_aux = nil; { F453(num, expP, decimals);
 rv_aux= F450(V52);
}

 return rv_aux;
}
}

int F190(int lp)
{{int rv_aux = nil; {int csym = nil; int version = nil; while (lp != nil){ { csym= _SH[lp];
 lp= _SH[(lp + 1)];
}

 if (csym == 46)
{if (lp == nil)
;
else {if (F461(_SH[lp]) != nil)
{{ version= _c(csym, version);
 version= _c(_SH[lp], version);
 { _SH[lp];
 lp= _SH[(lp + 1)];
}

}
}
else {lp= nil;}}}
else {version= _c(csym, version);}
}

 rv_aux= F574(version);
}

 return rv_aux;
}
}

int F191(int lines)
{{int rv_aux = nil; {int cookie_lines = nil; int lang = nil; cookie_lines= F647(lines);
 if (cookie_lines != nil)
{{ lang= F27(cookie_lines);
 if (lang != nil)
{rv_aux= F643(cookie_lines, lang);}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F192(int exa_num_sym)
{return F179(F7(F292(exa_num_sym)), 16);}

int F193(int str)
{return F664(66734, _SH[66760], _c(str, nil));}

int F194(int value, int stream)
{return F776(value, V8[stream]);}

int F195(int stream, int delta_x, int fill_str)
{{int rv_aux = nil; { while (delta_x > 0){ delta_x= (delta_x - 1);
 F235(stream, fill_str);
}

 rv_aux= nil;
}

 return rv_aux;
}
}

int F196(int nums, int exp1_fn)
{return F483(exp1_fn, nums);}

int F197(int stream_index, int64_t position)
{{int rv_aux = nil; { fflush(F579(stream_index));
 fseek(F579(stream_index), position, 0);
 rv_aux= nil;
}

 return rv_aux;
}
}

int F198(int stream)
{return V13[stream];}

int F199(int object, int index)
{{int rv_aux; {int value = nil; int list_len = nil; int buf_index = nil; if (!(F364(object) != nil))
{{ list_len= F510(object);
 if (list_len > C9)
{{ { F87(_SH[66435]);
 F87(_SH[65555]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 index= (index + list_len);
 buf_index= index;
 while (object != nil){ { value= _SH[object];
 object= _SH[(object + 1)];
}

 { F91(_SH[66486], buf_index, value);
 buf_index= (buf_index - 1);
}

}

}
}
 rv_aux= index;
}

 return rv_aux;
}
}

int F200(int stream, int condition)
{{int rv_aux = nil; { if (condition == 66714)
{F102(stream, F418(stream));}
else {F548(stream, condition, 66462);}
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F201(int stream, int condition, int out_stream, int inTOout_fn)
{return F680(stream, out_stream, condition, 66709, inTOout_fn);}

void F202()
{{ F160();
 sharkInitSyms();
 _SH[66435]= F262(10);
 F210();
 F331();
 F152();
}
}

int F203(int regex, int start_node, int end_node)
{{int rv_aux = nil; if (F245(regex) != nil)
{rv_aux= F507(start_node, end_node);}
else {if (F123(regex) != nil)
{rv_aux= F99(regex, start_node, end_node);}
else {if (F774(regex, 66409) != nil)
{rv_aux= F274(regex, start_node, end_node);}
else {if (F774(regex, 66405) != nil)
{rv_aux= F94(regex, start_node, end_node);}
else {if (F774(regex, 66494) != nil)
{rv_aux= F379(regex, start_node, end_node);}
else {if (F774(regex, 66616) != nil)
{rv_aux= F633(regex, start_node, end_node);}
else {if (F774(regex, 66621) != nil)
{rv_aux= F99(regex, start_node, end_node);}
else {if (F774(regex, 66622) != nil)
{rv_aux= F99(regex, start_node, end_node);}
else {{ F110(regex, _SH[66416], 0, _SH[66436], _SH[66437]);
 { { F87(_SH[66435]);
 F87(_SH[65633]);
 F87(_SH[66435]);
 F373(nil);
}

 rv_aux= nil;
}

}
}}}}}}}}
 return rv_aux;
}
}

int F204(char* cstr)
{{int rv_aux = nil; {int str = nil; int index = 0; index= 0;
 if (!(cstr == NULL))
{while (!(cstr[index] == 0)){ str= _c(cstr[index], str);
 index= (index + 1);
}
}
 return F574(str);
}

 return rv_aux;
}
}

int F205(int fn, int args)
{return F717(fn, args);}

int F206(int table, int rtype)
{{int rv_aux = nil; {int spec = F443(rtype); if (F750(spec, 66808) != nil)
{{ _SH[table];
 table= _SH[(table + 1)];
}
}
 {int for_list_result = nil; for_list_result= nil;
 {int row = nil; int row_on = nil; { row_on= table;
 row= _SH[row_on];
}

 while (row_on != nil){ for_list_result= _c(F599(row, spec), for_list_result);
 { row_on= _SH[(row_on + 1)];
 row= _SH[row_on];
}

}

}

 rv_aux= F574(for_list_result);
}

}

 return rv_aux;
}
}

int F207(int csym, int stream)
{return F659(csym, V8[stream]);}

int F208(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(csym, 32);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 9);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

char* F209(int string)
{{char* rv_aux; {int index; { index= 0;
 while (string != nil){ V52[index]= (char)_SH[string];
 string= _SH[(string + 1)];
 index= (index + 1);
}

 V52[index]= (char)0;
 return V52;
}

}

 return rv_aux;
}
}

void F210()
{F156(66383, C12, _SH[65536]);}

int F211(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8)
{{int rv_aux = nil; switch (fn) {
   case 66341: rv_aux= F752(a1, a2, a3, a4, a5, a6, a7, a8); break;
   case 66538: rv_aux= F474(a1, a2, a3, a4, a5, a6, a7, a8); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5 - a6 - a7 - a8); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5 * a6 * a7 * a8); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5 / a6 / a7 / a8); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil)))))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil)))))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil)))))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil)))))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))))); break;
   default: return F563(fn, 8); break;
}
 return rv_aux;
}
}

int F212(int stream)
{return F143(V8[stream]);}

int F213(int iter_specs)
{{int rv_aux = nil; {int spec1 = nil; int for_ispec = nil; int iter_spec = nil; if (!(F730(iter_specs) != nil))
{iter_specs= _c(iter_specs, nil);}
 for_ispec= _c(66459, nil);
 while (iter_specs != nil){ { iter_spec= _SH[iter_specs];
 iter_specs= _SH[(iter_specs + 1)];
}

 if (_SH[iter_spec] == 66430)
{spec1= F266(iter_spec);}
else {if (_SH[iter_spec] == 66460)
{spec1= F70(iter_spec);}
else {if (_SH[iter_spec] == 66461)
{spec1= F390(iter_spec);}
else {if (_SH[iter_spec] == 66462)
{spec1= F699(iter_spec);}
else {if (_SH[iter_spec] == 66463)
{spec1= F227(iter_spec);}
else {if (F283(iter_spec) == 66464)
{spec1= F126(iter_spec);}
else {if (F283(iter_spec) == 66451)
{spec1= F6(iter_spec);}
else {if (F283(iter_spec) == 66465)
{spec1= F535(iter_spec);}
else {if (F283(iter_spec) == 66466)
{spec1= F369(iter_spec);}
else {if (F283(iter_spec) == 66467)
{spec1= F415(iter_spec);}
else {if (F283(iter_spec) == 66468)
{spec1= F636(iter_spec);}
else {if (F283(iter_spec) == 66469)
{spec1= F606(iter_spec);}
else {if (F283(iter_spec) == 66470)
{spec1= F435(iter_spec);}
else {if (F283(iter_spec) == 66471)
{spec1= F24(iter_spec);}
else {if (F283(iter_spec) == 66472)
{spec1= F649(iter_spec);}
else {if (F283(iter_spec) == 66473)
{spec1= F117(iter_spec);}
else {if (F283(iter_spec) == 66474)
{spec1= F732(iter_spec);}
else {if (F283(iter_spec) == 66475)
{spec1= F412(iter_spec);}
else {spec1= F490(iter_spec);}}}}}}}}}}}}}}}}}}
 F626(spec1, for_ispec);
}

 F158(for_ispec, 66476, F574(F750(for_ispec, 66476)));
 F158(for_ispec, 66477, F574(F750(for_ispec, 66477)));
 F158(for_ispec, 66461, F570(66478, F574(F750(for_ispec, 66461))));
 F158(for_ispec, 66479, F574(F750(for_ispec, 66479)));
 F158(for_ispec, 66480, F574(F750(for_ispec, 66480)));
 rv_aux= for_ispec;
}

 return rv_aux;
}
}

int F214(int* flag_vect, int vect_size)
{{int rv_aux; {int cur_index = 0; int index = -1; while (cur_index < vect_size){ if (flag_vect[cur_index] == 0)
{{ index= cur_index;
 cur_index= vect_size;
}
}
else {cur_index= (cur_index + 1);}
}

 if (index == -1)
{{ { F87(_SH[66435]);
 F87(_SH[65564]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 flag_vect[index]= 1;
 return index;
}

 return rv_aux;
}
}

int F215(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30, int a31, int a32)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, _c(a28, _c(a29, _c(a30, _c(a31, _c(a32, nil))))))))))))))))))))))))))))))));}

int F216(int regex_node, int stream)
{{int rv_aux = nil; {int vect_start = nil; int vect_end = nil; int pnode = nil; int end_pnode = nil; int cur_csym = nil; int first_pnode = nil; int cur_pnode = nil; V39= 0;
 first_pnode= _c(66630, _c(_c(66623, _c(regex_node, nil)), _c(_c(66567, _c(F750(regex_node, 66567), nil)), nil)));
 F272(first_pnode);
 if (F140(regex_node, first_pnode) != nil)
{end_pnode= first_pnode;}
 vect_start= 0;
 vect_end= (V39 - 1);
 {int test_aux_var; {int and_result = nil; { if (vect_start <= vect_end)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(end_pnode != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ cur_csym= F299(stream);
 {int index = nil; index= vect_start;
 while (index <= vect_end){ pnode= V38[index];
 {int arc = nil; int arc_on = nil; { arc_on= F750(F750(pnode, 66623), 66631);
 arc= _SH[arc_on];
}

 while (arc_on != nil){ {int label = nil; int node = nil; int aux_rec = nil; aux_rec= arc;
 label= F750(aux_rec, 66632);
 node= F750(aux_rec, 66623);
 {int test_aux_var; {int or_result = nil; { or_result= F256(label, cur_csym);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(label, 66633);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F774(label, 66621);
 if (and_result != nil)
{{ and_result= F411(cur_csym, F283(label));
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F774(label, 66622);
 if (and_result != nil)
{{ if (!(F411(cur_csym, F283(label)) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { if (!(F16(label) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ and_result= F123(label);
 if (and_result != nil)
{{ and_result= F483(label, cur_csym);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}
}
}
}
}

}

 if (test_aux_var)
{{ cur_pnode= _c(66630, _c(_c(66623, _c(node, nil)), _c(_c(66634, _c(cur_csym, nil)), _c(_c(66635, _c(pnode, nil)), nil))));
 F272(cur_pnode);
 if (F100(node) != nil)
{end_pnode= cur_pnode;}
else {end_pnode= F140(node, cur_pnode);}
}
}
}

}

 { arc_on= _SH[(arc_on + 1)];
 arc= _SH[arc_on];
}

}

}

 index= (index + 1);
}

}

 vect_start= (vect_end + 1);
 vect_end= (V39 - 1);
 {int and_result = nil; { if (vect_start <= vect_end)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(end_pnode != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= end_pnode;
}

 return rv_aux;
}
}

int F217(int stream)
{{int rv_aux = nil; {int comment = nil; if (F353(stream, _SH[65744], nil) != nil)
{comment= F480(stream, _SH[65745], nil);}
else {comment= F480(stream, _SH[65746], nil);}
 rv_aux= F421(_c(66832, _c(comment, nil)));
}

 return rv_aux;
}
}

int F218(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, _c(a28, _c(a29, nil)))))))))))))))))))))))))))));}

int F219(int atom_name)
{{int rv_aux = nil; if (F388(atom_name) != nil)
{rv_aux= atom_name;}
else {rv_aux= F394(atom_name, 66407);}
 return rv_aux;
}
}

int F220(int dir1, int dest_dir, int traceP)
{{int rv_aux = nil; { {int file = nil; int file_on = nil; { file_on= F637(dir1, nil, nil);
 file= _SH[file_on];
}

 while (file_on != nil){ F138(F755(_c(dir1, _c(file, nil))), F755(_c(dest_dir, _c(file, nil))));
 { file_on= _SH[(file_on + 1)];
 file= _SH[file_on];
}

}

}

 {int subdir = nil; int subdir_on = nil; { subdir_on= F651(dir1, nil);
 subdir= _SH[subdir_on];
}

 { while (subdir_on != nil){ if (traceP != nil)
{{ F87(_SH[66435]);
 F87(_SH[65596]);
 F87(dest_dir);
 F87(subdir);
 F373(nil);
}
}
 F241(F755(_c(dest_dir, _c(subdir, nil))), nil);
 F220(F755(_c(dir1, _c(subdir, nil))), F755(_c(dest_dir, _c(subdir, nil))), traceP);
 { subdir_on= _SH[(subdir_on + 1)];
 subdir= _SH[subdir_on];
}

}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F221(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(csym, 32);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 13);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 10);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 9);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 12);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}
}
}
}
}

}

 return rv_aux;
}
}

int F222(int lp)
{{int rv_aux = nil; {int csym = nil; int new_lp = nil; int machine_name = F413(F341(66577), (F424(46, F341(66577)) - 1)); if (_SH[lp] == 47)
{if (F665(_SH[(lp + 1)], machine_name, 66443) != nil)
{{ _SH[lp];
 lp= _SH[(lp + 1)];
}
}
else {lp= F755(_c(machine_name, _c(lp, nil)));}}
 while (lp != nil){ { csym= _SH[lp];
 lp= _SH[(lp + 1)];
}

 if (csym == 47)
{new_lp= _c(58, new_lp);}
else {new_lp= _c(csym, new_lp);}
}

 rv_aux= F574(new_lp);
}

 return rv_aux;
}
}

int F223(int str)
{return F664(66734, _SH[66775], _c(str, nil));}

int F224(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, nil)))))))))))))))))))))))));}

int F225(int gmtP)
{{int rv_aux = nil; {time_t time1; struct tm tm; time(&time1);
 if (gmtP != nil)
{tm= (* gmtime(&time1));}
else {tm= (* localtime(&time1));}
 rv_aux= F464(tm);
}

 return rv_aux;
}
}

int F226(int index)
{{int rv_aux = nil; {int key = nil; while (!(F2(_SH[66486], index) == -2)){ key= _c(F335(F2(_SH[66486], index)), key);
 index= (index - 2);
}

 rv_aux= key;
}

 return rv_aux;
}
}

int F227(int iter_spec)
{{int rv_aux = nil; {int counter = nil; int aux = nil; aux= iter_spec;
 aux= _SH[(aux + 1)];
 counter= _SH[aux];
 rv_aux= _c(66481, _c(_c(66476, _c(_c(66483, nil), nil)), _c(_c(66477, _c(_c(66409, _c(66483, _c(1, nil))), nil)), _c(_c(66461, _c(_c(66428, _c(66483, _c(counter, nil))), nil)), _c(_c(66479, _c(_c(66484, _c(66483, _c(1, nil))), nil)), nil)))));
}

 return rv_aux;
}
}

int F228(int stream, int separator)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {; while (F243(stream, nil)){ for_list_result= _c(F52(stream, separator, nil), for_list_result);
}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F229(int sym_index)
{if ((sym_index >= 0) && (sym_index <= (C21 - 1)))
{return _SH[(sym_index + C21 + C21)];}
else {return nil;}}

int F230(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; {int F757 = nil; int csym = nil; int object_list = nil; F663(stream, 1);
 while (!(F757 != nil)){ F756(stream, sc_comment_flag, hm_comment_flag);
 csym= F396(stream, nil);
 if (csym == -1)
{{ F610(stream, _SH[65613]);
 F757= 66407;
}
}
else {if (csym == 35)
{object_list= _c(F346(stream), object_list);}
else {if (csym == 59)
{object_list= _c(F642(stream), object_list);}
else {if (csym == 41)
{{ F663(stream, 1);
 F757= 66407;
}
}
else {object_list= _c(F440(stream, sc_comment_flag, hm_comment_flag), object_list);}}}}
}

 rv_aux= F574(object_list);
}

 return rv_aux;
}
}

int F231(int specs, int body)
{{int rv_aux = nil; if (_SH[(specs + 1)] != nil)
{rv_aux= _c(66586, _c(_SH[specs], _c(F231(_SH[(specs + 1)], body), nil)));}
else {if (specs != nil)
{rv_aux= _c(66586, _c(_SH[specs], F498(body, nil)));}
else {rv_aux= nil;}}
 return rv_aux;
}
}

int F232(int str)
{return F280(_SH[66786], str);}

int F233(int vect, int index)
{{int rv_aux = nil; { { F87(_SH[66435]);
 F87(_SH[65653]);
 F87(F37(index, 10, nil));
 F87(_SH[65654]);
 F87(F37(_SH[(vect + 1)], 10, nil));
 F373(nil);
}

 { printf("\n***** EXIT: ---Bad vect index---\n");
 fflush(stdout);
 exit(0);
 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F234(int sock_index)
{return V57[sock_index];}

int F235(int stream, int str)
{{int rv_aux = nil; {int csym = nil; { while (str != nil){ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 F673(csym, stream);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F236(int str)
{return F664(66734, _SH[66757], _c(str, nil));}

int64_t F237(int stream)
{return V18[stream];}

int F238(int stream)
{{int rv_aux = nil; if (F353(stream, _SH[65747], nil) != nil)
{rv_aux= F366(stream, _SH[65748]);}
else {rv_aux= F366(stream, _SH[65749]);}
 return rv_aux;
}
}

int F239(int sym_index, int value_index)
{if ((sym_index >= 0) && (sym_index <= (C21 - 1)))
{{ _SH[sym_index]= value_index;
 return value_index;
}
}
else {return nil;}}

int F240(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F687(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

 while (test_aux_var){ csym= F22(aux_stream);
 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F687(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F241(int lp, int make_missing_dirsP)
{{int rv_aux = nil; {int file = nil; if (!(F757(lp, _SH[65584], 66443) != nil))
{{ file= lp;
 lp= F339(lp);
}
}
 if (make_missing_dirsP != nil)
{F351(F668(lp));}
else {if (!(F419(lp) != nil))
{F350(lp);}}
 if (file != nil)
{rv_aux= F337(file);}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F242(int object1, int object2)
{{int rv_aux = nil; {int consBuf = nil; if (V43 >= V44)
{if (V46 == 1)
{{ printf("\n***** EXIT: ---Temp Heap Full---\n");
 fflush(stdout);
 exit(0);
 rv_aux= nil;
}
}
else {{ printf("\n***** EXIT: ---Heap Full---\n");
 fflush(stdout);
 exit(0);
 rv_aux= nil;
}
}}
else {{ consBuf= V43;
 V43= (V43 + 2);
 _SH[consBuf]= object1;
 _SH[(consBuf + 1)]= object2;
 return consBuf;
}
}
}

 return rv_aux;
}
}

int F243(int stream, int64_t delta)
{{int rv_aux; if (!(delta != nil))
{rv_aux= F261(stream);}
else {{int64_t cur_pos = F237(stream); int result = nil; { F663(stream, delta);
 if (F261(stream))
{result= C24;}
else {result= nil;}
}

 F102(stream, cur_pos);
 rv_aux= (result != nil);
}
}
 return rv_aux;
}
}

int F244(int value, int stack)
{{int rv_aux = nil; if (!(F430(stack) != nil))
{{int top_pos = F2(stack, 1); F91(stack, top_pos, value);
 top_pos= (top_pos + 1);
 F91(stack, 1, top_pos);
 rv_aux= value;
}
}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F245(int x)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(x, nil);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(x, nil);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F246(int stream, int condition)
{return F602(stream, condition, 66711, 66407, nil);}

int F247(int stream, int split_condition, int transf_fn, int prune_fnP)
{{int rv_aux = nil; {int term_list = nil; int term_trie = F629(); int term_rec = nil; int term_recs = nil; term_list= F129(stream, split_condition, nil);
 {int term = nil; int term_on = nil; { term_on= term_list;
 term= _SH[term_on];
}

 while (term_on != nil){ if (transf_fn != nil)
{term= F483(transf_fn, term);}
 {int test_aux_var; {int _ARG1__9; {int and_result = nil; { and_result= prune_fnP;
 if (and_result != nil)
{{ and_result= F483(prune_fnP, term);
 if (and_result != nil)
{_ARG1__9= (and_result != nil);}
else {_ARG1__9= (nil != nil);}
}
}
else {_ARG1__9= (nil != nil);}
}

}

 test_aux_var= !(_ARG1__9);
}

 if (test_aux_var)
{{ term_rec= F280(term_trie, term);
 if (term_rec != nil)
{F158(term_rec, 66791, (F750(term_rec, 66791) + 1));}
else {{ term_rec= _c(66794, _c(_c(66790, _c(term, nil)), _c(_c(66791, _c(1, nil)), nil)));
 F437(term_trie, term, term_rec);
 term_recs= _c(term_rec, term_recs);
}
}
}
}
}

 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 rv_aux= _c(F574(term_recs), _c(term_trie, nil));
}

 return rv_aux;
}
}

int F248(int rec, int field, int allP)
{{int rv_aux = nil; {int field_value_list = nil; int f_38v = nil; {int test_aux_var; {int and_result = nil; { and_result= allP;
 if (and_result != nil)
{{ and_result= F556(_SH[rec]);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{field_value_list= rec;}
else {field_value_list= _SH[(rec + 1)];}
}

 while (field_value_list != nil){ f_38v= _SH[field_value_list];
 field_value_list= _SH[(field_value_list + 1)];
 if (_SH[f_38v] == field)
{field_value_list= nil;}
else {if (field_value_list == nil)
{f_38v= nil;}}
}

 rv_aux= f_38v;
}

 return rv_aux;
}
}

int F249(int stream, int split_condition, int transf_fn, int prune_fnP)
{{int rv_aux = nil; {int term_trie = F629(); {int term = nil; int term_on = nil; { term_on= F129(stream, split_condition, nil);
 term= _SH[term_on];
}

 while (term_on != nil){ if (transf_fn != nil)
{term= F483(transf_fn, term);}
 {int test_aux_var; {int _ARG1__9; {int and_result = nil; { and_result= prune_fnP;
 if (and_result != nil)
{{ and_result= F483(prune_fnP, term);
 if (and_result != nil)
{_ARG1__9= (and_result != nil);}
else {_ARG1__9= (nil != nil);}
}
}
else {_ARG1__9= (nil != nil);}
}

}

 test_aux_var= !(_ARG1__9);
}

 if (test_aux_var)
{if (!(F280(term_trie, term) != nil))
{F437(term_trie, term, 66407);}}
}

 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 rv_aux= term_trie;
}

 return rv_aux;
}
}

int F250(int f_38vs, int field_specs)
{{int rv_aux = nil; if (!(f_38vs != nil))
{rv_aux= nil;}
else {if (F260(f_38vs, field_specs) != nil)
{rv_aux= f_38vs;}
else {{ if (!(F510(f_38vs) == F510(field_specs)))
{{ { F87(_SH[66435]);
 F87(_SH[65563]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 {int for_list_result = nil; for_list_result= nil;
 {int field_spec = nil; int field_spec_on = nil; int value = nil; int value_on = nil; { field_spec_on= field_specs;
 field_spec= _SH[field_spec_on];
}

 { value_on= f_38vs;
 value= _SH[value_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(field_spec_on, nil);
 if (and_result != nil)
{{ and_result= F678(value_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ for_list_result= _c(_c(F750(field_spec, 66548), _c(value, nil)), for_list_result);
 { field_spec_on= _SH[(field_spec_on + 1)];
 field_spec= _SH[field_spec_on];
}

 { value_on= _SH[(value_on + 1)];
 value= _SH[value_on];
}

 {int and_result = nil; { and_result= F678(field_spec_on, nil);
 if (and_result != nil)
{{ and_result= F678(value_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= F574(for_list_result);
}

}
}}
 return rv_aux;
}
}

int F251(int in_stream, int out_stream)
{return F360(F299(in_stream), out_stream);}

int F252(int attlist)
{return _c(66828, attlist);}

int F253(int sock_index)
{{int rv_aux = nil; {int byte = nil; if (!(F61(sock_index) != nil))
{F114(sock_index);}
 if (F36(sock_index) != nil)
{rv_aux= -1;}
else {if (F61(sock_index) != nil)
{{ byte= V58[sock_index][V59[sock_index]];
 V59[sock_index]= (V59[sock_index] + 1);
 rv_aux= F335(byte);
}
}
else {rv_aux= nil;}}
}

 return rv_aux;
}
}

int F254(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int or_result = nil; { {int and_result = nil; { if (csym_code >= F773(48))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(57))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= F773(65))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(70))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= F773(97))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(102))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

}

 return rv_aux;
}
}

int F255(int stream)
{return F143(V8[stream]);}

int F256(int x, int y)
{if (x == y)
{return C24;}
else {return nil;}}

int F257(int list, int object)
{{int rv_aux = nil; if (list == nil)
{rv_aux= _c(object, nil);}
else {if (_SH[(list + 1)] == nil)
{rv_aux= _SH[list]= object;}
else {rv_aux= _SH[F676(list)]= object;}}
 return rv_aux;
}
}

void F258(int n)
{{int new_sym = V42; {int repeat_counter = nil; repeat_counter= n;
 while (repeat_counter > 0){ _SH[new_sym]= nil;
 new_sym= (new_sym + 1);
 repeat_counter= (repeat_counter - 1);
}

}

 V42= new_sym;
}
}

int F259(int csym)
{{uint32_t code = (uint32_t)F773(csym); if ((code >> 6) == 2)
{return 66407;}
else {return nil;}
}
}

int F260(int f_38v_list, int field_specs)
{{int rv_aux = nil; {int for_always_result = nil; for_always_result= 66407;
 {int f_38v = nil; int f_38v_on = nil; { f_38v_on= f_38v_list;
 f_38v= _SH[f_38v_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(f_38v_on, nil);
 if (and_result != nil)
{{ and_result= for_always_result;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ {int and_result = nil; { and_result= F556(f_38v);
 if (and_result != nil)
{{ and_result= F594(66548, _SH[f_38v], field_specs, 66443);
 if (and_result != nil)
{for_always_result= and_result;}
else {for_always_result= nil;}
}
}
else {for_always_result= nil;}
}

}

 { f_38v_on= _SH[(f_38v_on + 1)];
 f_38v= _SH[f_38v_on];
}

 {int and_result = nil; { and_result= F678(f_38v_on, nil);
 if (and_result != nil)
{{ and_result= for_always_result;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_always_result;
}

 return rv_aux;
}
}

int F261(int stream)
{{int rv_aux; if (V22[stream] <= V23[stream])
{rv_aux= (66407 != nil);}
else {if (F425(stream) != nil)
{if (F483(F436(stream), stream) != -1)
{rv_aux= (66407 != nil);}
else {rv_aux= (nil != nil);}}
else {if (F661(stream) != nil)
{if (V22[stream] <= V23[stream])
{rv_aux= (66407 != nil);}
else {rv_aux= (nil != nil);}}
else {rv_aux= (nil != nil);}}}
 return rv_aux;
}
}

int F262(int a1)
{return _c(a1, nil);}

int F263(int stream_index)
{{int rv_aux = nil; {int flag = nil; flag= fclose(F579(stream_index));
 V56[stream_index]= NULL;
 rv_aux= flag;
}

 return rv_aux;
}
}

int F264()
{{int rv_aux = nil; { _SH[66786]= F629();
 {int term = nil; int term_on = nil; { term_on= _SH[66775];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66787);
 F437(_SH[66786], F336(term), 66787);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66776];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66788);
 F437(_SH[66786], F336(term), 66788);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66777];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66766);
 F437(_SH[66786], F336(term), 66766);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66778];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66767);
 F437(_SH[66786], F336(term), 66767);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66779];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66768);
 F437(_SH[66786], F336(term), 66768);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66785];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66769);
 F437(_SH[66786], F336(term), 66769);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66784];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66770);
 F437(_SH[66786], F336(term), 66770);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66780];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66772);
 F437(_SH[66786], F336(term), 66772);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66781];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66774);
 F437(_SH[66786], F336(term), 66774);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66782];
 term= _SH[term_on];
}

 while (term_on != nil){ F437(_SH[66786], term, 66789);
 F437(_SH[66786], F336(term), 66789);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 {int term = nil; int term_on = nil; { term_on= _SH[66783];
 term= _SH[term_on];
}

 { while (term_on != nil){ F437(_SH[66786], term, 66773);
 F437(_SH[66786], F336(term), 66773);
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F265(int hashtable, int str, int value)
{{int rv_aux = nil; {int index = nil; int entry_list = nil; int entry = nil; int size = F552(hashtable); index= F456(str, size);
 entry_list= F2(hashtable, index);
 {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= entry_list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F343(_SH[el], str, 66443) != nil)
{for_thereis_result= _SH[(el + 1)];}
else {for_thereis_result= nil;}
 el= F7(el);
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 entry= for_thereis_result;
}

 if (entry != nil)
{_SH[entry]= value;}
else {F91(hashtable, index, _c(str, _c(value, entry_list)));}
 rv_aux= value;
}

 return rv_aux;
}
}

int F266(int iter_spec)
{{int rv_aux = nil; {int condition = nil; int aux = nil; aux= iter_spec;
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 rv_aux= _c(66481, _c(_c(66430, _c(condition, nil)), nil));
}

 return rv_aux;
}
}

int F267(int stream)
{{int rv_aux = nil; if (V22[stream] < V24[stream])
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F268(int stream, int separator, int terminator, int F443)
{{int rv_aux = nil; {int64_t slice_start; int64_t slice_end; int slice_result = nil; slice_start= F237(stream);
 { F200(stream, terminator);
 F554(stream, -1);
}

 slice_end= F237(stream);
 if (slice_end > F418(stream))
{slice_end= F418(stream);}
 {int64_t start = slice_start; int64_t end = slice_end; if (start > end)
;
else {if (V36 >= C16)
{_SH[66422]= _SH[65702];}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { {int test_aux_var; {int or_result = nil; { if (!(F443 != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (!(F750(F443, 66804) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{slice_result= F228(stream, separator);}
else {{int for_list_result = nil; for_list_result= nil;
 {int conversion_fn = nil; int conversion_fn_on = nil; { conversion_fn_on= F750(F443, 66804);
 conversion_fn= _SH[conversion_fn_on];
}

 while (conversion_fn_on != nil){ for_list_result= _c(F52(stream, separator, conversion_fn), for_list_result);
 { conversion_fn_on= _SH[(conversion_fn_on + 1)];
 conversion_fn= _SH[conversion_fn_on];
}

}

}

 slice_result= F574(for_list_result);
}
}
}

 stream_slice_rv= slice_result;
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
}

}
}}
}

 F102(stream, (slice_end + 1));
 F366(stream, terminator);
 rv_aux= slice_result;
}

 return rv_aux;
}
}

int F269(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; {int csym = nil; F663(stream, 1);
 csym= F396(stream, nil);
 if (csym == -1)
{rv_aux= F610(stream, _SH[65612]);}
else {if (csym == 64)
{{ F663(stream, 1);
 rv_aux= _c(_SH[66604], _c(F440(stream, sc_comment_flag, hm_comment_flag), nil));
}
}
else {rv_aux= _c(_SH[66603], _c(F440(stream, sc_comment_flag, hm_comment_flag), nil));}}
}

 return rv_aux;
}
}

int F270(int object, int stream)
{{int rv_aux = nil; if (F16(object) != nil)
{rv_aux= F659(object, stream);}
else {if (F123(object) != nil)
{rv_aux= F277(stream, F292(object));}
else {rv_aux= F277(stream, object);}}
 return rv_aux;
}
}

int F271(int tag)
{return F664(66443, _SH[66823], _c(tag, nil));}

int F272(int pnode)
{{int rv_aux = nil; { V38[V39]= pnode;
 { V39= (V39 + 1);
 rv_aux= V39;
}

}

 return rv_aux;
}
}

int F273(int rec_name, int size)
{return _c(66550, _c(_c(F700(rec_name), _c(66433, _c(66558, nil))), _c(_c(66559, _c(_c(66413, _c(rec_name, nil)), _c(size, _c(66558, nil)))), nil)));}

int F274(int regex, int start_node, int end_node)
{{int rv_aux = nil; {int set_node1 = nil; int set_node2 = nil; {int var_name = nil; int value = nil; int aux = nil; aux= _SH[(regex + 1)];
 var_name= _SH[aux];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 set_node1= _c(66623, _c(_c(66567, _c(66627, nil)), _c(_c(66628, _c(var_name, nil)), nil)));
 set_node2= _c(66623, _c(_c(66567, _c(66629, nil)), _c(_c(66628, _c(var_name, nil)), nil)));
 F646(set_node1, start_node);
 F203(value, set_node1, set_node2);
 rv_aux= F646(end_node, set_node2);
}

}

 return rv_aux;
}
}

int F275(int object, int list, int size, int mode)
{{int rv_aux = nil; if (mode == 66444)
{rv_aux= F755(_c(list, _c(F514(size, object), nil)));}
else {if (mode == 66445)
{rv_aux= F755(_c(F514(size, object), _c(list, nil)));}
else {if (mode == 66446)
{rv_aux= F755(_c(F514((size / 2), object), _c(list, _c(F514((size / 2), object), nil))));}
else {if (mode == 66447)
{rv_aux= F755(_c(F514(size, object), _c(list, _c(F514(size, object), nil))));}
else {rv_aux= nil;}}}}
 return rv_aux;
}
}

int F276(int object, int stream)
{{int rv_aux = nil; if (F16(object) != nil)
{rv_aux= F83(object, stream);}
else {if (F123(object) != nil)
{rv_aux= F142(object, stream);}
else {if (F44(object))
{rv_aux= F235(stream, F37(object, _SH[66598], nil));}
else {if (F29(object))
{rv_aux= F235(stream, F189(object, _SH[66599], nil));}
else {rv_aux= nil;}}}}
 return rv_aux;
}
}

int F277(int stream, int str)
{{int rv_aux = nil; {int csym = nil; int csym_on = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 { while (csym_on != nil){ F659(csym, stream);
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F278(int sock_index)
{{int rv_aux = nil; { close(V57[sock_index]);
 V57[sock_index]= -1;
 rv_aux= nil;
}

 return rv_aux;
}
}

int F279(int test_fn, int key, int clauses)
{{int rv_aux = nil; {int clause = nil; int test = nil; int instrs = nil; int test1 = nil; int expansion = nil; while (clauses != nil){ { clause= _SH[clauses];
 clauses= _SH[(clauses + 1)];
}

 test= _SH[clause];
 instrs= _SH[(clause + 1)];
 if (test == 66407)
{if (clauses != nil)
{expansion= _c(_c(_c(test_fn, _c(key, _c(_c(66413, _c(test, nil)), nil))), F498(instrs, nil)), expansion);}
else {expansion= _c(_c(66407, F498(instrs, nil)), expansion);}}
else {if (F439(test) != nil)
{expansion= _c(_c(_c(test_fn, _c(key, _c(_c(66413, _c(test, nil)), nil))), F498(instrs, nil)), expansion);}
else {while (test != nil){ { test1= _SH[test];
 test= _SH[(test + 1)];
}

 expansion= _c(_c(_c(test_fn, _c(key, _c(_c(66413, _c(test1, nil)), nil))), F498(instrs, nil)), expansion);
}
}}
}

 rv_aux= _c(66420, F498(F574(expansion), nil));
}

 return rv_aux;
}
}

int F280(int trie, int str)
{return _SH[(F326(trie, str) + 1)];}

int F281(int str)
{return F664(66734, _SH[66763], _c(str, nil));}

int F282(int stream, int act_spec)
{{int rv_aux = nil; {int type = nil; int condition = nil; int out_stream = nil; int inTOout_fn = nil; int aux = nil; aux= act_spec;
 type= _SH[aux];
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 aux= _SH[(aux + 1)];
 out_stream= _SH[aux];
 aux= _SH[(aux + 1)];
 inTOout_fn= _SH[aux];
 if (type == 66668)
{rv_aux= _c(66673, _c(stream, _c(condition, _c(out_stream, _c(inTOout_fn, nil)))));}
else {if (type == 66669)
{rv_aux= _c(66674, _c(stream, _c(condition, _c(out_stream, _c(inTOout_fn, nil)))));}
else {if (type == 66670)
{rv_aux= _c(66675, _c(stream, _c(condition, _c(out_stream, _c(inTOout_fn, nil)))));}
else {if (type == 66671)
{rv_aux= _c(66676, _c(stream, _c(condition, _c(out_stream, _c(inTOout_fn, nil)))));}
else {if (type == 66672)
{rv_aux= _c(66677, _c(stream, _c(condition, _c(out_stream, _c(inTOout_fn, nil)))));}
else {rv_aux= nil;}}}}}
}

 return rv_aux;
}
}

int F283(int list)
{return _SH[_SH[(list + 1)]];}

int F284(int str)
{return F664(66734, _SH[66756], _c(str, nil));}

int F285()
{{int rv_aux = nil; { _SH[66754]= _SH[65668];
 _SH[66755]= _SH[65669];
 _SH[66756]= _SH[65670];
 _SH[66757]= _SH[65671];
 _SH[66758]= _SH[65672];
 _SH[66759]= _SH[65673];
 _SH[66760]= _SH[65674];
 _SH[66761]= _SH[65675];
 _SH[66762]= _SH[65676];
 _SH[66763]= _SH[65677];
 F58();
 rv_aux= nil;
}

 return rv_aux;
}
}

int F286(int list, int base)
{{int rv_aux = nil; {int flag = nil; int csym = nil; if (F556(list) != nil)
{{ flag= 66407;
 {int test_aux_var; {int or_result = nil; { or_result= F256(_SH[list], 43);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(_SH[list], 45);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ _SH[list];
 list= _SH[(list + 1)];
}
}
}

 if (list == nil)
{flag= nil;}
 while (list != nil){ { csym= _SH[list];
 list= _SH[(list + 1)];
}

 if (!(F65(csym, base) != nil))
{{ flag= nil;
 list= nil;
}
}
}

 rv_aux= flag;
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F287(int align)
{{int rv_aux = nil; if (align == 66444)
{rv_aux= 66445;}
else {if (align == 66445)
{rv_aux= 66444;}
else {if (align == 66446)
{rv_aux= 66446;}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F288(int recs)
{{int rv_aux = nil; {int spec = F443(_SH[_SH[recs]]); int table = nil; {int for_list_result = nil; for_list_result= nil;
 {int rec = nil; int rec_on = nil; { rec_on= recs;
 rec= _SH[rec_on];
}

 while (rec_on != nil){ for_list_result= _c(F319(rec, spec), for_list_result);
 { rec_on= _SH[(rec_on + 1)];
 rec= _SH[rec_on];
}

}

}

 table= F574(for_list_result);
}

 if (F750(spec, 66808) != nil)
{table= _c(F750(spec, 66803), table);}
 rv_aux= table;
}

 return rv_aux;
}
}

int F289(int list)
{{int rv_aux = nil; {int new_list = nil; while (list != nil){ new_list= _c(_SH[list], new_list);
 list= _SH[(list + 1)];
}

 rv_aux= F574(new_list);
}

 return rv_aux;
}
}

int F290(int stream, int condition)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int cond_type = nil; int cond_type_on = nil; { cond_type_on= _SH[65641];
 cond_type= _SH[cond_type_on];
}

 while (cond_type_on != nil){ for_list_result= _c(_c(cond_type, _c(_c(F163(cond_type), _c(stream, _c(condition, nil))), nil)), for_list_result);
 { cond_type_on= _SH[(cond_type_on + 1)];
 cond_type= _SH[cond_type_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F291(int object, int stream, int delta, int levels, int len1)
{{int rv_aux = nil; { if (levels == 0)
{F235(stream, _SH[65618]);}
else {if (object == nil)
{F235(stream, _SH[65619]);}
else {if (F439(object) != nil)
{F276(object, stream);}
else {if (F88(object, 66407) != nil)
{F3(object, stream, len1);}
else {if (F116(object) != nil)
{{ F235(stream, _SH[65620]);
 F235(stream, F291(F283(object), stream, (delta + 1), (levels - 1), len1));
}
}
else {if (F741(object) != nil)
{F683(object, stream, delta, levels, len1);}
else {F470(object, stream, delta, levels, len1);}}}}}}
 rv_aux= nil;
}

 return rv_aux;
}
}

int F292(int sym_index)
{{ if (!((sym_index >= 0) && (sym_index <= (C21 - 1))))
{return nil;}
 {int name = nil; name= _SH[(sym_index + C21)];
 if (name)
{return name;}
else {if ((sym_index >= 128) && (sym_index <= (C20 - 1)))
{{ name= F95(sym_index);
 _SH[(sym_index + C21)]= F289(name);
 return name;
}
}
else {return nil;}}
}

}
}

int F293(int list, int object)
{{int rv_aux = nil; { _SH[_SH[(list + 1)]]= object;
 rv_aux= object;
}

 return rv_aux;
}
}

int F294(int lp)
{return F289(lp);}

int F295(int csym1, int csym2)
{{int rv_aux = nil; if (F773(csym1) < F773(csym2))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F296(int host)
{{int rv_aux = nil; if (gethostbyname(F711(host, nil)) == NULL)
{rv_aux= nil;}
else {rv_aux= 66407;}
 return rv_aux;
}
}

int F297(int class_value)
{{int rv_aux = nil; {int test_aux_var; {int and_result = nil; { and_result= F88(class_value, 66407);
 if (and_result != nil)
{{ and_result= F256(F283(class_value), 45);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{int for_list_result = nil; for_list_result= nil;
 {int index = nil; index= F773(_SH[class_value]);
 while (index <= F773(F503(class_value))){ for_list_result= _c(F335(index), for_list_result);
 index= (index + 1);
}

}

 rv_aux= F574(for_list_result);
}
}
else {rv_aux= class_value;}
}

 return rv_aux;
}
}

int F298(int new, int old, int list)
{return F479(new, 66443, list, _c(old, nil));}

int F299(int stream)
{{int rv_aux = nil; if (V22[stream] <= V23[stream])
{{int value; {int F60 = V22[stream]; value= V33[F60];
}

 { V22[stream]= (V22[stream] + 1);
 V18[stream]= (V18[stream] + 1);
}

 rv_aux= value;
}
}
else {if (F520(stream) != nil)
{rv_aux= F483(F198(stream), stream);}
else {{int test_aux_var; {int and_result = nil; { and_result= F661(stream);
 if (and_result != nil)
{{ if (V22[stream] <= V23[stream])
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{int value; {int F60 = V22[stream]; value= V33[F60];
}

 { V22[stream]= (V22[stream] + 1);
 V18[stream]= (V18[stream] + 1);
}

 rv_aux= value;
}
}
else {rv_aux= -1;}
}
}}
 return rv_aux;
}
}

int F300(int str1, int str2)
{return F256(F63(str1, str2), 66453);}

int F301(int lp, int type)
{{int rv_aux = nil; {int lp_type1 = nil; int flag = nil; lp_type1= F376(lp);
 if (F343(lp_type1, type, 66443) != nil)
{flag= 66407;}
 F69(lp_type1, nil);
 rv_aux= flag;
}

 return rv_aux;
}
}

int F302(int command, int read_answerP)
{{int rv_aux = nil; if (!(read_answerP != nil))
{{int answer; answer= system(F209(command));
 return answer;
}
}
else {{int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= _SH[65542];
 V10[STREAM__BUF_935]= 66421;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; aux_stream= F712(command);
 if (aux_stream == -1)
{error= _SH[65543];}
else {{ { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= (C1 - 1);
}

 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ {int line = nil; {int for_list_result = nil; for_list_result= nil;
 {; while (F243(stream, nil)){ {int object1__71; int object2__72 = for_list_result; { { line= F624(stream, 10, nil);
 F554(stream, 1);
}

 object1__71= line;
}

 for_list_result= _c(object1__71, object2__72);
}

}

}

 stream_rv= F574(for_list_result);
}

}

 if (!(stream == -1))
{{ F143(stream);
 F344(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}
}
 return rv_aux;
}
}

int F303(int fn, int a1, int a2, int a3, int a4)
{{int rv_aux = nil; switch (fn) {
   case 66326: rv_aux= F50(a1, a2, a3, a4); break;
   case 66538: rv_aux= F462(a1, a2, a3, a4); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, nil))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, nil))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, nil))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, nil))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, nil))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, nil)); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, nil)); break;
   case 66325: rv_aux= F275(a1, a2, a3, a4); break;
   case 66324: rv_aux= F173(a1, a2, a3, a4); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, nil)); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, nil)); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, nil))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, nil))))); break;
   case 66323: rv_aux= F744(a1, a2, a3, a4); break;
   case 66322: rv_aux= F89(a1, a2, a3, a4); break;
   case 66321: rv_aux= F348(a1, a2, a3, a4); break;
   case 66320: rv_aux= F534(a1, a2, a3, a4); break;
   case 66319: rv_aux= F533(a1, a2, a3, a4); break;
   case 66336: rv_aux= F714(a1, a2, a3, a4, 10); break;
   case 66318: rv_aux= F564(a1, a2, a3, a4); break;
   case 66317: rv_aux= F594(a1, a2, a3, a4); break;
   case 66316: rv_aux= F357(a1, a2, a3, a4); break;
   case 66566: rv_aux= F593(a1, a2, a3, a4); break;
   case 66315: rv_aux= F9(a1, a2, a3, a4); break;
   case 66610: rv_aux= F110(a1, a2, a3, a4, _SH[66437]); break;
   case 66673: rv_aux= F634(a1, a2, a3, a4); break;
   case 66674: rv_aux= F611(a1, a2, a3, a4); break;
   case 66675: rv_aux= F201(a1, a2, a3, a4); break;
   case 66676: rv_aux= F553(a1, a2, a3, a4); break;
   case 66677: rv_aux= F765(a1, a2, a3, a4); break;
   case 66314: rv_aux= F72(a1, a2, a3, a4); break;
   case 66329: rv_aux= F602(a1, a2, a3, a4, nil); break;
   case 66313: rv_aux= F501(a1, a2, a3, a4); break;
   case 66312: rv_aux= F57(a1, a2, a3, a4); break;
   case 66311: rv_aux= F34(a1, a2, a3, a4); break;
   case 66310: rv_aux= F247(a1, a2, a3, a4); break;
   case 66309: rv_aux= F414(a1, a2, a3, a4); break;
   case 66308: rv_aux= F249(a1, a2, a3, a4); break;
   case 66307: rv_aux= F315(a1, a2, a3, a4); break;
   case 66306: rv_aux= F316(a1, a2, a3, a4); break;
   case 66305: rv_aux= F345(a1, a2, a3, a4); break;
   case 66304: rv_aux= F670(a1, a2, a3, a4); break;
   case 66303: rv_aux= F485(a1, a2, a3, a4); break;
   case 66302: rv_aux= F268(a1, a2, a3, a4); break;
   case 66327: rv_aux= F386(a1, a2, a3, a4, nil); break;
   case 66301: rv_aux= F71(a1, a2, a3, a4); break;
   case 66300: rv_aux= F144(a1, a2, a3, a4); break;
   case 66339: rv_aux= F635(a1, a2, a3, a4, 66407, 66407, nil); break;
   default: return F563(fn, 4); break;
}
 return rv_aux;
}
}

int F304(int stream)
{return F366(stream, 62);}

int F305(int field_specs)
{{int rv_aux = nil; {int field_38value = nil; int on_var = nil; int list_var = nil; on_var= field_specs;
 while (on_var != nil){ field_38value= _SH[on_var];
 {int object1__2; int object2__3 = list_var; if (F123(field_38value) != nil)
{object1__2= _c(66455, _c(_c(66413, _c(field_38value, nil)), _c(field_38value, nil)));}
else {object1__2= _c(66455, _c(_c(66413, _c(_SH[field_38value], nil)), _c(F283(field_38value), nil)));}
 list_var= _c(object1__2, object2__3);
}

 on_var= _SH[(on_var + 1)];
}

 rv_aux= F574(list_var);
}

 return rv_aux;
}
}

int F306(int line)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int csym = nil; int csym_on = nil; { csym_on= line;
 csym= _SH[csym_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (!(F221(csym) != nil))
{for_thereis_result= C24;}
else {for_thereis_result= nil;}
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

 {int and_result = nil; { and_result= F678(csym_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F307(int str1, int str2)
{return F256(F63(str1, str2), 66454);}

int F308(int stream, int act_spec)
{{int rv_aux = nil; {int type = nil; int condition = nil; int aux = nil; aux= act_spec;
 type= _SH[aux];
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 if (type == 66649)
{rv_aux= _c(66658, _c(stream, _c(condition, nil)));}
else {if (type == 66650)
{rv_aux= _c(66659, _c(stream, _c(condition, nil)));}
else {if (type == 66651)
{rv_aux= _c(66660, _c(stream, _c(condition, nil)));}
else {if (type == 66652)
{rv_aux= _c(66661, _c(stream, _c(condition, nil)));}
else {if (type == 66653)
{rv_aux= _c(66662, _c(stream, _c(condition, nil)));}
else {if (type == 66654)
{rv_aux= _c(66663, _c(stream, _c(condition, nil)));}
else {if (type == 66655)
{rv_aux= _c(66664, _c(stream, _c(condition, nil)));}
else {if (type == 66656)
{rv_aux= _c(66665, _c(stream, _c(condition, nil)));}
else {if (type == 66657)
{rv_aux= _c(66666, _c(stream, _c(condition, nil)));}
else {if (type == 66519)
{rv_aux= _c(66667, _c(stream, _c(condition, nil)));}
else {rv_aux= nil;}}}}}}}}}}
}

 return rv_aux;
}
}

int F309(int lp_rec, int lp_name)
{return F343(lp_name, F750(lp_rec, 66548), 66443);}

int F310(int it_counter, int en_counter)
{{int rv_aux = nil; {int test_aux_var; {int or_result = nil; { {int and_result = nil; { if (it_counter == 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (en_counter == 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (it_counter == en_counter)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{rv_aux= nil;}
else {if (it_counter > en_counter)
{rv_aux= 66792;}
else {rv_aux= 66793;}}
}

 return rv_aux;
}
}

int F311(int list, int object)
{return _SH[_SH[(_SH[(list + 1)] + 1)]]= object;}

int F312()
{return _SH[(F75() + 1)];}

int F313(int hashtable)
{{int rv_aux = nil; {int size = F552(hashtable); {int index = nil; index= 1;
 { while (index <= size){ F69(F2(hashtable, index), nil);
 F91(hashtable, index, nil);
 index= (index + 1);
}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F314(int stream)
{return F353(stream, _SH[65738], nil);}

int F315(int table, int file, int F443, int row_n)
{{int rv_aux = nil; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66806;
 V11[STREAM__BUF_935]= 66807;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; int filename = file; int mode = 66583; int64_t end; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F419(filename) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 aux_stream= F703(filename, mode);
 if (!(aux_stream != nil))
{error= _SH[65699];}
else {{ end= F617(filename);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ stream_rv= F316(table, stream, F443, row_n);
 if (!(stream == -1))
{{ F143(stream);
 F263(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}

 return rv_aux;
}
}

int F316(int table, int stream, int F443, int row_n)
{{int rv_aux = nil; {int separator = nil; int terminator = nil; int row_i = 0; int row = nil; if (F443 != nil)
{{ separator= F750(F443, 66798);
 terminator= F750(F443, 66799);
}
}
else {{ separator= 9;
 terminator= 10;
}
}
 { {int test_aux_var; {int and_result = nil; { and_result= table;
 if (and_result != nil)
{{ if (row_i <= row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ { row= _SH[table];
 table= _SH[(table + 1)];
}

 row_i= (row_i + 1);
 {int test_aux_var; {int or_result = nil; { if (!(F443 != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { if (row_i == 1)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ and_result= F750(F443, 66808);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{F71(row, stream, separator, terminator);}
else {F386(row, stream, separator, terminator, F443);}
}

 {int and_result = nil; { and_result= table;
 if (and_result != nil)
{{ if (row_i <= row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F317(int stream)
{return F678(V11[stream], nil);}

int F318(int stream, int F443)
{{int rv_aux = nil; {int headers = nil; int separator = nil; int terminator = nil; int aux_rec = nil; aux_rec= F443;
 headers= F750(aux_rec, 66803);
 separator= F750(aux_rec, 66798);
 terminator= F750(aux_rec, 66799);
 rv_aux= F71(headers, stream, separator, terminator);
}

 return rv_aux;
}
}

int F319(int rec, int F443)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int field = nil; int field_on = nil; { field_on= F750(F443, 66802);
 field= _SH[field_on];
}

 while (field_on != nil){ for_list_result= _c(F750(rec, field), for_list_result);
 { field_on= _SH[(field_on + 1)];
 field= _SH[field_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F320(int csym)
{{int rv_aux = nil; if (F431(csym) != nil)
{rv_aux= F335(F773(csym) - C6);}
else {rv_aux= csym;}
 return rv_aux;
}
}

int F321(int expr)
{{int rv_aux = nil; {int and_result = nil; { and_result= F556(expr);
 if (and_result != nil)
{{ and_result= F256(_SH[expr], 66414);
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F322(int csym1, int csym2)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(csym1, csym2);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(F320(csym1), F320(csym2));
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F323(int stream, int buf_index)
{{int rv_aux = nil; if (F771(buf_index) != nil)
{rv_aux= V25[stream];}
else {rv_aux= V29[stream];}
 return rv_aux;
}
}

int F324(int exp_instr)
{{int rv_aux = nil; {int condition = F503(exp_instr); int set_vars = nil; if (F159(condition) != nil)
{{ set_vars= F472(condition);
 if (set_vars != nil)
{exp_instr= _c(66424, _c(_c(66647, nil), _c(_c(66409, _c(66647, _c(exp_instr, nil))), F498(set_vars, _c(66647, nil)))));}
}
}
 rv_aux= exp_instr;
}

 return rv_aux;
}
}

int F325(int fn, int a1, int a2)
{{int rv_aux = nil; switch (fn) {
   case 66239: rv_aux= F592(a1, a2); break;
   case 66238: rv_aux= F774(a1, a2); break;
   case 66237: rv_aux= F88(a1, a2); break;
   case 66406: rv_aux= _c(a1, a2); break;
   case 66236: rv_aux= F177(a1, a2); break;
   case 66235: rv_aux= F497(a1, a2); break;
   case 66419: rv_aux= F235(a1, a2); break;
   case 66234: rv_aux= F73(a1, a2); break;
   case 66538: rv_aux= F483(a1, a2); break;
   case 66233: rv_aux= F717(a1, a2); break;
   case 66232: rv_aux= F302(a1, a2); break;
   case 66443: rv_aux= F256(a1, a2); break;
   case 66231: rv_aux= F104(a1, a2); break;
   case 66485: rv_aux= F678(a1, a2); break;
   case 66230: rv_aux= F377(a1, a2); break;
   case 66229: rv_aux= F429(a1, a2); break;
   case 66228: rv_aux= F758(a1, a2); break;
   case 66432: rv_aux= (a1 + a2); break;
   case 66493: rv_aux= (a1 - a2); break;
   case 66616: rv_aux= (a1 * a2); break;
   case 66352: rv_aux= (a1 / a2); break;
   case 66470: if (a1 == a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66227: if (a1 != a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66505: if (a1 > a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66431: if (a1 < a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66429: if (a1 >= a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66428: if (a1 <= a2)
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66226: rv_aux= F196(a1, a2); break;
   case 66225: rv_aux= F124(a1, a2); break;
   case 66224: rv_aux= F15(a1, a2); break;
   case 66410: rv_aux= F137(a1, a2); break;
   case 66223: rv_aux= F125(a1, a2); break;
   case 66439: rv_aux= F779(a1, a2); break;
   case 66222: rv_aux= F413(a1, a2); break;
   case 66221: rv_aux= F675(a1, a2); break;
   case 66220: rv_aux= F694(a1, a2); break;
   case 66219: rv_aux= F363(a1, a2); break;
   case 66218: rv_aux= F167(a1, a2); break;
   case 66688: rv_aux= F664(a1, a2, nil); break;
   case 66351: rv_aux= F747(a1, a2, nil); break;
   case 66217: rv_aux= F424(a1, a2); break;
   case 66368: rv_aux= F60(a1, a2, nil); break;
   case 66216: rv_aux= F514(a1, a2); break;
   case 66296: rv_aux= F728(a1, a2, -1); break;
   case 66295: rv_aux= F422(a1, a2, -1); break;
   case 66215: rv_aux= F519(a1, a2); break;
   case 66214: rv_aux= F452(a1, a2); break;
   case 66350: rv_aux= F588(a1, a2, nil); break;
   case 66349: rv_aux= F66(a1, a2, nil); break;
   case 66325: rv_aux= F275(a1, a2, 1, 66447); break;
   case 66324: rv_aux= F173(a1, a2, 2, 66447); break;
   case 66346: rv_aux= F721(a1, a2, 66448, nil); break;
   case 66345: rv_aux= F560(a1, a2, 66448, nil); break;
   case 66213: rv_aux= _SH[a1]= a2; break;
   case 66212: rv_aux= F293(a1, a2); break;
   case 66211: rv_aux= F311(a1, a2); break;
   case 66210: rv_aux= F257(a1, a2); break;
   case 66209: rv_aux= _SH[(a1 + 1)]= a2; break;
   case 66208: rv_aux= F420(a1, a2); break;
   case 66207: rv_aux= F101(a1, a2); break;
   case 66206: rv_aux= F498(a1, a2); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, nil))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, nil))); break;
   case 66205: rv_aux= F763(a1, a2); break;
   case 66323: rv_aux= F744(a1, a2, 66582, nil); break;
   case 66322: rv_aux= F89(a1, a2, 66582, nil); break;
   case 66321: rv_aux= F348(a1, a2, 66443, nil); break;
   case 66204: rv_aux= F69(a1, a2); break;
   case 66286: rv_aux= F343(a1, a2, 66443); break;
   case 66285: rv_aux= F665(a1, a2, 66443); break;
   case 66717: rv_aux= F757(a1, a2, 66443); break;
   case 66284: rv_aux= F718(a1, a2, 66443); break;
   case 66733: rv_aux= F322(a1, a2); break;
   case 66203: rv_aux= F295(a1, a2); break;
   case 66202: rv_aux= F286(a1, a2); break;
   case 66201: rv_aux= F146(a1, a2); break;
   case 66734: rv_aux= F754(a1, a2); break;
   case 66200: rv_aux= F724(a1, a2); break;
   case 66199: rv_aux= F300(a1, a2); break;
   case 66198: rv_aux= F768(a1, a2); break;
   case 66197: rv_aux= F307(a1, a2); break;
   case 66196: rv_aux= F108(a1, a2); break;
   case 66195: rv_aux= F179(a1, a2); break;
   case 66336: rv_aux= F714(a1, a2, 48, 66444, 10); break;
   case 66318: rv_aux= F564(a1, a2, 32, 66444); break;
   case 66194: rv_aux= F63(a1, a2); break;
   case 66193: rv_aux= F65(a1, a2); break;
   case 66283: rv_aux= F400(a1, a2, 1); break;
   case 66192: rv_aux= F239(a1, a2); break;
   case 66191: rv_aux= F394(a1, a2); break;
   case 66190: rv_aux= F750(a1, a2); break;
   case 66537: rv_aux= F248(a1, a2, nil); break;
   case 66189: rv_aux= F136(a1, a2); break;
   case 66188: rv_aux= F650(a1, a2); break;
   case 66187: rv_aux= F244(a1, a2); break;
   case 66186: rv_aux= F383(a1, a2); break;
   case 66458: rv_aux= F78(a1, a2); break;
   case 66185: rv_aux= F626(a1, a2); break;
   case 66184: rv_aux= F570(a1, a2); break;
   case 66183: rv_aux= F759(a1, a2); break;
   case 66540: rv_aux= F162(a1, a2); break;
   case 66543: rv_aux= F727(a1, a2); break;
   case 66182: rv_aux= F475(a1, a2); break;
   case 66181: rv_aux= F53(a1, a2); break;
   case 66180: rv_aux= F172(a1, a2); break;
   case 66179: rv_aux= F456(a1, a2); break;
   case 66178: rv_aux= F280(a1, a2); break;
   case 66177: rv_aux= F326(a1, a2); break;
   case 66176: rv_aux= F371(a1, a2); break;
   case 66175: rv_aux= F630(a1, a2); break;
   case 66174: rv_aux= F442(a1, a2); break;
   case 66173: rv_aux= F557(a1, a2); break;
   case 66172: rv_aux= F273(a1, a2); break;
   case 66171: rv_aux= F250(a1, a2); break;
   case 66170: rv_aux= F260(a1, a2); break;
   case 66169: rv_aux= F600(a1, a2); break;
   case 66168: rv_aux= F710(a1, a2); break;
   case 66167: rv_aux= F438(a1, a2); break;
   case 66166: rv_aux= F40(a1, a2); break;
   case 66165: rv_aux= F330(a1, a2); break;
   case 66164: rv_aux= F301(a1, a2); break;
   case 66163: rv_aux= F82(a1, a2); break;
   case 66273: rv_aux= F637(a1, a2, nil); break;
   case 66162: rv_aux= F651(a1, a2); break;
   case 66161: rv_aux= F241(a1, a2); break;
   case 66272: rv_aux= F352(a1, a2, nil); break;
   case 66160: rv_aux= F530(a1, a2); break;
   case 66159: rv_aux= F596(a1, a2); break;
   case 66581: rv_aux= F309(a1, a2); break;
   case 66158: rv_aux= F526(a1, a2); break;
   case 66157: rv_aux= F465(a1, a2); break;
   case 66156: rv_aux= F323(a1, a2); break;
   case 66269: rv_aux= F129(a1, a2, nil); break;
   case 66155: rv_aux= F231(a1, a2); break;
   case 66154: rv_aux= F194(a1, a2); break;
   case 66153: rv_aux= F527(a1, a2); break;
   case 66267: rv_aux= SHARKread(a1, a2, nil); break;
   case 66152: rv_aux= F516(a1, a2); break;
   case 66151: rv_aux= F134(a1, a2); break;
   case 66150: rv_aux= F610(a1, a2); break;
   case 66415: rv_aux= F195(a1, a2, _SH[66601]); break;
   case 66610: rv_aux= F110(a1, a2, 0, _SH[66436], _SH[66437]); break;
   case 66149: rv_aux= F614(a1, a2); break;
   case 66148: rv_aux= F276(a1, a2); break;
   case 66147: rv_aux= F83(a1, a2); break;
   case 66146: rv_aux= F142(a1, a2); break;
   case 66145: rv_aux= F674(a1, a2); break;
   case 66144: rv_aux= F639(a1, a2); break;
   case 66143: rv_aux= F507(a1, a2); break;
   case 66142: rv_aux= F216(a1, a2); break;
   case 66141: rv_aux= F411(a1, a2); break;
   case 66140: rv_aux= F140(a1, a2); break;
   case 66614: rv_aux= F372(a1, a2); break;
   case 66139: rv_aux= F644(a1, a2); break;
   case 66138: rv_aux= F646(a1, a2); break;
   case 66137: rv_aux= F45(a1, a2); break;
   case 66136: rv_aux= F628(a1, a2); break;
   case 66135: rv_aux= F733(a1, a2); break;
   case 66134: rv_aux= F476(a1, a2); break;
   case 66133: rv_aux= F168(a1, a2); break;
   case 66132: rv_aux= F308(a1, a2); break;
   case 66131: rv_aux= F282(a1, a2); break;
   case 66130: rv_aux= F666(a1, a2); break;
   case 66129: rv_aux= F692(a1, a2); break;
   case 66128: rv_aux= F591(a1, a2); break;
   case 66704: rv_aux= F509(a1, a2, nil); break;
   case 66705: rv_aux= F329(a1, a2, nil); break;
   case 66706: rv_aux= F353(a1, a2, nil); break;
   case 66698: rv_aux= F764(a1, a2); break;
   case 66700: rv_aux= F148(a1, a2); break;
   case 66701: rv_aux= F631(a1, a2); break;
   case 66699: rv_aux= F640(a1, a2); break;
   case 66702: rv_aux= F246(a1, a2); break;
   case 66703: rv_aux= F544(a1, a2); break;
   case 66707: rv_aux= F690(a1, a2, nil); break;
   case 66658: rv_aux= F361(a1, a2); break;
   case 66659: rv_aux= F23(a1, a2); break;
   case 66660: rv_aux= F200(a1, a2); break;
   case 66662: rv_aux= F366(a1, a2); break;
   case 66663: rv_aux= F459(a1, a2); break;
   case 66661: rv_aux= F669(a1, a2); break;
   case 66664: rv_aux= F738(a1, a2); break;
   case 66665: rv_aux= F713(a1, a2); break;
   case 66666: rv_aux= F554(a1, a2); break;
   case 66667: rv_aux= F155(a1, a2); break;
   case 66683: rv_aux= F433(a1, a2, nil); break;
   case 66684: rv_aux= F624(a1, a2, nil); break;
   case 66685: rv_aux= F480(a1, a2, nil); break;
   case 66686: rv_aux= F581(a1, a2, nil); break;
   case 66687: rv_aux= F11(a1, a2, nil); break;
   case 66127: rv_aux= F290(a1, a2); break;
   case 66126: rv_aux= F513(a1, a2); break;
   case 66125: rv_aux= F362(a1, a2); break;
   case 66124: rv_aux= F151(a1, a2); break;
   case 66123: rv_aux= F524(a1, a2); break;
   case 66122: rv_aux= F772(a1, a2); break;
   case 66121: rv_aux= F563(a1, a2); break;
   case 66120: rv_aux= F242(a1, a2); break;
   case 66119: rv_aux= F233(a1, a2); break;
   case 66118: rv_aux= F613(a1, a2); break;
   case 66117: rv_aux= F138(a1, a2); break;
   case 66116: rv_aux= F695(a1, a2); break;
   case 66590: rv_aux= stdoutTyo(a1, a2); break;
   case 66593: rv_aux= F128(a1, a2); break;
   case 66115: rv_aux= F776(a1, a2); break;
   case 66250: rv_aux= F671(a1, a2, nil); break;
   case 66114: rv_aux= F59(a1, a2); break;
   case 66113: rv_aux= F310(a1, a2); break;
   case 66310: rv_aux= F247(a1, a2, nil, nil); break;
   case 66309: rv_aux= F414(a1, a2, nil, nil); break;
   case 66308: rv_aux= F249(a1, a2, nil, nil); break;
   case 66112: rv_aux= F106(a1, a2); break;
   case 66249: rv_aux= F775(a1, a2, 10); break;
   case 66111: rv_aux= F318(a1, a2); break;
   case 66248: rv_aux= F469(a1, a2, C1); break;
   case 66307: rv_aux= F315(a1, a2, nil, C1); break;
   case 66247: rv_aux= F122(a1, a2, C1); break;
   case 66306: rv_aux= F316(a1, a2, nil, C1); break;
   case 66246: rv_aux= F359(a1, a2, C1); break;
   case 66245: rv_aux= F558(a1, a2, C1); break;
   case 66302: rv_aux= F268(a1, a2, 10, nil); break;
   case 66110: rv_aux= F228(a1, a2); break;
   case 66243: rv_aux= F52(a1, a2, nil); break;
   case 66327: rv_aux= F386(a1, a2, 9, 10, nil); break;
   case 66109: rv_aux= F334(a1, a2); break;
   case 66108: rv_aux= F319(a1, a2); break;
   case 66107: rv_aux= F206(a1, a2); break;
   case 66106: rv_aux= F599(a1, a2); break;
   case 66105: rv_aux= F659(a1, a2); break;
   case 66104: rv_aux= F150(a1, a2); break;
   case 66103: rv_aux= F770(a1, a2); break;
   case 66102: rv_aux= F270(a1, a2); break;
   case 66101: rv_aux= F277(a1, a2); break;
   case 66100: rv_aux= F207(a1, a2); break;
   case 66339: rv_aux= F635(a1, a2, 66843, nil, 66407, 66407, nil); break;
   case 66099: rv_aux= F426(a1, a2); break;
   case 66098: rv_aux= F616(a1, a2); break;
   case 66097: rv_aux= F175(a1, a2); break;
   case 66096: rv_aux= F113(a1, a2); break;
   case 66095: rv_aux= F354(a1, a2); break;
   case 66094: rv_aux= F51(a1, a2); break;
   case 66093: rv_aux= F360(a1, a2); break;
   case 66092: rv_aux= F251(a1, a2); break;
   case 66091: rv_aux= F491(a1, a2); break;
   case 66090: rv_aux= F427(a1, a2); break;
   case 66089: rv_aux= F643(a1, a2); break;
   default: return F563(fn, 2); break;
}
 return rv_aux;
}
}

int F326(int trie, int str)
{{int rv_aux = nil; {int end = nil; int tnode = nil; int sub_tnode = nil; tnode= trie;
 while (!(end != nil)){ sub_tnode= F630(_SH[str], tnode);
 if (sub_tnode == nil)
{end= 66407;}
else {if (_SH[(str + 1)] != nil)
{{ { _SH[str];
 str= _SH[(str + 1)];
}

 tnode= sub_tnode;
}
}
else {end= 66407;}}
}

 if (sub_tnode != nil)
{rv_aux= F632(sub_tnode);}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F327(int rec_type_name)
{return F750(F172(_SH[66569], F292(rec_type_name)), 66572);}

int F328(int str)
{{int rv_aux = nil; {int str_buf = nil; while (str != nil){ str_buf= _c(F672(_SH[str]), str_buf);
 { _SH[str];
 str= _SH[(str + 1)];
}

}

 rv_aux= F574(str_buf);
}

 return rv_aux;
}
}

int F329(int stream, int condition, int max_delta)
{return F602(stream, condition, 66708, nil, max_delta);}

int F330(int lp_name, int path)
{{int rv_aux = nil; {int lp_rec = nil; path= F668(path);
 lp_rec= F582(lp_name);
 if (lp_rec == nil)
{{ lp_rec= _c(66578, _c(_c(66548, _c(lp_name, nil)), _c(_c(66579, _c(path, nil)), nil)));
 F158(_SH[66427], 66580, _c(lp_rec, F750(_SH[66427], 66580)));
}
}
else {F158(lp_rec, 66579, path);}
 rv_aux= lp_name;
}

 return rv_aux;
}
}

void F331()
{{ _SH[66427]= _c(66404, _c(_c(66403, _c(66402, nil)), _c(_c(66401, _c(66400, nil)), _c(_c(66399, _c(F615(115, 112, 97, 105, 46, 102, 117, 98, 46, 105, 116), nil)), _c(_c(66740, _c(F492(108, 105, 110, 117, 120), nil)), _c(_c(66398, _c(F262(99), nil)), _c(_c(66397, _c(F504(47, 104, 111, 109, 101, 47, 103, 114, 111, 109, 97, 110, 111), nil)), _c(_c(66396, _c(66407, nil)), _c(_c(66395, _c(_c(66394, nil), nil)), _c(_c(66393, _c(nil, nil)), _c(_c(66392, _c(66391, nil)), _c(_c(66390, _c(32, nil)), _c(_c(66389, _c(32, nil)), _c(_c(66388, _c(23, nil)), _c(_c(66387, _c(8, nil)), _c(_c(66386, _c(2147483647, nil)), _c(_c(66385, _c(268435456, nil)), _c(_c(66384, _c(3, nil)), nil))))))))))))))))));
 F330(_SH[65537], F504(47, 104, 111, 109, 101, 47, 103, 114, 111, 109, 97, 110, 111));
}
}

int F332(int stream)
{{int rv_aux = nil; { {int buf_index = V20[stream]; if (F771(buf_index) != nil)
{V28[stream]= 66407;}
else {V32[stream]= 66407;}
}

 rv_aux= nil;
}

 return rv_aux;
}
}

int F333(int stream_index)
{return fflush(F579(stream_index));}

int F334(int delimiter, int stream)
{{int rv_aux = nil; if (F123(delimiter) != nil)
{rv_aux= F673(delimiter, stream);}
else {rv_aux= F235(stream, delimiter);}
 return rv_aux;
}
}

int F335(int num)
{return num;}

int F336(int str)
{{int rv_aux = nil; {int str_buf = nil; while (str != nil){ str_buf= _c(F320(_SH[str]), str_buf);
 { _SH[str];
 str= _SH[(str + 1)];
}

}

 rv_aux= F574(str_buf);
}

 return rv_aux;
}
}

int F337(int lp_file)
{return F302(F755(_c(_SH[65660], _c(F40(lp_file, F750(_SH[66427], 66740)), nil))), nil);}

int F338(int list, int fn, int include_empty_itemsP)
{{int rv_aux = nil; {int cur_csym = nil; int cur_item = nil; int items = nil; while (list != nil){ { cur_csym= _SH[list];
 list= _SH[(list + 1)];
}

 if (F483(fn, cur_csym) != nil)
{{ {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 cur_item= nil;
}
}
else {cur_item= _c(cur_csym, cur_item);}
}

 {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 rv_aux= F574(items);
}

 return rv_aux;
}
}

int F339(int lp)
{{int rv_aux = nil; {int slash_pos = nil; slash_pos= F526(47, lp);
 if (slash_pos == nil)
{rv_aux= nil;}
else {if (slash_pos == 1)
{rv_aux= F289(lp);}
else {rv_aux= F532(lp, 1, slash_pos);}}
}

 return rv_aux;
}
}

int F340(int aux_stream, int64_t F237)
{{int rv_aux = nil; { F197(aux_stream, (F237 - 1));
 rv_aux= aux_stream;
}

 return rv_aux;
}
}

int F341(int par_name)
{return F750(_SH[66427], par_name);}

int F342(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, nil)))))))))))))))))))))))))));}

int F343(int list1, int list2, int pred1)
{{int rv_aux = nil; {int flag = nil; int e1 = nil; int e2 = nil; flag= 66407;
 while (list1 != nil){ if (list2 == nil)
{{ flag= nil;
 list1= nil;
}
}
else {{ { e1= _SH[list1];
 list1= _SH[(list1 + 1)];
}

 { e2= _SH[list2];
 list2= _SH[(list2 + 1)];
}

 if (!(F325(pred1, e1, e2) != nil))
{{ flag= nil;
 list1= nil;
}
}
}
}
}

 if (list2 != nil)
{flag= nil;}
 rv_aux= flag;
}

 return rv_aux;
}
}

int F344(int stream_index)
{{int rv_aux; {int flag = nil; flag= pclose(F579(stream_index));
 V56[stream_index]= NULL;
 rv_aux= flag;
}

 return rv_aux;
}
}

int F345(int recs, int out_file, int F443, int row_n)
{{int rv_aux = nil; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66806;
 V11[STREAM__BUF_935]= 66807;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; int filename = out_file; int mode = 66583; int64_t end; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F419(filename) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 aux_stream= F703(filename, mode);
 if (!(aux_stream != nil))
{error= _SH[65701];}
else {{ end= F617(filename);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ stream_rv= F485(recs, stream, F443, row_n);
 if (!(stream == -1))
{{ F143(stream);
 F263(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}

 return rv_aux;
}
}

int F346(int stream)
{{int rv_aux = nil; {int comment = nil; {int csym = nil; int csym2 = nil; int end_of_hm_commentP = nil; int counter = nil; counter= 1;
 comment= _c(F299(stream), comment);
 comment= _c(F299(stream), comment);
 while (!(end_of_hm_commentP != nil)){ csym= F299(stream);
 if (csym == -1)
{{ F610(stream, _SH[65609]);
 end_of_hm_commentP= 66407;
}
}
else {{ comment= _c(csym, comment);
 if (csym == 124)
{{ csym2= F396(stream, nil);
 if (csym2 == 35)
{{ comment= _c(csym2, comment);
 F663(stream, 1);
 counter= (counter - 1);
 if (counter == 0)
{end_of_hm_commentP= 66407;}
}
}
}
}
else {if (csym == 35)
{{ csym2= F396(stream, nil);
 if (csym2 == 124)
{{ comment= _c(csym2, comment);
 F663(stream, 1);
 counter= (counter + 1);
}
}
}
}}
}
}
}

}

 rv_aux= _c(66606, _c(F574(comment), nil));
}

 return rv_aux;
}
}

int F347(int stream, int buf_index, int buf_start, int nbytes)
{{int rv_aux = nil; {int min_buf_pos; int max_buf_pos; int cur_buf_pos; int in_buf_min; int in_buf_max; {int buf_size = V6[stream]; int64_t F60 = V18[stream]; int64_t min = V15[stream]; int64_t max = V16[stream]; min_buf_pos= F623(min, buf_size, buf_index, buf_start);
 cur_buf_pos= F623(F60, buf_size, buf_index, buf_start);
 max_buf_pos= F623(max, buf_size, buf_index, buf_start);
 V20[stream]= buf_index;
 V22[stream]= cur_buf_pos;
 if (F478(stream) != nil)
{{ if (buf_start >= min_buf_pos)
{in_buf_min= buf_start;}
else {in_buf_min= min_buf_pos;}
 V21[stream]= in_buf_min;
 if (max_buf_pos <= (buf_start + nbytes + -1))
{in_buf_max= max_buf_pos;}
else {in_buf_max= (buf_start + nbytes + -1);}
 V23[stream]= in_buf_max;
}
}
else {{ V21[stream]= C1;
 V23[stream]= (- C1);
}
}
 V24[stream]= (- C1);
}

 rv_aux= nil;
}

 return rv_aux;
}
}

int F348(int list, int delimiter, int pred1, int include_empty_itemsP)
{{int rv_aux = nil; if (list == nil)
{rv_aux= nil;}
else {if (F16(delimiter) != nil)
{rv_aux= F534(list, delimiter, pred1, include_empty_itemsP);}
else {if (F88(delimiter, 66407) != nil)
{rv_aux= F533(list, delimiter, pred1, include_empty_itemsP);}
else {if (F123(delimiter) != nil)
{rv_aux= F338(list, delimiter, include_empty_itemsP);}
else {if (_SH[delimiter] == 66451)
{rv_aux= F607(list, F283(delimiter), include_empty_itemsP);}
else {{ { { F87(_SH[66435]);
 F87(_SH[65546]);
 F87(_SH[66435]);
 F373(nil);
}

}

 rv_aux= list;
}
}}}}}
 return rv_aux;
}
}

int F349(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F447(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

 while (test_aux_var){ csym= F253(aux_stream);
 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F447(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F350(int F339)
{return F302(F755(_c(_SH[65659], _c(F40(F339, F750(_SH[66427], 66740)), nil))), nil);}

int F351(int F339)
{{int rv_aux = nil; {int dir_list = nil; int cur_dir = nil; dir_list= F348(F668(F339), _SH[65592], 66443, nil);
 cur_dir= F289(_SH[65593]);
 {int sub_dir = nil; int sub_dir_on = nil; { sub_dir_on= dir_list;
 sub_dir= _SH[sub_dir_on];
}

 { while (sub_dir_on != nil){ cur_dir= F735(_c(cur_dir, _c(sub_dir, _c(F289(_SH[65594]), nil))));
 if (F419(cur_dir) != nil)
;
else {{ { F87(_SH[66435]);
 F87(_SH[65595]);
 F87(cur_dir);
 F373(nil);
}

 F350(cur_dir);
}
}
 { sub_dir_on= _SH[(sub_dir_on + 1)];
 sub_dir= _SH[sub_dir_on];
}

}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F352(int lp, int new_lp, int traceP)
{{int rv_aux = nil; if (F757(lp, _SH[65585], 66443) != nil)
{if (F757(new_lp, _SH[65586], 66443) != nil)
{rv_aux= F220(lp, new_lp, traceP);}
else {{ F87(_SH[66435]);
 F87(new_lp);
 F87(_SH[65587]);
 rv_aux= F373(nil);
}
}}
else {if (F757(new_lp, _SH[65588], 66443) != nil)
{rv_aux= F138(lp, F755(_c(new_lp, _c(F82(66407, _SH[66407]), nil))));}
else {rv_aux= F138(lp, new_lp);}}
 return rv_aux;
}
}

int F353(int stream, int condition, int delta)
{return F501(stream, condition, delta, nil);}

int F354(int unmatched_tags_as_singleP, int print_errorsP)
{{int rv_aux = nil; {int html = nil; F395((_SH[66836] - 1), unmatched_tags_as_singleP, print_errorsP);
 if (F312() != nil)
{html= F627();}
else {html= _SH[F75()];}
 _SH[66836]= (_SH[66836] - 1);
 rv_aux= html;
}

 return rv_aux;
}
}

int F355(int stream)
{return fflush(stderr);}

int F356(int object)
{{int rv_aux = nil; {int and_result = nil; { {int or_result = nil; { or_result= F116(object);
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= F321(object);
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}

}

 if (and_result != nil)
{{ and_result= F88(_SH[_SH[(object + 1)]], 66407);
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F357(int rec, int rec_name, int fields, int value)
{{int rv_aux = nil; if (F123(fields) != nil)
{rv_aux= F9(rec_name, rec, fields, value);}
else {if (!(_SH[(fields + 1)] != nil))
{rv_aux= F9(rec_name, rec, _SH[fields], value);}
else {{ rec= F751(rec_name, rec, _SH[fields]);
 rv_aux= F357(rec, F710(rec_name, _SH[fields]), _SH[(fields + 1)], value);
}
}}
 return rv_aux;
}
}

int F358(int stream1, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int csym = nil; aux_stream= V8[stream1];
 aux_stream= F499(aux_stream, V37);
 {int repeat_var = nil; int buf_pos = nil; repeat_var= 1;
 buf_pos= buf_start;
 { while (repeat_var <= nbytes){ csym= V33[buf_pos];
 if (_SH[(aux_stream + 1)] != nil)
{{ F293(aux_stream, csym);
 aux_stream= _SH[(aux_stream + 1)];
}
}
else {{ _SH[(aux_stream + 1)]= _c(csym, nil);
 aux_stream= _SH[(aux_stream + 1)];
}
}
 repeat_var= (repeat_var + 1);
 buf_pos= (buf_pos + 1);
}

 rv_aux= nil;
}

}

}

 return rv_aux;
}
}

int F359(int file, int F443, int row_n)
{{int rv_aux = nil; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66806;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; int filename = file; int mode = nil; int64_t end; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F419(filename) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 aux_stream= F703(filename, mode);
 if (!(aux_stream != nil))
{error= _SH[65700];}
else {{ end= F617(filename);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ stream_rv= F558(stream, F443, row_n);
 if (!(stream == -1))
{{ F143(stream);
 F263(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}

 return rv_aux;
}
}

int F360(int csym, int stream)
{{uint32_t code = (uint32_t)F773(csym); if (code < 128)
{F673(csym, stream);}
else {if (code < 2048)
{{ F673(F335((((uint32_t)code >> 6) & 31) | 192), stream);
 F673(F335((((uint32_t)code >> 0) & 63) | 128), stream);
}
}
else {if (code < 65536)
{{ F673(F335((((uint32_t)code >> 12) & 15) | 224), stream);
 F673(F335((((uint32_t)code >> 6) & 63) | 128), stream);
 F673(F335((((uint32_t)code >> 0) & 63) | 128), stream);
}
}
else {if (code < 1114112)
{{ F673(F335((((uint32_t)code >> 18) & 7) | 240), stream);
 F673(F335((((uint32_t)code >> 12) & 63) | 128), stream);
 F673(F335((((uint32_t)code >> 6) & 63) | 128), stream);
 F673(F335((((uint32_t)code >> 0) & 63) | 128), stream);
}
}}}}
}
}

int F361(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66461);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F362(int stream, int or_condition)
{{int rv_aux = nil; {int delta = nil; {int test_aux_var; {int for_thereis_result = nil; for_thereis_result= nil;
 {int condition = nil; int condition_on = nil; { condition_on= _SH[(or_condition + 1)];
 condition= _SH[condition_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(condition_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ { delta= F524(stream, condition);
 if (delta >= 0)
{for_thereis_result= C24;}
else {for_thereis_result= nil;}
}

 { condition_on= _SH[(condition_on + 1)];
 condition= _SH[condition_on];
}

 {int and_result = nil; { and_result= F678(condition_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 test_aux_var= (for_thereis_result != nil);
}

 if (test_aux_var)
{rv_aux= delta;}
else {rv_aux= -1;}
}

}

 return rv_aux;
}
}

int F363(int list, int n)
{{int rv_aux = nil; {int len1 = nil; int F424 = nil; int cons1 = nil; if (!(n != nil))
{n= 1;}
 len1= F510(list);
 F424= (len1 - n);
 if (F424 > 0)
{{ cons1= F125(list, F424);
 F69(_SH[(cons1 + 1)], nil);
 _SH[(cons1 + 1)]= nil;
 rv_aux= list;
}
}
else {{ F69(list, nil);
 rv_aux= nil;
}
}
}

 return rv_aux;
}
}

int F364(int object)
{{int rv_aux = nil; {int or_result = nil; { or_result= F439(object);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F88(object, 66407);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F365(int stream)
{{int rv_aux = nil; {int64_t F60 = V18[stream]; int64_t min = V15[stream]; int64_t max = V16[stream]; int64_t end = V17[stream]; {int and_result = nil; { if (F60 >= min)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F60 <= max)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F60 <= (end + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

}

 return rv_aux;
}
}

int F366(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66709);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F367(int str)
{return F664(66734, _SH[66778], _c(str, nil));}

int64_t F368(int stream, int condition)
{{int64_t rv_aux; {int64_t F424; int64_t pos2; F424= F237(stream);
 F200(stream, condition);
 pos2= F237(stream);
 F102(stream, F424);
 rv_aux= pos2;
}

 return rv_aux;
}
}

int F369(int iter_spec)
{{int rv_aux = nil; {int var = nil; int expansion = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 {int in_depthA = F78(iter_spec, 66466); int exp_by = F78(iter_spec, 66487); int exp_while = F78(iter_spec, 66488); expansion= F93(var, exp_by, exp_while);
 {int object1__12 = 66481; int object2__13; {int object1__14 = _c(66476, _c(_c(var, _c(_c(66489, _c(66490, nil)), _c(_c(66489, _c(66491, nil)), nil))), nil)); int object2__15; {int object1__34 = _c(66477, _c(_c(66405, _c(_c(66409, _c(66490, _c(_c(66492, nil), nil))), _c(_c(66409, _c(66491, _c(_c(66493, _c(66490, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(in_depthA, nil))), nil)))), nil)); int object2__35; {int object1__74 = _c(66461, _c(_c(66494, _c(_c(66485, _c(var, _c(nil, nil))), _c(_c(66429, _c(66491, _c(66490, nil))), nil))), nil)); int object2__75; {int object1__98; int object2__99; {int object1__100 = 66479; int object2__101; {int object1__102; int object2__103; {int object1__104 = 66405; int object2__105; {int list__106; int sl__107; if (expansion != nil)
{list__106= _c(expansion, nil);}
else {list__106= nil;}
 sl__107= _c(_c(66409, _c(66491, _c(_c(66498, _c(var, _c(66491, nil))), nil))), _c(_c(66420, _c(_c(_c(66429, _c(66491, _c(66490, nil))), _c(_c(66409, _c(var, _c(_c(66496, _c(66491, nil)), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(nil, nil))), nil)), nil))), nil));
 object2__105= F498(list__106, sl__107);
}

 object1__102= _c(object1__104, object2__105);
}

 object2__103= nil;
 object2__101= _c(object1__102, object2__103);
}

 object1__98= _c(object1__100, object2__101);
}

 object2__99= _c(_c(66480, _c(_c(66497, nil), nil)), nil);
 object2__75= _c(object1__98, object2__99);
}

 object2__35= _c(object1__74, object2__75);
}

 object2__15= _c(object1__34, object2__35);
}

 object2__13= _c(object1__14, object2__15);
}

 rv_aux= _c(object1__12, object2__13);
}

}

}

 return rv_aux;
}
}

int F370(int lp_name)
{{int rv_aux = nil; {int lp_rec = nil; lp_rec= F582(lp_name);
 if (lp_rec != nil)
{rv_aux= F750(lp_rec, 66579);}
else {rv_aux= lp_name;}
}

 return rv_aux;
}
}

int F371(int trie, int str)
{{int rv_aux = nil; {int end = nil; int tnode = nil; int sub_tnode = nil; int leaf = nil; int sub_tnode_temp = nil; tnode= trie;
 while (!(end != nil)){ sub_tnode= F630(_SH[str], tnode);
 if (sub_tnode == nil)
{{ sub_tnode_temp= F557(str, nil);
 {int aux = nil; aux= sub_tnode_temp;
 sub_tnode= _SH[aux];
 aux= _SH[(aux + 1)];
 leaf= _SH[aux];
}

 F69(sub_tnode_temp, nil);
 F442(sub_tnode, tnode);
 tnode= leaf;
 end= 66407;
}
}
else {if (_SH[(str + 1)] != nil)
{{ { _SH[str];
 str= _SH[(str + 1)];
}

 tnode= sub_tnode;
}
}
else {{ tnode= F632(sub_tnode);
 if (!(tnode != nil))
{{ tnode= _c(C11, nil);
 F442(tnode, sub_tnode);
}
}
 end= 66407;
}
}}
}

 rv_aux= tnode;
}

 return rv_aux;
}
}

int F372(int pnode, int var_name)
{{int rv_aux = nil; {int value = nil; int values = nil; while (pnode != nil){ {int aux = nil; aux= F644(pnode, var_name);
 value= _SH[aux];
 aux= _SH[(aux + 1)];
 pnode= _SH[aux];
}

 {int test_aux_var; {int or_result = nil; { or_result= pnode;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= value;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{values= _c(value, values);}
}

}

 if (_SH[(values + 1)] != nil)
{rv_aux= F574(values);}
else {rv_aux= _SH[values];}
}

 return rv_aux;
}
}

int F373(int stream)
{return fflush(stdout);}

int F374(int rec_name, int size, int field_specs)
{{int rv_aux = nil; {int object1__1 = _c(66545, _c(66546, _c(F10(rec_name), _c(_c(66547, _c(size, nil)), nil)))); int object2__2; {int list__16; int sl__17; {int for_list_result = nil; for_list_result= nil;
 {int field_spec = nil; int field_spec_on = nil; { field_spec_on= field_specs;
 field_spec= _SH[field_spec_on];
}

 while (field_spec_on != nil){ {int object1__21; int object2__22 = for_list_result; {int name = nil; int type_name = nil; int aux_rec = nil; aux_rec= field_spec;
 name= F750(aux_rec, 66548);
 type_name= F750(aux_rec, 66549);
 object1__21= _c(66545, _c(F180(type_name), _c(F600(rec_name, name), _c(_c(66547, _c(size, nil)), nil))));
}

 for_list_result= _c(object1__21, object2__22);
}

 { field_spec_on= _SH[(field_spec_on + 1)];
 field_spec= _SH[field_spec_on];
}

}

}

 list__16= F574(for_list_result);
}

 sl__17= nil;
 object2__2= F498(list__16, sl__17);
}

 rv_aux= _c(object1__1, object2__2);
}

 return rv_aux;
}
}

int F375(int stream)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int csym_test = nil; int csym_test_on = nil; { csym_test_on= _SH[65640];
 csym_test= _SH[csym_test_on];
}

 while (csym_test_on != nil){ for_list_result= _c(_c(csym_test, _c(_c(F163(csym_test), _c(stream, nil)), nil)), for_list_result);
 { csym_test_on= _SH[(csym_test_on + 1)];
 csym_test= _SH[csym_test_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F376(int lp)
{{int rv_aux = nil; {int point_pos = nil; point_pos= F424(46, lp);
 if (point_pos == -1)
{rv_aux= nil;}
else {{ lp= F779(lp, point_pos);
 if (F461(_SH[lp]) != nil)
{rv_aux= F376(lp);}
else {if (F424(47, lp) == -1)
{rv_aux= F289(lp);}
else {rv_aux= F376(lp);}}
}
}
}

 return rv_aux;
}
}

int F377(int args, int result_var)
{{int rv_aux = nil; if (!(args != nil))
{rv_aux= result_var;}
else {rv_aux= _c(66405, _c(_c(66409, _c(result_var, _c(_SH[args], nil))), _c(_c(66408, _c(result_var, _c(F377(_SH[(args + 1)], result_var), nil))), nil)));}
 return rv_aux;
}
}

int F378(int regex)
{{int rv_aux = nil; if (F774(regex, 66409) != nil)
{rv_aux= _c(F283(regex), F378(F503(regex)));}
else {{int test_aux_var; {int or_result = nil; { or_result= F774(regex, 66405);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F774(regex, 66494);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{int for_conc_result = nil; for_conc_result= nil;
 {int el = nil; int el_on = nil; { el_on= _SH[(regex + 1)];
 el= _SH[el_on];
}

 while (el_on != nil){ for_conc_result= F735(_c(for_conc_result, _c(F378(el), nil)));
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

}

 rv_aux= for_conc_result;
}
}
else {if (F774(regex, 66616) != nil)
{rv_aux= F378(F283(regex));}
else {rv_aux= nil;}}
}
}
 return rv_aux;
}
}

int F379(int regex, int start_node, int end_node)
{{int rv_aux = nil; {int or_node = nil; or_node= _c(66623, _c(_c(66567, _c(66494, nil)), nil));
 {int el = nil; int el_on = nil; { el_on= _SH[(regex + 1)];
 el= _SH[el_on];
}

 while (el_on != nil){ F203(el, or_node, end_node);
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

}

 rv_aux= F646(or_node, start_node);
}

 return rv_aux;
}
}

int F380()
{return F522();}

int F381(int str)
{return F664(66734, _SH[66779], _c(str, nil));}

int F382(int node)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(F750(node, 66567), 66627);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(F750(node, 66567), 66629);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F383(int patterns, int expr)
{{int rv_aux = nil; {int pattern = nil; int prop_name = nil; int var_name = nil; int inits = nil; while (patterns != nil){ { pattern= _SH[patterns];
 patterns= _SH[(patterns + 1)];
}

 if (F556(pattern) != nil)
{{int aux = nil; aux= pattern;
 var_name= _SH[aux];
 aux= _SH[(aux + 1)];
 prop_name= _SH[aux];
}
}
else {{ var_name= pattern;
 prop_name= pattern;
}
}
 inits= _c(_c(var_name, _c(_c(66458, _c(expr, _c(_c(66413, _c(prop_name, nil)), nil))), nil)), inits);
}

 rv_aux= F574(inits);
}

 return rv_aux;
}
}

int F384(int rec_name, int rec, int field_38values)
{{int rv_aux = nil; if (F7(field_38values) != nil)
{{int object1__2 = 66405; int object2__3; {int list__4; int sl__5; {int for_list_result = nil; for_list_result= nil;
 {int field_38value = nil; field_38value= field_38values;
 while (field_38value != nil){ for_list_result= _c(F9(rec_name, rec, _SH[field_38value], F283(field_38value)), for_list_result);
 field_38value= F7(field_38value);
}

}

 list__4= F574(for_list_result);
}

 sl__5= nil;
 object2__3= F498(list__4, sl__5);
}

 rv_aux= _c(object1__2, object2__3);
}
}
else {rv_aux= F9(rec_name, rec, _SH[field_38values], F283(field_38values));}
 return rv_aux;
}
}

int F385(int rec_name)
{return _c(66550, _c(_c(F432(rec_name), _c(_c(66551, _c(66552, nil)), _c(66555, _c(66556, nil)))), _c(_c(66557, _c(_c(66413, _c(rec_name, nil)), _c(66552, _c(66556, nil)))), nil)));}

int F386(int row, int stream, int separator, int terminator, int F443)
{{int rv_aux = nil; {int test_aux_var; {int or_result = nil; { if (!(F443 != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (!(F750(F443, 66805) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{rv_aux= F71(row, stream, separator, terminator);}
else {{int el = nil; int el_on = nil; int conversion_fn = nil; int conversion_fn_on = nil; { el_on= row;
 el= _SH[el_on];
}

 { conversion_fn_on= F750(F443, 66805);
 conversion_fn= _SH[conversion_fn_on];
}

 { {int test_aux_var; {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ and_result= F678(conversion_fn_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (_SH[(el_on + 1)] != nil)
{F144(el, separator, stream, conversion_fn);}
else {F144(el, terminator, stream, conversion_fn);}
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 { conversion_fn_on= _SH[(conversion_fn_on + 1)];
 conversion_fn= _SH[conversion_fn_on];
}

 {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ and_result= F678(conversion_fn_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= nil;
}

}
}
}

 return rv_aux;
}
}

int F387(int list, int fn, int other_args)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F717(fn, _c(_SH[el], other_args)) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 el= _SH[(el + 1)];
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F388(int str)
{{int rv_aux = nil; {int or_result = nil; { or_result= F286(str, 10);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F146(str, 10);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F389(int stream)
{{int rv_aux = nil; if (V18[stream] > V17[stream])
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F390(int iter_spec)
{{int rv_aux = nil; {int condition = nil; int aux = nil; aux= iter_spec;
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 rv_aux= _c(66481, _c(_c(66461, _c(condition, nil)), nil));
}

 return rv_aux;
}
}

int F391(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int test_aux_var; {int and_result = nil; { if (csym_code >= F773(48))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(57))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= (csym_code - F773(48));}
else {{int test_aux_var; {int and_result = nil; { if (csym_code >= F773(65))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(70))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= (10 + (csym_code - F773(65)));}
else {{int test_aux_var; {int and_result = nil; { if (csym_code >= F773(97))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(102))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= (10 + (csym_code - F773(97)));}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F392(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9)
{{int rv_aux = nil; switch (fn) {
   case 66342: rv_aux= F708(a1, a2, a3, a4, a5, a6, a7, a8, a9); break;
   case 66538: rv_aux= F211(a1, a2, a3, a4, a5, a6, a7, a8, a9); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5 - a6 - a7 - a8 - a9); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5 * a6 * a7 * a8 * a9); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5 / a6 / a7 / a8 / a9); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil))))))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil))))))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil))))))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil))))))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))))); break;
   default: return F563(fn, 9); break;
}
 return rv_aux;
}
}

int F393(int var_name)
{return F394(F755(_c(F292(var_name), _c(_SH[65560], nil))), 66407);}

int F394(int str, int makeP)
{{int rv_aux = nil; {int sym = nil; if (str == nil)
{if (makeP != nil)
{if (V46 == 1)
{{ F164();
 F681(nil);
 rv_aux= F380();
}
}
else {rv_aux= F681(nil);}}
else {rv_aux= nil;}}
else {{ sym= F575(str);
 if (sym != nil)
{rv_aux= sym;}
else {if (makeP != nil)
{if (V46 == 1)
{{ F164();
 F681(str);
 rv_aux= F380();
}
}
else {rv_aux= F681(str);}}
else {rv_aux= nil;}}
}
}
}

 return rv_aux;
}
}

int F395(int n, int unmatched_tags_as_singleP, int print_errorsP)
{{int rv_aux = nil; {int repeat_counter = nil; repeat_counter= n;
 { while (repeat_counter > 0){ if (print_errorsP != nil)
{{ F87(_SH[66435]);
 F87(F292(F506()));
 F87(_SH[65760]);
 F373(nil);
}
}
 F540(unmatched_tags_as_singleP);
 repeat_counter= (repeat_counter - 1);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F396(int stream, int64_t delta)
{{int rv_aux = nil; if (!(delta != nil))
{rv_aux= F416(stream);}
else {{int64_t cur_pos = F237(stream); int result = nil; { F663(stream, delta);
 result= F416(stream);
}

 F102(stream, cur_pos);
 rv_aux= result;
}
}
 return rv_aux;
}
}

int F397(int size)
{{int vect = nil; if ((size + V43) >= V44)
{{ printf("\n***** EXIT: ---Heap Full---\n");
 fflush(stdout);
 exit(0);
}
}
else {{ vect= V43;
 _SH[vect]= C28;
 _SH[(vect + 1)]= size;
 V43= (V43 + (size + 2));
 return vect;
}
}
}
}

int F398(int in_stream, int out_stream, int64_t start, int64_t end)
{{int rv_aux = nil; {int64_t start__1 = start; int64_t start = start__1; int64_t end__2 = end; int64_t end = end__2; if (start > end)
{rv_aux= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65599];
 rv_aux= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[in_stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[in_stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[in_stream])
{start= V15[in_stream];}
 V15[in_stream]= start;
 F102(in_stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[in_stream])
{end= V16[in_stream];}
 V16[in_stream]= end;
}
}
 { { while (F243(in_stream, nil)){ F673(F299(in_stream), out_stream);
}

 stream_slice_rv= nil;
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[in_stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[in_stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(in_stream, (end + 1));}
 rv_aux= unwind_value;
}

}
}}
}

 return rv_aux;
}
}

int F399(int stream)
{{int rv_aux = nil; if (V22[stream] == V24[stream])
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F400(int object, int list, int size)
{return F755(_c(F514(size, object), _c(list, nil)));}

int F401()
{{int rv_aux = nil; { V44= C19;
 V50= V41;
 V51= V43;
 V41= V48;
 V43= V49;
 { V46= 0;
 rv_aux= V46;
}

}

 return rv_aux;
}
}

int F402(int var)
{{int rv_aux = nil; { if (F556(var) != nil)
{var= _SH[var];}
 rv_aux= F394(F755(_c(F292(var), _c(_SH[65552], nil))), 66407);
}

 return rv_aux;
}
}

int F403(int stream_index)
{{int rv_aux = nil; {int csym = nil; csym= F139(stream_index);
 if (csym == -1)
{rv_aux= nil;}
else {{ ungetc(csym, F579(stream_index));
 rv_aux= 66407;
}
}
}

 return rv_aux;
}
}

int F404(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; { F663(stream, 1);
 rv_aux= _c(66414, _c(F440(stream, sc_comment_flag, hm_comment_flag), nil));
}

 return rv_aux;
}
}

int F405(int stream)
{{int rv_aux = nil; { V4= (V4 - 1);
 {int buf_size = V6[stream]; V34= (V34 - (buf_size * 2));
}

 rv_aux= nil;
}

 return rv_aux;
}
}

int F406(int rec_name)
{return _c(66550, _c(_c(F576(rec_name), _c(_c(66551, _c(66552, nil)), _c(66553, nil))), _c(_c(66554, _c(_c(66413, _c(rec_name, nil)), _c(66552, _c(66553, nil)))), nil)));}

int F407(int tag_name)
{return F664(66734, _SH[66829], _c(tag_name, nil));}

int F408(int str)
{return F664(66734, _SH[66785], _c(str, nil));}

int F409(int mode)
{return F256(mode, 66583);}

int F410(int value, int stream)
{{int rv_aux = nil; if (F267(stream) != nil)
{{ { {int F60 = V22[stream]; V33[F60]= value;
}

 { V22[stream]= (V22[stream] + 1);
 V18[stream]= (V18[stream] + 1);
}

}

 rv_aux= 66407;
}
}
else {if (F399(stream) != nil)
{{ if (F389(stream) != nil)
{{ V17[stream]= (V17[stream] + 1);
 F79(stream);
}
}
 { {int F60 = V22[stream]; V33[F60]= value;
}

 { V22[stream]= (V22[stream] + 1);
 V18[stream]= (V18[stream] + 1);
}

}

 if (F746(stream) != nil)
{V24[stream]= (- C1);}
 rv_aux= 66407;
}
}
else {rv_aux= nil;}}
 return rv_aux;
}
}

int F411(int csym, int class_spec)
{return F167(csym, class_spec);}

int F412(int iter_spec)
{{int rv_aux = nil; {int var = nil; int key = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 if (F556(var) != nil)
{{int aux = nil; aux= var;
 key= _SH[aux];
 aux= _SH[(aux + 1)];
 var= _SH[aux];
}
}
 {int in_trie = F78(iter_spec, 66475); {int object1__9 = 66481; int object2__10; {int object1__11; int object2__12; {int object1__13 = 66476; int object2__14; {int object1__15; int object2__16; {int object1__17 = var; int object2__18; {int list__19; int sl__20; if (key != nil)
{list__19= _c(key, nil);}
else {list__19= nil;}
 sl__20= _c(66526, _c(_c(66489, _c(66527, nil)), nil));
 object2__18= F498(list__19, sl__20);
}

 object1__15= _c(object1__17, object2__18);
}

 object2__16= nil;
 object2__14= _c(object1__15, object2__16);
}

 object1__11= _c(object1__13, object2__14);
}

 object2__12= _c(_c(66477, _c(_c(66405, _c(_c(66409, _c(66527, _c(_c(66492, nil), nil))), _c(_c(66514, _c(66527, nil)), _c(_c(66528, _c(_c(66529, _c(in_trie, nil)), _c(66527, nil))), _c(_c(66528, _c(_c(66530, _c(in_trie, nil)), _c(66527, nil))), _c(_c(66531, _c(66527, _c(var, _c(key, _c(66526, nil))))), nil)))))), nil)), _c(_c(66461, _c(_c(66443, _c(66526, _c(nil, nil))), nil)), _c(_c(66430, _c(_c(66443, _c(66526, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66405, _c(_c(66531, _c(66527, _c(var, _c(key, _c(66526, nil))))), nil)), nil)), _c(_c(66480, _c(_c(66497, nil), nil)), nil)))));
 object2__10= _c(object1__11, object2__12);
}

 rv_aux= _c(object1__9, object2__10);
}

}

}

 return rv_aux;
}
}

int F413(int list, int n)
{{int rv_aux = nil; {int prefix = nil; while (list != nil){ if (n == 0)
{list= nil;}
else {{ n= (n - 1);
 prefix= _c(_SH[list], prefix);
 list= _SH[(list + 1)];
}
}
}

 rv_aux= F574(prefix);
}

 return rv_aux;
}
}

int F414(int stream, int split_condition, int transf_fn, int prune_fnP)
{{int rv_aux = nil; {int term_list = nil; int pruned_term_list = nil; term_list= F129(stream, split_condition, nil);
 {int term = nil; int term_on = nil; { term_on= term_list;
 term= _SH[term_on];
}

 while (term_on != nil){ if (transf_fn != nil)
{term= F483(transf_fn, term);}
 {int test_aux_var; {int _ARG1__9; {int and_result = nil; { and_result= prune_fnP;
 if (and_result != nil)
{{ and_result= F483(prune_fnP, term);
 if (and_result != nil)
{_ARG1__9= (and_result != nil);}
else {_ARG1__9= (nil != nil);}
}
}
else {_ARG1__9= (nil != nil);}
}

}

 test_aux_var= !(_ARG1__9);
}

 if (test_aux_var)
{pruned_term_list= _c(term, pruned_term_list);}
}

 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 rv_aux= F574(pruned_term_list);
}

 return rv_aux;
}
}

int F415(int iter_spec)
{{int rv_aux = nil; {int var = nil; int expansion = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 {int in_width = F78(iter_spec, 66467); int exp_by = F78(iter_spec, 66487); int exp_while = F78(iter_spec, 66488); expansion= F93(var, exp_by, exp_while);
 {int object1__12 = 66481; int object2__13; {int object1__14 = _c(66476, _c(_c(var, _c(_c(66489, _c(66490, nil)), _c(_c(66489, _c(66491, nil)), _c(_c(66489, _c(66499, nil)), _c(_c(66489, _c(66500, nil)), _c(_c(66489, _c(66501, nil)), _c(_c(66489, _c(66502, nil)), _c(_c(66489, _c(F80(var), _c(0, nil))), nil)))))))), nil)); int object2__15; {int object1__67 = _c(66477, _c(_c(66405, _c(_c(66409, _c(66490, _c(_c(66492, nil), nil))), _c(_c(66409, _c(66491, _c(66490, nil))), _c(_c(66409, _c(66499, _c(_c(66493, _c(66490, _c(1, nil))), nil))), _c(_c(66409, _c(66500, _c(_c(66432, _c(66490, _c(66503, nil))), nil))), _c(_c(66409, _c(66501, _c(_c(66493, _c(66500, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(in_width, nil))), nil))))))), nil)); int object2__68; {int object1__143 = _c(66461, _c(_c(66494, _c(_c(66485, _c(var, _c(nil, nil))), _c(_c(66494, _c(_c(66428, _c(66491, _c(66499, nil))), _c(_c(66429, _c(66501, _c(66500, nil))), nil))), nil))), nil)); int object2__144; {int object1__179; int object2__180; {int object1__181 = 66479; int object2__182; {int object1__183; int object2__184; {int object1__185 = 66405; int object2__186; {int list__187; int sl__188; if (expansion != nil)
{list__187= _c(expansion, nil);}
else {list__187= nil;}
 sl__188= _c(_c(66409, _c(66501, _c(_c(66504, _c(var, _c(66501, nil))), nil))), _c(_c(66420, _c(_c(_c(66505, _c(66491, _c(66499, nil))), _c(_c(66420, _c(_c(_c(66429, _c(66501, _c(66500, nil))), _c(_c(66484, _c(F80(var), nil)), _c(_c(66409, _c(66502, _c(66490, nil))), _c(_c(66409, _c(66490, _c(66500, nil))), _c(_c(66409, _c(66499, _c(66501, nil))), _c(_c(66409, _c(66491, _c(66490, nil))), _c(_c(66409, _c(66500, _c(66502, nil))), _c(_c(66409, _c(66501, _c(_c(66493, _c(66500, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(_c(66506, _c(66491, nil)), nil))), nil))))))))), _c(_c(66407, _c(_c(66409, _c(var, _c(nil, nil))), nil)), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(_c(66506, _c(66491, nil)), nil))), nil)), nil))), nil));
 object2__186= F498(list__187, sl__188);
}

 object1__183= _c(object1__185, object2__186);
}

 object2__184= nil;
 object2__182= _c(object1__183, object2__184);
}

 object1__179= _c(object1__181, object2__182);
}

 object2__180= _c(_c(66480, _c(_c(66497, nil), nil)), nil);
 object2__144= _c(object1__179, object2__180);
}

 object2__68= _c(object1__143, object2__144);
}

 object2__15= _c(object1__67, object2__68);
}

 object2__13= _c(object1__14, object2__15);
}

 rv_aux= _c(object1__12, object2__13);
}

}

}

 return rv_aux;
}
}

int F416(int stream)
{{int rv_aux = nil; if (V22[stream] <= V23[stream])
{{int F60 = V22[stream]; rv_aux= V33[F60];
}
}
else {if (F425(stream) != nil)
{rv_aux= F483(F436(stream), stream);}
else {if (F661(stream) != nil)
{if (V22[stream] <= V23[stream])
{{int F60 = V22[stream]; rv_aux= V33[F60];
}
}
else {rv_aux= -1;}}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F417(int cons1)
{{int rv_aux = nil; {int test_aux_var; {int _ARG1__1; {int or_result = nil; { if (V46 == 1)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {{ or_result= F256(cons1, nil);
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {_ARG1__1= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__1);
}

 if (test_aux_var)
{{ _SH[(cons1 + 1)]= V41;
 V41= cons1;
 return nil;
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int64_t F418(int stream)
{return V16[stream];}

int F419(int lp)
{{int rv_aux = nil; if (lp != nil)
{rv_aux= F118(lp);}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F420(int list, int sl)
{{int rv_aux = nil; { _SH[(_SH[(list + 1)] + 1)]= sl;
 rv_aux= sl;
}

 return rv_aux;
}
}

int F421(int element)
{return F91(_SH[66838], _SH[66836], _c(element, F2(_SH[66838], _SH[66836])));}

int F422(int object, int list, int F60)
{{int rv_aux = nil; {int index = nil; int new_list = nil; if (F60 == -1)
{rv_aux= F755(_c(list, _c(_c(object, nil), nil)));}
else {{ if (F60 < 0)
{{int list_len_var = nil; list_len_var= F510(list);
 if (F60 < 0)
{F60= (list_len_var + F60 + 1);}
}
}
 if (F60 == 1)
{rv_aux= _c(object, F289(list));}
else {{ index= 1;
 while (list != nil){ if (index == F60)
{new_list= _c(object, new_list);}
 new_list= _c(_SH[list], new_list);
 { _SH[list];
 list= _SH[(list + 1)];
}

 index= (index + 1);
}

 if (index <= F60)
{{ while (index < F60){ new_list= _c(nil, new_list);
 index= (index + 1);
}

 new_list= _c(object, new_list);
}
}
 rv_aux= F574(new_list);
}
}
}
}
}

 return rv_aux;
}
}

int F423(int str)
{return F664(66734, _SH[66762], _c(str, nil));}

int F424(int object, int list)
{return F60(66443, list, _c(object, nil));}

int F425(int stream)
{return F678(V12[stream], nil);}

int F426(int tag, int stream)
{{int rv_aux = nil; {int F757 = nil; int start_pos = nil; int cur_tag_name = nil; { while (!(F757 != nil)){ F200(stream, 60);
 if (!(F243(stream, nil)))
{F757= 66423;}
else {if (F115(stream) != nil)
{F238(stream);}
else {if (F466(stream) != nil)
{F366(stream, 62);}
else {{ start_pos= F237(stream);
 F663(stream, 1);
 cur_tag_name= F583(F51(stream, nil));
 if (cur_tag_name == tag)
{{ F757= 66407;
 F102(stream, start_pos);
}
}
else {{ F31(stream);
 F366(stream, 62);
}
}
}
}}}
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F427(int csym, int stream)
{return F360(csym, V8[stream]);}

int F428(int lp)
{return F348(lp, 47, 66443, 66407);}

int F429(int args, int result_var)
{{int rv_aux = nil; if (args != nil)
{{int object1__1 = 66405; int object2__2; {int object1__3 = _c(66409, _c(result_var, _c(_SH[args], nil))); int object2__4; {int object1__12; int object2__13; {int object1__14 = 66408; int object2__15; {int object1__16 = result_var; int object2__17; {int object1__18 = result_var; int object2__19; {int list__20; int sl__21; if (F429(_SH[(args + 1)], result_var) != nil)
{list__20= _c(F429(_SH[(args + 1)], result_var), nil);}
else {list__20= nil;}
 sl__21= nil;
 object2__19= F498(list__20, sl__21);
}

 object2__17= _c(object1__18, object2__19);
}

 object2__15= _c(object1__16, object2__17);
}

 object1__12= _c(object1__14, object2__15);
}

 object2__13= nil;
 object2__4= _c(object1__12, object2__13);
}

 object2__2= _c(object1__3, object2__4);
}

 rv_aux= _c(object1__1, object2__2);
}
}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F430(int stack)
{{int rv_aux = nil; if (F2(stack, 1) == (F552(stack) + 1))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F431(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int or_result = nil; { {int and_result = nil; { if (csym_code >= 97)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 122)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= 224)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 254)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code != 247)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

}

 return rv_aux;
}
}

int F432(int rec_name)
{return F394(F755(_c(F292(rec_name), _c(_SH[65571], nil))), 66407);}

int F433(int stream, int condition, int inTOout_fn)
{return F57(stream, condition, 66461, inTOout_fn);}

int F434(int str)
{return F664(66734, _SH[66783], _c(str, nil));}

int F435(int iter_spec)
{{int rv_aux = nil; {int var = nil; int init_expr = nil; int update_expr = nil; int aux = nil; aux= iter_spec;
 var= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 init_expr= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 update_expr= _SH[aux];
 rv_aux= _c(66481, _c(_c(66476, _c(_c(var, nil), nil)), _c(_c(66477, _c(_c(66409, _c(var, _c(init_expr, nil))), nil)), _c(_c(66461, _c(_c(66485, _c(var, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66409, _c(var, _c(update_expr, nil))), nil)), nil)))));
}

 return rv_aux;
}
}

int F436(int stream)
{return V12[stream];}

int F437(int trie, int str, int value)
{{int rv_aux = nil; {int node = nil; node= F371(trie, str);
 rv_aux= _SH[(node + 1)]= value;
}

 return rv_aux;
}
}

int F438(int rec_type_name, int field_name)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int spec = nil; int spec_on = nil; { spec_on= F327(rec_type_name);
 spec= _SH[spec_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F750(spec, 66548) == field_name)
{for_thereis_result= F750(spec, 66567);}
else {for_thereis_result= nil;}
 { spec_on= _SH[(spec_on + 1)];
 spec= _SH[spec_on];
}

 {int and_result = nil; { and_result= F678(spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F439(int expr)
{{int rv_aux = nil; {int or_result = nil; { or_result= F245(expr);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F551(expr);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F123(expr);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F440(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; {int csym = nil; F756(stream, sc_comment_flag, hm_comment_flag);
 if (!(hm_comment_flag == 66447))
{hm_comment_flag= nil;}
 if (!(sc_comment_flag == 66447))
{sc_comment_flag= nil;}
 if (F243(stream, nil))
{{ csym= F396(stream, nil);
 if (csym == 124)
{rv_aux= F538(stream);}
else {if (csym == 59)
{rv_aux= F642(stream);}
else {if (csym == 35)
{rv_aux= F346(stream);}
else {if (csym == 39)
{rv_aux= F130(stream, sc_comment_flag, hm_comment_flag);}
else {if (csym == 96)
{rv_aux= F404(stream, sc_comment_flag, hm_comment_flag);}
else {if (csym == 44)
{rv_aux= F269(stream, sc_comment_flag, hm_comment_flag);}
else {if (csym == 34)
{rv_aux= F537(stream);}
else {if (csym == 40)
{rv_aux= F230(stream, sc_comment_flag, hm_comment_flag);}
else {if (csym == 41)
{{ F610(stream, _SH[65606]);
 rv_aux= nil;
}
}
else {rv_aux= F693(stream);}}}}}}}}}
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F441(int object, int index)
{{int rv_aux; {int value = nil; if (!(F364(object) != nil))
{while (object != nil){ { value= _SH[object];
 object= _SH[(object + 1)];
}

 { index= (index + 1);
 if (index > C8)
{{ { F87(_SH[66435]);
 F87(_SH[65554]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 F91(_SH[66486], index, value);
}

}
}
 rv_aux= index;
}

 return rv_aux;
}
}

int F442(int sub, int tnode)
{{int rv_aux = nil; {int code = nil; int sub_list = nil; int end = nil; code= _SH[sub];
 sub_list= tnode;
 { while (!(end != nil)){ {int test_aux_var; {int or_result = nil; { or_result= F256(_SH[(sub_list + 1)], nil);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ if (_SH[F283(sub_list)] > code)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ _SH[(sub_list + 1)]= _c(sub, _SH[(sub_list + 1)]);
 end= 66407;
}
}
else {{ _SH[sub_list];
 sub_list= _SH[(sub_list + 1)];
}
}
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F443(int name)
{return F594(66548, name, F341(66795), 66443);}

int F444(int str)
{{int rv_aux = nil; {int str_buf = nil; str_buf= str;
 while (str_buf != nil){ _SH[str_buf]= F320(_SH[str_buf]);
 { _SH[str_buf];
 str_buf= _SH[(str_buf + 1)];
}

}

 rv_aux= str;
}

 return rv_aux;
}
}

int F445(int tag_name)
{{int rv_aux = nil; { _SH[66836]= (_SH[66836] + 1);
 F91(_SH[66837], _SH[66836], tag_name);
 rv_aux= F91(_SH[66838], _SH[66836], nil);
}

 return rv_aux;
}
}

int F446(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F243(aux_stream, nil)))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

 while (test_aux_var){ csym= F684(aux_stream);
 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F243(aux_stream, nil)))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F447(int sock_index)
{{int rv_aux = nil; {int byte = nil; if (!(F61(sock_index) != nil))
{F114(sock_index);}
 if (F36(sock_index) != nil)
{rv_aux= -1;}
else {if (F61(sock_index) != nil)
{{ byte= V58[sock_index][V59[sock_index]];
 rv_aux= F335(byte);
}
}
else {rv_aux= nil;}}
}

 return rv_aux;
}
}

int F448(int stream)
{return F366(stream, _SH[65751]);}

int F449(int list)
{return F256(list, nil);}

int F450(char* cstr)
{{int rv_aux = nil; {int str = nil; int index = 0; { index= 0;
 if (!(cstr == NULL))
{while (!(cstr[index] == 0)){ str= _c(cstr[index], str);
 index= (index + 1);
}
}
}

 rv_aux= F574(str);
}

 return rv_aux;
}
}

int F451(int stream)
{return F366(stream, _SH[65742]);}

int F452(int object, int list)
{return F66(66443, list, _c(object, nil));}

void F453(double num, int expP, int size)
{if (expP != nil)
{snprintf(V52, 4096, "%E", num);}
else {snprintf(V52, 4096, "%.*F", size, num);}}

int F454(int stream)
{{int rv_aux = nil; {int out_buf_max = nil; int buf_start = nil; int buf_nbytes = nil; {int64_t max = V16[stream]; int64_t end = V17[stream]; int buf_size = V6[stream]; int buf_index = V20[stream]; if (F771(buf_index) != nil)
{{ buf_start= V25[stream];
 buf_nbytes= V27[stream];
}
}
else {{ buf_start= V29[stream];
 buf_nbytes= V31[stream];
}
}
 if (F623(max, buf_size, buf_index, buf_start) <= F623((end + 1), buf_size, buf_index, buf_start))
{if (F623(max, buf_size, buf_index, buf_start) <= (buf_start + buf_nbytes))
{out_buf_max= F623(max, buf_size, buf_index, buf_start);}
else {out_buf_max= (buf_start + buf_nbytes);}}
else {if (F623((end + 1), buf_size, buf_index, buf_start) <= (buf_start + buf_nbytes))
{out_buf_max= F623((end + 1), buf_size, buf_index, buf_start);}
else {out_buf_max= (buf_start + buf_nbytes);}}
}

 V24[stream]= out_buf_max;
 rv_aux= nil;
}

 return rv_aux;
}
}

int F455(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, nil))))))))))));}

int F456(int str, int size)
{{int rv_aux = nil; {uint64_t hash = 5381; {int csym = nil; int csym_on = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 while (csym_on != nil){ hash= ((hash << 5) + hash + F773(csym));
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

}

 rv_aux= ((hash % size) + 1);
}

 return rv_aux;
}
}

int F457(int list, int F60, int object)
{{int rv_aux = nil; {int nth_cons = nil; nth_cons= F125(list, F60);
 if (nth_cons != nil)
{rv_aux= _SH[nth_cons]= object;}
else {rv_aux= _c(object, nil);}
}

 return rv_aux;
}
}

int F458(int var_38value_list)
{{int rv_aux = nil; {int var_38value = nil; int settings = nil; while (var_38value_list != nil){ { var_38value= _SH[var_38value_list];
 var_38value_list= _SH[(var_38value_list + 1)];
}

 if (F556(var_38value) != nil)
{settings= _c(_c(66409, _c(_SH[var_38value], _c(F283(var_38value), nil))), settings);}
}

 rv_aux= F574(settings);
}

 return rv_aux;
}
}

int F459(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66710);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F460(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, nil))))))))))))))))))));}

int F461(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int and_result = nil; { if (csym_code >= F773(48))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(57))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

}

 return rv_aux;
}
}

int F462(int fn, int a1, int a2, int a3)
{{int rv_aux = nil; switch (fn) {
   case 66299: rv_aux= F84(a1, a2, a3); break;
   case 66298: rv_aux= F597(a1, a2, a3); break;
   case 66297: rv_aux= F279(a1, a2, a3); break;
   case 66538: rv_aux= F325(a1, a2, a3); break;
   case 66432: rv_aux= (a1 + a2 + a3); break;
   case 66493: rv_aux= (a1 - a2 - a3); break;
   case 66616: rv_aux= (a1 * a2 * a3); break;
   case 66352: rv_aux= (a1 / a2 / a3); break;
   case 66438: rv_aux= F532(a1, a2, a3); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, nil)); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, nil)); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, nil)); break;
   case 66296: rv_aux= F728(a1, a2, a3); break;
   case 66295: rv_aux= F422(a1, a2, a3); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, nil)); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, nil)); break;
   case 66294: rv_aux= F515(a1, a2, a3); break;
   case 66348: rv_aux= F90(a1, a2, a3, nil); break;
   case 66293: rv_aux= F298(a1, a2, a3); break;
   case 66347: rv_aux= F479(a1, a2, a3, nil); break;
   case 66325: rv_aux= F275(a1, a2, a3, 66447); break;
   case 66324: rv_aux= F173(a1, a2, a3, 66447); break;
   case 66346: rv_aux= F721(a1, a2, a3, nil); break;
   case 66345: rv_aux= F560(a1, a2, a3, nil); break;
   case 66292: rv_aux= F578(a1, a2, a3); break;
   case 66291: rv_aux= F68(a1, a2, a3); break;
   case 66290: rv_aux= F654(a1, a2, a3); break;
   case 66411: rv_aux= F457(a1, a2, a3); break;
   case 66289: rv_aux= F496(a1, a2, a3); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, nil)))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, nil)))); break;
   case 66323: rv_aux= F744(a1, a2, a3, nil); break;
   case 66322: rv_aux= F89(a1, a2, a3, nil); break;
   case 66321: rv_aux= F348(a1, a2, a3, nil); break;
   case 66288: rv_aux= F338(a1, a2, a3); break;
   case 66287: rv_aux= F607(a1, a2, a3); break;
   case 66286: rv_aux= F343(a1, a2, a3); break;
   case 66285: rv_aux= F665(a1, a2, a3); break;
   case 66717: rv_aux= F757(a1, a2, a3); break;
   case 66284: rv_aux= F718(a1, a2, a3); break;
   case 66336: rv_aux= F714(a1, a2, a3, 66444, 10); break;
   case 66318: rv_aux= F564(a1, a2, a3, 66444); break;
   case 66283: rv_aux= F400(a1, a2, a3); break;
   case 66282: rv_aux= F158(a1, a2, a3); break;
   case 66537: rv_aux= F248(a1, a2, a3); break;
   case 66317: rv_aux= F594(a1, a2, a3, 66443); break;
   case 66281: rv_aux= F25(a1, a2, a3); break;
   case 66280: rv_aux= F93(a1, a2, a3); break;
   case 66539: rv_aux= F387(a1, a2, a3); break;
   case 66542: rv_aux= F42(a1, a2, a3); break;
   case 66279: rv_aux= F488(a1, a2, a3); break;
   case 66278: rv_aux= F265(a1, a2, a3); break;
   case 66277: rv_aux= F437(a1, a2, a3); break;
   case 66276: rv_aux= F374(a1, a2, a3); break;
   case 66275: rv_aux= F585(a1, a2, a3); break;
   case 66557: rv_aux= F384(a1, a2, a3); break;
   case 66559: rv_aux= F731(a1, a2, a3); break;
   case 66274: rv_aux= F156(a1, a2, a3); break;
   case 66554: rv_aux= F751(a1, a2, a3); break;
   case 66273: rv_aux= F637(a1, a2, a3); break;
   case 66272: rv_aux= F352(a1, a2, a3); break;
   case 66271: rv_aux= F706(a1, a2, a3); break;
   case 66270: rv_aux= F220(a1, a2, a3); break;
   case 66269: rv_aux= F129(a1, a2, a3); break;
   case 66611: rv_aux= F97(a1, a2, a3); break;
   case 66612: rv_aux= F358(a1, a2, a3); break;
   case 66806: rv_aux= F716(a1, a2, a3); break;
   case 66807: rv_aux= F145(a1, a2, a3); break;
   case 66268: rv_aux= F349(a1, a2, a3); break;
   case 66421: rv_aux= F240(a1, a2, a3); break;
   case 66267: rv_aux= SHARKread(a1, a2, a3); break;
   case 66266: rv_aux= F440(a1, a2, a3); break;
   case 66265: rv_aux= F756(a1, a2, a3); break;
   case 66264: rv_aux= F130(a1, a2, a3); break;
   case 66263: rv_aux= F404(a1, a2, a3); break;
   case 66262: rv_aux= F269(a1, a2, a3); break;
   case 66261: rv_aux= F230(a1, a2, a3); break;
   case 66415: rv_aux= F195(a1, a2, a3); break;
   case 66610: rv_aux= F110(a1, a2, a3, _SH[66436], _SH[66437]); break;
   case 66260: rv_aux= F3(a1, a2, a3); break;
   case 66259: rv_aux= F203(a1, a2, a3); break;
   case 66258: rv_aux= F274(a1, a2, a3); break;
   case 66257: rv_aux= F94(a1, a2, a3); break;
   case 66256: rv_aux= F379(a1, a2, a3); break;
   case 66255: rv_aux= F633(a1, a2, a3); break;
   case 66254: rv_aux= F99(a1, a2, a3); break;
   case 66704: rv_aux= F509(a1, a2, a3); break;
   case 66705: rv_aux= F329(a1, a2, a3); break;
   case 66706: rv_aux= F353(a1, a2, a3); break;
   case 66707: rv_aux= F690(a1, a2, a3); break;
   case 66673: rv_aux= F634(a1, a2, a3, nil); break;
   case 66674: rv_aux= F611(a1, a2, a3, nil); break;
   case 66675: rv_aux= F201(a1, a2, a3, nil); break;
   case 66676: rv_aux= F553(a1, a2, a3, nil); break;
   case 66677: rv_aux= F765(a1, a2, a3, nil); break;
   case 66683: rv_aux= F433(a1, a2, a3); break;
   case 66684: rv_aux= F624(a1, a2, a3); break;
   case 66685: rv_aux= F480(a1, a2, a3); break;
   case 66686: rv_aux= F581(a1, a2, a3); break;
   case 66687: rv_aux= F11(a1, a2, a3); break;
   case 66253: rv_aux= F169(a1, a2, a3); break;
   case 66252: rv_aux= F521(a1, a2, a3); break;
   case 66251: rv_aux= F548(a1, a2, a3); break;
   case 66250: rv_aux= F671(a1, a2, a3); break;
   case 66311: rv_aux= F34(a1, a2, a3, nil); break;
   case 66310: rv_aux= F247(a1, a2, a3, nil); break;
   case 66309: rv_aux= F414(a1, a2, a3, nil); break;
   case 66308: rv_aux= F249(a1, a2, a3, nil); break;
   case 66249: rv_aux= F775(a1, a2, a3); break;
   case 66248: rv_aux= F469(a1, a2, a3); break;
   case 66307: rv_aux= F315(a1, a2, a3, C1); break;
   case 66247: rv_aux= F122(a1, a2, a3); break;
   case 66306: rv_aux= F316(a1, a2, a3, C1); break;
   case 66246: rv_aux= F359(a1, a2, a3); break;
   case 66305: rv_aux= F345(a1, a2, a3, C1); break;
   case 66245: rv_aux= F558(a1, a2, a3); break;
   case 66303: rv_aux= F485(a1, a2, a3, C1); break;
   case 66244: rv_aux= F641(a1, a2, a3); break;
   case 66302: rv_aux= F268(a1, a2, a3, nil); break;
   case 66243: rv_aux= F52(a1, a2, a3); break;
   case 66327: rv_aux= F386(a1, a2, a3, 10, nil); break;
   case 66300: rv_aux= F144(a1, a2, a3, nil); break;
   case 66242: rv_aux= F446(a1, a2, a3); break;
   case 66339: rv_aux= F635(a1, a2, a3, nil, 66407, 66407, nil); break;
   case 66241: rv_aux= F17(a1, a2, a3); break;
   case 66240: rv_aux= F395(a1, a2, a3); break;
   case 66845: rv_aux= F685(a1, a2, a3); break;
   default: return F563(fn, 3); break;
}
 return rv_aux;
}
}

int F463(int object, int index)
{{int rv_aux; { if (!(F364(object) != nil))
{{ if (_SH[(object + 1)] != nil)
{{ index= (index + 1);
 if (index > C8)
{{ { F87(_SH[66435]);
 F87(_SH[65556]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 F91(_SH[66486], index, _SH[(object + 1)]);
}
}
 { index= (index + 1);
 if (index > C8)
{{ { F87(_SH[66435]);
 F87(_SH[65557]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 F91(_SH[66486], index, _SH[object]);
}

}
}
 rv_aux= index;
}

 return rv_aux;
}
}

int F464(struct tm tm)
{{int rv_aux = nil; {int year = (tm.tm_year + 1900); int month = (tm.tm_mon + 1); int day = tm.tm_mday; int wday = tm.tm_wday; int hour = tm.tm_hour; int min = tm.tm_min; int sec = tm.tm_sec; int isdst = tm.tm_isdst; rv_aux= _c(66744, _c(_c(66745, _c(year, nil)), _c(_c(66746, _c(month, nil)), _c(_c(66747, _c(day, nil)), _c(_c(66748, _c(wday, nil)), _c(_c(66749, _c(hour, nil)), _c(_c(66750, _c(min, nil)), _c(_c(66751, _c(sec, nil)), _c(_c(66752, _c(isdst, nil)), nil)))))))));
}

 return rv_aux;
}
}

int F465(int F339, int full_pathP)
{{int rv_aux = nil; {int file_list = nil; int file = nil; int new_file_list = nil; file_list= F85(F339);
 if (full_pathP != nil)
{{ while (file_list != nil){ { file= _SH[file_list];
 file_list= _SH[(file_list + 1)];
}

 new_file_list= _c(F735(_c(F289(F339), _c(file, nil))), new_file_list);
}

 file_list= F574(new_file_list);
}
}
 rv_aux= file_list;
}

 return rv_aux;
}
}

int F466(int stream)
{return F690(stream, _SH[65756], nil);}

int F467(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, _c(a28, nil))))))))))))))))))))))))))));}

int F468(int act_spec)
{return F664(66443, _SH[65634], _c(_SH[act_spec], nil));}

int F469(int file, int F443, int row_n)
{{int rv_aux = nil; {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66806;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int aux_stream; int filename = file; int mode = nil; int64_t end; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(F419(filename) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 aux_stream= F703(filename, mode);
 if (!(aux_stream != nil))
{error= _SH[65698];}
else {{ end= F617(filename);
 { V8[STREAM__BUF_935]= aux_stream;
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}
}
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ stream_rv= F122(stream, F443, row_n);
 if (!(stream == -1))
{{ F143(stream);
 F263(V8[stream]);
 F405(stream);
}
}
}
}
}

 rv_aux= stream_rv;
}

 return rv_aux;
}
}

int F470(int expr, int stream, int delta, int levels, int F510)
{{int rv_aux = nil; if (F510 == 0)
{rv_aux= F235(stream, F235(stream, _SH[65628]));}
else {if (!(_SH[66596] != nil))
{rv_aux= F689(expr, stream, delta, levels, F510);}
else {if (F1(expr, stream, delta, levels, F510) == -1)
{rv_aux= F689(expr, stream, delta, levels, F510);}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int stdoutPs1(char* str)
{{ printf("%s", str);
 return nil;
}
}

int F472(int condition)
{return F112(F571(F283(F283(condition))));}

int F473(int args)
{{int rv_aux = nil; if (!(args != nil))
{rv_aux= 66407;}
else {rv_aux= _c(66424, _c(_c(66425, nil), _c(F377(args, 66425), nil)));}
 return rv_aux;
}
}

int F474(int fn, int a1, int a2, int a3, int a4, int a5, int a6, int a7)
{{int rv_aux = nil; switch (fn) {
   case 66340: rv_aux= F545(a1, a2, a3, a4, a5, a6, a7); break;
   case 66538: rv_aux= F562(a1, a2, a3, a4, a5, a6, a7); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5 + a6 + a7); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5 - a6 - a7); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5 * a6 * a7); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5 / a6 / a7); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, nil))))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, nil))))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, nil))))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, _c(a6, _c(a7, nil))))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))))); break;
   case 66339: rv_aux= F635(a1, a2, a3, a4, a5, a6, a7); break;
   default: return F563(fn, 7); break;
}
 return rv_aux;
}
}

int F475(int list, int spec)
{{int rv_aux = nil; {int value = nil; if (F16(spec) != nil)
{rv_aux= F137(list, F773(spec));}
else {if (F123(spec) != nil)
{{int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; int el_on = nil; { el_on= list;
 el= _SH[el_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ {int test_aux_var; {int and_result = nil; { and_result= F556(el);
 if (and_result != nil)
{{ and_result= F256(_SH[el], spec);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
}

 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}
}
else {if (F44(spec))
{rv_aux= F137(list, spec);}
else {if (F774(spec, 66538) != nil)
{{int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; int el_on = nil; { el_on= list;
 el= _SH[el_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F717(F283(spec), _c(el, F7(spec))) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}
}
else {if (F774(spec, 66541) != nil)
{{int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F717(F283(spec), _c(el, F7(spec))) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 el= _SH[(el + 1)];
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}
}
else {if (F556(spec) != nil)
{{ value= list;
 {int spec1 = nil; int spec1_on = nil; { spec1_on= spec;
 spec1= _SH[spec1_on];
}

 while (spec1_on != nil){ value= F475(value, spec1);
 { spec1_on= _SH[(spec1_on + 1)];
 spec1= _SH[spec1_on];
}

}

}

 rv_aux= value;
}
}
else {rv_aux= nil;}}}}}}
}

 return rv_aux;
}
}

int F476(int stream, int act_spec)
{return _c(66405, F498(F733(stream, _SH[(act_spec + 1)]), nil));}

int F477(int F339)
{{int rv_aux = nil; {int answer = nil; answer= F302(F755(_c(_SH[65663], _c(F40(F339, F750(_SH[66427], 66740)), nil))), 66407);
 {int for_list_result = nil; for_list_result= nil;
 {int line = nil; int line_on = nil; { line_on= answer;
 line= _SH[line_on];
}

 while (line_on != nil){ if (F757(line, _SH[65664], 66443) != nil)
{for_list_result= _c(F532(line, 1, -2), for_list_result);}
 { line_on= _SH[(line_on + 1)];
 line= _SH[line_on];
}

}

}

 rv_aux= F574(for_list_result);
}

}

 return rv_aux;
}
}

int F478(int stream)
{return F678(V10[stream], nil);}

int F479(int object, int pred, int list, int args)
{{int rv_aux = nil; {int aux = nil; args= _c(nil, args);
 aux= list;
 while (list != nil){ _SH[args]= _SH[list];
 if (F717(pred, args) != nil)
{_SH[list]= object;}
 { _SH[list];
 list= _SH[(list + 1)];
}

}

 F417(args);
 rv_aux= aux;
}

 return rv_aux;
}
}

int F480(int stream, int condition, int inTOout_fn)
{return F57(stream, condition, 66709, inTOout_fn);}

int F481(int stream)
{return F620(V8[stream]);}

int F482(int var_name)
{return F394(F755(_c(F292(var_name), _c(_SH[65541], nil))), 66407);}

int F483(int fn, int a1)
{{int rv_aux = nil; switch (fn) {
   case 66088: rv_aux= F262(a1); break;
   case 66087: rv_aux= F753(a1); break;
   case 66086: rv_aux= F720(a1); break;
   case 66085: rv_aux= F187(a1); break;
   case 66084: rv_aux= F32(a1); break;
   case 66083: rv_aux= F116(a1); break;
   case 66082: rv_aux= F356(a1); break;
   case 66081: rv_aux= F321(a1); break;
   case 66080: rv_aux= F245(a1); break;
   case 66079: rv_aux= F682(a1); break;
   case 66078: rv_aux= F439(a1); break;
   case 66237: rv_aux= F88(a1, 66407); break;
   case 66077: rv_aux= F767(a1); break;
   case 66076: rv_aux= F574(a1); break;
   case 66236: rv_aux= F177(a1, nil); break;
   case 66418: rv_aux= F87(a1); break;
   case 66075: rv_aux= F482(a1); break;
   case 66074: rv_aux= F729(a1); break;
   case 66073: rv_aux= F458(a1); break;
   case 66072: rv_aux= F780(a1); break;
   case 66538: rv_aux= F76(a1); break;
   case 66232: rv_aux= F302(a1, nil); break;
   case 66071: rv_aux= SHARKexit(a1); break;
   case 66070: rv_aux= F473(a1); break;
   case 66069: rv_aux= F188(a1); break;
   case 66068: rv_aux= F149(a1); break;
   case 66067: rv_aux= F341(a1); break;
   case 66744: rv_aux= F225(a1); break;
   case 66066: rv_aux= F551(a1); break;
   case 66065: if (F44(a1))
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66064: if (F29(a1))
{rv_aux= C24;}
else {rv_aux= nil;} break;
   case 66432: rv_aux= (+ a1); break;
   case 66493: rv_aux= (- a1); break;
   case 66063: rv_aux= F77(a1); break;
   case 66062: rv_aux= F494(a1); break;
   case 66061: rv_aux= F55(a1); break;
   case 66060: rv_aux= F170(a1); break;
   case 66059: rv_aux= F119(a1); break;
   case 66442: rv_aux= _SH[a1]; break;
   case 66525: rv_aux= F283(a1); break;
   case 66058: rv_aux= F503(a1); break;
   case 66057: rv_aux= F161(a1); break;
   case 66441: rv_aux= _SH[(a1 + 1)]; break;
   case 66056: rv_aux= F7(a1); break;
   case 66055: rv_aux= F612(a1); break;
   case 66054: rv_aux= F676(a1); break;
   case 66220: rv_aux= F694(a1, nil); break;
   case 66219: rv_aux= F363(a1, nil); break;
   case 66216: rv_aux= F514(a1, nil); break;
   case 66053: rv_aux= F289(a1); break;
   case 66344: rv_aux= F755(_c(a1, nil)); break;
   case 66343: rv_aux= F735(_c(a1, nil)); break;
   case 66052: rv_aux= F486(a1); break;
   case 66051: rv_aux= F542(a1); break;
   case 66050: rv_aux= F417(a1); break;
   case 66204: rv_aux= F69(a1, nil); break;
   case 66049: rv_aux= F186(a1); break;
   case 66048: rv_aux= F556(a1); break;
   case 66047: rv_aux= F449(a1); break;
   case 66046: rv_aux= F16(a1); break;
   case 66045: rv_aux= F495(a1); break;
   case 66044: rv_aux= F621(a1); break;
   case 66043: rv_aux= F697(a1); break;
   case 66042: rv_aux= F391(a1); break;
   case 66041: rv_aux= F740(a1); break;
   case 66040: rv_aux= F672(a1); break;
   case 66039: rv_aux= F320(a1); break;
   case 66038: rv_aux= F701(a1); break;
   case 66037: rv_aux= F618(a1); break;
   case 66036: rv_aux= F461(a1); break;
   case 66035: rv_aux= F254(a1); break;
   case 66034: rv_aux= F586(a1); break;
   case 66033: rv_aux= F431(a1); break;
   case 66032: rv_aux= F166(a1); break;
   case 66834: rv_aux= F221(a1); break;
   case 66031: rv_aux= F208(a1); break;
   case 66030: rv_aux= F658(a1); break;
   case 66029: rv_aux= F652(a1); break;
   case 66028: rv_aux= F4(a1); break;
   case 66027: rv_aux= F549(a1); break;
   case 66026: rv_aux= F653(a1); break;
   case 66739: rv_aux= F132(a1); break;
   case 66202: rv_aux= F286(a1, 10); break;
   case 66201: rv_aux= F146(a1, 10); break;
   case 66025: rv_aux= F388(a1); break;
   case 66848: rv_aux= F328(a1); break;
   case 66024: rv_aux= F67(a1); break;
   case 66023: rv_aux= F336(a1); break;
   case 66022: rv_aux= F444(a1); break;
   case 66021: rv_aux= F569(a1); break;
   case 66020: rv_aux= F30(a1); break;
   case 66195: rv_aux= F179(a1, 10); break;
   case 66336: rv_aux= F714(a1, nil, 48, 66444, 10); break;
   case 66019: rv_aux= F287(a1); break;
   case 66018: rv_aux= F719(a1); break;
   case 66017: rv_aux= F111(a1); break;
   case 66016: rv_aux= F95(a1); break;
   case 66015: rv_aux= F123(a1); break;
   case 66014: rv_aux= F292(a1); break;
   case 66013: rv_aux= F565(a1); break;
   case 66012: rv_aux= F229(a1); break;
   case 66191: rv_aux= F394(a1, nil); break;
   case 66011: rv_aux= F305(a1); break;
   case 66010: rv_aux= F777(a1); break;
   case 66009: rv_aux= F645(a1); break;
   case 66008: rv_aux= F103(a1); break;
   case 66007: rv_aux= F430(a1); break;
   case 66006: rv_aux= F28(a1); break;
   case 66005: rv_aux= F157(a1); break;
   case 66004: rv_aux= F580(a1); break;
   case 66003: rv_aux= F213(a1); break;
   case 66002: rv_aux= F730(a1); break;
   case 66001: rv_aux= F266(a1); break;
   case 66000: rv_aux= F70(a1); break;
   case 65999: rv_aux= F390(a1); break;
   case 65998: rv_aux= F699(a1); break;
   case 65997: rv_aux= F227(a1); break;
   case 65996: rv_aux= F126(a1); break;
   case 65995: rv_aux= F402(a1); break;
   case 65994: rv_aux= F6(a1); break;
   case 65993: rv_aux= F364(a1); break;
   case 65992: rv_aux= F535(a1); break;
   case 65991: rv_aux= F369(a1); break;
   case 65990: rv_aux= F415(a1); break;
   case 65989: rv_aux= F80(a1); break;
   case 65988: rv_aux= F636(a1); break;
   case 65987: rv_aux= F606(a1); break;
   case 65986: rv_aux= F435(a1); break;
   case 65985: rv_aux= F24(a1); break;
   case 65984: rv_aux= F393(a1); break;
   case 65983: rv_aux= F649(a1); break;
   case 65982: rv_aux= F117(a1); break;
   case 65981: rv_aux= F226(a1); break;
   case 65980: rv_aux= F412(a1); break;
   case 65979: rv_aux= F732(a1); break;
   case 65978: rv_aux= F490(a1); break;
   case 65977: rv_aux= F688(a1); break;
   case 65976: rv_aux= F39(a1); break;
   case 65975: rv_aux= F313(a1); break;
   case 65974: rv_aux= F632(a1); break;
   case 66173: rv_aux= F557(a1, nil); break;
   case 65973: rv_aux= F406(a1); break;
   case 65972: rv_aux= F385(a1); break;
   case 65971: rv_aux= F543(a1); break;
   case 65970: rv_aux= F10(a1); break;
   case 65969: rv_aux= F180(a1); break;
   case 65968: rv_aux= F576(a1); break;
   case 65967: rv_aux= F432(a1); break;
   case 65966: rv_aux= F182(a1); break;
   case 65965: rv_aux= F700(a1); break;
   case 65964: rv_aux= F62(a1); break;
   case 65963: rv_aux= F655(a1); break;
   case 65962: rv_aux= F327(a1); break;
   case 65961: rv_aux= F748(a1); break;
   case 65960: rv_aux= F603(a1); break;
   case 65959: rv_aux= F529(a1); break;
   case 65958: rv_aux= F662(a1); break;
   case 66166: rv_aux= F40(a1, F750(_SH[66427], 66740)); break;
   case 65957: rv_aux= F294(a1); break;
   case 65956: rv_aux= F709(a1); break;
   case 65955: rv_aux= F222(a1); break;
   case 65954: rv_aux= F419(a1); break;
   case 65953: rv_aux= F619(a1); break;
   case 65952: rv_aux= F638(a1); break;
   case 65951: rv_aux= F339(a1); break;
   case 66163: rv_aux= F82(a1, _SH[66407]); break;
   case 65950: rv_aux= F376(a1); break;
   case 65949: rv_aux= F769(a1); break;
   case 65948: rv_aux= F668(a1); break;
   case 66273: rv_aux= F637(a1, nil, nil); break;
   case 66162: rv_aux= F651(a1, nil); break;
   case 65947: rv_aux= F517(a1); break;
   case 66161: rv_aux= F241(a1, nil); break;
   case 66159: rv_aux= F596(a1, nil); break;
   case 65946: rv_aux= F582(a1); break;
   case 65945: rv_aux= F428(a1); break;
   case 65944: rv_aux= F722(a1); break;
   case 65943: rv_aux= F370(a1); break;
   case 65942: rv_aux= F96(a1); break;
   case 65941: rv_aux= F190(a1); break;
   case 66157: rv_aux= F465(a1, nil); break;
   case 65940: rv_aux= F351(a1); break;
   case 65939: rv_aux= F723(a1); break;
   case 65938: rv_aux= F405(a1); break;
   case 65937: rv_aux= F478(a1); break;
   case 65936: rv_aux= F317(a1); break;
   case 65935: rv_aux= F425(a1); break;
   case 65934: rv_aux= F436(a1); break;
   case 65933: rv_aux= F520(a1); break;
   case 65932: rv_aux= F198(a1); break;
   case 65931: rv_aux= F760(a1); break;
   case 65930: rv_aux= F660(a1); break;
   case 65929: rv_aux= F736(a1); break;
   case 65928: rv_aux= F409(a1); break;
   case 65927: rv_aux= F141(a1); break;
   case 65926: rv_aux= F566(a1); break;
   case 65925: rv_aux= F771(a1); break;
   case 65924: rv_aux= F365(a1); break;
   case 65923: rv_aux= F267(a1); break;
   case 65922: rv_aux= F399(a1); break;
   case 65921: rv_aux= F746(a1); break;
   case 65920: rv_aux= F389(a1); break;
   case 65919: rv_aux= F454(a1); break;
   case 65918: rv_aux= F332(a1); break;
   case 65917: rv_aux= F79(a1); break;
   case 65916: rv_aux= F20(a1); break;
   case 65915: rv_aux= F481(a1); break;
   case 66267: rv_aux= SHARKread(a1, nil, nil); break;
   case 65914: rv_aux= F185(a1); break;
   case 65913: rv_aux= F642(a1); break;
   case 65912: rv_aux= F547(a1); break;
   case 65911: rv_aux= F346(a1); break;
   case 65910: rv_aux= F538(a1); break;
   case 65909: rv_aux= F537(a1); break;
   case 65908: rv_aux= F693(a1); break;
   case 65907: rv_aux= F219(a1); break;
   case 66610: rv_aux= F110(a1, _SH[66416], 0, _SH[66436], _SH[66437]); break;
   case 65906: rv_aux= F112(a1); break;
   case 65905: rv_aux= F571(a1); break;
   case 65904: rv_aux= F297(a1); break;
   case 65903: rv_aux= F531(a1); break;
   case 65902: rv_aux= F272(a1); break;
   case 65901: rv_aux= F100(a1); break;
   case 65900: rv_aux= F382(a1); break;
   case 65899: rv_aux= F378(a1); break;
   case 65898: rv_aux= F324(a1); break;
   case 65897: rv_aux= F159(a1); break;
   case 65896: rv_aux= F472(a1); break;
   case 65895: rv_aux= F468(a1); break;
   case 65894: rv_aux= F761(a1); break;
   case 65893: rv_aux= F561(a1); break;
   case 65892: rv_aux= F64(a1); break;
   case 65891: rv_aux= F163(a1); break;
   case 65890: rv_aux= F174(a1); break;
   case 65889: rv_aux= F778(a1); break;
   case 65888: rv_aux= F705(a1); break;
   case 65887: rv_aux= F171(a1); break;
   case 65886: rv_aux= F587(a1); break;
   case 65885: rv_aux= F33(a1); break;
   case 65884: rv_aux= F696(a1); break;
   case 65883: rv_aux= F375(a1); break;
   case 65882: rv_aux= F539(a1); break;
   case 65881: rv_aux= F741(a1); break;
   case 65880: rv_aux= F681(a1); break;
   case 65879: rv_aux= F135(a1); break;
   case 65878: rv_aux= F48(a1); break;
   case 65877: rv_aux= F575(a1); break;
   case 65876: rv_aux= F118(a1); break;
   case 65875: rv_aux= F572(a1); break;
   case 65874: rv_aux= F350(a1); break;
   case 65873: rv_aux= F337(a1); break;
   case 65872: rv_aux= F85(a1); break;
   case 65871: rv_aux= F477(a1); break;
   case 65870: rv_aux= F121(a1); break;
   case 66753: rv_aux= F47(a1); break;
   case 66587: rv_aux= F568(a1); break;
   case 66588: rv_aux= F54(a1); break;
   case 66589: rv_aux= F373(a1); break;
   case 66592: rv_aux= F355(a1); break;
   case 65869: rv_aux= F296(a1); break;
   case 65868: rv_aux= F114(a1); break;
   case 65867: rv_aux= F36(a1); break;
   case 65866: rv_aux= F61(a1); break;
   case 65865: rv_aux= F447(a1); break;
   case 65864: rv_aux= F253(a1); break;
   case 65863: rv_aux= F620(a1); break;
   case 65862: rv_aux= F8(a1); break;
   case 65861: rv_aux= F523(a1); break;
   case 65860: rv_aux= F236(a1); break;
   case 65859: rv_aux= F284(a1); break;
   case 65858: rv_aux= F56(a1); break;
   case 65857: rv_aux= F528(a1); break;
   case 65856: rv_aux= F423(a1); break;
   case 65855: rv_aux= F193(a1); break;
   case 65854: rv_aux= F679(a1); break;
   case 65853: rv_aux= F281(a1); break;
   case 65852: rv_aux= F13(a1); break;
   case 65851: rv_aux= F223(a1); break;
   case 65850: rv_aux= F742(a1); break;
   case 65849: rv_aux= F98(a1); break;
   case 65848: rv_aux= F367(a1); break;
   case 65847: rv_aux= F381(a1); break;
   case 65846: rv_aux= F105(a1); break;
   case 65845: rv_aux= F512(a1); break;
   case 65844: rv_aux= F165(a1); break;
   case 65843: rv_aux= F434(a1); break;
   case 65842: rv_aux= F109(a1); break;
   case 65841: rv_aux= F408(a1); break;
   case 65840: rv_aux= F232(a1); break;
   case 66250: rv_aux= F671(a1, nil, nil); break;
   case 65839: rv_aux= F715(a1); break;
   case 66847: rv_aux= F762(a1); break;
   case 66824: rv_aux= F443(a1); break;
   case 65838: rv_aux= F677(a1); break;
   case 65837: rv_aux= F5(a1); break;
   case 66112: rv_aux= F106(a1, 10); break;
   case 66248: rv_aux= F469(a1, nil, C1); break;
   case 66247: rv_aux= F122(a1, nil, C1); break;
   case 66302: rv_aux= F268(a1, 9, 10, nil); break;
   case 65836: rv_aux= F288(a1); break;
   case 65835: rv_aux= F493(a1); break;
   case 65834: rv_aux= F725(a1); break;
   case 65833: rv_aux= F271(a1); break;
   case 65832: rv_aux= F749(a1); break;
   case 65831: rv_aux= F737(a1); break;
   case 65830: rv_aux= F74(a1); break;
   case 65829: rv_aux= F252(a1); break;
   case 65828: rv_aux= F684(a1); break;
   case 65827: rv_aux= F212(a1); break;
   case 65826: rv_aux= F407(a1); break;
   case 65825: rv_aux= F306(a1); break;
   case 65824: rv_aux= F745(a1); break;
   case 66339: rv_aux= F635(a1, 66407, 66843, nil, 66407, 66407, nil); break;
   case 65823: rv_aux= F314(a1); break;
   case 65822: rv_aux= F743(a1); break;
   case 65821: rv_aux= F451(a1); break;
   case 65820: rv_aux= F115(a1); break;
   case 65819: rv_aux= F217(a1); break;
   case 65818: rv_aux= F238(a1); break;
   case 65817: rv_aux= F707(a1); break;
   case 65816: rv_aux= F686(a1); break;
   case 65815: rv_aux= F304(a1); break;
   case 65814: rv_aux= F448(a1); break;
   case 65813: rv_aux= F153(a1); break;
   case 65812: rv_aux= F546(a1); break;
   case 65811: rv_aux= F608(a1); break;
   case 65810: rv_aux= F466(a1); break;
   case 65809: rv_aux= F445(a1); break;
   case 65808: rv_aux= F421(a1); break;
   case 65807: rv_aux= F609(a1); break;
   case 65806: rv_aux= F540(a1); break;
   case 65805: rv_aux= F35(a1); break;
   case 65804: rv_aux= F81(a1); break;
   case 66094: rv_aux= F51(a1, nil); break;
   case 65803: rv_aux= F31(a1); break;
   case 65802: rv_aux= F583(a1); break;
   case 65801: rv_aux= F518(a1); break;
   case 65800: rv_aux= F192(a1); break;
   case 65799: rv_aux= F259(a1); break;
   case 65798: rv_aux= F255(a1); break;
   case 65797: rv_aux= F184(a1); break;
   case 65796: rv_aux= F191(a1); break;
   case 65795: rv_aux= F647(a1); break;
   case 65794: rv_aux= F27(a1); break;
   default: return F563(fn, 1); break;
}
 return rv_aux;
}
}

int F484(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, nil))))))))))))))))));}

int F485(int recs, int stream, int F443, int row_n)
{{int rv_aux = nil; {int rec = nil; int row_i = 0; if (F750(F443, 66808) != nil)
{F318(stream, F443);}
 { {int test_aux_var; {int and_result = nil; { and_result= recs;
 if (and_result != nil)
{{ if (row_i <= row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ { rec= _SH[recs];
 recs= _SH[(recs + 1)];
}

 row_i= (row_i + 1);
 F641(rec, stream, F443);
 {int and_result = nil; { and_result= recs;
 if (and_result != nil)
{{ if (row_i <= row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F486(int lists)
{{int rv_aux = nil; {int list1 = nil; int el = nil; int res = nil; while (lists != nil){ { list1= _SH[lists];
 lists= _SH[(lists + 1)];
}

 while (list1 != nil){ { el= _SH[list1];
 list1= _SH[(list1 + 1)];
}

 res= _c(el, res);
}

}

 rv_aux= F574(res);
}

 return rv_aux;
}
}

int F487()
{{int rv_aux = nil; { V44= C17;
 V48= V41;
 V49= V43;
 V41= nil;
 V43= (C17 - (C18 + 2));
 { V46= 1;
 rv_aux= V46;
}

}

 return rv_aux;
}
}

int F488(int guard, int size, int test_fn)
{{int rv_aux = nil; {int pl1 = nil; int pl2 = nil; int l1 = nil; int l2 = nil; int end_l1 = nil; int end = nil; int index2 = nil; pl1= guard;
 pl2= guard;
 index2= 1;
 while (index2 <= size){ index2= (index2 + 1);
 { _SH[pl2];
 pl2= _SH[(pl2 + 1)];
}

 if (_SH[(pl2 + 1)] == nil)
{index2= (size + 1);}
}

 end_l1= pl2;
 if (_SH[(pl2 + 1)] != nil)
{{ l1= _SH[(pl1 + 1)];
 l2= _SH[(pl2 + 1)];
 index2= 0;
 end= nil;
 while (end == nil){ if (F325(test_fn, _SH[l1], _SH[l2]) != nil)
{if (l1 == end_l1)
{{ end= _SH[66407];
 index2= (index2 + 1);
 while (index2 < size){ index2= (index2 + 1);
 { _SH[l2];
 l2= _SH[(l2 + 1)];
}

}

}
}
else {{ pl1= l1;
 l1= _SH[(l1 + 1)];
}
}}
else {{ _SH[(pl2 + 1)]= _SH[(l2 + 1)];
 _SH[(pl1 + 1)]= l2;
 pl1= l2;
 _SH[(l2 + 1)]= l1;
 l2= _SH[(pl2 + 1)];
 index2= (index2 + 1);
 if (index2 >= size)
{{ l2= pl2;
 end= _SH[66407];
}
}
 if (l2 == nil)
{{ l2= pl2;
 end= _SH[66407];
}
}
}
}
}

 guard= l2;
}
}
else {guard= end_l1;}
 rv_aux= guard;
}

 return rv_aux;
}
}

int F489(int stream, int buf_start)
{{int rv_aux = nil; {int64_t F237; int nbytes; int max_buf_pos = nil; {int buf_size = V6[stream]; int flush_fn = V11[stream]; int64_t max = V16[stream]; int buf_index = V20[stream]; int buf0_start = V25[stream]; if (buf_start == buf0_start)
{{int buf0_modP = V28[stream]; int buf0_nbytes = V27[stream]; int buf0_index = V26[stream]; int buf0_start = V25[stream]; if (!(buf0_modP == nil))
{{ max_buf_pos= F623(max, buf_size, buf_index, buf_start);
 V28[stream]= nil;
 F237= F41(buf_size, buf0_index);
 V37= F237;
 if (buf0_nbytes <= (1 + (max_buf_pos - buf0_start)))
{nbytes= buf0_nbytes;}
else {nbytes= (1 + (max_buf_pos - buf0_start));}
 rv_aux= F462(flush_fn, stream, buf_start, nbytes);
}
}
else {rv_aux= nil;}
}
}
else {{int buf1_modP = V32[stream]; int buf1_nbytes = V31[stream]; int buf1_index = V30[stream]; int buf1_start = V29[stream]; if (!(buf1_modP == nil))
{{ max_buf_pos= F623(max, buf_size, buf_index, buf_start);
 V32[stream]= nil;
 F237= F41(buf_size, buf1_index);
 V37= F237;
 if (buf1_nbytes <= (1 + (max_buf_pos - buf1_start)))
{nbytes= buf1_nbytes;}
else {nbytes= (1 + (max_buf_pos - buf1_start));}
 rv_aux= F462(flush_fn, stream, buf_start, nbytes);
}
}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F490(int iter_spec)
{return F483(F688(F283(iter_spec)), iter_spec);}

int F491(int in_stream, int out_stream)
{return F673(F518(in_stream), out_stream);}

int F492(int a1, int a2, int a3, int a4, int a5)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, nil)))));}

int F493(int str)
{{int rv_aux = nil; {int els = nil; int splitted_elements = nil; els= F348(str, 32, 66443, nil);
 {int el = nil; int el_on = nil; { el_on= els;
 el= _SH[el_on];
}

 while (el_on != nil){ if (F718(el, _SH[65703], 66443) != nil)
{{int start = nil; int end = nil; int aux = nil; aux= F348(el, _SH[65704], 66443, nil);
 start= _SH[aux];
 aux= _SH[(aux + 1)];
 end= _SH[aux];
 {int i = nil; i= F179(start, 10);
 while (i <= F179(end, 10)){ splitted_elements= _c(F714(i, nil, 48, 66444, 10), splitted_elements);
 i= (i + 1);
}

}

}
}
else {splitted_elements= _c(el, splitted_elements);}
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

}

 rv_aux= F574(splitted_elements);
}

 return rv_aux;
}
}

int F494(int exprs)
{{int rv_aux = nil; {int expr = nil; int i = 0; int temp_var = nil; int temp_vars = nil; int inits = nil; while (exprs != nil){ { expr= _SH[exprs];
 exprs= _SH[(exprs + 1)];
}

 i= (i + 1);
 temp_var= F394(F755(_c(_SH[65544], _c(F37(i, 10, nil), nil))), 66407);
 temp_vars= _c(temp_var, temp_vars);
 inits= _c(_c(temp_var, _c(expr, nil)), inits);
}

 rv_aux= _c(F574(temp_vars), _c(F574(inits), nil));
}

 return rv_aux;
}
}

int F495(int str)
{{int rv_aux = nil; {int and_result = nil; { and_result= F256(_SH[str], 124);
 if (and_result != nil)
{{ and_result= F256(F161(str), 124);
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F496(int list, int F60, int sl)
{{int rv_aux = nil; {int cons_i = nil; cons_i= F125(list, F60);
 if (cons_i != nil)
{rv_aux= _SH[(cons_i + 1)]= sl;}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F497(int stream, int object)
{{int rv_aux = nil; if (F245(stream) != nil)
{rv_aux= _c(66418, _c(object, nil));}
else {rv_aux= _c(66419, _c(stream, _c(object, nil)));}
 return rv_aux;
}
}

int F498(int list, int sl)
{return F735(_c(list, _c(sl, nil)));}

int F499(int aux_stream, int64_t F237)
{return F779(aux_stream, (F237 - 1));}

int64_t F500(int stream)
{return V17[stream];}

int F501(int stream, int condition, int delta, int plusP)
{{int rv_aux = nil; {int condition_delta = nil; if (!(F245(delta) != nil))
{F663(stream, delta);}
 condition_delta= F524(stream, condition);
 if (condition_delta >= 0)
{{ if (plusP != nil)
{F663(stream, condition_delta);}
else {if (F682(delta) != nil)
{F663(stream, (- delta));}}
 rv_aux= 66407;
}
}
else {{ if (!(F245(delta) != nil))
{F663(stream, (- delta));}
 rv_aux= nil;
}
}
}

 return rv_aux;
}
}

int64_t F502(int stream)
{return V15[stream];}

int F503(int list)
{return _SH[_SH[(_SH[(list + 1)] + 1)]];}

int F504(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, nil)))))))))))));}

int F505(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, nil))))))))))))))));}

int F506()
{return F2(_SH[66837], _SH[66836]);}

int F507(int start_node, int end_node)
{return F158(start_node, 66626, _c(end_node, F750(start_node, 66626)));}

double F508(int num_str)
{{double rv_aux; { F209(num_str);
 rv_aux= F525();
}

 return rv_aux;
}
}

int F509(int stream, int condition, int max_delta)
{return F602(stream, condition, 66462, nil, max_delta);}

int F510(int list)
{{int rv_aux; {int length = 0; {int test_aux_var; {int and_result = nil; { and_result= F556(list);
 if (and_result != nil)
{{ and_result= list;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ { _SH[list];
 list= _SH[(list + 1)];
}

 length= (length + 1);
 {int and_result = nil; { and_result= F556(list);
 if (and_result != nil)
{{ and_result= list;
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= length;
}

 return rv_aux;
}
}

int F511(int lp_file)
{return F38(lp_file);}

int F512(int str)
{return F664(66734, _SH[66781], _c(str, nil));}

int F513(int stream, int seq_condition)
{{int rv_aux = nil; {int conditions = _SH[(seq_condition + 1)]; int condition1 = nil; int F757 = nil; int delta = 0; int delta1 = nil; while (!(F757 != nil)){ { condition1= _SH[conditions];
 conditions= _SH[(conditions + 1)];
}

 delta1= F524(stream, condition1);
 if (delta1 < 0)
{F757= 66423;}
else {if (conditions != nil)
{{ F663(stream, delta1);
 delta= (delta + delta1);
}
}
else {F757= 66407;}}
}

 F663(stream, (- delta));
 if (F757 == 66423)
{rv_aux= -1;}
else {rv_aux= (delta + delta1);}
}

 return rv_aux;
}
}

int F514(int size, int object)
{{int rv_aux = nil; {int list = nil; while (size > 0){ list= _c(object, list);
 size= (size - 1);
}

 rv_aux= list;
}

 return rv_aux;
}
}

int F515(int new, int old, int list)
{return F90(new, 66443, list, _c(old, nil));}

int F516(int stream, int end_csym)
{{int rv_aux = nil; {int F757 = nil; int csym = nil; int string = nil; while (!(F757 != nil)){ csym= F299(stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(csym, -1);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(csym, end_csym);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{F757= 66407;}
else {string= _c(csym, string);}
}

}

 rv_aux= F574(string);
}

 return rv_aux;
}
}

int F517(int lp_file)
{return F572(lp_file);}

int F518(int stream)
{{int csym = nil; unsigned int code; unsigned int tag; int byte_n = 0; unsigned int utf8; csym= F299(stream);
 code= F773(csym);
 tag= (code & 128);
 { if (tag == 0)
{return csym;}
else {{ tag= (code & 240);
 if (tag < 224)
{{ byte_n= 2;
 utf8= (code & 31);
}
}
else {if (tag < 240)
{{ byte_n= 3;
 utf8= (code & 15);
}
}
else {if (tag == 240)
{{ byte_n= 4;
 utf8= (code & 7);
}
}
else {{ byte_n= 0;
 utf8= -1;
}
}}}
 {int x; x= (byte_n - 1);
 while (x >= 1){ code= F773(F299(stream));
 utf8= (utf8 << 6);
 utf8= (utf8 | (code & 127));
 x= (x - 1);
}

}

}
}
 return F335(utf8);
}

}
}

int F519(int object, int list)
{return F588(66443, list, _c(object, nil));}

int F520(int stream)
{return F678(V13[stream], nil);}

int F521(int stream, int strs, int test1_fn)
{{int rv_aux = nil; {int delta = nil; delta= -1;
 {int str = nil; int str_on = nil; { str_on= strs;
 str= _SH[str_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(str_on, nil);
 if (and_result != nil)
{{ if (!(delta >= 0))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ delta= F169(stream, str, test1_fn);
 { str_on= _SH[(str_on + 1)];
 str= _SH[str_on];
}

 {int and_result = nil; { and_result= F678(str_on, nil);
 if (and_result != nil)
{{ if (!(delta >= 0))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= delta;
}

 return rv_aux;
}
}

int F522()
{{int rv_aux = nil; { V44= C17;
 V48= V41;
 V49= V43;
 V41= V50;
 V43= V51;
 { V46= 1;
 rv_aux= V46;
}

}

 return rv_aux;
}
}

int F523(int str)
{return F664(66734, _SH[66755], _c(str, nil));}

int F524(int stream, int condition)
{{int rv_aux = nil; if (!(F243(stream, nil)))
{rv_aux= -1;}
else {if (condition == 66407)
{rv_aux= 0;}
else {if (condition == 66715)
{if (F778(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66714)
{if (F705(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66716)
{if (F171(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66717)
{if (F587(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66718)
{if (F33(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66719)
{if (F696(stream) != nil)
{rv_aux= 0;}
else {rv_aux= -1;}}
else {if (condition == 66720)
{if (F618(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66721)
{if (F461(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66722)
{if (F254(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66723)
{if (F586(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66724)
{if (F431(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66725)
{if (F166(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66726)
{if (F221(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66727)
{if (F208(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66728)
{if (F658(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66729)
{if (F652(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66730)
{if (F4(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66731)
{if (F549(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66732)
{if (F653(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (condition == 66739)
{if (F132(F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (F16(condition) != nil)
{if (F396(stream, nil) == condition)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (F123(condition) != nil)
{if (F483(condition, F396(stream, nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (F88(condition, 66407) != nil)
{rv_aux= F169(stream, condition, 66443);}
else {if (_SH[condition] == 66451)
{rv_aux= F483(F283(condition), stream);}
else {if (_SH[condition] == 66733)
{if (F322(F396(stream, nil), F283(condition)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (_SH[condition] == 66734)
{rv_aux= F169(stream, F283(condition), 66733);}
else {if (_SH[condition] == 66735)
{if (F664(66443, F283(condition), _c(F396(stream, nil), nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (_SH[condition] == 66736)
{if (F664(66733, F283(condition), _c(F396(stream, nil), nil)) != nil)
{rv_aux= 1;}
else {rv_aux= -1;}}
else {if (_SH[condition] == 66737)
{rv_aux= F521(stream, F283(condition), 66443);}
else {if (_SH[condition] == 66738)
{rv_aux= F521(stream, _SH[(condition + 1)], 66733);}
else {if (_SH[condition] == 66482)
{rv_aux= F151(stream, condition);}
else {if (_SH[condition] == 66494)
{rv_aux= F362(stream, condition);}
else {if (_SH[condition] == 66405)
{rv_aux= F513(stream, condition);}
else {if (_SH[condition] == 66648)
{rv_aux= F639(stream, condition);}
else {{ { { F87(_SH[66435]);
 F87(_SH[65642]);
 F87(_SH[66435]);
 F373(nil);
}

}

 rv_aux= -1;
}
}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
 return rv_aux;
}
}

double F525()
{{double result; sscanf(V52, "%lf", &result);
 return result;
}
}

int F526(int el, int list)
{{int rv_aux = nil; {int F60 = nil; int e1 = nil; int last_pos = nil; F60= 0;
 while (list != nil){ { e1= _SH[list];
 list= _SH[(list + 1)];
}

 F60= (F60 + 1);
 if (e1 == el)
{last_pos= F60;}
}

 rv_aux= last_pos;
}

 return rv_aux;
}
}

int F527(int prefix, int print_env)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int par_name_38value = nil; int par_name_38value_on = nil; { par_name_38value_on= print_env;
 par_name_38value= _SH[par_name_38value_on];
}

 while (par_name_38value_on != nil){ {int object1__4; int object2__5 = for_list_result; {int par_name = nil; int value = nil; int aux = nil; aux= par_name_38value;
 par_name= _SH[aux];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 object1__4= _c(F394(F755(_c(_SH[65603], _c(F292(prefix), _c(_SH[65604], _c(F292(par_name), _c(_SH[65605], nil)))))), nil), _c(value, nil));
}

 for_list_result= _c(object1__4, object2__5);
}

 { par_name_38value_on= _SH[(par_name_38value_on + 1)];
 par_name_38value= _SH[par_name_38value_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F528(int str)
{return F664(66734, _SH[66759], _c(str, nil));}

int F529(int type)
{return F172(_SH[66569], _SH[(F292(type) + 1)]);}

int F530(int old_lp, int new_lp)
{return F695(old_lp, new_lp);}

int F531(int norm_regex)
{{int rv_aux = nil; {int start_node = nil; int end_node = nil; start_node= _c(66623, _c(_c(66567, _c(66624, nil)), nil));
 end_node= _c(66623, _c(_c(66567, _c(66625, nil)), nil));
 F203(norm_regex, start_node, end_node);
 rv_aux= start_node;
}

 return rv_aux;
}
}

int F532(int list, int F424, int pos2)
{{int rv_aux = nil; {int list_len = nil; list_len= F510(list);
 {int list_len_var = nil; list_len_var= list_len;
 if (F424 < 0)
{F424= (list_len_var + F424 + 1);}
 if (pos2 < 0)
{pos2= (list_len_var + pos2 + 1);}
}

 list= F125(list, F424);
 rv_aux= F413(list, ((pos2 - F424) + 1));
}

 return rv_aux;
}
}

int F533(int list, int str, int pred1, int include_empty_itemsP)
{{int rv_aux = nil; {int cur_csym = nil; int cur_item = nil; int items = nil; while (list != nil){ if (F665(list, str, pred1) != nil)
{{ {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 list= F779(list, F510(str));
 cur_item= nil;
}
}
else {{ { cur_csym= _SH[list];
 list= _SH[(list + 1)];
}

 cur_item= _c(cur_csym, cur_item);
}
}
}

 {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 rv_aux= F574(items);
}

 return rv_aux;
}
}

int F534(int list, int csym, int pred1, int include_empty_itemsP)
{{int rv_aux = nil; {int cur_csym = nil; int cur_item = nil; int items = nil; while (list != nil){ { cur_csym= _SH[list];
 list= _SH[(list + 1)];
}

 if (F325(pred1, csym, cur_csym) != nil)
{{ {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 cur_item= nil;
}
}
else {cur_item= _c(cur_csym, cur_item);}
}

 {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 rv_aux= F574(items);
}

 return rv_aux;
}
}

int F535(int iter_spec)
{{int rv_aux = nil; {int var = nil; int expansion = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 {int in_depth = F78(iter_spec, 66465); int exp_by = F78(iter_spec, 66487); int exp_while = F78(iter_spec, 66488); expansion= F93(var, exp_by, exp_while);
 {int object1__12 = 66481; int object2__13; {int object1__14 = _c(66476, _c(_c(var, _c(_c(66489, _c(66490, nil)), _c(_c(66489, _c(66491, nil)), nil))), nil)); int object2__15; {int object1__34 = _c(66477, _c(_c(66405, _c(_c(66409, _c(66490, _c(_c(66492, nil), nil))), _c(_c(66409, _c(66491, _c(_c(66493, _c(66490, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(in_depth, nil))), nil)))), nil)); int object2__35; {int object1__74 = _c(66461, _c(_c(66494, _c(_c(66485, _c(var, _c(nil, nil))), _c(_c(66429, _c(66491, _c(66490, nil))), nil))), nil)); int object2__75; {int object1__98; int object2__99; {int object1__100 = 66479; int object2__101; {int object1__102; int object2__103; {int object1__104 = 66405; int object2__105; {int list__106; int sl__107; if (expansion != nil)
{list__106= _c(expansion, nil);}
else {list__106= nil;}
 sl__107= _c(_c(66409, _c(66491, _c(_c(66495, _c(var, _c(66491, nil))), nil))), _c(_c(66420, _c(_c(_c(66429, _c(66491, _c(66490, nil))), _c(_c(66409, _c(var, _c(_c(66496, _c(66491, nil)), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(nil, nil))), nil)), nil))), nil));
 object2__105= F498(list__106, sl__107);
}

 object1__102= _c(object1__104, object2__105);
}

 object2__103= nil;
 object2__101= _c(object1__102, object2__103);
}

 object1__98= _c(object1__100, object2__101);
}

 object2__99= _c(_c(66480, _c(_c(66497, nil), nil)), nil);
 object2__75= _c(object1__98, object2__99);
}

 object2__35= _c(object1__74, object2__75);
}

 object2__15= _c(object1__34, object2__35);
}

 object2__13= _c(object1__14, object2__15);
}

 rv_aux= _c(object1__12, object2__13);
}

}

}

 return rv_aux;
}
}

int F536(int expr, int stream, int delta, int levels, int F510)
{{int rv_aux = nil; { levels= (levels - 1);
 delta= (delta + 1);
 { F235(stream, _SH[65625]);
 F235(stream, F291(F2(expr, 1), stream, delta, levels, F510));
}

 {int el = nil; int el_index = nil; int for_vect_end = nil; int index = nil; { el_index= 2;
 el= F2(expr, el_index);
 for_vect_end= F552(expr);
}

 index= 2;
 {int test_aux_var; {int and_result = nil; { if (el_index <= for_vect_end)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (index <= (F510 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ F674(stream, delta);
 if (index == (F510 + 1))
{F235(stream, _SH[65626]);}
else {F291(el, stream, delta, levels, F510);}
 { el_index= (el_index + 1);
 if (el_index <= for_vect_end)
{el= F2(expr, el_index);}
}

 index= (index + 1);
 {int and_result = nil; { if (el_index <= for_vect_end)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (index <= (F510 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= F235(stream, _SH[65627]);
}

 return rv_aux;
}
}

int F537(int stream)
{{int rv_aux = nil; {int csym = nil; int csym_list = nil; int F757 = nil; F663(stream, 1);
 while (!(F757 != nil)){ csym= F299(stream);
 if (csym == -1)
{{ F610(stream, _SH[65614]);
 F757= 66407;
}
}
else {if (csym == 34)
{F757= 66407;}
else {if (csym == 124)
{{ F663(stream, -1);
 csym_list= _c(F538(stream), csym_list);
}
}
else {csym_list= _c(csym, csym_list);}}}
}

 csym_list= F574(csym_list);
 if (_SH[66595] != nil)
{rv_aux= F483(_SH[66595], csym_list);}
else {rv_aux= csym_list;}
}

 return rv_aux;
}
}

int F538(int stream)
{{int rv_aux = nil; {int F757 = nil; int csym = nil; int csym_name = nil; csym_name= _c(F299(stream), csym_name);
 while (!(F757 != nil)){ csym= F299(stream);
 if (csym == -1)
{F610(stream, _SH[65610]);}
else {{ csym_name= _c(csym, csym_name);
 if (csym == 124)
{F757= 66407;}
else {if (F510(csym_name) > 7)
{F610(stream, _SH[65611]);}}
}
}
}

 csym_name= F574(csym_name);
 if (F612(csym_name) != nil)
{rv_aux= F394(csym_name, 66407);}
else {rv_aux= F283(csym_name);}
}

 return rv_aux;
}
}

int F539(int condition_type)
{return F167(condition_type, _SH[65643]);}

int F540(int cur_tag_singleP)
{{int rv_aux = nil; {int tag_name = nil; int tag_body = nil; int last_el_on = nil; tag_name= F506();
 tag_body= F75();
 _SH[66836]= (_SH[66836] - 1);
 if (cur_tag_singleP != nil)
{{ last_el_on= F676(tag_body);
 if (F74(_SH[last_el_on]) != nil)
{_SH[last_el_on]= _c(tag_name, _c(_SH[last_el_on], nil));}
else {F421(_c(tag_name, nil));}
 rv_aux= F609(tag_body);
}
}
else {rv_aux= F421(_c(tag_name, F574(tag_body)));}
}

 return rv_aux;
}
}

int F541()
{{int rv_aux = nil; { V2= -2;
 rv_aux= nil;
}

 return rv_aux;
}
}

int F542(int lists)
{{int rv_aux = nil; {int buf = nil; int cur = nil; int list1 = nil; {int test_aux_var; {int and_result = nil; { and_result= lists;
 if (and_result != nil)
{{ and_result= F256(_SH[lists], nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ lists= _SH[(lists + 1)];
 {int and_result = nil; { and_result= lists;
 if (and_result != nil)
{{ and_result= F256(_SH[lists], nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 if (lists != nil)
{{ { buf= _SH[lists];
 lists= _SH[(lists + 1)];
}

 cur= buf;
 while (lists != nil){ { list1= _SH[lists];
 lists= _SH[(lists + 1)];
}

 if (list1 != nil)
{{ F763(cur, list1);
 cur= list1;
}
}
}

 rv_aux= buf;
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F543(int rec_name)
{return _c(66550, _c(_c(F182(rec_name), _c(66564, _c(_c(66551, _c(66552, nil)), _c(66555, _c(66565, nil))))), _c(_c(66566, _c(_c(66413, _c(rec_name, nil)), _c(66564, _c(66552, _c(66565, nil))))), nil)));}

int F544(int stream, int condition)
{return F602(stream, condition, 66712, 66407, nil);}

int F545(int a1, int a2, int a3, int a4, int a5, int a6, int a7)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, nil)))))));}

int F546(int stream)
{return F366(stream, _SH[65753]);}

int F547(int stream)
{{int rv_aux = nil; {int csym = nil; int csym2 = nil; int end_of_hm_commentP = nil; int counter = nil; counter= 1;
 F663(stream, 2);
 { while (!(end_of_hm_commentP != nil)){ csym= F299(stream);
 if (csym == -1)
{{ F610(stream, _SH[65608]);
 end_of_hm_commentP= 66407;
}
}
else {if (csym == 124)
{{ csym2= F396(stream, nil);
 if (csym2 == 35)
{{ F663(stream, 1);
 counter= (counter - 1);
 if (counter == 0)
{end_of_hm_commentP= 66407;}
}
}
}
}
else {if (csym == 35)
{{ csym2= F396(stream, nil);
 if (csym2 == 124)
{{ F663(stream, 1);
 counter= (counter + 1);
}
}
}
}}}
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F548(int stream, int condition, int condition_type)
{{int rv_aux = nil; {int F757 = nil; int delta = nil; int initial_pos = F237(stream); int last_pos = (initial_pos + 1); F772(stream, condition_type);
 {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(F757 != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ delta= F524(stream, condition);
 if (condition_type == 66461)
{if (delta < 0)
{F757= 66407;}
else {if (delta == 0)
{F663(stream, 1);}
else {F663(stream, delta);}}}
else {if (condition_type == 66462)
{if (delta >= 0)
{F757= 66407;}
else {F663(stream, 1);}}
else {if (condition_type == 66709)
{if (delta >= 0)
{{ F757= 66407;
 F663(stream, delta);
}
}
else {F663(stream, 1);}}
else {if (condition_type == 66710)
{if (delta >= 0)
{{ F757= 66407;
 F663(stream, -1);
}
}
else {F663(stream, 1);}}
else {if (condition_type == 66713)
{if (delta >= 0)
{if (delta == 0)
{{ last_pos= F237(stream);
 F663(stream, -1);
}
}
else {if ((F237(stream) + delta) == last_pos)
{{ last_pos= F237(stream);
 F663(stream, -1);
}
}
else {F757= 66423;}}}
else {F663(stream, -1);}}
else {if (condition_type == 66708)
{if (delta >= 0)
{F757= 66407;}
else {F663(stream, -1);}}
else {if (condition_type == 66711)
{if (delta >= 0)
{{ F757= 66407;
 F663(stream, delta);
}
}
else {F663(stream, -1);}}
else {if (condition_type == 66712)
{if (delta >= 0)
{{ F757= 66407;
 F663(stream, -1);
}
}
else {F663(stream, -1);}}}}}}}}}
 {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(F757 != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 if (condition_type == 66713)
{F757= F72(stream, F757, last_pos, initial_pos);}
 rv_aux= F757;
}

 return rv_aux;
}
}

int F549(int csym)
{{int rv_aux = nil; {int and_result = nil; { and_result= F652(csym);
 if (and_result != nil)
{{ if (!(F221(csym) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F550(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, nil))))))))));}

int F551(int x)
{{int rv_aux = nil; {int or_result = nil; { if (F44(x))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {{ if (F29(x))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F552(int vect)
{if ((vect >= C22) && (vect <= (C17 - 2)) && ((unsigned int)_SH[vect] == C28))
{return _SH[(vect + 1)];}
else {{ printf("\n***** EXIT: ---NOT a vect---\n");
 fflush(stdout);
 exit(0);
}
}}

int F553(int stream, int condition, int out_stream, int inTOout_fn)
{return F680(stream, out_stream, condition, 66710, inTOout_fn);}

int F554(int stream, int n)
{{int rv_aux = nil; { F663(stream, n);
 rv_aux= 66407;
}

 return rv_aux;
}
}

char* F555(int lp, int buf2P)
{return F711(F40(lp, F750(_SH[66427], 66740)), buf2P);}

int F556(int object_index)
{if ((object_index >= C22) && (object_index <= (C17 - 3)))
{return C24;}
else {return nil;}}

int F557(int str, int datum)
{{int rv_aux = nil; {int leaf = nil; int cur = nil; str= F186(str);
 leaf= _c(C11, datum);
 cur= leaf;
 {int csym = nil; int csym_on = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 while (csym_on != nil){ cur= _c(F773(csym), _c(cur, nil));
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

}

 F69(str, nil);
 rv_aux= _c(cur, _c(leaf, nil));
}

 return rv_aux;
}
}

int F558(int stream, int F443, int row_n)
{{int rv_aux = nil; {int row_i = 0; int rec = nil; int recs = nil; {int headerP = nil; int separator = nil; int terminator = nil; int aux_rec = nil; aux_rec= F443;
 headerP= F750(aux_rec, 66808);
 separator= F750(aux_rec, 66798);
 terminator= F750(aux_rec, 66799);
 if (headerP != nil)
{F106(stream, F750(F443, 66799));}
 {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (row_i < row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ row_i= (row_i + 1);
 rec= F670(stream, separator, terminator, F443);
 recs= _c(rec, recs);
 {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (row_i < row_n)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= F574(recs);
}

}

 return rv_aux;
}
}

char* F559(int string)
{{char* rv_aux; {int index; { index= 0;
 while (string != nil){ V53[index]= (char)_SH[string];
 string= _SH[(string + 1)];
 index= (index + 1);
}

 V53[index]= (char)0;
 return V53;
}

}

 return rv_aux;
}
}

int F560(int pred, int list, int mode, int args)
{{int rv_aux = nil; if (mode == 66445)
{rv_aux= F578(pred, list, args);}
else {if (mode == 66444)
{rv_aux= F654(pred, list, args);}
else {if (mode == 66448)
{rv_aux= F654(pred, F578(pred, list, args), args);}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F561(int act_spec)
{return F664(66443, _SH[65636], _c(_SH[act_spec], nil));}

int F562(int fn, int a1, int a2, int a3, int a4, int a5, int a6)
{{int rv_aux = nil; switch (fn) {
   case 66338: rv_aux= F147(a1, a2, a3, a4, a5, a6); break;
   case 66538: rv_aux= F726(a1, a2, a3, a4, a5, a6); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5 + a6); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5 - a6); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5 * a6); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5 / a6); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, _c(a6, nil)))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, _c(a6, nil)))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, _c(a6, nil)))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, _c(a6, nil)))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, nil))))))); break;
   case 66339: rv_aux= F635(a1, a2, a3, a4, a5, a6, nil); break;
   default: return F563(fn, 6); break;
}
 return rv_aux;
}
}

int F563(int fn, int argn)
{{int rv_aux = nil; { { F87(_SH[66435]);
 F87(_SH[65648]);
 F87(F292(fn));
 F87(_SH[65649]);
 F87(_SH[65650]);
 F87(F37(argn, 10, nil));
 F87(_SH[65651]);
 F87(_SH[65652]);
 F87(_SH[66435]);
 F373(nil);
}

 { printf("\n***** EXIT: \n");
 fflush(stdout);
 exit(0);
 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F564(int str, int size, int pad_csym, int align)
{{int rv_aux = nil; {int pad_size = nil; pad_size= (size - F510(str));
 if (pad_size > 0)
{rv_aux= F275(pad_csym, str, pad_size, F287(align));}
else {if (pad_size == 0)
{rv_aux= F289(str);}
else {rv_aux= F532(str, 1, size);}}
}

 return rv_aux;
}
}

int F565(int sym_index)
{if ((sym_index >= 0) && (sym_index <= (C21 - 1)))
{return _SH[sym_index];}
else {return nil;}}

int F566(int stream)
{{int rv_aux = nil; if (V7[stream] != nil)
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F567(int64_t F60, int buf_size)
{{int rv_aux = nil; if (F60 < 1)
{rv_aux= -1;}
else {rv_aux= ((F60 - 1) / buf_size);}
 return rv_aux;
}
}

int F568(int stream)
{{int rv_aux = nil; {int csym = nil; csym= getc(stdin);
 if (csym != nil)
{ungetc(csym, stdin);}
 rv_aux= csym;
}

 return rv_aux;
}
}

int F569(int str)
{{int rv_aux = nil; {int new_str = nil; int csym = nil; int word_startP = nil; word_startP= 66407;
 while (str != nil){ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 if (!(F586(csym) != nil))
{word_startP= 66407;}
else {if (word_startP != nil)
{{ csym= F320(csym);
 word_startP= nil;
}
}
else {csym= F672(csym);}}
 new_str= _c(csym, new_str);
}

 rv_aux= F574(new_str);
}

 return rv_aux;
}
}

int F570(int opr, int exprs)
{{int rv_aux = nil; if (_SH[(exprs + 1)] != nil)
{rv_aux= _c(opr, _c(_SH[exprs], _c(F570(opr, _SH[(exprs + 1)]), nil)));}
else {if (exprs != nil)
{rv_aux= _SH[exprs];}
else {rv_aux= nil;}}
 return rv_aux;
}
}

int F571(int regex)
{{int rv_aux = nil; if (F123(regex) != nil)
{rv_aux= regex;}
else {if (F88(regex, 66407) != nil)
{rv_aux= _c(66405, F498(regex, nil));}
else {if (F774(regex, 66409) != nil)
{rv_aux= _c(66409, _c(F283(regex), _c(F571(F503(regex)), nil)));}
else {if (F774(regex, 66615) != nil)
{rv_aux= _c(66494, _c(nil, _c(F571(F283(regex)), nil)));}
else {if (F774(regex, 66432) != nil)
{rv_aux= _c(66405, _c(F571(F283(regex)), _c(_c(66616, _c(F571(F283(regex)), nil)), nil)));}
else {if (F774(regex, 66617) != nil)
{{int object1__47 = 66405; int object2__48; {int list__49; int sl__50; {int for_list_result = nil; for_list_result= nil;
 {int repeat_var = nil; repeat_var= 1;
 while (repeat_var <= F283(regex)){ for_list_result= _c(F571(F503(regex)), for_list_result);
 repeat_var= (repeat_var + 1);
}

}

 list__49= F574(for_list_result);
}

 sl__50= nil;
 object2__48= F498(list__49, sl__50);
}

 rv_aux= _c(object1__47, object2__48);
}
}
else {if (F774(regex, 66618) != nil)
{{int object1__63 = 66405; int object2__64; {int list__65; int sl__66; {int for_list_result = nil; for_list_result= nil;
 {int repeat_var = nil; repeat_var= 1;
 while (repeat_var <= F283(regex)){ for_list_result= _c(F571(F503(regex)), for_list_result);
 repeat_var= (repeat_var + 1);
}

}

 list__65= F574(for_list_result);
}

 sl__66= _c(_c(66616, _c(F571(F503(regex)), nil)), nil);
 object2__64= F498(list__65, sl__66);
}

 rv_aux= _c(object1__63, object2__64);
}
}
else {if (F774(regex, 66619) != nil)
{{int object1__87 = 66405; int object2__88; {int list__89; int sl__90; {int for_list_result = nil; for_list_result= nil;
 {int repeat_var = nil; repeat_var= 1;
 while (repeat_var <= F283(regex)){ for_list_result= _c(F571(_c(66615, _c(F503(regex), nil))), for_list_result);
 repeat_var= (repeat_var + 1);
}

}

 list__89= F574(for_list_result);
}

 sl__90= nil;
 object2__88= F498(list__89, sl__90);
}

 rv_aux= _c(object1__87, object2__88);
}
}
else {if (F774(regex, 66620) != nil)
{{int object1__107 = 66405; int object2__108; {int list__109; int sl__110; {int for_list_result = nil; for_list_result= nil;
 {int repeat_var = nil; repeat_var= 1;
 while (repeat_var <= F283(regex)){ for_list_result= _c(F571(F137(regex, 4)), for_list_result);
 repeat_var= (repeat_var + 1);
}

}

 list__109= F574(for_list_result);
}

 {int list__122; int sl__123; {int for_list_result = nil; for_list_result= nil;
 {int repeat_var = nil; repeat_var= 1;
 while (repeat_var <= F503(regex)){ for_list_result= _c(F571(_c(66615, _c(F137(regex, 4), nil))), for_list_result);
 repeat_var= (repeat_var + 1);
}

}

 list__122= F574(for_list_result);
}

 sl__123= nil;
 sl__110= F498(list__122, sl__123);
}

 object2__108= F498(list__109, sl__110);
}

 rv_aux= _c(object1__107, object2__108);
}
}
else {if (F774(regex, 66621) != nil)
{rv_aux= _c(66621, _c(F297(F283(regex)), nil));}
else {if (F774(regex, 66622) != nil)
{rv_aux= _c(66622, _c(F297(F283(regex)), nil));}
else {{int object1__155 = _SH[regex]; int object2__156; {int for_list_result = nil; for_list_result= nil;
 {int el = nil; int el_on = nil; { el_on= _SH[(regex + 1)];
 el= _SH[el_on];
}

 while (el_on != nil){ for_list_result= _c(F571(el), for_list_result);
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

}

}

 object2__156= F574(for_list_result);
}

 rv_aux= _c(object1__155, object2__156);
}
}}}}}}}}}}}
 return rv_aux;
}
}

int F572(int lp_file)
{{int rv_aux = nil; {int answer = nil; answer= F302(F755(_c(_SH[65655], _c(F40(lp_file, F750(_SH[66427], 66740)), nil))), 66407);
 answer= F348(_SH[answer], _SH[65656], 66443, nil);
 rv_aux= F755(_c(F137(answer, 6), _c(_SH[65657], _c(F137(answer, 7), _c(_SH[65658], _c(F137(answer, 8), nil))))));
}

 return rv_aux;
}
}

int F573(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, _c(a28, _c(a29, _c(a30, nil))))))))))))))))))))))))))))));}

int F574(int list)
{{int rv_aux = nil; {int previous = nil; int next = nil; while (list != nil){ next= _SH[(list + 1)];
 _SH[(list + 1)]= previous;
 previous= list;
 list= next;
}

 rv_aux= previous;
}

 return rv_aux;
}
}

int F575(int str)
{if (F719(str) != nil)
{return F111(str);}
else {return F280(V45, str);}}

int F576(int rec_name)
{return F394(F755(_c(F292(rec_name), _c(_SH[65570], nil))), 66407);}

int F577()
{{int rv_aux = nil; { _SH[66775]= _SH[65678];
 _SH[66776]= _SH[65679];
 _SH[66777]= _SH[65680];
 _SH[66778]= _SH[65681];
 _SH[66779]= _SH[65682];
 _SH[66780]= _SH[65683];
 _SH[66781]= _SH[65684];
 _SH[66782]= _SH[65685];
 _SH[66783]= _SH[65686];
 _SH[66784]= _SH[65687];
 _SH[66785]= _SH[65688];
 rv_aux= F264();
}

 return rv_aux;
}
}

int F578(int pred, int list, int args)
{{int rv_aux = nil; {int res = nil; args= _c(nil, args);
 while (list != nil){ _SH[args]= _SH[list];
 if (F717(pred, args) != nil)
{{ _SH[list];
 list= _SH[(list + 1)];
}
}
else {{ res= list;
 list= nil;
}
}
}

 F417(args);
 rv_aux= res;
}

 return rv_aux;
}
}

FILE* F579(int stream_index)
{return V56[stream_index];}

int F580(int stack)
{return F2(stack, 1);}

int F581(int stream, int condition, int inTOout_fn)
{return F57(stream, condition, 66710, inTOout_fn);}

int F582(int lp_name)
{return F664(66581, F750(_SH[66427], 66580), _c(lp_name, nil));}

int F583(int str)
{{int rv_aux = nil; { {int csym = nil; int csym_on = nil; { csym_on= str;
 csym= _SH[csym_on];
}

 while (csym_on != nil){ if (csym == 58)
{_SH[csym_on]= 45;}
 { csym_on= _SH[(csym_on + 1)];
 csym= _SH[csym_on];
}

}

}

 if (_SH[66839] == 66842)
{rv_aux= F394(F444(str), 66407);}
else {if (_SH[66839] == 66843)
{rv_aux= F394(F67(str), 66407);}
else {if (_SH[66839] == 66844)
{rv_aux= F394(F30(str), 66407);}
else {rv_aux= F394(str, 66407);}}}
}

 return rv_aux;
}
}

int64_t F584(int lp_file)
{{int64_t rv_aux; {struct stat file_status; int64_t size; if (stat(F555(lp_file, nil), &file_status) >= 0)
{{ size= file_status.st_size;
 rv_aux= size;
}
}
else {rv_aux= -1;}
}

 return rv_aux;
}
}

int F585(int rec, int rec_name, int fields)
{{int rv_aux = nil; if (F123(fields) != nil)
{rv_aux= F751(rec_name, rec, fields);}
else {if (!(_SH[(fields + 1)] != nil))
{rv_aux= F751(rec_name, rec, _SH[fields]);}
else {{ rec= F751(rec_name, rec, _SH[fields]);
 rv_aux= F585(rec, F710(rec_name, _SH[fields]), _SH[(fields + 1)]);
}
}}
 return rv_aux;
}
}

int F586(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F618(csym);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F461(csym);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F587(int stream)
{{int rv_aux = nil; if (F237(stream) == F500(stream))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F588(int pred, int list, int args)
{{int rv_aux = nil; {int el = nil; int new_list = nil; args= _c(nil, args);
 while (list != nil){ { el= _SH[list];
 list= _SH[(list + 1)];
}

 _SH[args]= el;
 if (!(F717(pred, args) != nil))
{new_list= _c(el, new_list);}
}

 F417(args);
 rv_aux= F574(new_list);
}

 return rv_aux;
}
}

int F589(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, nil)))))))))))))));}

int F590(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, nil)))))))))))))))))))))));}

int F591(int stream, int act_spec)
{{int rv_aux = nil; {int type = nil; int condition = nil; int delta = nil; int aux = nil; aux= act_spec;
 type= _SH[aux];
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 aux= _SH[(aux + 1)];
 delta= _SH[aux];
 if (type == 66688)
{rv_aux= _c(66698, _c(stream, _c(condition, nil)));}
else {if (type == 66689)
{rv_aux= _c(66699, _c(stream, _c(condition, nil)));}
else {if (type == 66690)
{rv_aux= _c(66700, _c(stream, _c(condition, nil)));}
else {if (type == 66691)
{rv_aux= _c(66701, _c(stream, _c(condition, nil)));}
else {if (type == 66692)
{rv_aux= _c(66702, _c(stream, _c(condition, nil)));}
else {if (type == 66693)
{rv_aux= _c(66703, _c(stream, _c(condition, nil)));}
else {if (type == 66694)
{rv_aux= _c(66704, _c(stream, _c(condition, nil)));}
else {if (type == 66695)
{rv_aux= _c(66705, _c(stream, _c(condition, nil)));}
else {if (type == 66696)
{rv_aux= _c(66706, _c(stream, _c(condition, _c(delta, nil))));}
else {if (type == 66697)
{rv_aux= _c(66707, _c(stream, _c(condition, _c(delta, nil))));}
else {rv_aux= nil;}}}}}}}}}}
}

 return rv_aux;
}
}

int F592(int a1, int a2)
{return _c(a1, _c(a2, nil));}

int F593(int rec_name, int field_var_specs, int rec, int body)
{{int rv_aux = nil; {int field_specs = F327(rec_name); int field_type = nil; int field_type_name = nil; int var_name = nil; int field_name = nil; {int object1__2 = 66424; int object2__3; {int object1__4; int object2__5; {int for_list_result = nil; for_list_result= nil;
 {int field_var_spec = nil; int field_var_spec_on = nil; { field_var_spec_on= field_var_specs;
 field_var_spec= _SH[field_var_spec_on];
}

 while (field_var_spec_on != nil){ {int object1__9; int object2__10 = for_list_result; { if (F556(field_var_spec) != nil)
{{int aux = nil; aux= field_var_spec;
 var_name= _SH[aux];
 aux= _SH[(aux + 1)];
 field_name= _SH[aux];
}
}
else {{ var_name= field_var_spec;
 field_name= field_var_spec;
}
}
 {int for_thereis_result = nil; for_thereis_result= nil;
 {int field_spec = nil; int field_spec_on = nil; { field_spec_on= field_specs;
 field_spec= _SH[field_spec_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(field_spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F750(field_spec, 66548) == field_name)
{{ field_type= F750(field_spec, 66567);
 field_type_name= F750(field_spec, 66549);
 for_thereis_result= 66407;
}
}
else {for_thereis_result= nil;}
 { field_spec_on= _SH[(field_spec_on + 1)];
 field_spec= _SH[field_spec_on];
}

 {int and_result = nil; { and_result= F678(field_spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

}

 if (F603(field_type_name) != nil)
{field_type= 66562;}
 object1__9= _c(field_type, _c(var_name, _c(F751(rec_name, rec, field_name), nil)));
}

 for_list_result= _c(object1__9, object2__10);
}

 { field_var_spec_on= _SH[(field_var_spec_on + 1)];
 field_var_spec= _SH[field_var_spec_on];
}

}

}

 object1__4= F574(for_list_result);
}

 object2__5= F498(body, nil);
 object2__3= _c(object1__4, object2__5);
}

 rv_aux= _c(object1__2, object2__3);
}

}

 return rv_aux;
}
}

int F594(int field, int value, int rec_list, int test_fn)
{{int rv_aux = nil; {int cur_rec = nil; int rec = nil; while (rec_list != nil){ { cur_rec= _SH[rec_list];
 rec_list= _SH[(rec_list + 1)];
}

 if (F25(F750(cur_rec, field), value, test_fn) != nil)
{{ rec= cur_rec;
 rec_list= nil;
}
}
}

 rv_aux= rec;
}

 return rv_aux;
}
}

int F595(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, nil)))))))))))))))));}

int F596(int lp, int remove_subdirsP)
{{int rv_aux = nil; {int file_list = nil; int file = nil; int subdir_list = nil; int subdir = nil; if (remove_subdirsP != nil)
{{ file_list= F637(lp, nil, nil);
 while (file_list != nil){ { file= _SH[file_list];
 file_list= _SH[(file_list + 1)];
}

 F121(file);
}

 subdir_list= F651(lp, nil);
 while (subdir_list != nil){ { subdir= _SH[subdir_list];
 subdir_list= _SH[(subdir_list + 1)];
}

 F596(subdir, _SH[66407]);
}

}
}
 rv_aux= F121(lp);
}

 return rv_aux;
}
}

int F597(int get_form, int opr, int par)
{{int rv_aux = nil; if (F123(get_form) != nil)
{rv_aux= _c(66409, _c(get_form, _c(_c(opr, _c(get_form, _c(par, nil))), nil)));}
else {if (F774(get_form, 66410) != nil)
{{int list1 = F283(get_form); int index = F503(get_form); rv_aux= _c(66411, _c(list1, _c(index, _c(_c(opr, _c(get_form, _c(par, nil))), nil))));
}
}
else {{int get_form_name = nil; int get_form_args = nil; int set_form_name = nil; get_form_name= _SH[get_form];
 get_form_args= _SH[(get_form + 1)];
 set_form_name= F394(F755(_c(F694(F292(get_form_name), 3), _c(_SH[65538], nil))), nil);
 rv_aux= _c(set_form_name, F498(get_form_args, _c(_c(opr, _c(get_form, _c(par, nil))), nil)));
}
}}
 return rv_aux;
}
}

int F598(const char* cstr)
{{int rv_aux = nil; {int str = nil; int index = 0; index= 0;
 if (!(cstr == NULL))
{while (!(cstr[index] == 0)){ str= _c(cstr[index], str);
 index= (index + 1);
}
}
 return F574(str);
}

 return rv_aux;
}
}

int F599(int row, int F443)
{{int rv_aux = nil; {int name = nil; int field_names = nil; int aux_rec = nil; aux_rec= F443;
 name= F750(aux_rec, 66548);
 field_names= F750(aux_rec, 66802);
 {int object1__5 = name; int object2__6; {int for_list_result = nil; for_list_result= nil;
 {int el = nil; int el_on = nil; int field = nil; int field_on = nil; { el_on= row;
 el= _SH[el_on];
}

 { field_on= field_names;
 field= _SH[field_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ and_result= F678(field_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ for_list_result= _c(_c(field, _c(el, nil)), for_list_result);
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 { field_on= _SH[(field_on + 1)];
 field= _SH[field_on];
}

 {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ and_result= F678(field_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 object2__6= F574(for_list_result);
}

 rv_aux= _c(object1__5, object2__6);
}

}

 return rv_aux;
}
}

int F600(int rec_name, int field_name)
{return F394(F755(_c(F292(rec_name), _c(_SH[65566], _c(F292(field_name), _c(_SH[65567], nil))))), 66407);}

int F601(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, nil)))))))))))))))))))));}

int F602(int stream, int condition, int condition_type, int moveP, int max_delta)
{{int rv_aux = nil; if (moveP != nil)
{if (F245(max_delta) != nil)
{rv_aux= F548(stream, condition, condition_type);}
else {if (condition_type == 66462)
{{int64_t start = F237(stream); int64_t end = (F237(stream) + max_delta); if (start > end)
{rv_aux= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65644];
 rv_aux= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { stream_slice_rv= F548(stream, condition, condition_type);
 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
 rv_aux= unwind_value;
}

}
}}
}
}
else {{int cur_pos = F237(stream); {int64_t start = (F237(stream) - max_delta); int64_t end = nil; if (start > end)
{rv_aux= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65645];
 rv_aux= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { F102(stream, cur_pos);
 stream_slice_rv= F548(stream, condition, condition_type);
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
 rv_aux= unwind_value;
}

}
}}
}

}
}}}
else {{int64_t cur_pos = F237(stream); int result = nil; if (F245(max_delta) != nil)
{result= F548(stream, condition, condition_type);}
else {if (condition_type == 66462)
{{int64_t start = F237(stream); int64_t end = (F237(stream) + max_delta); if (start > end)
{result= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65646];
 result= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { stream_slice_rv= F548(stream, condition, condition_type);
 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
 result= unwind_value;
}

}
}}
}
}
else {{int cur_pos = F237(stream); {int64_t start = (F237(stream) - max_delta); int64_t end = nil; if (start > end)
{result= nil;}
else {if (V36 >= C16)
{{ _SH[66422]= _SH[65647];
 result= _SH[66422];
}
}
else {{int stream_slice_rv = nil; _SH[66422]= nil;
 {int unwind_value = nil; { { V35[V36]= V15[stream];
 V36= (V36 + 1);
}

 { V35[V36]= V16[stream];
 V36= (V36 + 1);
}

 if (F682(start) != nil)
{{ if (start < V15[stream])
{start= V15[stream];}
 V15[stream]= start;
 F102(stream, start);
}
}
 if (F682(end) != nil)
{{ if (end > V16[stream])
{end= V16[stream];}
 V16[stream]= end;
}
}
 { { F102(stream, cur_pos);
 stream_slice_rv= F548(stream, condition, condition_type);
}

 unwind_value= stream_slice_rv;
}

}

 { V36= (V36 - 1);
 V16[stream]= V35[V36];
}

 { V36= (V36 - 1);
 V15[stream]= V35[V36];
}

 if (F682(end) != nil)
{F102(stream, (end + 1));}
 result= unwind_value;
}

}
}}
}

}
}}
 F102(stream, cur_pos);
 rv_aux= result;
}
}
 return rv_aux;
}
}

int F603(int type_name)
{return F172(_SH[66569], F292(type_name));}

int F604(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22, int a23, int a24, int a25, int a26, int a27, int a28, int a29, int a30, int a31)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, _c(a23, _c(a24, _c(a25, _c(a26, _c(a27, _c(a28, _c(a29, _c(a30, _c(a31, nil)))))))))))))))))))))))))))))));}

int F605(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, nil)))))))))))))))))));}

int F606(int iter_spec)
{{int rv_aux = nil; {int var = nil; int var_def = nil; int update_expr = nil; int while_expr = nil; var= _SH[iter_spec];
 if (F556(var) != nil)
{{ var_def= var;
 var= F283(var);
}
}
else {var_def= var;}
 {int from = F78(_SH[(iter_spec + 1)], 66469); int to = F78(_SH[(iter_spec + 1)], 66509); int downto = F78(_SH[(iter_spec + 1)], 66510); int below = F78(_SH[(iter_spec + 1)], 66511); int above = F78(_SH[(iter_spec + 1)], 66512); int by = F78(_SH[(iter_spec + 1)], 66513); if (!(by != nil))
{by= 1;}
 {int test_aux_var; {int or_result = nil; { {int and_result = nil; { and_result= to;
 if (and_result != nil)
{{ {int or_result = nil; { or_result= downto;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= below;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= above;
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}
}
}

}

 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { and_result= downto;
 if (and_result != nil)
{{ {int or_result = nil; { or_result= to;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= below;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= above;
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}
}
}

}

 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { and_result= below;
 if (and_result != nil)
{{ {int or_result = nil; { or_result= to;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= downto;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= above;
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}
}
}

}

 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { and_result= above;
 if (and_result != nil)
{{ {int or_result = nil; { or_result= to;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= downto;
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= below;
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}
}
}

}

 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}
}
}

}

 if (test_aux_var)
{{ F87(_SH[66435]);
 F87(_SH[65559]);
 F373(nil);
}
}
else {if (to != nil)
{{ update_expr= _c(66484, _c(var, _c(by, nil)));
 while_expr= _c(66428, _c(var, _c(to, nil)));
}
}
else {if (below != nil)
{{ update_expr= _c(66484, _c(var, _c(by, nil)));
 while_expr= _c(66431, _c(var, _c(below, nil)));
}
}
else {if (above != nil)
{{ update_expr= _c(66514, _c(var, _c(by, nil)));
 while_expr= _c(66505, _c(var, _c(above, nil)));
}
}
else {if (downto != nil)
{{ update_expr= _c(66514, _c(var, _c(by, nil)));
 while_expr= _c(66429, _c(var, _c(downto, nil)));
}
}
else {update_expr= _c(66484, _c(var, _c(by, nil)));}}}}}
}

 rv_aux= _c(66481, _c(_c(66476, _c(_c(var_def, nil), nil)), _c(_c(66477, _c(_c(66409, _c(var, _c(from, nil))), nil)), _c(_c(66461, _c(while_expr, nil)), _c(_c(66479, _c(update_expr, nil)), nil)))));
}

}

 return rv_aux;
}
}

int F607(int list, int test_fn, int include_empty_itemsP)
{{int rv_aux = nil; {int size = nil; int cur_csym = nil; int cur_item = nil; int items = nil; while (list != nil){ size= F483(test_fn, list);
 if (size > 0)
{{ {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 list= F779(list, size);
 cur_item= nil;
}
}
else {{ { cur_csym= _SH[list];
 list= _SH[(list + 1)];
}

 cur_item= _c(cur_csym, cur_item);
}
}
}

 {int test_aux_var; {int or_result = nil; { or_result= cur_item;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{items= _c(F574(cur_item), items);}
}

 rv_aux= F574(items);
}

 return rv_aux;
}
}

int F608(int stream)
{return F690(stream, _SH[65754], nil);}

int F609(int elements)
{{int rv_aux = nil; { F91(_SH[66838], _SH[66836], F735(_c(elements, _c(F75(), nil))));
 rv_aux= nil;
}

 return rv_aux;
}
}

int F610(int stream, int message)
{{int rv_aux = nil; {int sample = nil; F663(stream, -20);
 if (F237(stream) == 0)
{F663(stream, 1);}
 sample= F134(stream, 40);
 { F87(_SH[66435]);
 F87(_SH[65617]);
 F87(message);
 F87(_SH[66435]);
 F87(sample);
 F373(nil);
}

 { { F87(_SH[66435]);
 F87(_SH[66435]);
 F373(nil);
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F611(int stream, int condition, int out_stream, int inTOout_fn)
{return F680(stream, out_stream, condition, 66462, inTOout_fn);}

int F612(int list)
{return _SH[(_SH[(_SH[(list + 1)] + 1)] + 1)];}

int F613(int str, int expFn)
{{int rv_aux = nil; {int new_sym = nil; int prop_rec = nil; { new_sym= V42;
 V42= (V42 + 1);
 _SH[new_sym]= nil;
 if (str)
{{ _SH[(new_sym + C21)]= str;
 prop_rec= _c(66741, _c(_c(66743, _c(expFn, nil)), nil));
 _SH[(new_sym + C21 + C21)]= prop_rec;
 F437(V45, str, new_sym);
}
}
 return new_sym;
}

}

 return rv_aux;
}
}

int F614(int stream, int objects)
{{int rv_aux = nil; {int object1__1 = 66607; int object2__2; {int object1__3 = _c(_c(66608, _c(nil, nil)), nil); int object2__4; {int list__11; int sl__12; {int for_list_result = nil; for_list_result= nil;
 {int object = nil; int object_on = nil; { object_on= objects;
 object= _SH[object_on];
}

 while (object_on != nil){ {int object1__16; int object2__17 = for_list_result; if (F774(object, 66415) != nil)
{object1__16= _c(66415, _c(stream, F498(_SH[(object + 1)], nil)));}
else {if (F774(object, 66417) != nil)
{object1__16= _c(66609, _c(stream, _c(F262(34), _c(_c(66610, _c(F283(object), _c(stream, _c(0, _c(66436, _c(66437, _c(nil, nil))))))), _c(F262(34), nil)))));}
else {object1__16= _c(66610, _c(object, _c(stream, _c(0, _c(66436, _c(66437, nil))))));}}
 for_list_result= _c(object1__16, object2__17);
}

 { object_on= _SH[(object_on + 1)];
 object= _SH[object_on];
}

}

}

 list__11= F574(for_list_result);
}

 sl__12= nil;
 object2__4= F498(list__11, sl__12);
}

 object2__2= _c(object1__3, object2__4);
}

 rv_aux= _c(object1__1, object2__2);
}

 return rv_aux;
}
}

int F615(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, nil)))))))))));}

int F616(int tag, int stream)
{{int rv_aux = nil; {int F757 = nil; int cur_tag_name = nil; { while (!(F757 != nil)){ F200(stream, 60);
 if (!(F243(stream, nil)))
{F757= 66423;}
else {if (F115(stream) != nil)
{F238(stream);}
else {if (F466(stream) != nil)
{{ cur_tag_name= F583(F51(stream, nil));
 if (cur_tag_name == tag)
{F757= 66407;}
 F31(stream);
 F366(stream, 62);
}
}
else {{ F51(stream, nil);
 F31(stream);
 F366(stream, 62);
}
}}}
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int64_t F617(int lp_file)
{return F584(lp_file);}

int F618(int csym)
{{int rv_aux = nil; {int csym_code; csym_code= F773(csym);
 {int or_result = nil; { {int and_result = nil; { if (csym_code >= F773(65))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(90))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= F773(97))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(122))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= 192)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 255)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code != 215)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code != 247)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}
}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

}

 return rv_aux;
}
}

int F619(int lp)
{return F757(lp, 47, 66443);}

int F620(int sock_index)
{{int rv_aux = nil; {int flag = nil; flag= 0;
 if (V62[sock_index] > 0)
{{ flag= write(V57[sock_index], V61[sock_index], V62[sock_index]);
 V62[sock_index]= 0;
}
}
 rv_aux= flag;
}

 return rv_aux;
}
}

int F621(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int test_aux_var; {int and_result = nil; { if (csym_code >= F773(48))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(57))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= (csym_code - F773(48));}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

void F622()
{{ V4= 0;
 V34= 0;
 V36= 0;
 {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(0);
 if (STREAM__BUF_935 == -1)
{_SH[66464]= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= nil;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= 66587;
 V13[STREAM__BUF_935]= 66588;
 V14[STREAM__BUF_935]= nil;
}

 _SH[66464]= STREAM__BUF_935;
}
}
}

 {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(0);
 if (STREAM__BUF_935 == -1)
{_SH[66416]= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= nil;
 V11[STREAM__BUF_935]= 66589;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= 66590;
}

 _SH[66416]= STREAM__BUF_935;
}
}
}

 {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(0);
 if (STREAM__BUF_935 == -1)
{_SH[66591]= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= nil;
 V11[STREAM__BUF_935]= 66592;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= 66593;
}

 _SH[66591]= STREAM__BUF_935;
}
}
}

}
}

int F623(int64_t F60, int buf_size, int buf_index, int buf_start)
{{int64_t delta = buf_size; delta= (delta * buf_index);
 F60= (F60 - delta - 1);
 F60= (F60 + buf_start);
 if (F60 >= C1)
{return (C1 - 2);}
else {if (F60 < 0)
{return 0;}
else {return (int)F60;}}
}
}

int F624(int stream, int condition, int inTOout_fn)
{return F57(stream, condition, 66462, inTOout_fn);}

int F625(int stream, int buf_index)
{{int rv_aux = nil; {int buf_start = nil; int nbytes = nil; int64_t stream_start_pos; {int buf_size = V6[stream]; int fill_fn = V10[stream]; buf_start= F323(stream, buf_index);
 F489(stream, buf_start);
 stream_start_pos= F41(buf_size, buf_index);
 V37= stream_start_pos;
 nbytes= F19(F462(fill_fn, stream, buf_start, buf_size));
}

 F739(stream, buf_index, nbytes);
 F347(stream, buf_index, buf_start, nbytes);
 rv_aux= nbytes;
}

 return rv_aux;
}
}

int F626(int spec, int spec_rec)
{{int rv_aux = nil; {int f_38v = nil; { _SH[spec];
 spec= _SH[(spec + 1)];
}

 { while (spec != nil){ { f_38v= _SH[spec];
 spec= _SH[(spec + 1)];
}

 {int field = nil; int value = nil; int aux = nil; aux= f_38v;
 field= _SH[aux];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 if (field == 66476)
{F158(spec_rec, field, F755(_c(F186(value), _c(F750(spec_rec, field), nil))));}
else {if (field == 66430)
{F158(spec_rec, field, value);}
else {if (value != nil)
{F158(spec_rec, field, _c(value, F750(spec_rec, field)));}}}
}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F627()
{return _c(F506(), F574(F75()));}

int F628(int stream, int instr)
{{int rv_aux = nil; if (_SH[instr] == 66405)
{rv_aux= F476(stream, instr);}
else {if (_SH[instr] == 66478)
{rv_aux= _c(66637, _c(stream, F498(_SH[(instr + 1)], nil)));}
else {if (_SH[instr] == 66430)
{rv_aux= _c(66638, _c(stream, _c(F283(instr), F498(F7(instr), nil))));}
else {if (_SH[instr] == 66460)
{rv_aux= _c(66639, _c(stream, _c(F283(instr), F498(F7(instr), nil))));}
else {if (_SH[instr] == 66420)
{rv_aux= _c(66640, _c(stream, F498(_SH[(instr + 1)], nil)));}
else {if (_SH[instr] == 66641)
{rv_aux= _c(66642, _c(stream, F498(_SH[(instr + 1)], nil)));}
else {if (_SH[instr] == 66461)
{rv_aux= _c(66643, _c(stream, _c(F283(instr), F498(F7(instr), nil))));}
else {if (_SH[instr] == 66462)
{rv_aux= _c(66644, _c(stream, _c(F283(instr), F498(F7(instr), nil))));}
else {if (_SH[instr] == 66463)
{rv_aux= _c(66645, _c(stream, _c(F283(instr), F498(F7(instr), nil))));}
else {rv_aux= F168(stream, instr);}}}}}}}}}
 return rv_aux;
}
}

int F629()
{return _c(C10, nil);}

int F630(int csym, int tnode)
{{int rv_aux = nil; {int found = nil; {int child = nil; int child_on = nil; { child_on= _SH[(tnode + 1)];
 child= _SH[child_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(child_on, nil);
 if (and_result != nil)
{{ if (!(found != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (_SH[child] == F773(csym))
{found= child;}
else {if (_SH[child] > F773(csym))
{found= 66423;}}
 { child_on= _SH[(child_on + 1)];
 child= _SH[child_on];
}

 {int and_result = nil; { and_result= F678(child_on, nil);
 if (and_result != nil)
{{ if (!(found != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 if (found == 66423)
{rv_aux= nil;}
else {rv_aux= found;}
}

 return rv_aux;
}
}

int F631(int stream, int condition)
{return F602(stream, condition, 66710, 66407, nil);}

int F632(int tnode)
{{int rv_aux = nil; {int tleaf = nil; tleaf= _SH[_SH[(tnode + 1)]];
 {int test_aux_var; {int and_result = nil; { and_result= tleaf;
 if (and_result != nil)
{{ if (_SH[tleaf] == C11)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{rv_aux= tleaf;}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F633(int regex, int start_node, int end_node)
{{int rv_aux = nil; {int node = nil; node= _c(66623, _c(_c(66567, _c(66616, nil)), nil));
 F203(F283(regex), node, node);
 F646(node, start_node);
 rv_aux= F646(end_node, node);
}

 return rv_aux;
}
}

int F634(int stream, int condition, int out_stream, int inTOout_fn)
{return F680(stream, out_stream, condition, 66461, inTOout_fn);}

int F635(int stream, int unmatched_tags_as_singleP, int tag_case, int print_errorsP, int trim_spaceP, int read_attlistP, int stream_skip_commentP)
{{int rv_aux = nil; {int F757 = nil; F81(tag_case);
 {int unwind_value = nil; { while (!(F757 != nil)){ if (trim_spaceP != nil)
{F361(stream, 66726);}
 if (!(F243(stream, nil)))
{F757= 66407;}
else {if (F314(stream) != nil)
{F743(stream);}
else {if (F115(stream) != nil)
{if (stream_skip_commentP != nil)
{F238(stream);}
else {F217(stream);}}
else {if (F707(stream) != nil)
{F686(stream);}
else {if (F466(stream) != nil)
{F17(stream, unmatched_tags_as_singleP, print_errorsP);}
else {if (F608(stream) != nil)
{F175(stream, read_attlistP);}
else {F113(stream, trim_spaceP);}}}}}}
}

 unwind_value= nil;
}

 _SH[66830]= nil;
}

 rv_aux= F354(unmatched_tags_as_singleP, print_errorsP);
}

 return rv_aux;
}
}

int F636(int iter_spec)
{{int rv_aux = nil; {int var = nil; int expansion = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 {int in_widthA = F78(iter_spec, 66468); int exp_by = F78(iter_spec, 66487); int exp_while = F78(iter_spec, 66488); expansion= F93(var, exp_by, exp_while);
 {int object1__12 = 66481; int object2__13; {int object1__14 = _c(66476, _c(_c(var, _c(_c(66489, _c(66490, nil)), _c(_c(66489, _c(66491, nil)), _c(_c(66489, _c(66499, nil)), _c(_c(66489, _c(66500, nil)), _c(_c(66489, _c(66501, nil)), _c(_c(66489, _c(66502, nil)), _c(_c(66489, _c(F80(var), _c(0, nil))), nil)))))))), nil)); int object2__15; {int object1__67 = _c(66477, _c(_c(66405, _c(_c(66409, _c(66490, _c(_c(66492, nil), nil))), _c(_c(66409, _c(66491, _c(66490, nil))), _c(_c(66409, _c(66499, _c(66490, nil))), _c(_c(66409, _c(66500, _c(_c(66432, _c(66490, _c(66503, nil))), nil))), _c(_c(66409, _c(66501, _c(_c(66493, _c(66500, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(in_widthA, nil))), nil))))))), nil)); int object2__68; {int object1__137 = _c(66461, _c(_c(66494, _c(_c(66485, _c(var, _c(nil, nil))), _c(_c(66494, _c(_c(66428, _c(66491, _c(66499, nil))), _c(_c(66429, _c(66501, _c(66500, nil))), nil))), nil))), nil)); int object2__138; {int object1__173; int object2__174; {int object1__175 = 66479; int object2__176; {int object1__177; int object2__178; {int object1__179 = 66405; int object2__180; {int list__181; int sl__182; if (expansion != nil)
{list__181= _c(expansion, nil);}
else {list__181= nil;}
 sl__182= _c(_c(66507, _c(var, _c(66491, _c(66501, nil)))), _c(_c(66420, _c(_c(_c(66505, _c(66491, _c(66499, nil))), _c(_c(66420, _c(_c(_c(66429, _c(66501, _c(66500, nil))), _c(_c(66484, _c(F80(var), nil)), _c(_c(66409, _c(66502, _c(66490, nil))), _c(_c(66409, _c(66490, _c(66500, nil))), _c(_c(66409, _c(66499, _c(66501, nil))), _c(_c(66409, _c(66491, _c(66490, nil))), _c(_c(66409, _c(66500, _c(66502, nil))), _c(_c(66409, _c(66501, _c(_c(66493, _c(66500, _c(1, nil))), nil))), _c(_c(66409, _c(var, _c(_c(66508, _c(66491, nil)), nil))), nil))))))))), _c(_c(66407, _c(_c(66409, _c(var, _c(nil, nil))), nil)), nil))), nil)), _c(_c(66407, _c(_c(66409, _c(var, _c(_c(66508, _c(66491, nil)), nil))), nil)), nil))), nil));
 object2__180= F498(list__181, sl__182);
}

 object1__177= _c(object1__179, object2__180);
}

 object2__178= nil;
 object2__176= _c(object1__177, object2__178);
}

 object1__173= _c(object1__175, object2__176);
}

 object2__174= _c(_c(66480, _c(_c(66497, nil), nil)), nil);
 object2__138= _c(object1__173, object2__174);
}

 object2__68= _c(object1__137, object2__138);
}

 object2__15= _c(object1__67, object2__68);
}

 object2__13= _c(object1__14, object2__15);
}

 rv_aux= _c(object1__12, object2__13);
}

}

}

 return rv_aux;
}
}

int F637(int F339, int full_pathP, int levels)
{{int rv_aux = nil; if (levels != nil)
{rv_aux= F706(F339, levels, full_pathP);}
else {rv_aux= F465(F339, full_pathP);}
 return rv_aux;
}
}

int F638(int lp)
{{int rv_aux = nil; if (!(F718(lp, 47, 66443) != nil))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F639(int stream, int re_condition)
{{int rv_aux = nil; {int norm_regex = nil; int regex_node = nil; int start_pos = F237(stream); int delta = nil; norm_regex= F571(F283(re_condition));
 regex_node= F531(norm_regex);
 _SH[66613]= F216(regex_node, stream);
 delta= (F237(stream) - start_pos);
 F102(stream, start_pos);
 if (_SH[66613] != nil)
{rv_aux= delta;}
else {rv_aux= -1;}
}

 return rv_aux;
}
}

int F640(int stream, int condition)
{return F602(stream, condition, 66708, 66407, nil);}

int F641(int rec, int stream, int F443)
{{int rv_aux = nil; {int value = nil; int conversion_fn = nil; {int separator = nil; int terminator = nil; int field_names = nil; int field_write_conversion_fns = nil; int aux_rec = nil; aux_rec= F443;
 separator= F750(aux_rec, 66798);
 terminator= F750(aux_rec, 66799);
 field_names= F750(aux_rec, 66802);
 field_write_conversion_fns= F750(aux_rec, 66805);
 {int field_name = nil; int field_name_on = nil; { field_name_on= field_names;
 field_name= _SH[field_name_on];
}

 { while (field_name_on != nil){ { conversion_fn= _SH[field_write_conversion_fns];
 field_write_conversion_fns= _SH[(field_write_conversion_fns + 1)];
}

 value= F750(rec, field_name);
 F144(value, stream, conversion_fn, nil);
 if (_SH[(field_name_on + 1)] != nil)
{F334(terminator, stream);}
else {F334(separator, stream);}
 { field_name_on= _SH[(field_name_on + 1)];
 field_name= _SH[field_name_on];
}

}

 rv_aux= nil;
}

}

}

}

 return rv_aux;
}
}

int F642(int stream)
{{int rv_aux = nil; {int comment = nil; {int csym = nil; int end_of_lineP = nil; while (!(end_of_lineP != nil)){ csym= F299(stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(csym, -1);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F658(csym);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{end_of_lineP= 66407;}
else {comment= _c(csym, comment);}
}

}

}

 rv_aux= _c(66605, _c(F574(comment), nil));
}

 return rv_aux;
}
}

int F643(int lines, int lang)
{{int rv_aux = nil; {int agree_terms = nil; int term_trie = nil; int max_counter = 0; int max_line = nil; int counter = nil; if (lang == 66792)
{agree_terms= _SH[66849];}
else {if (lang == 66793)
{agree_terms= _SH[66850];}}
 {int line = nil; int line_on = nil; { line_on= lines;
 line= _SH[line_on];
}

 while (line_on != nil){ counter= 0;
 {int stream = nil; int stream_rv = nil; {int unwind_value = nil; { {int STREAM__BUF_935 = nil; STREAM__BUF_935= F723(nil);
 if (STREAM__BUF_935 == -1)
{stream= -1;}
else {{ { V7[STREAM__BUF_935]= nil;
 V10[STREAM__BUF_935]= 66611;
 V11[STREAM__BUF_935]= nil;
 V12[STREAM__BUF_935]= nil;
 V13[STREAM__BUF_935]= nil;
 V14[STREAM__BUF_935]= nil;
}

 {int error = nil; {int str_buf = nil; int end; int mode = nil; {int test_aux_var; {int and_result = nil; { and_result= F256(mode, 66585);
 if (and_result != nil)
{{ if (!(line != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{mode= 66583;}
}

 if (!(mode == 66583))
{str_buf= line;}
 end= F510(str_buf);
 { V8[STREAM__BUF_935]= _c(0, str_buf);
 V17[STREAM__BUF_935]= end;
}

 if (mode == 66585)
{F102(STREAM__BUF_935, (end + 1));}
 error= nil;
}

 _SH[66422]= error;
 if (_SH[66422] != nil)
{{ F405(STREAM__BUF_935);
 STREAM__BUF_935= -1;
}
}
}

 stream= STREAM__BUF_935;
}
}
}

 unwind_value= stream;
}

 if (stream == -1)
;
else {{ { term_trie= F249(stream, 66847, 66848, nil);
 {int term = nil; int term_on = nil; { term_on= agree_terms;
 term= _SH[term_on];
}

 { while (term_on != nil){ if (F280(term_trie, term) != nil)
{counter= (counter + 1);}
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

 stream_rv= nil;
}

}

}

 if (!(stream == -1))
{{ F143(stream);
 {int test_aux_var; {int or_result = nil; { or_result= F256(nil, 66583);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F256(nil, 66585);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ line= _SH[(V8[stream] + 1)];
 F417(V8[stream]);
}
}
}

 F405(stream);
}
}
}
}
}

}

 {int test_aux_var; {int or_result = nil; { if (max_counter == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { if (counter > max_counter)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F510(line) < 1024)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ {int and_result = nil; { if (counter == max_counter)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (F510(line) < F510(max_line))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}

}

 if (test_aux_var)
{{ max_counter= counter;
 max_line= line;
}
}
}

 { line_on= _SH[(line_on + 1)];
 line= _SH[line_on];
}

}

}

 if (max_counter > 0)
{rv_aux= max_line;}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F644(int pnode, int var_name)
{{int rv_aux = nil; {int value = nil; {int test_aux_var; {int _ARG1__1; {int or_result = nil; { if (!(pnode != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F256(F750(pnode, 66567), 66629);
 if (and_result != nil)
{{ and_result= F256(F750(pnode, 66628), var_name);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {_ARG1__1= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__1);
}

 while (test_aux_var){ pnode= F750(pnode, 66635);
 {int _ARG1__1; {int or_result = nil; { if (!(pnode != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F256(F750(pnode, 66567), 66629);
 if (and_result != nil)
{{ and_result= F256(F750(pnode, 66628), var_name);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__1= (or_result != nil);}
else {_ARG1__1= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__1);
}

}

}

 if (pnode != nil)
{{int test_aux_var; {int _ARG1__13; {int or_result = nil; { if (!(pnode != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F256(F750(pnode, 66567), 66627);
 if (and_result != nil)
{{ and_result= F256(F750(pnode, 66628), var_name);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {_ARG1__13= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__13);
}

 while (test_aux_var){ if (F750(pnode, 66634) != nil)
{value= _c(F750(pnode, 66634), value);}
 pnode= F750(pnode, 66635);
 {int _ARG1__13; {int or_result = nil; { if (!(pnode != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {{ {int and_result = nil; { and_result= F256(F750(pnode, 66567), 66627);
 if (and_result != nil)
{{ and_result= F256(F750(pnode, 66628), var_name);
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {_ARG1__13= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__13);
}

}

}
}
 rv_aux= _c(value, _c(pnode, nil));
}

 return rv_aux;
}
}

int F645(int size)
{{int rv_aux = nil; {int stack_vect = nil; stack_vect= F397(size + 1);
 F91(stack_vect, 1, 2);
 rv_aux= stack_vect;
}

 return rv_aux;
}
}

int F646(int linked_node, int node)
{return F158(node, 66626, _c(linked_node, F750(node, 66626)));}

int F647(int lines)
{{int rv_aux = nil; {int line = nil; int pruned_lines = nil; while (lines != nil){ { line= _SH[lines];
 lines= _SH[(lines + 1)];
}

 if (F718(line, _SH[65766], 66733) != nil)
{pruned_lines= _c(line, pruned_lines);}
else {if (_SH[pruned_lines] != nil)
{pruned_lines= _c(nil, pruned_lines);}}
}

 if (_SH[pruned_lines] == nil)
{{ _SH[pruned_lines];
 pruned_lines= _SH[(pruned_lines + 1)];
}
}
 rv_aux= F574(pruned_lines);
}

 return rv_aux;
}
}

int64_t F648(int stream)
{{int64_t rv_aux; {int64_t F424; {int buf0_start = V25[stream]; int buf1_start = V29[stream]; int buf_cur = V22[stream]; int buf_index = V20[stream]; int buf_size = V6[stream]; if (F771(buf_index) != nil)
{F424= buf0_start;}
else {F424= buf1_start;}
 F424= (buf_cur - F424);
 return (F424 + (buf_index * buf_size) + 1);
}

}

 return rv_aux;
}
}

int F649(int iter_spec)
{{int rv_aux = nil; {int var = nil; int stream = nil; int start = nil; int end = nil; int aux = nil; aux= iter_spec;
 var= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 stream= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 start= _SH[aux];
 aux= _SH[(aux + 1)];
 aux= _SH[(aux + 1)];
 end= _SH[aux];
 {int object1__11 = 66481; int object2__12; {int object1__13 = _c(66476, _c(_c(var, _c(66518, nil)), nil)); int object2__14; {int object1__23; int object2__24; {int object1__25 = 66477; int object2__26; {int object1__27; int object2__28; {int object1__29 = 66405; int object2__30; {int list__31; int sl__32; if (start != nil)
{list__31= _c(_c(66519, _c(stream, _c(start, nil))), nil);}
else {list__31= nil;}
 {int object1__41; int object2__42; if (end != nil)
{object1__41= _c(66409, _c(66518, _c(end, nil)));}
else {object1__41= _c(66409, _c(66518, _c(66520, nil)));}
 object2__42= nil;
 sl__32= _c(object1__41, object2__42);
}

 object2__30= F498(list__31, sl__32);
}

 object1__27= _c(object1__29, object2__30);
}

 object2__28= nil;
 object2__26= _c(object1__27, object2__28);
}

 object1__23= _c(object1__25, object2__26);
}

 object2__24= _c(_c(66461, _c(_c(66478, _c(_c(66521, _c(stream, nil)), _c(_c(66428, _c(_c(66522, _c(stream, nil)), _c(66518, nil))), _c(_c(66494, _c(_c(66409, _c(var, _c(_c(66523, _c(stream, nil)), nil))), _c(_c(66413, _c(66407, nil)), nil))), nil)))), nil)), _c(_c(66479, _c(nil, nil)), nil));
 object2__14= _c(object1__23, object2__24);
}

 object2__12= _c(object1__13, object2__14);
}

 rv_aux= _c(object1__11, object2__12);
}

}

 return rv_aux;
}
}

int F650(int fields, int aux_rec)
{{int rv_aux = nil; {int field = nil; int on_var = nil; int list_var = nil; on_var= fields;
 while (on_var != nil){ field= _SH[on_var];
 {int object1__2; int object2__3 = list_var; if (F556(field) != nil)
{object1__2= _c(66457, _c(aux_rec, _c(_SH[field], _c(F283(field), nil))));}
else {object1__2= _c(66457, _c(aux_rec, _c(field, _c(field, nil))));}
 list_var= _c(object1__2, object2__3);
}

 on_var= _SH[(on_var + 1)];
}

 rv_aux= F574(list_var);
}

 return rv_aux;
}
}

int F651(int F339, int full_pathP)
{{int rv_aux = nil; {int subdir_list = nil; int subdir = nil; int new_subdir_list = nil; subdir_list= F477(F339);
 while (subdir_list != nil){ { subdir= _SH[subdir_list];
 subdir_list= _SH[(subdir_list + 1)];
}

 if (full_pathP != nil)
{new_subdir_list= _c(F735(_c(F289(F339), _c(subdir, _c(_c(47, nil), nil)))), new_subdir_list);}
else {new_subdir_list= _c(F735(_c(F289(subdir), _c(_c(47, nil), nil))), new_subdir_list);}
}

 subdir_list= F574(new_subdir_list);
 rv_aux= subdir_list;
}

 return rv_aux;
}
}

int F652(int csym)
{{int rv_aux = nil; {int csym_code = nil; csym_code= F773(csym);
 {int and_result = nil; { if (F44(csym_code))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code >= 32)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= 126)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

}

 return rv_aux;
}
}

int F653(int csym)
{{int rv_aux = nil; {int and_result = nil; { if (!(F4(csym) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(F586(csym) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F654(int pred, int list, int args)
{{int rv_aux = nil; {int buf = list; int last = nil; if (list != nil)
{{ args= _c(nil, args);
 while (list != nil){ _SH[args]= _SH[list];
 if (!(F717(pred, args) != nil))
{last= list;}
 { _SH[list];
 list= _SH[(list + 1)];
}

}

 F417(args);
 if (last != nil)
{{ _SH[(last + 1)]= nil;
 rv_aux= buf;
}
}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

 return rv_aux;
}
}

int F655(int rec_type)
{return F394(_SH[(F292(rec_type) + 1)], 66407);}

int F656(int stream, int buf_index)
{{int rv_aux = nil; {int cur_buf_index = nil; int nbytes = nil; int eosP = nil; {int64_t serial_pos = V19[stream]; int buf_size = V6[stream]; cur_buf_index= F567(serial_pos, buf_size);
 if (buf_index < cur_buf_index)
{rv_aux= nil;}
else {{ {int test_aux_var; {int and_result = nil; { if (cur_buf_index <= buf_index)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(eosP != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ nbytes= F625(stream, cur_buf_index);
 serial_pos= (serial_pos + nbytes);
 cur_buf_index= (cur_buf_index + 1);
 V19[stream]= serial_pos;
 if (!(nbytes == buf_size))
{eosP= 66407;}
 {int and_result = nil; { if (cur_buf_index <= buf_index)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(eosP != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 if (nbytes > 0)
{rv_aux= 66407;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F657(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, nil))))))))))))));}

int F658(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(csym, 13);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 10);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 12);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F659(int csym, int stream)
{{int rv_aux = nil; if (csym == 34)
{rv_aux= F235(stream, _SH[65708]);}
else {if (csym == 38)
{rv_aux= F235(stream, _SH[65709]);}
else {if (csym == 39)
{rv_aux= F235(stream, _SH[65710]);}
else {if (csym == 60)
{rv_aux= F235(stream, _SH[65711]);}
else {if (csym == 62)
{rv_aux= F235(stream, _SH[65712]);}
else {rv_aux= F673(csym, stream);}}}}}
 return rv_aux;
}
}

int F660(int stream)
{return V14[stream];}

int F661(int stream)
{{int rv_aux = nil; {int buf_index = nil; int flagP = nil; if (F365(stream) != nil)
{{ flagP= 66407;
 {int64_t F60 = V18[stream]; int buf_size = V6[stream]; int buf0_index = V26[stream]; int buf1_index = V30[stream]; buf_index= F567(F60, buf_size);
 if (buf_index == buf0_index)
{{int buf0_start = V25[stream]; int buf0_nbytes = V27[stream]; F347(stream, buf_index, buf0_start, buf0_nbytes);
}
}
else {if (buf_index == buf1_index)
{{int buf1_start = V29[stream]; int buf1_nbytes = V31[stream]; F347(stream, buf_index, buf1_start, buf1_nbytes);
}
}
else {if (F566(stream) != nil)
{flagP= F656(stream, buf_index);}
else {if (F625(stream, buf_index) == 0)
{flagP= nil;}}}}
}

}
}
 rv_aux= flagP;
}

 return rv_aux;
}
}

int F662(int os)
{{int rv_aux = nil; {int test_aux_var; {int or_result = nil; { or_result= F754(os, F50(117, 110, 105, 120));
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F754(os, F492(108, 105, 110, 117, 120));
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F754(os, F84(111, 115, 120));
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}
}
}

}

 if (test_aux_var)
{rv_aux= F262(10);}
else {if (F754(os, _SH[65575]) != nil)
{rv_aux= _SH[65576];}
else {{ { F87(_SH[66435]);
 F87(_SH[65577]);
 F87(os);
 F373(nil);
}

 rv_aux= nil;
}
}}
}

 return rv_aux;
}
}

int F663(int stream, int64_t delta)
{{int rv_aux = nil; {int64_t new_cur; int new_cur1 = nil; {int buf_cur = V22[stream]; int in_buf_max = V23[stream]; int out_buf_max = V24[stream]; int in_buf_min = V21[stream]; new_cur= (buf_cur + delta);
 {int test_aux_var; {int and_result = nil; { if (new_cur >= in_buf_min)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ {int or_result = nil; { if (new_cur <= in_buf_max)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{and_result= or_result;}
else {{ if (new_cur <= out_buf_max)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}

}

 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{ new_cur1= (int)new_cur;
 V22[stream]= new_cur1;
 V18[stream]= (F237(stream) + delta);
 rv_aux= 66407;
}
}
else {rv_aux= F102(stream, (F237(stream) + delta));}
}

}

}

 return rv_aux;
}
}

int F664(int pred, int list, int args)
{{int rv_aux = nil; {int el = nil; int res = nil; args= _c(nil, args);
 while (list != nil){ { el= _SH[list];
 list= _SH[(list + 1)];
}

 _SH[args]= el;
 if (F717(pred, args) != nil)
{{ res= el;
 list= nil;
}
}
}

 F417(args);
 rv_aux= res;
}

 return rv_aux;
}
}

int F665(int list, int prefix, int pred1)
{{int rv_aux = nil; {int flagP = nil; int csym1 = nil; int csym2 = nil; if (F556(list) != nil)
{if (F556(prefix) != nil)
{{ flagP= 66407;
 while (prefix != nil){ { csym1= _SH[prefix];
 prefix= _SH[(prefix + 1)];
}

 { csym2= _SH[list];
 list= _SH[(list + 1)];
}

 if (csym2 == nil)
{{ prefix= nil;
 flagP= nil;
}
}
else {if (pred1 == nil)
{if (!(csym1 == csym2))
{{ prefix= nil;
 flagP= nil;
}
}}
else {if (!(F325(pred1, csym1, csym2) != nil))
{{ prefix= nil;
 flagP= nil;
}
}}}
}

}
}
else {if (pred1 != nil)
{if (F325(pred1, _SH[list], prefix) != nil)
{flagP= 66407;}}
else {if (_SH[list] == prefix)
{flagP= 66407;}}}}
 rv_aux= flagP;
}

 return rv_aux;
}
}

int F666(int stream, int act_spec)
{{int rv_aux = nil; {int type = nil; int condition = nil; int str_var = nil; int inTOout_fn = nil; int aux = nil; aux= act_spec;
 type= _SH[aux];
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 aux= _SH[(aux + 1)];
 str_var= _SH[aux];
 aux= _SH[(aux + 1)];
 inTOout_fn= _SH[aux];
 if (type == 66678)
{rv_aux= F692(str_var, _c(66683, _c(stream, _c(condition, _c(inTOout_fn, nil)))));}
else {if (type == 66679)
{rv_aux= F692(str_var, _c(66684, _c(stream, _c(condition, _c(inTOout_fn, nil)))));}
else {if (type == 66680)
{rv_aux= F692(str_var, _c(66685, _c(stream, _c(condition, _c(inTOout_fn, nil)))));}
else {if (type == 66681)
{rv_aux= F692(str_var, _c(66686, _c(stream, _c(condition, _c(inTOout_fn, nil)))));}
else {if (type == 66682)
{rv_aux= F692(str_var, _c(66687, _c(stream, _c(condition, _c(inTOout_fn, nil)))));}
else {rv_aux= nil;}}}}}
}

 return rv_aux;
}
}

int F667(int stream)
{{int rv_aux = nil; if (V22[stream] >= V21[stream])
{{int value; {int F60 = V22[stream]; value= V33[F60];
}

 { V22[stream]= (V22[stream] - 1);
 V18[stream]= (V18[stream] - 1);
}

 rv_aux= value;
}
}
else {{int test_aux_var; {int and_result = nil; { and_result= F661(stream);
 if (and_result != nil)
{{ if (V22[stream] >= V21[stream])
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{int value; {int F60 = V22[stream]; value= V33[F60];
}

 { V22[stream]= (V22[stream] - 1);
 V18[stream]= (V18[stream] - 1);
}

 rv_aux= value;
}
}
else {rv_aux= nil;}
}
}
 return rv_aux;
}
}

int F668(int lp)
{{int rv_aux = nil; {int parts = nil; int part1 = nil; parts= F428(lp);
 if (!(_SH[parts] == nil))
{{ { part1= _SH[parts];
 parts= _SH[(parts + 1)];
}

 parts= F755(_c(F428(F370(part1)), _c(parts, nil)));
}
}
 parts= F96(parts);
 rv_aux= F722(parts);
}

 return rv_aux;
}
}

int F669(int stream, int condition)
{{int rv_aux = nil; { if (condition == 66715)
{F102(stream, F502(stream));}
else {F548(stream, condition, 66708);}
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F670(int stream, int separator, int terminator, int F443)
{{int rv_aux = nil; {int row = nil; row= F268(stream, separator, terminator, F443);
 {int name = nil; int field_names = nil; int aux_rec = nil; aux_rec= F443;
 name= F750(aux_rec, 66548);
 field_names= F750(aux_rec, 66802);
 {int object1__9 = name; int object2__10; {int for_list_result = nil; for_list_result= nil;
 {int field_name = nil; int field_name_on = nil; int el = nil; int el_on = nil; { field_name_on= field_names;
 field_name= _SH[field_name_on];
}

 { el_on= row;
 el= _SH[el_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(field_name_on, nil);
 if (and_result != nil)
{{ and_result= F678(el_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ for_list_result= _c(_c(field_name, _c(el, nil)), for_list_result);
 { field_name_on= _SH[(field_name_on + 1)];
 field_name= _SH[field_name_on];
}

 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 {int and_result = nil; { and_result= F678(field_name_on, nil);
 if (and_result != nil)
{{ and_result= F678(el_on, nil);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 object2__10= F574(for_list_result);
}

 rv_aux= _c(object1__9, object2__10);
}

}

}

 return rv_aux;
}
}

int F671(int term_recs, int traceP, int string)
{{int rv_aux = nil; {int term = nil; int counter = 0; int it_counter = 0; int en_counter = 0; {int term_rec = nil; int term_rec_on = nil; { term_rec_on= term_recs;
 term_rec= _SH[term_rec_on];
}

 while (term_rec_on != nil){ term= F750(term_rec, 66790);
 counter= (counter + F750(term_rec, 66791));
 if (F232(term) != nil)
{it_counter= (it_counter + 1);}
 if (F13(term) != nil)
{en_counter= (en_counter + 1);}
 { term_rec_on= _SH[(term_rec_on + 1)];
 term_rec= _SH[term_rec_on];
}

}

}

 if (traceP != nil)
{F34(counter, it_counter, en_counter, string);}
 rv_aux= F310(it_counter, en_counter);
}

 return rv_aux;
}
}

int F672(int csym)
{{int rv_aux = nil; if (F166(csym) != nil)
{rv_aux= F335(F773(csym) + C6);}
else {rv_aux= csym;}
 return rv_aux;
}
}

int F673(int value, int stream)
{{int rv_aux = nil; if (F410(value, stream) != nil)
{rv_aux= value;}
else {if (F760(stream) != nil)
{rv_aux= F325(F660(stream), value, stream);}
else {if (F317(stream) != nil)
{{ F661(stream);
 F454(stream);
 F332(stream);
 rv_aux= F410(value, stream);
}
}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F674(int stream, int delta)
{{int rv_aux = nil; if (!(_SH[66596] != nil))
{rv_aux= F235(stream, _SH[66601]);}
else {{ F235(stream, _SH[66435]);
 rv_aux= F195(stream, delta, _SH[66601]);
}
}
 return rv_aux;
}
}

int F675(int list, int n)
{return F779(list, (F510(list) - n));}

int F676(int list)
{{int rv_aux = nil; { while (_SH[(list + 1)] != nil){ list= _SH[(list + 1)];
}

 rv_aux= list;
}

 return rv_aux;
}
}

int F677(int F443)
{{int rv_aux = nil; {int trspec_on = nil; F5(F443);
 {int for_thereis_result = nil; for_thereis_result= nil;
 {int trspec1 = nil; int trspec1_on = nil; { trspec1_on= F341(66795);
 trspec1= _SH[trspec1_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(trspec1_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F750(trspec1, 66548) == F750(F443, 66548))
{for_thereis_result= trspec1_on;}
else {for_thereis_result= nil;}
 { trspec1_on= _SH[(trspec1_on + 1)];
 trspec1= _SH[trspec1_on];
}

 {int and_result = nil; { and_result= F678(trspec1_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 trspec_on= for_thereis_result;
}

 if (trspec_on != nil)
{{ if (_SH[66796] != nil)
{{ F87(_SH[66435]);
 F87(_SH[65696]);
 F87(F292(F750(F443, 66548)));
 F87(_SH[65697]);
 F87(_SH[66435]);
 F373(nil);
}
}
 _SH[trspec_on]= F443;
}
}
else {F758(66795, _c(F443, F341(66795)));}
 rv_aux= nil;
}

 return rv_aux;
}
}

int F678(int x, int y)
{if (x != y)
{return C24;}
else {return nil;}}

int F679(int str)
{return F664(66734, _SH[66761], _c(str, nil));}

int F680(int in_stream, int out_stream, int condition, int condition_type, int inTOout_fn)
{{int rv_aux = nil; {int end_flag = nil; int delta = nil; if (F237(in_stream) < F502(in_stream))
{F102(in_stream, F502(in_stream));}
 while (!(end_flag != nil)){ delta= F524(in_stream, condition);
 if (condition_type == 66461)
{if (delta < 0)
{end_flag= 66407;}}
else {if (condition_type == 66462)
{if (delta >= 0)
{end_flag= 66407;}}
else {if (condition_type == 66709)
{if (delta >= 0)
{{ end_flag= 66407;
 F765(in_stream, delta, out_stream, inTOout_fn);
}
}}
else {if (condition_type == 66710)
{if (delta >= 0)
{{ end_flag= 66407;
 F765(in_stream, (delta - 1), out_stream, inTOout_fn);
}
}}}}}
 if (!(end_flag != nil))
{if (!(F243(in_stream, nil)))
{end_flag= 66423;}
else {if (inTOout_fn != nil)
{F325(inTOout_fn, in_stream, out_stream);}
else {F673(F299(in_stream), out_stream);}}}
}

 rv_aux= end_flag;
}

 return rv_aux;
}
}

int F681(int str)
{if (V42 < C21)
{return F135(F289(str));}
else {{ printf("\n***** EXIT: ---Heap sym Full---\n");
 fflush(stdout);
 exit(0);
}
}}

int F682(int x)
{{int rv_aux = nil; if (!(F245(x) != nil))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F683(int expr, int stream, int delta, int levels, int F510)
{{int rv_aux = nil; if (F510 == 0)
{rv_aux= F235(stream, _SH[65624]);}
else {if (!(_SH[66596] != nil))
{rv_aux= F536(expr, stream, delta, levels, F510);}
else {if (F1(expr, stream, delta, levels, F510) == -1)
{rv_aux= F536(expr, stream, delta, levels, F510);}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F684(int stream)
{{int rv_aux = nil; {int csym = nil; int entity = nil; csym= F396(stream, nil);
 {int test_aux_var; {int and_result = nil; { and_result= F256(csym, 38);
 if (and_result != nil)
{{ and_result= F509(stream, _SH[65713], 16);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{ entity= F480(stream, _SH[65714], nil);
 if (F754(entity, _SH[65715]) != nil)
{rv_aux= 34;}
else {if (F754(entity, _SH[65716]) != nil)
{rv_aux= 34;}
else {if (F754(entity, _SH[65717]) != nil)
{rv_aux= 34;}
else {if (F754(entity, _SH[65718]) != nil)
{rv_aux= 34;}
else {if (F754(entity, _SH[65719]) != nil)
{rv_aux= 38;}
else {if (F754(entity, _SH[65720]) != nil)
{rv_aux= 38;}
else {if (F754(entity, _SH[65721]) != nil)
{rv_aux= 38;}
else {if (F754(entity, _SH[65722]) != nil)
{rv_aux= 38;}
else {if (F754(entity, _SH[65723]) != nil)
{rv_aux= 39;}
else {if (F754(entity, _SH[65724]) != nil)
{rv_aux= 39;}
else {if (F754(entity, _SH[65725]) != nil)
{rv_aux= 39;}
else {if (F754(entity, _SH[65726]) != nil)
{rv_aux= 39;}
else {if (F754(entity, _SH[65727]) != nil)
{rv_aux= 60;}
else {if (F754(entity, _SH[65728]) != nil)
{rv_aux= 60;}
else {if (F754(entity, _SH[65729]) != nil)
{rv_aux= 60;}
else {if (F754(entity, _SH[65730]) != nil)
{rv_aux= 60;}
else {if (F754(entity, _SH[65731]) != nil)
{rv_aux= 62;}
else {if (F754(entity, _SH[65732]) != nil)
{rv_aux= 62;}
else {if (F754(entity, _SH[65733]) != nil)
{rv_aux= 62;}
else {if (F754(entity, _SH[65734]) != nil)
{rv_aux= 62;}
else {{ F663(stream, (1 - F510(entity)));
 rv_aux= 38;
}
}}}}}}}}}}}}}}}}}}}}
}
}
else {{ F663(stream, 1);
 rv_aux= csym;
}
}
}

}

 return rv_aux;
}
}

int F685(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F243(aux_stream, nil)))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

 while (test_aux_var){ csym= F518(aux_stream);
 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__3; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {{ if (!(F243(aux_stream, nil)))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__3= (or_result != nil);}
else {_ARG1__3= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__3);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F686(int stream)
{{int rv_aux = nil; {int pi_target = nil; int pi_icontent = nil; { F554(stream, 2);
 pi_target= F624(stream, _SH[66833], nil);
 F361(stream, 66834);
 pi_icontent= F624(stream, 62, nil);
 F554(stream, 1);
}

 rv_aux= F421(_c(66835, _c(pi_target, _c(pi_icontent, nil))));
}

 return rv_aux;
}
}

int F687(int stream_index)
{return F403(stream_index);}

int F688(int spec_name)
{return F394(F755(_c(_SH[65561], _c(F292(spec_name), _c(_SH[65562], nil)))), 66407);}

int F689(int expr, int stream, int delta, int levels, int F510)
{{int rv_aux = nil; { levels= (levels - 1);
 delta= (delta + 1);
 { F235(stream, _SH[65629]);
 F235(stream, F291(_SH[expr], stream, delta, levels, F510));
}

 if (F123(_SH[expr]) != nil)
{delta= (delta + _SH[66600]);}
 {int el = nil; int el_on = nil; int index = nil; { el_on= _SH[(expr + 1)];
 el= _SH[el_on];
}

 index= 2;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (index <= (F510 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ F674(stream, delta);
 if (index == (F510 + 1))
{F235(stream, _SH[65630]);}
else {F291(el, stream, delta, levels, F510);}
 { el_on= _SH[(el_on + 1)];
 el= _SH[el_on];
}

 index= (index + 1);
 {int and_result = nil; { and_result= F678(el_on, nil);
 if (and_result != nil)
{{ if (index <= (F510 + 1))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= F235(stream, _SH[65631]);
}

 return rv_aux;
}
}

int F690(int stream, int condition, int delta)
{return F501(stream, condition, delta, 66407);}

int F691(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16, int a17, int a18, int a19, int a20, int a21, int a22)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, _c(a10, _c(a11, _c(a12, _c(a13, _c(a14, _c(a15, _c(a16, _c(a17, _c(a18, _c(a19, _c(a20, _c(a21, _c(a22, nil))))))))))))))))))))));}

int F692(int var, int value)
{{int rv_aux = nil; if (F245(var) != nil)
{rv_aux= value;}
else {rv_aux= _c(66409, _c(var, _c(value, nil)));}
 return rv_aux;
}
}

int F693(int stream)
{{int rv_aux = nil; {int F757 = nil; int csym = nil; int atom_name = nil; while (!(F757 != nil)){ csym= F396(stream, nil);
 if (csym == -1)
{F757= 66407;}
else {if (csym == 46)
{{ F663(stream, 1);
 atom_name= _c(csym, atom_name);
}
}
else {if (F132(csym) != nil)
{F757= 66407;}
else {{ atom_name= _c(csym, atom_name);
 F663(stream, 1);
}
}}}
}

 atom_name= F574(atom_name);
 if (F343(atom_name, _SH[65615], 66443) != nil)
{rv_aux= nil;}
else {if (F343(atom_name, _SH[65616], 66443) != nil)
{rv_aux= nil;}
else {if (_SH[66594] != nil)
{rv_aux= F483(_SH[66594], atom_name);}
else {if (F286(atom_name, 10) != nil)
{rv_aux= F179(atom_name, 10);}
else {if (F146(atom_name, 10) != nil)
{rv_aux= F508(atom_name);}
else {rv_aux= F394(atom_name, 66407);}}}}}
}

 return rv_aux;
}
}

int F694(int list, int n)
{{int rv_aux = nil; if (n != nil)
{rv_aux= F532(list, 1, (- (n + 1)));}
else {rv_aux= F532(list, 1, -2);}
 return rv_aux;
}
}

int F695(int lp_old, int lp_new)
{return rename(F555(lp_old, nil), F555(lp_new, C24));}

int F696(int stream)
{{int rv_aux = nil; {int or_result = nil; { if (F237(stream) == F500(stream))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {{ if (F237(stream) == F418(stream))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F658(F396(stream, 1));
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F697(int int1)
{return F137(_SH[65547], (int1 + 1));}

int64_t F698(int stream_index)
{return ftell(F579(stream_index));}

int F699(int iter_spec)
{{int rv_aux = nil; {int condition = nil; int aux = nil; aux= iter_spec;
 aux= _SH[(aux + 1)];
 condition= _SH[aux];
 rv_aux= _c(66481, _c(_c(66461, _c(_c(66482, _c(condition, nil)), nil)), nil));
}

 return rv_aux;
}
}

int F700(int rec_name)
{return F394(F755(_c(F292(rec_name), _c(_SH[65574], nil))), 66407);}

int F701(int csym)
{{int rv_aux = nil; {int csym_code; csym_code= F773(csym);
 {int or_result = nil; { {int and_result = nil; { if (csym_code >= F773(65))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(90))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {{ {int and_result = nil; { if (csym_code >= F773(97))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(122))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

}

 return rv_aux;
}
}

int F702(int host, int port)
{{int rv_aux; {int sock; int flag = nil; int index; struct hostent* host_entry; struct sockaddr_in server; sock= socket(AF_INET, SOCK_STREAM, 0);
 if (sock < 0)
{rv_aux= nil;}
else {{ host_entry= gethostbyname(F209(host));
 if (host_entry == NULL)
{rv_aux= nil;}
else {{ server.sin_family=AF_INET;
 server.sin_port=htons(port);
 memcpy(&server.sin_addr, (* host_entry).h_addr, (* host_entry).h_length);
 flag= connect(sock, (struct sockaddr *)&server, sizeof(server));
 if (flag < 0)
{rv_aux= nil;}
else {{ {int tvect_endP = 0; int tvect_index = 0; tvect_index= 0;
 while (!(tvect_endP == 1)){ if (V57[tvect_index] == -1)
{{ V57[tvect_index]= sock;
 tvect_endP= 1;
}
}
else {{ tvect_index= (tvect_index + 1);
 if (tvect_index == C30)
{{ tvect_index= -1;
 tvect_endP= 1;
}
}
}
}
}

 index= tvect_index;
}

 if (index >= 0)
{{ V59[index]= 0;
 V60[index]= 0;
 V62[index]= 0;
 fcntl(sock, F_SETFL, O_NONBLOCK);
 rv_aux= index;
}
}
else {rv_aux= nil;}
}
}
}
}
}
}
}

 return rv_aux;
}
}

int F703(int filename, int open_flag)
{{int rv_aux; {FILE* stream; int index = -1; int lf_sym = 10; if (filename == nil)
{stream= tmpfile();}
else {if (F736(open_flag) != nil)
{stream= fopen(F555(filename, nil), "r");}
else {if (F409(open_flag) != nil)
{stream= fopen(F555(filename, nil), "w+");}
else {if (F141(open_flag) != nil)
{stream= fopen(F555(filename, nil), "r+");}
else {stream= NULL;}}}}
 if (stream == NULL)
{{ stdoutTyo(lf_sym, nil);
 printf("FILE %s", F555(filename, nil));
 printf("= NULL");
 fflush(stdout);
}
}
else {{int tvect_endP = 0; int tvect_index = 0; tvect_index= 0;
 while (!(tvect_endP == 1)){ if (V56[tvect_index] == NULL)
{{ V56[tvect_index]= stream;
 tvect_endP= 1;
}
}
else {{ tvect_index= (tvect_index + 1);
 if (tvect_index == C29)
{{ tvect_index= -1;
 tvect_endP= 1;
}
}
}
}
}

 index= tvect_index;
}
}
 rv_aux= index;
}

 return rv_aux;
}
}

int F704()
{{int rv_aux; {int cur_offset; cur_offset= V3;
 if (cur_offset > C8)
{{ { F87(_SH[66435]);
 F87(_SH[65553]);
 F87(_SH[66435]);
 F373(nil);
}

}
}
 V3= (V3 + (2 * C9));
 rv_aux= cur_offset;
}

 return rv_aux;
}
}

int F705(int stream)
{{int rv_aux = nil; if (F237(stream) == F418(stream))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F706(int F339, int levels, int full_pathP)
{{int rv_aux = nil; {int file_list = nil; int subdir_list = nil; int subdir = nil; levels= (levels - 1);
 if (levels >= 0)
{{ file_list= F637(F339, full_pathP, nil);
 subdir_list= F651(F339, 66407);
 while (subdir_list != nil){ { subdir= _SH[subdir_list];
 subdir_list= _SH[(subdir_list + 1)];
}

 file_list= F755(_c(file_list, _c(F706(subdir, levels, full_pathP), nil)));
}

}
}
 rv_aux= file_list;
}

 return rv_aux;
}
}

int F707(int stream)
{return F353(stream, _SH[65750], nil);}

int F708(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, _c(a9, nil)))))))));}

int F709(int lp)
{{int rv_aux = nil; {int csym = nil; int new_lp = nil; int end = nil; { _SH[lp];
 lp= _SH[(lp + 1)];
}

 while (end == nil){ { csym= _SH[lp];
 lp= _SH[(lp + 1)];
}

 if (csym == 47)
{{ new_lp= _c(58, new_lp);
 new_lp= _c(92, new_lp);
 end= _SH[66407];
}
}
else {new_lp= _c(csym, new_lp);}
}

 while (lp != nil){ { csym= _SH[lp];
 lp= _SH[(lp + 1)];
}

 if (csym == 47)
{new_lp= _c(92, new_lp);}
else {new_lp= _c(csym, new_lp);}
}

 rv_aux= F574(new_lp);
}

 return rv_aux;
}
}

int F710(int rec_type_name, int field_name)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int spec = nil; int spec_on = nil; { spec_on= F327(rec_type_name);
 spec= _SH[spec_on];
}

 {int test_aux_var; {int and_result = nil; { and_result= F678(spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F750(spec, 66548) == field_name)
{for_thereis_result= F750(spec, 66549);}
else {for_thereis_result= nil;}
 { spec_on= _SH[(spec_on + 1)];
 spec= _SH[spec_on];
}

 {int and_result = nil; { and_result= F678(spec_on, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

char* F711(int string, int temp_buf2)
{{int index; char* buffer; index= 0;
 if (temp_buf2 == nil)
{buffer= V54;}
else {buffer= V55;}
 while (string != nil){ buffer[index]= (char)_SH[string];
 string= _SH[(string + 1)];
 index= (index + 1);
}

 buffer[index]= (char)0;
 return buffer;
}
}

int F712(int command)
{{int rv_aux; {int tvect_endP = 0; int tvect_index = 0; tvect_index= 0;
 while (!(tvect_endP == 1)){ if (V56[tvect_index] == NULL)
{{ V56[tvect_index]= popen(F209(command), "r");
 tvect_endP= 1;
}
}
else {{ tvect_index= (tvect_index + 1);
 if (tvect_index == C29)
{{ tvect_index= -1;
 tvect_endP= 1;
}
}
}
}
}

 rv_aux= tvect_index;
}

 return rv_aux;
}
}

int F713(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66712);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F714(int number, int min_size, int pad_csym, int align, int base)
{{int rv_aux = nil; {int num_str = F37(number, base, nil); int num_str2 = nil; if (!(min_size != nil))
{rv_aux= num_str;}
else {if (F510(num_str) > min_size)
{rv_aux= num_str;}
else {{ num_str2= F564(num_str, min_size, pad_csym, align);
 F69(num_str, nil);
 rv_aux= num_str2;
}
}}
}

 return rv_aux;
}
}

int F715(int term_list)
{{int rv_aux = nil; {int it_counter = 0; int en_counter = 0; {int term = nil; int term_on = nil; { term_on= term_list;
 term= _SH[term_on];
}

 while (term_on != nil){ if (F232(term) != nil)
{it_counter= (it_counter + 1);}
 if (F13(term) != nil)
{en_counter= (en_counter + 1);}
 { term_on= _SH[(term_on + 1)];
 term= _SH[term_on];
}

}

}

 rv_aux= F310(it_counter, en_counter);
}

 return rv_aux;
}
}

int F716(int stream, int buf_start, int nbytes)
{{int rv_aux = nil; {int aux_stream = nil; int buf_pos; int csym = nil; aux_stream= V8[stream];
 aux_stream= F340(aux_stream, V37);
 buf_pos= buf_start;
 {int test_aux_var; {int _ARG1__5; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {{ if (!(F403(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {_ARG1__5= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__5);
}

 while (test_aux_var){ csym= F139(aux_stream);
 nbytes= (nbytes - 1);
 V33[buf_pos]= csym;
 buf_pos= (buf_pos + 1);
 {int _ARG1__5; {int or_result = nil; { if (nbytes == 0)
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {{ if (!(F403(aux_stream) != nil))
{or_result= C24;}
else {or_result= nil;}
 if (or_result != nil)
{_ARG1__5= (or_result != nil);}
else {_ARG1__5= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__5);
}

}

}

 rv_aux= (buf_pos - buf_start);
}

 return rv_aux;
}
}

int F717(int fn, int args)
{{int rv_aux = nil; {int a1 = nil; int a2 = nil; int a3 = nil; int a4 = nil; int a5 = nil; int a6 = nil; int a7 = nil; int a8 = nil; int a9 = nil; int a10 = nil; if (args != nil)
{{ a1= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a2= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a3= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a4= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a5= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a6= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a7= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a8= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a9= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{{ a10= _SH[args];
 args= _SH[(args + 1)];
 if (args != nil)
{rv_aux= F563(fn, F510(args));}
else {rv_aux= F176(fn, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);}
}
}
else {rv_aux= F392(fn, a1, a2, a3, a4, a5, a6, a7, a8, a9);}
}
}
else {rv_aux= F211(fn, a1, a2, a3, a4, a5, a6, a7, a8);}
}
}
else {rv_aux= F474(fn, a1, a2, a3, a4, a5, a6, a7);}
}
}
else {rv_aux= F562(fn, a1, a2, a3, a4, a5, a6);}
}
}
else {rv_aux= F726(fn, a1, a2, a3, a4, a5);}
}
}
else {rv_aux= F303(fn, a1, a2, a3, a4);}
}
}
else {rv_aux= F462(fn, a1, a2, a3);}
}
}
else {rv_aux= F325(fn, a1, a2);}
}
}
else {rv_aux= F483(fn, a1);}
}
}
else {rv_aux= F76(fn);}
}

 return rv_aux;
}
}

int F718(int list, int sublist, int pred1)
{{int rv_aux = nil; {int flag = nil; while (list != nil){ if (F665(list, sublist, pred1) != nil)
{{ list= nil;
 flag= 66407;
}
}
 { _SH[list];
 list= _SH[(list + 1)];
}

}

 rv_aux= flag;
}

 return rv_aux;
}
}

int F719(int str)
{{int rv_aux = nil; {int flag = nil; int csym = nil; int csym_code = nil; flag= 66407;
 {int test_aux_var; {int and_result = nil; { and_result= F665(str, _SH[65550], 66443);
 if (and_result != nil)
{{ if (F510(str) == 6)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{{ str= F7(str);
 while (str != nil){ { csym= _SH[str];
 str= _SH[(str + 1)];
}

 csym_code= F773(csym);
 {int test_aux_var; {int _ARG1__13; {int or_result = nil; { {int and_result = nil; { if (csym_code >= F773(48))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(57))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {{ {int and_result = nil; { if (csym_code >= F773(97))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (csym_code <= F773(102))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{or_result= and_result;}
else {or_result= nil;}
}
}
else {or_result= nil;}
}

}

 if (or_result != nil)
{_ARG1__13= (or_result != nil);}
else {_ARG1__13= (nil != nil);}
}
}
}

}

 test_aux_var= !(_ARG1__13);
}

 if (test_aux_var)
{{ flag= nil;
 str= nil;
}
}
}

}

 rv_aux= flag;
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F720(int els)
{{int rv_aux = nil; if (els != nil)
{rv_aux= _c(66406, _c(_SH[els], _c(F720(_SH[(els + 1)]), nil)));}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F721(int pred, int list, int mode, int args)
{{int rv_aux = nil; if (mode == 66445)
{rv_aux= F578(pred, list, args);}
else {if (mode == 66444)
{rv_aux= F68(pred, list, args);}
else {if (mode == 66448)
{rv_aux= F68(pred, F578(pred, list, args), args);}
else {rv_aux= nil;}}}
 return rv_aux;
}
}

int F722(int lp_parts)
{return F89(lp_parts, _SH[65589], 66582, 66407);}

int F723(int buf_size)
{{int rv_aux = nil; { if (F245(buf_size) != nil)
{buf_size= C14;}
 _SH[66422]= nil;
 if (V4 >= C12)
{{ _SH[66422]= _SH[65597];
 rv_aux= -1;
}
}
else {if ((V34 + (buf_size * 2)) > C13)
{{ _SH[66422]= _SH[65598];
 rv_aux= -1;
}
}
else {{int stream = nil; int buf_start = nil; stream= V4;
 V4= (V4 + 1);
 buf_start= V34;
 V34= (V34 + (buf_size * 2));
 { V6[stream]= buf_size;
 V25[stream]= buf_start;
 V29[stream]= (buf_start + buf_size);
 V26[stream]= -1;
 V28[stream]= nil;
 V30[stream]= -1;
 V32[stream]= nil;
 V18[stream]= 1;
 V19[stream]= 1;
 V20[stream]= -1;
 V22[stream]= C1;
 V15[stream]= 1;
 V16[stream]= C4;
}

 { V21[stream]= C1;
 V23[stream]= (- C1);
}

 rv_aux= stream;
}
}}
}

 return rv_aux;
}
}

int F724(int str1, int str2)
{{int rv_aux = nil; if (!(F63(str1, str2) == 66452))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F725(int strs)
{return F89(strs, _SH[65705], 66582, nil);}

int F726(int fn, int a1, int a2, int a3, int a4, int a5)
{{int rv_aux = nil; switch (fn) {
   case 66337: rv_aux= F492(a1, a2, a3, a4, a5); break;
   case 66538: rv_aux= F303(a1, a2, a3, a4, a5); break;
   case 66432: rv_aux= (a1 + a2 + a3 + a4 + a5); break;
   case 66493: rv_aux= (a1 - a2 - a3 - a4 - a5); break;
   case 66616: rv_aux= (a1 * a2 * a3 * a4 * a5); break;
   case 66352: rv_aux= (a1 / a2 / a3 / a4 / a5); break;
   case 66688: rv_aux= F664(a1, a2, _c(a3, _c(a4, _c(a5, nil)))); break;
   case 66351: rv_aux= F747(a1, a2, _c(a3, _c(a4, _c(a5, nil)))); break;
   case 66368: rv_aux= F60(a1, a2, _c(a3, _c(a4, _c(a5, nil)))); break;
   case 66350: rv_aux= F588(a1, a2, _c(a3, _c(a4, _c(a5, nil)))); break;
   case 66349: rv_aux= F66(a1, a2, _c(a3, _c(a4, _c(a5, nil)))); break;
   case 66348: rv_aux= F90(a1, a2, a3, _c(a4, _c(a5, nil))); break;
   case 66347: rv_aux= F479(a1, a2, a3, _c(a4, _c(a5, nil))); break;
   case 66346: rv_aux= F721(a1, a2, a3, _c(a4, _c(a5, nil))); break;
   case 66345: rv_aux= F560(a1, a2, a3, _c(a4, _c(a5, nil))); break;
   case 66344: rv_aux= F755(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, nil)))))); break;
   case 66343: rv_aux= F735(_c(a1, _c(a2, _c(a3, _c(a4, _c(a5, nil)))))); break;
   case 66336: rv_aux= F714(a1, a2, a3, a4, a5); break;
   case 66610: rv_aux= F110(a1, a2, a3, a4, a5); break;
   case 66335: rv_aux= F291(a1, a2, a3, a4, a5); break;
   case 66334: rv_aux= F683(a1, a2, a3, a4, a5); break;
   case 66333: rv_aux= F536(a1, a2, a3, a4, a5); break;
   case 66332: rv_aux= F470(a1, a2, a3, a4, a5); break;
   case 66331: rv_aux= F689(a1, a2, a3, a4, a5); break;
   case 66330: rv_aux= F1(a1, a2, a3, a4, a5); break;
   case 66329: rv_aux= F602(a1, a2, a3, a4, a5); break;
   case 66328: rv_aux= F680(a1, a2, a3, a4, a5); break;
   case 66327: rv_aux= F386(a1, a2, a3, a4, a5); break;
   case 66339: rv_aux= F635(a1, a2, a3, a4, a5, 66407, nil); break;
   default: return F563(fn, 5); break;
}
 return rv_aux;
}
}

int F727(int list, int fn)
{{int rv_aux = nil; {int for_thereis_result = nil; for_thereis_result= nil;
 {int el = nil; el= list;
 {int test_aux_var; {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ if (F483(fn, el) != nil)
{for_thereis_result= el;}
else {for_thereis_result= nil;}
 el= _SH[(el + 1)];
 {int and_result = nil; { and_result= F678(el, nil);
 if (and_result != nil)
{{ if (!(for_thereis_result != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

}

 rv_aux= for_thereis_result;
}

 return rv_aux;
}
}

int F728(int object, int list, int F60)
{{int rv_aux = nil; {int nth_cons = nil; int list_len = nil; if (list == nil)
{rv_aux= _c(object, nil);}
else {{ list_len= F510(list);
 if (F60 < 0)
{{ F60= (list_len + 2 + F60);
 if (F60 <= 0)
{F60= 1;}
}
}
 if (F60 == 1)
{rv_aux= _c(object, list);}
else {if (F60 > (list_len + 1))
{rv_aux= F735(_c(list, _c(F514(((F60 - list_len) - 1), nil), _c(_c(object, nil), nil))));}
else {{ nth_cons= F125(list, (F60 - 1));
 _SH[(nth_cons + 1)]= _c(object, _SH[(nth_cons + 1)]);
 rv_aux= list;
}
}}
}
}
}

 return rv_aux;
}
}

int F729(int var_38value_list)
{{int rv_aux = nil; {int var_38value = nil; int var_name = nil; int settings = nil; while (var_38value_list != nil){ { var_38value= _SH[var_38value_list];
 var_38value_list= _SH[(var_38value_list + 1)];
}

 if (F556(var_38value) != nil)
{var_name= _SH[var_38value];}
else {var_name= var_38value;}
 settings= _c(_c(F482(var_name), _c(var_name, nil)), settings);
}

 rv_aux= F574(settings);
}

 return rv_aux;
}
}

int F730(int iter_spec)
{{int rv_aux = nil; {int and_result = nil; { if (!(F123(_SH[iter_spec]) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ {int or_result = nil; { or_result= F256(F283(iter_spec), nil);
 if (or_result != nil)
{and_result= or_result;}
else {{ or_result= F556(F283(iter_spec));
 if (or_result != nil)
{and_result= or_result;}
else {and_result= nil;}
}
}
}

}

 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F731(int rec_name, int size, int f_38vs)
{{int rv_aux = nil; {int type = nil; f_38vs= F250(f_38vs, F327(rec_name));
 {int object1__4 = 66424; int object2__5; {int object1__6 = _c(_c(66551, _c(66560, _c(_c(66561, _c(F10(rec_name), _c(size, nil))), nil))), nil); int object2__7; {int object1__23; int object2__24; {int object1__25 = 66460; int object2__26; {int object1__27 = _c(66470, _c(66560, _c(-1, nil))); int object2__28; {int list__35; int sl__36; {int for_list_result = nil; for_list_result= nil;
 {int field_38value = nil; int field_38value_on = nil; { field_38value_on= f_38vs;
 field_38value= _SH[field_38value_on];
}

 while (field_38value_on != nil){ {int object1__40; int object2__41 = for_list_result; {int field = nil; int value = nil; int aux = nil; aux= field_38value;
 field= _SH[aux];
 aux= _SH[(aux + 1)];
 value= _SH[aux];
 {int test_aux_var; {int or_result = nil; { or_result= F439(value);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= F88(value, 66407);
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{object1__40= _c(F432(rec_name), _c(66560, _c(field, _c(value, nil))));}
else {{ type= F438(rec_name, field);
 if (F529(type) != nil)
{type= 66562;}
 object1__40= _c(66424, _c(_c(_c(type, _c(66563, nil)), nil), _c(_c(66409, _c(66563, _c(value, nil))), _c(_c(F432(rec_name), _c(66560, _c(field, _c(66563, nil)))), nil))));
}
}
}

}

 for_list_result= _c(object1__40, object2__41);
}

 { field_38value_on= _SH[(field_38value_on + 1)];
 field_38value= _SH[field_38value_on];
}

}

}

 list__35= F574(for_list_result);
}

 sl__36= nil;
 object2__28= F498(list__35, sl__36);
}

 object2__26= _c(object1__27, object2__28);
}

 object1__23= _c(object1__25, object2__26);
}

 object2__24= _c(66560, nil);
 object2__7= _c(object1__23, object2__24);
}

 object2__5= _c(object1__6, object2__7);
}

 rv_aux= _c(object1__4, object2__5);
}

}

 return rv_aux;
}
}

int F732(int iter_spec)
{{int rv_aux = nil; {int var = nil; int key = nil; { var= _SH[iter_spec];
 iter_spec= _SH[(iter_spec + 1)];
}

 if (F556(var) != nil)
{{int aux = nil; aux= var;
 key= _SH[aux];
 aux= _SH[(aux + 1)];
 var= _SH[aux];
}
}
 {int in_hash = F78(iter_spec, 66474); {int object1__9 = 66481; int object2__10; {int object1__11; int object2__12; {int object1__13 = 66476; int object2__14; {int object1__15; int object2__16; {int object1__17 = var; int object2__18; {int list__19; int sl__20; if (key != nil)
{list__19= _c(key, nil);}
else {list__19= nil;}
 sl__20= _c(66532, _c(66533, _c(66534, nil)));
 object2__18= F498(list__19, sl__20);
}

 object1__15= _c(object1__17, object2__18);
}

 object2__16= nil;
 object2__14= _c(object1__15, object2__16);
}

 object1__11= _c(object1__13, object2__14);
}

 object2__12= _c(_c(66477, _c(_c(66405, _c(_c(66409, _c(66532, _c(2, nil))), _c(_c(66409, _c(66533, _c(_c(66516, _c(in_hash, _c(1, nil))), nil))), _c(_c(66535, _c(in_hash, _c(66532, _c(66533, _c(var, _c(key, _c(66534, nil))))))), nil)))), nil)), _c(_c(66461, _c(_c(66443, _c(66534, _c(nil, nil))), nil)), _c(_c(66479, _c(_c(66405, _c(_c(66535, _c(in_hash, _c(66532, _c(66533, _c(var, _c(key, _c(66534, nil))))))), nil)), nil)), nil)));
 object2__10= _c(object1__11, object2__12);
}

 rv_aux= _c(object1__9, object2__10);
}

}

}

 return rv_aux;
}
}

int F733(int stream, int act_specs)
{{int rv_aux = nil; {int for_list_result = nil; for_list_result= nil;
 {int act_spec = nil; int act_spec_on = nil; { act_spec_on= act_specs;
 act_spec= _SH[act_spec_on];
}

 while (act_spec_on != nil){ for_list_result= _c(F628(stream, act_spec), for_list_result);
 { act_spec_on= _SH[(act_spec_on + 1)];
 act_spec= _SH[act_spec_on];
}

}

}

 rv_aux= F574(for_list_result);
}

 return rv_aux;
}
}

int F734()
{{int rv_aux = nil; { _SH[66544]= F39(C7);
 rv_aux= _SH[66544];
}

 return rv_aux;
}
}

int F735(int lists)
{{int rv_aux = nil; {int buf = nil; if (_SH[(lists + 1)] != nil)
{buf= F542(lists);}
else {buf= F542(_SH[lists]);}
 F69(lists, nil);
 rv_aux= buf;
}

 return rv_aux;
}
}

int F736(int mode)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(mode, nil);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(mode, 66444);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F737(int csym)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(csym, 34);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 39);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(csym, 96);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}
}
}

}

 return rv_aux;
}
}

int F738(int stream, int condition)
{{int rv_aux = nil; { F548(stream, condition, 66711);
 rv_aux= 66407;
}

 return rv_aux;
}
}

int F739(int stream, int buf_index, int nbytes)
{{int rv_aux = nil; { if (F771(buf_index) != nil)
{{ V26[stream]= buf_index;
 V27[stream]= nbytes;
 V28[stream]= nil;
}
}
else {{ V30[stream]= buf_index;
 V31[stream]= nbytes;
 V32[stream]= nil;
}
}
 rv_aux= nil;
}

 return rv_aux;
}
}

int F740(int int1)
{{int rv_aux = nil; if (int1 < 16)
{rv_aux= F137(_SH[65548], (int1 + 1));}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F741(int object)
{if ((object >= C22) && (object <= (C17 - 2)) && ((unsigned int)_SH[object] == C28))
{return C24;}
else {return nil;}}

int F742(int str)
{return F664(66734, _SH[66776], _c(str, nil));}

int F743(int stream)
{{int rv_aux = nil; {int cdata = nil; { F366(stream, _SH[65739]);
 cdata= F624(stream, _SH[65740], nil);
 F366(stream, _SH[65741]);
}

 rv_aux= F421(_c(66831, _c(cdata, nil)));
}

 return rv_aux;
}
}

int F744(int lists, int delimiter, int grammar, int include_empty_itemsP)
{{int rv_aux = nil; {int cur_last = nil; int join_list = nil; cur_last= F676(_SH[lists]);
 {int test_aux_var; {int and_result = nil; { and_result= lists;
 if (and_result != nil)
{{ and_result= F256(grammar, 66449);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{join_list= F735(_c(F289(delimiter), _c(_SH[lists], nil)));}
else {join_list= _SH[lists];}
}

 lists= _SH[(lists + 1)];
 while (lists != nil){ {int test_aux_var; {int or_result = nil; { or_result= include_empty_itemsP;
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {{ or_result= _SH[lists];
 if (or_result != nil)
{test_aux_var= (or_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
}

}

 if (test_aux_var)
{{ F735(_c(cur_last, _c(F289(delimiter), _c(_SH[lists], nil))));
 cur_last= F676(cur_last);
}
}
}

 lists= _SH[(lists + 1)];
}

 {int test_aux_var; {int and_result = nil; { and_result= cur_last;
 if (and_result != nil)
{{ and_result= F256(grammar, 66450);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{F735(_c(cur_last, _c(F289(delimiter), nil)));}
}

 rv_aux= join_list;
}

 return rv_aux;
}
}

int F745(int stream)
{{int rv_aux = nil; {int F757 = nil; int tag_name = nil; int csym = nil; int cur_line = nil; int lines = nil; F426(66565, stream);
 while (!(F757 != nil)){ if (!(F243(stream, nil)))
{F757= 66407;}
else {if (F314(stream) != nil)
{F451(stream);}
else {if (F115(stream) != nil)
{F238(stream);}
else {if (F707(stream) != nil)
{F304(stream);}
else {if (F466(stream) != nil)
{{ tag_name= F51(stream, 66407);
 if (F407(tag_name) != nil)
{if (F221(32) != nil)
{{int test_aux_var; {int and_result = nil; { and_result= cur_line;
 if (and_result != nil)
{{ and_result= F678(_SH[cur_line], 32);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{cur_line= _c(32, cur_line);}
}
}
else {cur_line= _c(32, cur_line);}}
else {if (F306(cur_line) != nil)
{{ if (_SH[cur_line] == 32)
{{ _SH[cur_line];
 cur_line= _SH[(cur_line + 1)];
}
}
 lines= _c(F574(cur_line), lines);
 cur_line= nil;
}
}}
}
}
else {if (F608(stream) != nil)
{{ tag_name= F51(stream, 66407);
 if (F754(tag_name, _SH[65735]) != nil)
{F448(stream);}
else {if (F754(tag_name, _SH[65736]) != nil)
{F153(stream);}
else {if (F754(tag_name, _SH[65737]) != nil)
{F546(stream);}
else {if (F407(tag_name) != nil)
{if (F221(32) != nil)
{{int test_aux_var; {int and_result = nil; { and_result= cur_line;
 if (and_result != nil)
{{ and_result= F678(_SH[cur_line], 32);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{cur_line= _c(32, cur_line);}
}
}
else {cur_line= _c(32, cur_line);}}
else {if (F306(cur_line) != nil)
{{ if (_SH[cur_line] == 32)
{{ _SH[cur_line];
 cur_line= _SH[(cur_line + 1)];
}
}
 lines= _c(F574(cur_line), lines);
 cur_line= nil;
}
}}}}}
}
}
else {{ csym= F684(stream);
 if (F221(csym) != nil)
{{int test_aux_var; {int and_result = nil; { and_result= cur_line;
 if (and_result != nil)
{{ and_result= F678(_SH[cur_line], 32);
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 if (test_aux_var)
{cur_line= _c(32, cur_line);}
}
}
else {cur_line= _c(csym, cur_line);}
}
}}}}}}
}

 if (F306(cur_line) != nil)
{{ if (_SH[cur_line] == 32)
{{ _SH[cur_line];
 cur_line= _SH[(cur_line + 1)];
}
}
 lines= _c(F574(cur_line), lines);
 cur_line= nil;
}
}
 rv_aux= F574(lines);
}

 return rv_aux;
}
}

int F746(int stream)
{{int rv_aux = nil; {int buf_index = V20[stream]; int buf_cur = V22[stream]; int buf_size = V6[stream]; if (F771(buf_index) != nil)
{if (buf_cur >= (V25[stream] + buf_size))
{rv_aux= C24;}
else {rv_aux= nil;}}
else {if (buf_cur >= (V29[stream] + buf_size))
{rv_aux= C24;}
else {rv_aux= nil;}}
}

 return rv_aux;
}
}

int F747(int pred, int list, int args)
{{int rv_aux = nil; {int el = nil; int res = nil; args= _c(nil, args);
 while (list != nil){ { el= _SH[list];
 list= _SH[(list + 1)];
}

 _SH[args]= el;
 if (F717(pred, args) != nil)
{{ res= 66407;
 list= nil;
}
}
}

 F417(args);
 rv_aux= res;
}

 return rv_aux;
}
}

int F748(int rec_type_name)
{return F750(F172(_SH[66569], F292(rec_type_name)), 66571);}

int F749(int object)
{return F88(object, 66407);}

int F750(int rec, int field)
{return F283(F248(rec, field, nil));}

int F751(int rec_name, int rec, int field_name)
{return _c(66574, _c(F600(rec_name, field_name), _c(rec, nil)));}

int F752(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8)
{return _c(a1, _c(a2, _c(a3, _c(a4, _c(a5, _c(a6, _c(a7, _c(a8, nil))))))));}

int F753(int instrs)
{{int rv_aux = nil; if (_SH[(instrs + 1)] != nil)
{rv_aux= _c(66405, F498(instrs, nil));}
else {rv_aux= _SH[instrs];}
 return rv_aux;
}
}

int F754(int str1, int str2)
{return F256(F63(str1, str2), 66452);}

int F755(int lists)
{{int rv_aux = nil; {int buf = nil; if (_SH[(lists + 1)] != nil)
{buf= F486(lists);}
else {buf= F486(_SH[lists]);}
 F69(lists, nil);
 rv_aux= buf;
}

 return rv_aux;
}
}

int F756(int stream, int sc_comment_flag, int hm_comment_flag)
{{int rv_aux = nil; {int F757 = nil; int csym = nil; { while (!(F757 != nil)){ csym= F396(stream, nil);
 if (csym == -1)
{F757= 66407;}
else {if (F221(csym) != nil)
{F663(stream, 1);}
else {if (csym == 35)
{{ F663(stream, 1);
 csym= F396(stream, nil);
 if (csym != 124)
{{ F610(stream, _SH[65607]);
 F757= 66407;
}
}
else {if (hm_comment_flag != nil)
{{ F663(stream, -1);
 F757= 66407;
}
}
else {F547(stream);}}
}
}
else {if (csym == 59)
{if (sc_comment_flag != nil)
{F757= 66407;}
else {F185(stream);}}
else {F757= 66407;}}}}
}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F757(int list, int suffix, int pred1)
{{int rv_aux = nil; if (F556(suffix) != nil)
{rv_aux= F343(F779(list, (F510(list) - F510(suffix))), suffix, pred1);}
else {if (pred1 != nil)
{rv_aux= F325(pred1, F161(list), suffix);}
else {rv_aux= F256(F161(list), suffix);}}
 return rv_aux;
}
}

int F758(int par_name, int par_value)
{return F158(_SH[66427], par_name, par_value);}

int F759(int list, int spec)
{{int rv_aux = nil; if (F245(spec) != nil)
{rv_aux= list;}
else {if (F16(spec) != nil)
{rv_aux= _c(66410, _c(list, _c(_c(66536, _c(spec, nil)), nil)));}
else {if (F123(spec) != nil)
{rv_aux= _c(66537, _c(list, _c(_c(66413, _c(spec, nil)), _c(_c(66413, _c(66407, nil)), nil))));}
else {if (F44(spec))
{rv_aux= _c(66410, _c(list, _c(spec, nil)));}
else {if (F774(spec, 66538) != nil)
{if (F7(spec) != nil)
{rv_aux= _c(66539, _c(list, _c(_c(66413, _c(F283(spec), nil)), _c(_c(66455, F498(F7(spec), nil)), nil))));}
else {rv_aux= _c(66540, _c(list, _c(_c(66413, _c(F283(spec), nil)), nil)));}}
else {if (F774(spec, 66541) != nil)
{if (F7(spec) != nil)
{rv_aux= _c(66542, _c(list, _c(_c(66413, _c(F283(spec), nil)), _c(_c(66455, F498(F7(spec), nil)), nil))));}
else {rv_aux= _c(66543, _c(list, _c(_c(66413, _c(F283(spec), nil)), nil)));}}
else {rv_aux= F759(F759(list, _SH[spec]), _SH[(spec + 1)]);}}}}}}
 return rv_aux;
}
}

int F760(int stream)
{return F678(V14[stream], nil);}

int F761(int act_spec)
{return F664(66443, _SH[65635], _c(_SH[act_spec], nil));}

int F762(int csym)
{{int rv_aux = nil; {int and_result = nil; { if (F773(csym) < 256)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (!(F586(csym) != nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F763(int list1, int list2)
{{int rv_aux = nil; {int tmp_list = nil; if (list1 == nil)
{list1= list2;}
else {{ tmp_list= list1;
 while (_SH[(tmp_list + 1)] != nil){ tmp_list= _SH[(tmp_list + 1)];
}

 _SH[(tmp_list + 1)]= list2;
}
}
 rv_aux= list1;
}

 return rv_aux;
}
}

int F764(int stream, int condition)
{return F602(stream, condition, 66462, 66407, nil);}

int F765(int stream, int n, int out_stream, int inTOout_fn)
{{int rv_aux = nil; { if (F237(stream) < F502(stream))
{F102(stream, F502(stream));}
 { {int test_aux_var; {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (n > 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

 while (test_aux_var){ n= (n - 1);
 if (inTOout_fn != nil)
{F325(inTOout_fn, stream, out_stream);}
else {F673(F299(stream), out_stream);}
 {int and_result = nil; { if (F243(stream, nil))
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{{ if (n > 0)
{and_result= C24;}
else {and_result= nil;}
 if (and_result != nil)
{test_aux_var= (and_result != nil);}
else {test_aux_var= (nil != nil);}
}
}
else {test_aux_var= (nil != nil);}
}

}

}

}

 rv_aux= nil;
}

}

 return rv_aux;
}
}

int F766()
{{int rv_aux = nil; { _SH[66594]= nil;
 _SH[66595]= nil;
 _SH[66596]= 66407;
 _SH[66597]= 66407;
 _SH[66598]= 10;
 _SH[66599]= 4;
 _SH[66436]= 8;
 _SH[66437]= 128;
 _SH[66600]= 3;
 _SH[66601]= _SH[65600];
 _SH[66602]= 80;
 _SH[66603]= F394(_SH[65601], 66407);
 { _SH[66604]= F394(_SH[65602], 66407);
 rv_aux= _SH[66604];
}

}

 return rv_aux;
}
}

int F767(int object)
{{int rv_aux = nil; {int flag = 66407; int element = nil; while (object != nil){ element= _SH[object];
 object= _SH[(object + 1)];
 if (F16(element) != nil)
;
else {{ object= nil;
 flag= nil;
}
}
}

 rv_aux= flag;
}

 return rv_aux;
}
}

int F768(int str1, int str2)
{{int rv_aux = nil; {int or_result = nil; { or_result= F256(F63(str1, str2), 66453);
 if (or_result != nil)
{rv_aux= or_result;}
else {{ or_result= F256(F63(str1, str2), 66452);
 if (or_result != nil)
{rv_aux= or_result;}
else {rv_aux= nil;}
}
}
}

}

 return rv_aux;
}
}

int F769(int lp)
{{int rv_aux = nil; {int F424 = nil; F424= F526(45, lp);
 if (F424 == nil)
{rv_aux= nil;}
else {{ lp= F779(lp, F424);
 if (F461(_SH[lp]) != nil)
{rv_aux= F190(lp);}
else {rv_aux= nil;}
}
}
}

 return rv_aux;
}
}

int F770(int in_stream, int out_stream)
{return F673(F684(in_stream), out_stream);}

int F771(int buf_index)
{{int rv_aux = nil; if ((buf_index % 2) == 0)
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F772(int stream, int condition_type)
{{int rv_aux = nil; {int max_pos = nil; if (F539(condition_type) != nil)
{{ if (F418(stream) > F500(stream))
{max_pos= F500(stream);}
else {max_pos= F418(stream);}
 if (F237(stream) > max_pos)
{rv_aux= F102(stream, max_pos);}
else {rv_aux= nil;}
}
}
else {if (F237(stream) < F502(stream))
{rv_aux= F102(stream, F502(stream));}
else {rv_aux= nil;}}
}

 return rv_aux;
}
}

int F773(int csym_index)
{return csym_index;}

int F774(int object, int type)
{{int rv_aux = nil; {int and_result = nil; { and_result= F556(object);
 if (and_result != nil)
{{ and_result= F256(_SH[object], type);
 if (and_result != nil)
{rv_aux= and_result;}
else {rv_aux= nil;}
}
}
else {rv_aux= nil;}
}

}

 return rv_aux;
}
}

int F775(int stream, int out_stream, int terminator)
{return F201(stream, terminator, out_stream, nil);}

int F776(int csym, int sock_index)
{{int rv_aux = nil; { V61[sock_index][V62[sock_index]]= F773(csym);
 V62[sock_index]= (V62[sock_index] + 1);
 if (V62[sock_index] >= C31)
{rv_aux= F620(sock_index);}
else {rv_aux= 0;}
}

 return rv_aux;
}
}

int F777(int fields)
{{int rv_aux = nil; {int field = nil; int var_names = nil; while (fields != nil){ { field= _SH[fields];
 fields= _SH[(fields + 1)];
}

 if (F556(field) != nil)
{var_names= _c(F283(field), var_names);}
else {if (field != nil)
{var_names= _c(field, var_names);}}
}

 rv_aux= F574(var_names);
}

 return rv_aux;
}
}

int F778(int stream)
{{int rv_aux = nil; if (F237(stream) == F502(stream))
{rv_aux= C24;}
else {rv_aux= nil;}
 return rv_aux;
}
}

int F779(int list, int F60)
{{int rv_aux = nil; if (F60 == 0)
{rv_aux= list;}
else {{ {int list_len_var = nil; list_len_var= F510(list);
 if (F60 < 0)
{F60= (list_len_var + F60 + 1);}
}

 rv_aux= F125(list, (F60 + 1));
}
}
 return rv_aux;
}
}

int F780(int var_38value_list)
{{int rv_aux = nil; {int var_38value = nil; int var_name = nil; int settings = nil; while (var_38value_list != nil){ { var_38value= _SH[var_38value_list];
 var_38value_list= _SH[(var_38value_list + 1)];
}

 if (F556(var_38value) != nil)
{var_name= _SH[var_38value];}
else {var_name= var_38value;}
 settings= _c(_c(66409, _c(var_name, _c(F482(var_name), nil))), settings);
}

 rv_aux= F574(settings);
}

 return rv_aux;
}
}

