Cookoo
=======

Which cookies are you setting me?

This project tries to make it easier to understand how does a website set cookies, what it does to inform the
user, etc.

Code structure
----------------

`cookoo` is a library to help you understand how does a website behave regarding cookies, privacy notifications,
and such.

`whichcookie` is a webservice meant to make the information provided by `cookoo` more easy to access. It's
just some HTTP APIs over `cookoo`. It will also provide a decent WebUI to make it more accessible.
