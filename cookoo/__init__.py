import time
import os.path
import sqlite3
import logging

from selenium.common.exceptions import NoSuchFrameException

__author__ = "Davide Lo Re"
__copyright__ = "Copyright 2015-2016, Davide Lo Re"
__license__ = "AGPLv3"


def accurate_visit(browser):
    height_vars = ['document.body.scrollHeight',
                   'document.body.offsetHeight',
                   'document.documentElement.clientHeight',
                   'document.documentElement.scrollHeight',
                   'document.documentElement.offsetHeight']
    cmd = 'Math.max({})'.format(', '.join(height_vars))
    height = browser.evaluate_script(cmd)

    step = 300
    for _ in range(height // step):
        browser.execute_script(r'window.scrollBy(0, {step});'.format(
            step=step))
        time.sleep(0.05)


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def get_cookies(browser):
    if browser.driver_name == 'Firefox':
        dbpath = os.path.join(browser.driver.profile.path, 'cookies.sqlite')
        connection = sqlite3.connect(dbpath)
        connection.row_factory = dict_factory
        cur = connection.cursor()
        cur.execute('SELECT name, basedomain AS domain, value '
                    'FROM moz_cookies;')
        for row in cur:
            yield row['name'], row
    else:
        logging.warning("No 3rd party cookie will get dumped, sorry")
        for cookie in browser.cookies.all():
            yield cookie, browser.driver.get_cookie(cookie)


def get_iframes(browser):
    for iframeid in (iframe._element.get_attribute('id')
                     for iframe in browser.find_by_tag('iframe')):
        try:
            with browser.get_iframe(iframeid) as iframe:
                yield iframeid, iframe
        except NoSuchFrameException:
            logging.exception('Error retrieving frame')


def get_text(browser):
    script_gettextlist = '''
    (function (n) {
    var s = [];
    function getStrings(n, s) {
    var m;
    if (n.nodeType == 3) { s.push(n.data); }
    else if (n.nodeType == 1) {
    var tagname = n.nodeName.toLowerCase();
    if(!tagname.endsWith("script") && tagname !== "style" ) {
    for (m = n.firstChild; null != m; m = m.nextSibling) {
    getStrings(m, s);
    } } } }
    getStrings(n, s); return s; })(document.body)
    '''.replace('\n', ' ')
    texts = browser.evaluate_script(script_gettextlist)
    for _, iframe in get_iframes(browser):
        iframe_texts = iframe.evaluate_script(script_gettextlist)
        if iframe_texts:
            texts.extend(iframe_texts)
    return texts


def get_html(browser):
    return browser.html


def get_iframes_html(browser):
    ret = []
    for iframeid, iframe in get_iframes(browser):
        ret.append({
            'html': iframe.html,
            'url': iframe.url,
            'id': iframeid
        })
    return ret
