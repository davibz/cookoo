import json
from urllib.parse import urlparse
import time

from splinter import Browser

from . import accurate_visit, get_cookies, get_text, get_html, get_iframes_html

__author__ = "Davide Lo Re"
__copyright__ = "Copyright 2015-2016, Davide Lo Re"
__license__ = "AGPLv3"


def main_dump(args):
    def filtercookies(cookies):
        if args.third_party_only:
            return filter_3rdparties(cookies, args.url)
        else:
            return cookies

    def get_status(b):
        return dict(cookies=dict(filtercookies(get_cookies(b))),
                    text=get_text(b),
                    html=get_html(b),
                    iframes=get_iframes_html(b)
                    )
    res = {
        'visit': {'url': args.url},
        'immediate': {},
        'afterconsensus': {}
    }
    with Browser(args.driver) as b:
        b.visit(args.url)
        time.sleep(1)

        res['immediate'] = get_status(b)
        if args.move:
            accurate_visit(b)
            res['afterconsensus'] = get_status(b)
    json.dump(res, args.out)


def filter_3rdparties(cookies, url):
    domain = urlparse(url).netloc
    for key, cookieinfo in cookies:
        if cookieinfo['domain'] != domain:
            yield key, cookieinfo


def main_3rdparties(args):
    dump = json.load(open(args.dump))
    url = dump.get('visit').get('url')
    cookies = dump.get('cookies')
    json.dump(dict(filter_3rdparties(cookies.items(), url)), args.out)


def get_parser():
    import argparse
    p = argparse.ArgumentParser(description='A tool to understand '
                                'which cookies '
                                'is a webpage setting on your browser')
    sub = p.add_subparsers(dest='subcommand')
    sub.required = True

    dump = sub.add_parser('dump',
                          description='Visit a webpage and get the '
                          'cookies in it')
    dump.set_defaults(func=main_dump)
    dump.add_argument('url')
    dump.add_argument('--driver',
                      choices=['phantomjs', 'firefox', 'chrome'],
                      default='firefox')
    dump.add_argument('-o', '--out', metavar='JSON',
                      type=argparse.FileType('w'),
                      default='-')
    dump.add_argument('--third-party-only', default=False, action='store_true')
    dump.add_argument('--move', default=False, action='store_true')

    third = sub.add_parser('thirdparties',
                           description='Only show third party cookies')
    third.set_defaults(func=main_3rdparties)
    third.add_argument('dump', metavar='JSON')
    third.add_argument('-o', '--out', type=argparse.FileType('w'),
                       default='-')

    return p


def main():
    p = get_parser()
    args = p.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
