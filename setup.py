from setuptools import setup, find_packages, Extension


module_banner = Extension('banner',
                          sources=['checkbanner/pybanner.c'])
setup(name='cookoo',
      version='0.1',
      description='detect cookie usage for a website',
      long_description=open('README.md').read(),
      license='AGPL',
      packages=find_packages(),
      package_data={
          'static': 'whichcookie/static/*',
          'templates': 'whichcookie/templates/*'
      },
      ext_modules=[module_banner],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'splinter',
          'Flask',
          'Flask-Cache',
          'flask-bootstrap',
          'requests',
      ],
      entry_points={'console_scripts': ['cookoo=cookoo.cli:main']},
      classifiers=[
                  "License :: OSI Approved :: GNU Affero General Public License v3",
                  "Operating System :: POSIX :: Linux",
                  "Programming Language :: Python :: 3"
                  ]
      )
