import time
import os
import json
import tempfile

from flask import Flask, abort, jsonify, request, render_template, Response
from flask_bootstrap import Bootstrap
from flask_cache import Cache
from splinter import Browser
import requests

import cookoo
import pybanner

__author__ = "Davide Lo Re"
__copyright__ = "Copyright 2015-2016, Davide Lo Re"
__license__ = "AGPLv3"


def create_app():
    app = Flask(__name__)
    if 'WHICHCOOKIE_SETTINGS' in os.environ:
        app.config.from_envvar('WHICHCOOKIE_SETTINGS')
    app.config.setdefault('BOOTSTRAP_SERVE_LOCAL', True)
    app.config.setdefault('CACHE_TYPE', 'simple')
    app.config.setdefault('CACHE_KEY_PREFIX', 'whichcookie')
    app.config.setdefault('CACHE_THRESHOLD', 1000)
    app.config.setdefault('BADDOMAINS_ADD', [])
    Bootstrap(app)
    cache = Cache(app, config=app.config)

    driver = 'firefox'

    def cache_status(prefix, url, b=None):
        cachename = '%s/%s' % (prefix, url)
        if b is None:   # assume get
            oldvalue = cache.get(cachename)
            return json.loads(oldvalue) if oldvalue is not None else None
        status = dict(
            html=str(cookoo.get_html(b)),
            iframes=tuple(cookoo.get_iframes_html(b)),
            text=str(cookoo.get_text(b)),
            cookies=dict(cookoo.get_cookies(b))
        )
        cache.set(cachename, json.dumps(status), timeout=3600*12)
        return status

    def visit_get(url, move=False):
        if move is False:
            oldvalue = cache_status('immediate', url)
        else:
            oldvalue = cache_status('after', url)
        if oldvalue is not None:
            return oldvalue
        with Browser(driver) as br:
            br.visit(url)
            time.sleep(1)
            status = cache_status('immediate', url, br)
            if move:
                cookoo.accurate_visit(br)
                return cache_status('after', url, br)
            else:
                return status

    @cache.memoize(timeout=3600)
    def banner_get(url):
        status = visit_get(url, move=False)
        return pybanner.check(status['html'],
                              [i['html'] for i in status['iframes']])

    @cache.memoize(timeout=3600*12)
    def disconnect_get():
        app.logger.info('Fetching Disconnect list')
        url = """http://services.disconnect.me/disconnect-plaintext.json"""
        disconnect = requests.get(url).json()
        our = dict()
        for ctg in disconnect['categories']:
            urls = set()
            for group in disconnect['categories'][ctg]:
                for groupname in group:
                    for groupmainurl in group[groupname]:
                        for url in group[groupname][groupmainurl]:
                            urls.add(url)
            our[ctg] = tuple(sorted(urls))
        return our

    def bad_domains():
        s = set(disconnect_get()['Analytics'])
        s.update(disconnect_get()['Advertising'])
        s.update(app.config['BADDOMAINS_ADD'])
        return s

    @app.route('/')
    def main():
        return render_template('index.html')

    @app.route('/api/')
    def all_apis():
        '''list all the public methods'''
        return jsonify(api={url.rule: app.view_functions[url.endpoint].__doc__
                            for url in app.url_map.iter_rules()
                            if url.rule.startswith('/api/page/')
                            })

    @app.route('/api/page/get_cookies')
    def get_cookies():
        '''returns a list of cookie for the intended url'''
        try:
            url = request.args['url']
        except KeyError:
            abort(400)
        move = bool(int(request.args.get('move', '0')))
        cookies = visit_get(url, move)['cookies']
        return jsonify(
            links=dict(self=request.url),
            page={
                'url': url,
                'move': move,
                'cookies': cookies
            })

    @app.route('/api/page/cookielaw')
    def cookielaw():
        '''this is basically an enhancement over get_cookies + get_banner'''
        try:
            url = request.args['url']
        except KeyError:
            abort(400)
        move = bool(int(request.args.get('move', '0')))
        cookies = set(cook['domain']
                      for cook in visit_get(url, move)['cookies'].values())
        analytics = bad_domains()
        banner = banner_get(url)
        if not cookies:
            score = 10
            rationale = 'No cookies set at all, perfect'
        elif not analytics.intersection(cookies):
            score = 8
            rationale = "We haven't detected any tracking cookie"
        elif banner:
            score = 5
            rationale = "There are tracking cookies, but the banner is present"
        else:
            score = 0
            rationale = "There are tracking cookies, but there is no banner"
        return jsonify(
            links=dict(self=request.url),
            page={
                'url': url,
                'categorized': {
                    'profilingthird': tuple(analytics.intersection(cookies))
                },
                'banner': dict(found=True if banner else False,
                               html=banner if banner else None),
                'lawscore': dict(score=score, rationale=rationale)
            })

    @app.route('/api/page/get_text')
    def get_text():
        '''Text-only version of the page at load time'''
        try:
            url = request.args['url']
        except KeyError:
            abort(400)

        text = visit_get(url, move=False)['text']
        return jsonify(
            links=dict(self=request.url),
            page={
                'url': url,
                'text': text
            })

    @app.route('/api/page/get_html')
    def get_html():
        '''returns a JSON version of the page dom and iframes

        Note that this is not just the html source, but the complete DOM at
        page load time'''

        try:
            url = request.args['url']
        except KeyError:
            abort(400)
        status = visit_get(url, move=False)
        return jsonify(
            links=dict(self=request.url),
            page={
                'url': url,
                'html': status['html'],
                'iframes': status['iframes']
            })

    @app.route('/api/page/get_banner')
    def get_banner():
        '''Detects banner, returns both a boolean and the banner text'''
        try:
            url = request.args['url']
        except KeyError:
            abort(400)
        banner = banner_get(url)
        return jsonify(
            links=dict(self=request.url),
            page={
                'url': url,
                'banner': dict(found=True if banner else False,
                               html=banner if banner else None)
            }
        )

    @app.route('/api/page/get_screenshot')
    def get_screenshot():
        '''Detects banner, returns both a boolean and the banner text'''
        try:
            url = request.args['url']
        except KeyError:
            abort(400)
        with Browser(driver) as br:
            br.visit(url)
            time.sleep(1)
            temp_fd, temp_path = tempfile.mkstemp(suffix='.png', prefix='screenshot-')
            br.driver.save_screenshot(temp_path)
            os.close(temp_fd)
        with open(temp_path, 'rb') as buf:
            content = buf.read()
        return Response(content, mimetype='image/png')

    @app.route('/api/data/list/<name>')
    def get_list(name):
        d = disconnect_get()
        if name not in d:
            abort(404)
        return jsonify(data=d[name],
                       links=dict(self=request.url),
                       meta=dict(copyright="Disconnect.me"),
                       )

    @app.route('/api/data/list/')
    def get_all_lists():
        d = disconnect_get()
        return jsonify(
            links=dict(self=request.url),
            meta=dict(copyright="Disconnect.me"),
            lists=tuple(d.keys())
        )

    return app


if __name__ == '__main__':
    create_app().run(debug=True)
