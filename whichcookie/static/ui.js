jQuery(function($) {
	$('#pagecheck').submit(function(evt) {
        $('.start-hidden').hide(100);
		$('#result-score').text('');
		$('#result-banner').text('');
		$('#result-rationale').text('Loading...');
		$('#result-row').removeClass('score-ok score-notok alert-success alert-danger alert-warning');
		$.getJSON("/api/page/cookielaw",
			  {url: $('#pagecheck-url').val()},
			  function(data) {
				  console.log("ok");
				  console.log(data);
				  score = data['page']['lawscore'];
				  $('#result-rationale').text(score['rationale']);
				  $('#result-row').addClass('score-' + score['score']);
				  if(score['score']) {
					  $('#result-row').addClass('score-ok alert-success');
				  } else {
					  if(score['score'] === 0) {
						  $('#result-row').addClass('score-notok alert-danger');
					  } else {
						  $('#result-row').addClass('score-notok alert-warning');
					  }
				  }

				  if(data.page.banner.found) {
                      $('#summary-banner td.summary-value').text('Yes').attr('title', data.page.banner.html);
				  } else {
                      $('#summary-banner td.summary-value').text('No').attr('title', '');
                  }
                  if(data.page.categorized.profilingthird.length > 0) {
                      $('#summary-cookie td.summary-value').text('Yes').
                          attr('title', data.page.categorized.profilingthird.join(', '));
                  } else {
                      $('#summary-cookie td.summary-value').text('No').attr('title', '');
                  }
                  $('.start-hidden').show(100);
			  }).fail(function() {
                  $('#result-rationale').text('');
                  alert('Error checking');
			  });
			  evt.preventDefault();
	});
});

