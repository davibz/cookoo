import tempfile
import os

import banner


def check(html, iframes=[]):
    content = "\n".join(iframes + [html])
    fname = tempfile.mktemp(suffix='.html', prefix='pybanner_')
    with open(fname, 'w', encoding='utf8') as buf:
        buf.write(content)

    try:
        ret = banner.check(fname)
    finally:
        os.unlink(fname)
    return ret
